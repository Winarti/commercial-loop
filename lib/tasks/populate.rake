namespace :db do
  desc "Update attachment status for properties"
  task :update_attachment_on_properties => :environment do
    Property.find(:all, :limit => 5000, :offset => 0).each_with_index do |p, index|
       
      puts "--> Index: #{index + 1}"
      puts "Current Property ID: #{p.id}"


      if !p.images.blank?
        puts "----> Updating image_exist on Property ID: #{p.id}"
        sql = "UPDATE properties SET image_exist=1 WHERE ID=#{p.id}"
        ActiveRecord::Base.connection.execute(sql) unless p.image_exist
      end

      if p.images.blank?
        puts "----> Updating image_exist false on Property ID: #{p.id}"
        sql = "UPDATE properties SET image_exist=0 WHERE ID=#{p.id}"
        ActiveRecord::Base.connection.execute(sql) unless p.image_exist
      end

      if !p.floorplans.blank?
        puts "----> Updating plan_exist on Property ID: #{p.id}"
        sql="UPDATE properties SET floorplan_exist=1 WHERE ID=#{p.id}"
        ActiveRecord::Base.connection.execute(sql) unless p.floorplan_exist
      end

      if p.floorplans.blank?
        puts "----> Updating plan_exist false on Property ID: #{p.id}"
        sql="UPDATE properties SET floorplan_exist=0 WHERE ID=#{p.id}"
        ActiveRecord::Base.connection.execute(sql) unless p.floorplan_exist
      end
    end
  end

  desc "Update price per lease listings for properties"
  task :update_price_per_on_properties => :environment do
    Property.find(:all, :limit => 5000, :offset => 0).each_with_index do |p, index|
       
      puts "--> Index: #{index + 1}"
      puts "Current Property ID: #{p.id}"

      ptype = p.type.to_s.underscore

      case ptype
      when 'residential_lease'
        price_per = "Weekly"
      when 'commercial'
        price_per = "Monthly" if p.deal_type == "Lease"
      when 'holiday_lease'
        price_per = "Weekly"
      end

      if !price_per.blank?
        puts "----> Updating Property ID: #{p.id}"
        sql="UPDATE properties SET price_per = #{price_per} WHERE id=#{p.id}"
        ActiveRecord::Base.connection.execute(sql)
      end
    end
  end

  desc "set processed = 1 to properties belong to office that do not enable http api"
  task :update_processed_properties => :environment do
    Property.find(:all, :conditions => ["offices.http_api_enable = 0 OR offices.http_api_enable IS NULL"], :include => "office").each_with_index do |p, index|
       
      puts "--> Index: #{index + 1}"
      puts "Current Property ID: #{p.id}"
      if p.processed == false
        puts "----> Updating Property ID: #{p.id}"
        sql="UPDATE properties SET processed = 1 WHERE id=#{p.id}"
        ActiveRecord::Base.connection.execute(sql)
      end
    end
    puts "Finished updating property"
  end


  desc "Update auto subscribe for office"
  task :update_subscribe_for_office => :environment do
    Office.all.each do |off|
      puts "update auto subscribe of office with id = #{off.id}"
      off.portal_accounts.each do |portal_acc|
        if portal_acc.active_feeds == '1'
          portal = Portal.find_by_portal_name(portal_acc.portal_name,:limit=>1)
          check_portautosub = PortalAutosubcribe.find_by_office_id_and_portal_id(off.id,portal.id)
          puts "check_portautosub not exist ? ==> #{check_portautosub.blank?}"
          next unless check_portautosub.blank?
          portal_autosub =  PortalAutosubcribe.new
          portal_autosub.office_id = off.id
          portal_autosub.portal_id = portal.id
          portal_autosub.save
          puts "saved portal_autosub with id #{portal_autosub.id}"
        end
      end
    end
  end


  desc "Populate property vendor contact to contact list"
  task :populate_contact_list => :environment do
    Property.find(:all).each do |p|
      puts "----> populate contact for Property ID: #{p.id}"
      category_id = ["residential_lease", "commercial", "holiday_lease"].include?(p.type.to_s.underscore) ? 5 : 2
      unless p.agent_contact_id.blank?
        agent_contact= AgentContact.find_by_id(p.agent_contact_id)
        agent_contact.update_category_contact(category_id) unless agent_contact.blank?
      else
        team = []
        team << p.primary_contact_id unless p.primary_contact_id .blank?
        team << p.secondary_contact_id unless p.secondary_contact_id.blank?

        contact = {:category_contact_id => category_id, :agent_id => p.agent_id, :office_id => p.office_id,
          :first_name => p.vendor_first_name, :last_name => p.vendor_last_name,
          :email => p.vendor_email, :contact_number => p.vendor_phone
        }

        agent_contact = AgentContact.new(contact)
        if agent_contact.save
          puts "----> updating property for new agent contact ID: #{agent_contact.id}"
          agent_contact.add_categories([category_id])
          agent_contact.add_assigns(team)
          agent_contact.add_access(team)
          sql="UPDATE properties SET agent_contact_id=#{agent_contact.id} WHERE ID=#{p.id}"
          ActiveRecord::Base.connection.execute(sql)
        else
          if !p.vendor_first_name.blank? and !p.vendor_last_name.blank?
            contact =  AgentContact.find(:all,:conditions=>"(email = \"#{p.vendor_email}\" AND first_name = \"#{p.vendor_first_name}\" AND last_name=\"#{p.vendor_last_name}\") OR (contact_number = \"#{p.vendor_phone}\" AND first_name = \"#{p.vendor_first_name}\" AND last_name=\"#{p.vendor_last_name}\")",:limit=>1)
            if !contact.blank?
              puts "----> updating property for old agent contact ID: #{contact.first.id}"
              sql="UPDATE properties SET agent_contact_id=#{contact.first.id} WHERE ID=#{p.id}"
              ActiveRecord::Base.connection.execute(sql)
            end
          end
        end
      end
    end
  end

  desc "Update vendor detail for random id"
  task :update_for_vendor_random_id => :environment do
    Property.find(:all, :conditions => "`agent_contact_id` IS NOT NULL AND `vendor_first_name` IS NOT NULL").each do |p|
      puts "----> populate contact for Property ID: #{p.id}"
      puts "----------> agent contact ID: #{p.agent_contact_id}"
      if p.agent_contact.blank?
        puts "----------> detected as random ID: #{p.agent_contact_id}"
        contact =  AgentContact.find(:first,:conditions=>"(email = \"#{p.vendor_email}\" AND first_name = \"#{p.vendor_first_name}\" AND last_name=\"#{p.vendor_last_name}\") OR (contact_number = \"#{p.vendor_phone}\" AND first_name = \"#{p.vendor_first_name}\" AND last_name=\"#{p.vendor_last_name}\")")
        if !contact.blank?
          puts "---------------> updating random agent contact id to correct id: #{contact.id}"
          sql="UPDATE properties SET agent_contact_id=#{contact.id} WHERE ID=#{p.id}"
          ActiveRecord::Base.connection.execute(sql)
        end
      end
    end
  end

  desc "Update property portal exports"
  task :update_property_portal_export => :environment do
    Property.find(:all, :limit => 5000, :offset => 0, :conditions => "`id` != 26400").each_with_index do |property, index|
      puts "--> Index: #{index + 1}"
      puts "----> Scanning Property ID: #{property.id}"
      next unless property.property_portal_exports.blank?
      next if property.office_id.blank?
      office = Office.find_by_id(property.office_id)
      next if office.blank?
      country = Country.find_by_name(office.country)
      next if country.blank?
      portal_countries = country.portal_countries
      next if portal_countries.blank?
      #allowed = [1,5,6].include?(property.status)
      #next unless allowed
      portal_countries.each do |p_c|
        portal_autosubcribe = PortalAutosubcribe.find_by_portal_id_and_office_id(p_c.portal.id,office.id)
        unless portal_autosubcribe.blank?
          puts "---------> Autoscubcribe is checked for portal ID: #{p_c.portal.id}"
          cek = PropertyPortalExport.find_by_property_id_and_portal_id_and_uncheck(property.id,p_c.portal.id,false)
          if cek.blank?
            puts "--------------> Create property portal export for Property ID: #{property.id} and portal ID: #{p_c.portal.id}"
            sql="INSERT INTO `property_portal_exports` (`property_id` ,`portal_id`) VALUES ('#{property.id}', '#{p_c.portal.id}');"
            ActiveRecord::Base.connection.execute(sql)
          end
        end
      end
    end
  end

  desc "New Update property portal exports"
  task :new_update_property_portal_export => :environment do     
    Property.find(:all, :limit => 5000, :offset => 0, :conditions => "`id` != 26400").each_with_index do |property, index|
      puts "--> Index: #{index + 1}"
      puts "--------> Scanning Property ID: #{property.id}"
      next unless property.property_portal_exports.blank?
      next if property.office_id.blank?
      portal_autosubcribe = PortalAutosubcribe.find_all_by_office_id(property.office_id)
      next if portal_autosubcribe.blank?
      portal_autosubcribe.each do |p_a|
        puts "-------------> Autoscubcribe is checked for portal ID: #{p_a.portal_id}"
        cek = PropertyPortalExport.find_by_property_id_and_portal_id(property.id,p_a.portal_id,false)
        if cek.blank?
          puts "------------------> Create property portal export for Property ID: #{property.id} and portal ID: #{p_a.portal_id}"
          sql="INSERT INTO `property_portal_exports` (`property_id` ,`portal_id`) VALUES ('#{property.id}', '#{p_a.portal_id}');"
          ActiveRecord::Base.connection.execute(sql)
        end
      end
    end
  end

  desc "Update timestamp for updated at"
  task :update_timestamp_to_now => :environment do
    Property.find(:all, :conditions => "`updated_at` >= '2010-09-26'").each_with_index do |property, index|
      puts "--> Index: #{index + 1}"
      puts "--------> Scanning Property ID: #{property.id}"
      sql="UPDATE properties SET `updated_at`='#{Time.zone.now.to_formatted_s(:db_format)}' WHERE ID=#{property.id}"
      ActiveRecord::Base.connection.execute(sql)
    end
  end

  desc "Update all users diaplay on site to true"
  task :update_display_on_site => :environment do
    AgentUser.find(:all).each_with_index do |user, index|
      puts "--> Index: #{index + 1}"
      puts "--------> Scanning User ID: #{user.id}"
      sql="UPDATE users SET `display_on_site`= 1 WHERE ID=#{user.id}"
      ActiveRecord::Base.connection.execute(sql)
    end
  end

  desc "Update feature for new development"
  task :update_feature_for_newdev => :environment do
    Feature.find(:all, :conditions => "`property_type` = 'ResidentialSale' AND `office_id` IS NULL").each_with_index do |feature, index|
      puts "--> Index: #{index + 1}"
      puts "--------> Scanning Feature : #{feature.name}, Type : #{feature.type}"
      f = Feature.new(:name => "#{feature.name}", :type => "#{feature.type}", :property_type => "NewDevelopment")
      f.type = "#{feature.type}"
      f.save!
    end
  end

  desc "set office_id for all extra"
  task :update_office_for_extra => :environment do
    puts "=====================Update extra table======================="
    Extra.find(:all).each_with_index do |extra, index|
      puts "--> Index: #{index + 1}"
      puts "--------> Scanning Extra ID: #{extra.id}"
      begin
        ActiveRecord::Base.connection.execute("UPDATE extras SET `office_id`= #{extra.property.office_id} WHERE id=#{extra.id}")
      rescue
        puts "-----------------> Error Extra ID: #{extra.id}"
      end
    end
    puts "=====================Update extra option table======================="
    ExtraOption.find(:all).each_with_index do |extrao, index|
      puts "--> Index: #{index + 1}"
      puts "--------> Scanning Extra Op ID: #{extrao.id}"
      begin
        ActiveRecord::Base.connection.execute("UPDATE extra_options SET `office_id`= #{extrao.property.office_id} WHERE id=#{extrao.id}")
      rescue
        puts "-----------------> Error Extra ID: #{extra.id}"
      end
    end
  end

  desc "Update rent pa for commercial"
  task :update_rent_pa => :environment do
    Property.find(:all, :conditions => "`type` = 'Commercial' And `deal_type` = 'Lease'").each do |p|
      puts "----> populate commercial for Property ID: #{p.id}"
      detail = p.detail
      unless detail.blank?
        puts "----------> detected commercial detail ID: #{detail.id}"
        ActiveRecord::Base.connection.execute("UPDATE commercial_details SET current_rent=#{p.price} WHERE id=#{detail.id}") unless p.price.blank?
      end
    end
  end
  
  desc "csv import suburbs"
  task :csv_suburb_region => :environment do
    country_id = 12
    states = [699, 700, 701, 702, 703, 704, 705, 706]
    states.each do |state_id|
#      town_countries = Property.find_by_sql("SELECT DISTINCT(`area`) AS area FROM  `tmp_import` WHERE `state_id`=#{state_id}")
#      unless town_countries.blank?
#        town_countries.each do |town_country|
#          ActiveRecord::Base.connection.execute("Insert into town_countries_backup(`name`,`country_id`,`state_id`) Values('#{town_country.area}', '#{country_id}', '#{state_id}')")
#        end
#      end

      suburbs = Property.find_by_sql("SELECT DISTINCT(`suburb`) AS suburb, postcode, area  FROM `tmp_import` WHERE `state_id`=#{state_id}")
      unless suburbs.blank?
        suburbs.each do |sb|
          town = ActiveRecord::Base.connection.select_all("Select id from town_countries_backup Where `name` LIKE '#{sb.area}' And state_id=#{state_id}")
          ActiveRecord::Base.connection.execute("Insert into suburbs_backup(`name`, `postcode`,`country_id`,`state_id`,`town_country_id`) Values('#{sb.suburb.gsub("'", "''")}', '#{sb.postcode}', '#{country_id}', '#{state_id}', '#{town.first["id"]}')") unless town.blank?
        end
      end
    end

#    FasterCSV.foreach("#{RAILS_ROOT}/public/csv_suburb_region.csv", :headers => false) do |row|
#       ActiveRecord::Base.connection.execute("Insert into `tmp_import` Values('', '#{row[0]}', '#{row[1]}', '#{(row[2].gsub("'", "~") unless row[2].blank?)}', '#{row[3]}')")
#    end
  end

  desc "Update conjunctional user"
  task :update_conjunctional_user => :environment do
    agent_users = AgentUser.find(:all, :conditions => "users.conjunctional_office_name IS NOT NULL And users.conjunctional_id IS NULL And roles.id = 7", :include => "roles")
    agent_users.each_with_index do |au, index|
      cj = Conjunctional.find(:first, :conditions => "`conjunctional_office_name` LIKE '%#{au.conjunctional_office_name}%'")
      unless cj.blank?
        found_or_not = "Found - #{cj.conjunctional_office_name}"
      else
        cj = Conjunctional.create({:conjunctional_rea_agent_id => au.conjunction_rea_agent_id, :conjunctional_office_name => au.conjunctional_office_name}) unless au.conjunctional_office_name.blank?
        found_or_not = "Not Found"
      end
      puts "#{index+1}. #{au.full_name} - off: #{au.conjunctional_office_name}, rea id = #{au.conjunction_rea_agent_id} --> #{found_or_not}"
      ActiveRecord::Base.connection.execute("UPDATE users SET conjunctional_id = '#{cj.id}' WHERE id = #{au.id}")
    end
  end
  
  desc "checking inbox mail"
  task :checking_mail => :environment do
    require 'net/pop'
    require 'tmail'
    Net::POP3.enable_ssl(OpenSSL::SSL::VERIFY_NONE)

    message = "Running Mail Importer..."
    Net::POP3.start("pop.gmail.com", 995, "aribascom@gmail.com", "setya87890m") do |pop|
      if pop.mails.empty?
        message += "NO MAIL"
      else
        pop.mails.each do |email|
            begin
              message += "receiving mail...,"
              Mailman.receive(email.pop)
              email.delete
            rescue Exception => e
              message += "Error receiving email at " + Time.now.to_s + "::: " + e.message
            end
        end
      end
    end
    message += "Finished Mail Importer."
    Log.create({:message => "#{message}"})
  end

  desc "remove all same error message for property"
  task :remove_all_same_eror => :environment do
    office_ids = []
    all_export_id = ""
    er_o = ExportError.find_by_sql("SELECT DISTINCT(`office_id`) AS id from export_errors")
    er_o.each{|a| office_ids << a.id}
    office_ids.each_with_index do |office_id, index1|
      er_p = ExportError.find_by_sql("SELECT DISTINCT (e.`property_id`), e.`portal_name` , e.`error_message` , (SELECT d.id FROM `export_errors` d WHERE d.`property_id` = e.property_id ORDER BY id DESC LIMIT 0 , 1 ) AS e_id FROM `export_errors` e WHERE e.`office_id` =#{office_id}")
      er_p.each_with_index do |export, index|
        puts "#{export.e_id}, #{export.property_id}, #{export.portal_name}, #{export.error_message}"
        if index == 0 && index1 == 0
          all_export_id +="#{export.e_id}"
        else
          all_export_id +=",#{export.e_id}"
        end
      end
    end
    ActiveRecord::Base.connection.execute("DELETE FROM export_errors WHERE id NOT IN(#{all_export_id})")
  end

  desc "update conjunctional display"
  task :update_conjunctional_display => :environment do
    agent_users = AgentUser.find(:all, :conditions => "roles.id = 7", :include => "roles",:order => "users.first_name ASC")
    agent_users.each do |agent_user|
      ActiveRecord::Base.connection.execute("UPDATE users SET display_on_site = 1 WHERE id = #{agent_user.id}")
    end
  end

  desc "update re suburb and rca suburb display"
  task :update_nz_suburb => :environment do
    properties = Property.find(:all, :conditions => "country = 'New Zealand' And type = 'Commercial'")
    properties.each do |property|
      puts "-> update nz for #{property.id} with #{property.suburb}"
      ActiveRecord::Base.connection.execute("UPDATE properties SET re_suburb = '#{property.suburb}',	rca_suburb = '#{property.suburb}', re_state = '#{property.state}',	rca_state = '#{property.state}', re_postcode = '#{property.zipcode}',	rca_postcode = '#{property.zipcode}' WHERE id = #{property.id}")
    end
    puts "========Finished update #{properties.size} properties========"
  end
#
#  desc "generate js pdf"
#  task :generate_js_pdf => :environment do
#    require 'prawn'
#    #https://github.com/yob/prawn-js
#
#    NAME_TREE_CHILDREN_LIMIT = 20
#    template_file_name = File.join(File.dirname(__FILE__), 'template.pdf')
#    pdf = Prawn::Document.new(:template => template_file_name)
#
#    # Add javascript to pdf
#script = <<-EOF
#var LastDay = 1;
#var LastMonth = 3;
#var LastYear = 2012;
#var today = new Date();
#var myDate = new Date();
#myDate.setFullYear(LastYear, LastMonth-1, LastDay);
#if (myDate < today)
#{
#app.alert("This files has expired.",3);
#this.closeDoc(1);
#}
#EOF
#    js_root = pdf.ref!(Prawn::Core::NameTree::Node.new(pdf, NAME_TREE_CHILDREN_LIMIT))
#    pdf.names.data[:JavaScript] ||= js_root
#    obj = pdf.ref!(:S => :JavaScript, :JS => script)
#    js_root.data.add("alert", obj)
#    # End Add javascript to pdf
#
#    pdf.text('Ari Prasetya')
#    pdf.render_file('output.pdf')
#  end
#
#  desc "generate js pdf_test"
#  task :generate_js_pdf_test => :environment do
#    # Add javascript to pdf
#script = <<-EOF
#var LastDay = 20;
#var LastMonth = 3;
#var LastYear = 2012;
#var today = new Date();
#var myDate = new Date();
#myDate.setFullYear(LastYear, LastMonth-1, LastDay);
#if (myDate < today)
#{
#app.alert("This files has expired.",3);
#this.closeDoc(1);
#}
#EOF
#    require 'fpdf'
#     pdf=FPDF.new
#     pdf.AddPage
#     pdf.IncludeJs(script)
#     pdf.SetFont('Arial','B',21)
#     pdf.Cell(190,220,"test pdf ya",0,0,'C')
#     pdf.Output("output.pdf")
#  end

  desc "reset password for contact login"
  task :make_default_password_contact => :environment do
    password = "zooproperty"
    require "base64"
    agent_contacts = AgentContact.find(:all, :conditions => "username IS NOT NULL and password IS NOT NULL")
    agent_contacts.each do |agent_contact|
      puts "-> reset password for #{agent_contact.id}"
      curr_pass = agent_contact.password.gsub(/\s+/, "")
      ActiveRecord::Base.connection.execute("UPDATE agent_contacts SET password = '#{Base64.encode64(password)}'  WHERE id = #{agent_contact.id}") unless curr_pass.blank?
    end
  end

  desc "enable automatically Professionals WA"
  task :enable_automatically_api => :environment do
    developer = Developer.find(251)
    developer.offices.each do |office|
      puts "-> enable automatically api for office: #{office.id}"
      ActiveRecord::Base.connection.execute("UPDATE domains SET enable_export = 1 WHERE ipn LIKE '%mobilephonerealestate.com.au/index.php%' And office_id = #{office.id}")
    end
  end

  #========================= Begin Rake task for whenever ==========================
  desc "checking whenever"
  task :checking_whenever => :environment do
    Log.create({:message => "Whenever is running at #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}"})
  end

  desc "Whenever check eproperty alert"
  task :whn_check_eproperty_alert => :environment do
    ["daily", "weekly", "fortnightly", "monthly"].each do |fq|
      ealerts = Ealert.find(:all, :conditions => "send_time IS NOT NULL And start_date <= '#{Time.now.strftime("%Y-%m-%d")}' And sent_at IS NOT NULL And frequency = '#{fq}'")
      ealerts.each do |ealert|
        send_alert = false
        now = (Time.zone.now.utc.day + (Time.zone.now.utc.month * 30))
        last_update = (ealert.updated_at.utc.day + (ealert.updated_at.utc.month * 30))
        diff = now - last_update
        
        send_alert = true if ((diff == 1) || (diff == 0)) && (fq == 'daily')
        send_alert = true if ((diff == 7) || (diff == 0)) && (fq == 'weekly')
        send_alert = true if ((diff == 14) || (diff == 0)) && (fq == 'fortnightly')
        send_alert = true if ((diff == 30) || (diff == 0)) && (fq == 'monthly')

        if (ealert.send_time.strftime("%H") == Time.zone.now.strftime("%H")) && send_alert == true
          send_ealert(ealert)
          ActiveRecord::Base.connection.execute("UPDATE ealerts SET updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE id = '#{ealert.id}'")
        end
      end
    end
  end

 desc "Whenever check eproperty alert Test"
  task :whn_check_eproperty_alert_test => :environment do
    ["daily", "weekly", "fortnightly", "monthly"].each do |fq|
      ealerts = Ealert.find(:all, :conditions => "send_time IS NOT NULL And start_date <= '#{Time.now.strftime("%Y-%m-%d")}' And sent_at IS NOT NULL And frequency = '#{fq}'")
      ealerts.each do |ealert|
        send_alert = false
        now = (Time.zone.now.utc.day + (Time.zone.now.utc.month * 30))
        last_update = (ealert.updated_at.utc.day + (ealert.updated_at.utc.month * 30))
        diff = now - last_update

        send_alert = true if ((diff == 1) || (diff == 0)) && (fq == 'daily')
        send_alert = true if ((diff == 7) || (diff == 0)) && (fq == 'weekly')
        send_alert = true if ((diff == 14) || (diff == 0)) && (fq == 'fortnightly')
        send_alert = true if ((diff == 30) || (diff == 0)) && (fq == 'monthly')

        puts "================Ealert Id #{ealert.id}====================="
        puts "#{ealert.send_time.strftime("%H")} == #{Time.zone.now.strftime("%H")} : send alert = #{send_alert}"
        if (ealert.send_time.strftime("%H") == Time.zone.now.strftime("%H")) && send_alert == true
          puts "--> send email"
          #send_ealert(ealert)
          #ActiveRecord::Base.connection.execute("UPDATE ealerts SET updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE id = '#{ealert.id}'")
        end
      end
    end
  end

  desc "Whenever cron check property every 5 minutes"
  task :whn_check_property_5min => :environment do
    # delete which more than 25 sent
    ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE count_sent > 25")
    # delete hanging process, 5 minute ago
    ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE cron_checked = 0 And count_sent < 3 And updated_at < '#{(Time.now - 5.minutes).strftime("%Y-%m-%d %H:%M:%S")}'")
    require 'timeout'
    Timeout::timeout(270) {
      begin
       property_domains = PropertyDomain.find_by_sql("SELECT DISTINCT `property_id`, `domain_id` FROM property_domains WHERE cron_checked = 0 And count_sent >= 3")
       property_domains.each{|property_domain| send_property_api(property_domain)} unless property_domains.blank?
      rescue
      end
    }
  end

  desc "Whenever cron check user every 6 minutes"
  task :whn_check_user_6min => :environment do
    require 'timeout'
    Timeout::timeout(330) {
      begin
        agent_users = AgentUser.find_by_sql("SELECT * FROM users WHERE (cron_checked = 0 OR cron_checked IS NULL) AND type LIKE 'AgentUser' AND processed = 0 AND (`deleted_at` IS NULL)")
        unless agent_users.blank?
          agent_users.each do |user|
            sql = ActiveRecord::Base.connection()
            sql.update "UPDATE users SET cron_checked = 1 WHERE id = #{user.id}"
            sql.commit_db_transaction
            send_team_member_api(user)
          end
        end
      rescue
      end
    }
  end

  desc "Whenever update cron_checked to false"
  task :whn_update_cron_checked => :environment do
    #  scheduler.cron '15 1 * * *'
    begin
#      property_domains = PropertyDomain.find_by_sql("SELECT * FROM property_domains WHERE count_sent > 3 And cron_checked = 1")
#      unless property_domains.blank?
#        property_domains.each do |property_domain|
#          sql = ActiveRecord::Base.connection()
#          sql.update "UPDATE property_domains SET cron_checked = 0 WHERE id = #{property_domain.id}"
#          sql.commit_db_transaction
#        end
#      end
       ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE updated_at < '#{Time.zone.now.strftime("%Y-%m-%d 00:00:01")}'")
    rescue
    end
  end

  desc "Whenever update failed cron"
  task :whn_update_failed_cron => :environment do
    begin
      updated_at = Time.zone.now.strftime("%Y-%m-%d 00:00:01")
      property_domains = PropertyDomain.find_by_sql("SELECT * FROM property_domains WHERE count_sent > 3 And cron_checked = 1 AND (`updated_at` > '#{updated_at}')")
      unless property_domains.blank?
        property_domains.each do |property_domain|
          sql = ActiveRecord::Base.connection()
          sql.update "UPDATE property_domains SET cron_checked = 0 WHERE id = #{property_domain.id}"
          sql.commit_db_transaction
        end
      end
    rescue
    end
  end

  desc "Whenever scan export error"
  task :whn_scanning_export_errors => :environment do
    begin
      one_week_ago = (Time.zone.now - 1.weeks).strftime("%Y-%m-%d 00:00:01")
      ExportError.find_by_sql("DELETE FROM export_errors WHERE `created_at` < '#{one_week_ago}'")
    rescue
    end
  end

  desc "Whenever update properties weekly"
  task :whn_update_properties_weekly => :environment do
    begin
      Log.create(:message => "Update properties weekly at #{Time.now.strftime("%Y-%m-%d %H:%M:%S")}")
      offices = Office.find_by_sql("SELECT id FROM offices WHERE update_listing_weekly = 1")
      unless offices.blank?
        offices.each_with_index{|office, index|
          properties = Property.find_by_sql("SELECT * FROM properties WHERE `status` IN(1, 5) AND `type` IN('Commercial','BusinessSale') AND `office_id` = #{office.id}")
          properties.each{|property| ActiveRecord::Base.connection.execute("UPDATE properties SET updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE id = #{property.id}")} unless properties.blank?
        }
      end
    rescue
    end
  end

  desc "Whenever scanning opentimes"
  task :whn_scanning_opentimes => :environment do
    #  scheduler.cron '45 23 * * 0-6'

    #change from 24 hour to 15 hour in the past
    #23:45 - 15 = 13:45

    #SELECT * FROM `opentimes` WHERE (date <= '2011-01-12') OR (date = '2011-01-13' AND `end_time` < '13:45:00')

    opentimes = Opentime.find(:all, :conditions => ["(date <= ? ) OR (date = ? AND `end_time` < ?) ", (Time.zone.now - 24.hour).strftime("%Y-%m-%d"), Time.zone.now.strftime("%Y-%m-%d"), "13:45:00"])
    opentimes.each{|opentime|
     next if opentime.blank?
     #tmp_property = opentime.property unless opentime.property.blank?
     opentime.destroy
     #tmp_property.update_attribute(:updated_at,Time.zone.now) unless tmp_property.blank?
    } unless opentimes.blank?
  end

  desc "Whenever scanning notification for note"
  task :whn_scanning_note_notifications => :environment do
    # scheduler.cron '0 7 * * *' when 07:00 will scan notification for note
    contact_notes = ContactNote.find(:all, :conditions => ["note_date = (?) and email_reminder = 1", (Time.zone.now).strftime("%Y-%m-%d")])
    contact_notes.collect{|contact_note|
      user = AgentUser.find_by_id(contact_note.agent_user_id)
      NoteNotification.deliver_email_reminder(user,contact_note.description,contact_notes.agent_contact,user.first_name)
    } unless contact_notes.blank?
  end

  desc "Whenever scanning inactivate office whose trial date is over"
  task :whn_scanning_office_trial_at00 => :environment do
    #scheduler.cron '0 0 * * 0-6'
    inactivate_office_whose_trial_date_is_over
    update_office_whose_subscription_is_canceled
  end

  desc "Whenever scanning inactivate office whose trial date is over"
  task :whn_scanning_office_trial_at430 => :environment do
    # scheduler.cron '30 4 * * 0-6'
    inactivate_office_whose_trial_date_is_over
    update_office_whose_subscription_is_canceled
  end

  desc "Whenever scanning inactivate office whose trial date is over"
  task :whn_scanning_office_trial_at1200 => :environment do
    #scheduler.cron '0 12 * * 0-6'
    inactivate_office_whose_trial_date_is_over
    update_office_whose_subscription_is_canceled
  end

  desc "Whenever scanning inactivate office whose trial date is over"
  task :whn_scanning_office_trial_at1800 => :environment do
    #scheduler.cron '0 18 * * 0-6'
    inactivate_office_whose_trial_date_is_over
    update_office_whose_subscription_is_canceled
  end

  desc "whenever check for sphinx search"
  task :whn_check_for_sphinx_search => :environment do
    indexing_test
  end

  desc "whenever check for delayed jobs"
  task :whn_check_for_delayed_jobs => :environment do
    all_jobs = ActiveRecord::Base.connection.execute("SELECT * FROM `delayed_jobs` WHERE 1")
    if all_jobs.present? && all_jobs.all_hashes.count >= 100
      EcampaignMailer.deliver_send_delayed_jobs_notification
    end
  end

  desc "whenever update property image location"
  task :whn_update_image_location => :environment do
    Log.create(:message => "update property image START")
    begin
      attachments = Attachment.find(:all, :conditions => "parent_id IS NULL and attachable_type = 'Property' and type = 'Image' and created_at >= '2012-07-01 00:00:00' and created_at <= '2012-08-01 00:00:00'")
      attachments.each do |img|
        ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{img.position} WHERE parent_id = #{img.id}")
      end
      Log.create(:message => "attachments: #{attachments.count}")
    rescue Exception => ex
      Log.create(:message => "update property image ERR #{ex.inspect}")
    end
  end
  
  def indexing_test
    today = Date.today.beginning_of_day.strftime('%Y-%m-%d %H:%M:%S')
    yesterday = (Date.today-1.day).beginning_of_day.strftime('%Y-%m-%d %H:%M:%S')
    prop = Property.find(:last, :conditions => "created_at > '#{yesterday}' and created_at < '#{today}'")
    if prop.present?
      office_id = prop.office_id
      street = prop.street
      pid = prop.id
      begin
        prop_quick = Property.quick_search("#{street}", {:id => pid,:office_id => office_id}, {})
        if prop_quick.blank?
          EcampaignMailer.deliver_send_sphinx_notification
        end
      rescue
        EcampaignMailer.deliver_send_sphinx_notification
      end
    end
  end
  
  def inactivate_office_whose_trial_date_is_over
    offices = Office.find_by_sql("SELECT * FROM `subscriptions` , `offices` WHERE offices.status = 'active' AND offices.id = subscriptions.office_id")
    f = File.open(File.join(RAILS_ROOT, 'log/cron_log_' "#{Time.now.strftime("%d-%m-%y_%H-%M")}"), "w+")
    offices.each do |office|
      sub = office.subscription
      sp = office.subscription_plan
      if Time.now >= sp['trial_end_date'] && sub.status == 'pending'
        office.update_attribute(:status, 'inactive')
        f.write("#{office.name} --- #{office.status} --- #{office.created_at} \n")
        User.find(:all,:conditions =>["office_id = ?",office.id]).each do |user|
          if user['type'] == "DeveloperUser" && user.full_access?
            MessageNotification.deliver_notify_inactive(user,office)
          end
        end
      end
    end
    f.close
  end

  def update_office_whose_subscription_is_canceled
    subscriptions = Subscription.all
    subscriptions.each do |subscription|
      office = subscription.office
      if !subscription.canceled_at.blank? && subscription.status == 'canceled' && Time.now >= subscription.ends_on
        office.update_attribute(:status, "inactive")
      end
    end
  end

  def send_property_api(property_domain)
    begin
      pr = Property.find_by_sql("SELECT * FROM properties WHERE id = #{property_domain.property_id}").first
      dom = Domain.find_by_sql("SELECT * FROM domains WHERE id = #{property_domain.domain_id}").first
      return if pr.blank?
      if dom.blank?
        ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE domain_id = #{property_domain.domain_id}")
        return
      end

      return if pr.office.blank? || pr.office.agent.blank?
      url_domain = "#{pr.office.agent.developer.entry}.commercialloopcrm.com.au"
      ipn_url = dom.ipn
      domain_id = dom.id

      pd = PropertyDomain.find_by_sql("SELECT count(id) as count_api FROM property_domains WHERE property_id = #{property_domain.property_id} And domain_id = #{property_domain.domain_id} And count_sent >= 3").first
      return if pd.blank?
      if pd.count_api.to_i == 1
        ActiveRecord::Base.connection.execute("UPDATE property_domains SET count_sent = (count_sent + 1), cron_checked = 1, updated_at = '#{Time.now.strftime("%Y-%m-%d %H:%M:%S")}' WHERE property_id = #{property_domain.property_id} And domain_id = #{property_domain.domain_id} And count_sent >= 3")
        pd_c = PropertyDomain.find_by_sql("SELECT * FROM property_domains WHERE property_id = #{property_domain.property_id} And domain_id = #{property_domain.domain_id} And count_sent >= 3").first
        counter = pd_c.sender_uid
      else
        counter = Time.zone.now.to_i
        ActiveRecord::Base.connection.execute("DELETE FROM property_domains WHERE property_id = #{property_domain.property_id} And domain_id = #{property_domain.domain_id} And count_sent >= 3")
        PropertyDomain.create({:property_id => property_domain.property_id, :domain_id => property_domain.domain_id, :count_sent => 4, :cron_checked => 1, :sender_uid => counter})
      end

      token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
      GLOBAL_ATTR["#{pr.office_id}-#{pr.agent_id}-#{pr.id}"] = token
      property = pr.attributes
      encoded_property = {}
      property.each do |prop|
        if prop.last.is_a?(String)
          encoded_property.merge!(prop.first => Base64.encode64(prop.last))
        else
          encoded_property.merge!(prop.first => prop.last)
        end
      end

      rental_seasons = pr.is_a?(HolidayLease) ? pr.rental_seasons : nil
      rent_seasons = {}
      unless rental_seasons.blank?
        rental_seasons.each_with_index do |rent, i|
          rent_seasons.merge!(i.to_s => rent.attributes)
        end
      end

      property_detail = pr.detail.attributes.delete_if { |x,y| ["residential_sale_id","residential_lease_id","business_sale_id","commercial_id","commercial_building","project_sale_id","holiday_lease_id","new_development_id","land_release_id","id"].include?(x)  }
      property_detail["bedrooms"] = property_detail["bedrooms"] + 0.5 if property_detail["half_bedroom"] == true
      property_detail["bathrooms"] = property_detail["bathrooms"] + 0.5 if property_detail["half_bathroom"] == true
      encoded_property_detail ={}
      property_detail.each do |prop_detail|
        if prop_detail.last.is_a?(String)
          encoded_property_detail.merge!(prop_detail.first => Base64.encode64(prop_detail.last))
        else
          encoded_property_detail.merge!(prop_detail.first => prop_detail.last)
        end
      end

      if pr.is_a?(ProjectSale)
        pd = Property.find_by_id(pr.detail.project_development_id)
        encoded_property_detail.merge!("project_development_name" => (Base64.encode64(pd.detail.development_name) unless pd.blank?))
        pdt = ProjectDesignType.find_by_id(pr.detail.project_design_type_id)
        encoded_property_detail.merge!("project_design_type_name" => (Base64.encode64(pdt.name) unless pdt.blank?))
        ps = ProjectStyle.find_by_id(pr.detail.project_style_id)
        encoded_property_detail.merge!("project_style_name" => (Base64.encode64(ps.name) unless ps.blank?))
        ndc = NewDevelopmentCategory.find_by_id(pr.detail.new_development_category_id)
        encoded_property_detail.merge!("new_development_category_name" => (Base64.encode64(ndc.name) unless ndc.blank?))
      end

      encoded_extra ={}
      unless pr.extra.blank?
         extra_attr = pr.extra.attributes
         extra_attr.each do |ext|
          if ext.last.is_a?(String)
            encoded_extra.merge!(ext.first => Base64.encode64(ext.last))
          else
            encoded_extra.merge!(ext.first => ext.last)
          end
        end
      end

      encoded_extra_details = {}
      unless pr.extra_details.blank?
        pr.extra_details.map{|x| x.attributes}.each_with_index do |optd, i|
          optd.keys.each{|d|optd[d] = Base64.encode64(optd[d]) if optd[d].is_a?(String)}
          encoded_extra_details.merge!(i.to_s => optd)
        end
      end

      encoded_extra_detail_names = {}
      unless pr.office.extra_detail_names.blank?
        pr.office.extra_detail_names.map{|x| x.attributes}.each_with_index do |optdn, i|
          optdn.keys.each{|dn|optdn[dn] = Base64.encode64(optdn[dn]) if optdn[dn].is_a?(String)}
          encoded_extra_detail_names.merge!(i.to_s => optdn)
        end
      end

      encoded_extra_options = {}
      unless pr.office.extra_options.blank?
        pr.office.extra_options.map{|x| x.attributes}.each_with_index do |opteo, i|
          opteo.keys.each{|eo|opteo[eo] = Base64.encode64(opteo[eo]) if opteo[eo].is_a?(String)}
          encoded_extra_options.merge!(i.to_s => opteo)
        end
      end

      encoded_newdev_categories = {}
      newcat_property_id = pr.id
      (newcat_property_id = property_detail["project_development_id"] unless property_detail["project_development_id"].blank? ) if pr.type == "ProjectSale"
      new_cat = NewDevelopmentCategory.find(:all, :conditions => "property_id = #{newcat_property_id}")
      unless new_cat.blank?
        new_cat.map{|x| x.attributes}.each_with_index do |optnc, i|
          optnc.keys.each{|t|optnc[t] = Base64.encode64(optnc[t]) if optnc[t].is_a?(String)}
          encoded_newdev_categories.merge!(i.to_s => optnc)
        end
      end

      users = {}
      features = {}
      opentimes = {}

      unless pr.primary_contact.blank?
        primary_attr = pr.primary_contact.attributes
        encoded_primary = {}
        primary_attr.each do |prim_user|
          if prim_user.last.is_a?(String)
            encoded_primary.merge!(prim_user.first => Base64.encode64(prim_user.last))
          else
            encoded_primary.merge!(prim_user.first => prim_user.last)
          end
        end
        encoded_primary.merge!({ "landscape_images"=>{"1"=>( pr.primary_contact.landscape_image.first.blank? ? "" : pr.primary_contact.landscape_image.first.public_filename ),"2"=>( pr.primary_contact.landscape_image.second.blank? ? "" : pr.primary_contact.landscape_image.second.public_filename )},"portrait_images"=>{"1"=>( pr.primary_contact.portrait_image.first.blank? ? "" : pr.primary_contact.portrait_image.first.public_filename ),"2"=>( pr.primary_contact.portrait_image.second.blank? ? "" : pr.primary_contact.portrait_image.second.public_filename )}}) unless encoded_primary.blank?
        users.merge!({"primary" => encoded_primary}) unless encoded_primary.blank?
      end

      unless pr.secondary_contact.blank?
        secondary_attr = pr.secondary_contact.attributes
        encoded_secondary = {}
        secondary_attr.each do |sec_user|
          if sec_user.last.is_a?(String)
            encoded_secondary.merge!(sec_user.first => Base64.encode64(sec_user.last))
          else
            encoded_secondary.merge!(sec_user.first => sec_user.last)
          end
        end

        encoded_secondary.merge!("landscape_images"=>{"1"=>( pr.secondary_contact.landscape_image.first.blank? ? "" : pr.secondary_contact.landscape_image.first.public_filename ),"2"=>( pr.secondary_contact.landscape_image.second.blank? ? "" : pr.secondary_contact.landscape_image.second.public_filename )},"portrait_images"=>{"1"=>( pr.secondary_contact.portrait_image.first.blank? ? "" : pr.secondary_contact.portrait_image.first.public_filename ),"2"=>( pr.secondary_contact.portrait_image.second.blank? ? "" : pr.secondary_contact.portrait_image.second.public_filename )})  unless encoded_secondary.blank?
        users.merge!({"secondary" => encoded_secondary}) unless encoded_secondary.blank?
      end

      unless pr.third_contact.blank?
        third_attr = pr.third_contact.attributes
        encoded_third = {}
        third_attr.each do |third_user|
          if third_user.last.is_a?(String)
            encoded_third.merge!(third_user.first => Base64.encode64(third_user.last))
          else
            encoded_third.merge!(third_user.first => third_user.last)
          end
        end

        encoded_third.merge!("landscape_images"=>{"1"=>( pr.third_contact.landscape_image.first.blank? ? "" : pr.third_contact.landscape_image.first.public_filename ),"2"=>( pr.third_contact.landscape_image.second.blank? ? "" : pr.third_contact.landscape_image.second.public_filename )},"portrait_images"=>{"1"=>( pr.third_contact.portrait_image.first.blank? ? "" : pr.third_contact.portrait_image.first.public_filename ),"2"=>( pr.third_contact.portrait_image.second.blank? ? "" : pr.third_contact.portrait_image.second.public_filename )})  unless encoded_third.blank?
        users.merge!({"third" => encoded_third}) unless encoded_third.blank?
      end

      pr.opentimes.each_with_index{|o,i| opentimes.merge!(i.to_s => {"date"=> o.date, "property_id"=> o.property_id, "id"=> o.id, "start_time"=> o.start_time.strftime("%H:%M"), "end_time"=> o.end_time.strftime("%H:%M")})} unless pr.opentimes.blank?
      #pr.images.each_with_index{|im,i| images.merge!(i.to_s => im.attributes)} unless pr.images.blank?
      encoded_images = {}
      unless pr.images.blank?
        pr.images.map{|x| x.attributes}.each_with_index do |img, i|
          img.keys.each{|t| img[t] = Base64.encode64(img[t]) if img[t].is_a?(String)}
          encoded_images.merge!(i.to_s => img)
        end
      end

      #pr.brochure.each_with_index{|b,i| brochures.merge!(i.to_s => b.attributes)} unless pr.brochure.blank?
      encoded_brochures = {}
      unless pr.brochure.blank?
        pr.brochure.map{|x| x.attributes}.each_with_index do |bro, i|
          bro.keys.each{|t| bro[t] = Base64.encode64(bro[t]) if bro[t].is_a?(String) }
          encoded_brochures.merge!(i.to_s => bro)
        end
      end

      #pr.floorplans.each_with_index{|f,i| floorplans.merge!(i.to_s => f.attributes)} unless pr.floorplans.blank?
      encoded_floorplans = {}
      unless pr.floorplans.blank?
        pr.floorplans.map{|x| x.attributes}.each_with_index do |fp, i|
          fp.keys.each{|t| fp[t] = Base64.encode64(fp[t]) if fp[t].is_a?(String)}
          encoded_floorplans.merge!(i.to_s => fp)
        end
      end

      pr.features.each_with_index { |f,i| features.merge!({i.to_s => Base64.encode64(f.name)}) } unless pr.features.blank?

      #pr.videos.each_with_index{|b,i| videos.merge!(i.to_s => b.attributes)} unless pr.videos.blank?
      encoded_videos = {}
      unless pr.property_videos.blank?
        pr.property_videos.map{|x| x.attributes}.each_with_index do |vd, i|
          vd.keys.each{|t| vd[t] = Base64.encode64(vd[t]) if vd[t].is_a?(String)}
          encoded_videos.merge!(i.to_s => vd)
        end
      end

      audio = pr.mp3.blank? ? nil : {"0" => pr.mp3.attributes}
      encoded_testimonials = {}
      cn = ContactNote.find(:all, :conditions => "`note_type_id`= 11 And `property_id` = #{pr.id}")
      cn.each_with_index{|note, i| encoded_testimonials.merge!(i.to_s => {"first_name" => Base64.encode64(note.agent_contact.first_name), "last_name" => Base64.encode64(note.agent_contact.last_name),"contact_name" => Base64.encode64(note.agent_contact.full_name), "date" => note.note_date, "note" => Base64.encode64(note.description)})} unless cn.blank?

      ["updated_at","created_at","deleted_at"].each do |date|
        property[date] = property[date].to_formatted_s(:db_format) unless property[date].blank?
        property_detail[date] = property_detail[date].to_formatted_s(:db_format) unless property_detail[date].blank?
      end

      token_counter = "#{token}_#{counter}_#{domain_id}"
      system("curl -i -X POST -d 'property=#{encoded_property.to_json}&details=#{encoded_property_detail.to_json}&rental_seasons=#{rent_seasons.to_json}&contacts=#{users.to_json}&brochures=#{encoded_brochures.to_json}&opentimes=#{opentimes.to_json}&images=#{encoded_images.to_json}&floorplans=#{encoded_floorplans.to_json}&features=#{features.to_json}&videos=#{encoded_videos.to_json}&audio=#{audio.to_json}&extra=#{encoded_extra.to_json}&extra_details=#{encoded_extra_details.to_json}&extra_detail_names=#{encoded_extra_detail_names.to_json}&extra_options=#{encoded_extra_options.to_json}&new_development_categories=#{encoded_newdev_categories.to_json}&testimonials=#{encoded_testimonials.to_json}&callback_url=http://#{url_domain}/agents/#{pr.agent_id}/offices/#{pr.office_id}/properties/#{pr.id}/property_notification?token=#{token_counter}&type=property' #{ipn_url}?update_property_#{pr.id}")
    rescue Exception => ex
      #Log.create(:message => "Error Cron for property id #{property_domain.property_id}, domain id #{property_domain.domain_id} --> #{ex.inspect}")
    end
  end

  def send_team_member_api(a_user)
      return if a_user.office.blank? || a_user.office.agent.blank?
      url_domain = "#{a_user.office.agent.developer.entry}.commercialloopcrm.com.au"
      domains = a_user.office.domains
      is_http_api = a_user.office.http_api_enable
      is_office_active = a_user.office.status == 'active' ? true : false
      return if domains.blank? || (is_http_api == false || is_http_api.blank?) || is_office_active == false
      domains.each do |domain|
        next if domain.ipn.blank?
        begin
          team_member = a_user.attributes
          encoded_team_member = {}
          team_member.each do |tm|
            if tm.last.is_a?(String)
              encoded_team_member.merge!(tm.first => Base64.encode64(tm.last))
            else
              encoded_team_member.merge!(tm.first => tm.last)
            end
          end

          encoded_testimonials = {}
          unless a_user.testimonials.blank?
            a_user.testimonials.map{|x| x.attributes}.each_with_index do |testi, i|
              testi.keys.each do |t|
                if testi[t].is_a?(String)
                  testi[t] = Base64.encode64(testi[t])
                end
              end
              encoded_testimonials.merge!(i.to_s => testi)
            end
          end

          audio = a_user.mp3.blank? ? nil : a_user.mp3.attributes
          img_1 = img_2 = img_portrait_1 = img_portrait_2 = nil

          landscapes = a_user.landscape_image
          unless landscapes.blank?
            landscapes.each do |img|
              if img.position == 1
                img_1 = img
              else
                img_2 = img
              end
            end
          end

          portraits = a_user.portrait_image
          unless portraits.blank?
            portraits.each do |img_portrait|
              if img_portrait.position == 1
                img_portrait_1 = img_portrait
              else
                img_portrait_2 = img_portrait
              end
            end
          end

          encoded_groups = {}
          unless a_user.user_groups.blank?
            a_user.user_groups.map{|x| x.attributes}.each_with_index do |grp, i|
              grp.keys.each{|t| grp[t] = Base64.encode64(grp[t]) if grp[t].is_a?(String)}
              encoded_groups.merge!(i.to_s => grp)
            end
          end

          encoded_team_member.merge!({ "landscape_images"=>{"1"=>( img_1.blank? ? "" : img_1.public_filename ),"2"=>( img_2.blank? ? "" : img_2.public_filename )},"portrait_images"=>{"1"=>( img_portrait_1.blank? ? "" : img_portrait_1.public_filename ),"2"=>( img_portrait_2.blank? ? "" : img_portrait_2.public_filename )}}).merge!("testimonials" => encoded_testimonials).merge!("vcard" => (a_user.vcard.blank? ? "" : BASE_URL + a_user.vcard.public_filename)).merge!("audio" => audio).merge!("user_groups" => encoded_groups)

          token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
          GLOBAL_ATTR["#{a_user.office_id}-#{a_user.developer_id}-#{a_user.id}"] = token

          system("curl -i -X POST -d 'contacts=#{encoded_team_member.to_json}&callback_url=http://#{url_domain}/agents/#{a_user.agent.id}/offices/#{a_user.office_id}/agent_users/#{a_user.id}/team_notification?token=#{token}&type=team_member' #{domain.ipn}")

          #update api_is_sent field
          a_user.api_is_sent = true
        rescue Exception => ex
          a_user.api_is_sent = false
        end
        sql = ActiveRecord::Base.connection()
        sql.update "UPDATE users SET api_is_sent = #{a_user.api_is_sent} WHERE id = #{a_user.id}"
        sql.commit_db_transaction
      end
  end

  def send_ealert(ealert)
    if ealert
      require 'json'
      unless ealert.stored_data.blank?
        db_data =JSON.parse(ealert.stored_data)
        sender_id = db_data["ealert_data"]["sender_email"]
        contacts = db_data["ealert_contact_checkbox"]
        contact_names = db_data["ealert_contact_name_checkbox"]

        current_sales_condition = "office_id = #{ealert.office_id} And status = 1 And type IN('ResidentialSale','ProjectSale','BusinessSale','Commercial')"
        current_rental_conditon = "office_id = #{ealert.office_id} And status = 1 And type IN('ResidentialLease','HolidayLease','Commercial')"
        opentime_sales_condition = "office_id = #{ealert.office_id} And status = 1 And type IN('ResidentialSale','ProjectSale','BusinessSale','Commercial') And id IN (Select DISTINCT(property_id) from opentimes)"
        opentime_rentals_condition = "office_id = #{ealert.office_id} And status = 1 And type IN('ResidentialLease','HolidayLease','Commercial') And id IN (Select DISTINCT(property_id) from opentimes)"
        sold_condition = "office_id = #{ealert.office_id} And status = 2"
        lease_condition = "office_id = #{ealert.office_id} And status = 3"

        unless ealert.blank?
         unless ealert.sent_at.blank?
          current_sales_condition += " And updated_at > '#{ealert.updated_at.strftime("%Y-%m-%d")}'" if db_data["ealert_data"]["current_sales"] == "2"
          current_rental_conditon += " And updated_at > '#{ealert.updated_at.strftime("%Y-%m-%d")}'" if db_data["ealert_data"]["current_rentals"] == "2"
          opentime_sales_condition += " And updated_at > '#{ealert.updated_at.strftime("%Y-%m-%d")}'" if db_data["ealert_data"]["open_sales"] == "2"
          opentime_rentals_condition += " And updated_at > '#{ealert.updated_at.strftime("%Y-%m-%d")}'" if db_data["ealert_data"]["open_rentals"] == "2"
          sold_condition += " And updated_at > '#{ealert.updated_at.strftime("%Y-%m-%d")}'" if db_data["ealert_data"]["sold"] == "2"
          lease_condition += " And updated_at > '#{ealert.updated_at.strftime("%Y-%m-%d")}'" if db_data["ealert_data"]["leased"] == "2"
         end
       end

       current_sales_ids = Property.find(:all, :conditions => current_sales_condition, :order => "updated_at DESC", :limit => 5)
       current_rentals_ids = Property.find(:all, :conditions => current_rental_conditon, :order => "updated_at DESC", :limit => 5)
       opentime_sales_ids = Property.find(:all, :conditions => opentime_sales_condition, :order => "updated_at DESC", :limit => 5)
       opentime_rentals_ids = Property.find(:all, :conditions => opentime_rentals_condition, :order => "updated_at DESC", :limit => 5)
       sold_ids = Property.find(:all, :conditions => sold_condition, :order => "updated_at DESC", :limit => 5)
       lease_ids = Property.find(:all, :conditions => lease_condition, :order => "updated_at DESC", :limit => 5)

       all_listing = current_sales_ids.size + current_rentals_ids.size + opentime_sales_ids.size + opentime_rentals_ids.size + sold_ids.size + lease_ids.size

       if all_listing > 0
         db_data["info_links"].each{|info|
            destination_link = info["url"].gsub("http://", "").gsub("www", "")
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `destination_link` LIKE '%#{destination_link}%'")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :destination_name => info["title"], :destination_link => info["url"], :clicked => 0) if ec.blank?
          } unless db_data["info_links"].blank?

          db_data["web_links"].each{|info|
          destination_link = info["url"].gsub("http://", "").gsub("www", "")
          ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `destination_link` LIKE '%#{destination_link}%'")
          EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :destination_name => info["title"], :destination_link => info["url"], :clicked => 0) if ec.blank?
          } unless db_data["web_links"].blank?

          current_sales_ids.each{|property|
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `property_id` = #{property.id}")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :property_id => property.id, :clicked => 0) if ec.blank?
          } unless current_sales_ids.blank?
          current_rentals_ids.each{|property|
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `property_id` = #{property.id}")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :property_id => property.id, :clicked => 0) if ec.blank?
          } unless current_rentals_ids.blank?
          opentime_sales_ids.each{|property|
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `property_id` = #{property.id}")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :property_id => property.id, :clicked => 0) if ec.blank?
          } unless opentime_sales_ids.blank?
          opentime_rentals_ids.each{|property|
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `property_id` = #{property.id}")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :property_id => property.id, :clicked => 0) if ec.blank?
          } unless opentime_rentals_ids.blank?
          sold_ids.each{|property|
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `property_id` = #{property.id}")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :property_id => property.id, :clicked => 0) if ec.blank?
          } unless sold_ids.blank?
          lease_ids.each{|property|
            ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `property_id` = #{property.id}")
            EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :property_id => property.id, :clicked => 0) if ec.blank?
          } unless lease_ids.blank?

          ["facebook_social_link", "twitter_social_link", "linkedin_social_link", "youtube_social_link", "rss_social_link"].each do |sclink|
            unless db_data["social_links"][sclink].blank?
              destination_link = db_data["social_links"][sclink].gsub("http://", "").gsub("www", "")
              ec  = EcampaignClick.find(:first, :conditions => "type_ecampaign_id = #{ealert.id} And type_ecampaign='ealert' And `destination_link` LIKE '%#{destination_link}%'")
              EcampaignClick.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :destination_name => db_data["social_links"][sclink], :destination_link => db_data["social_links"][sclink], :clicked => 0) if ec.blank?
            end
          end
          begin
            contacts_ids = []
            if contacts.present?
              contacts_ids << contacts
            end
            if contact_names.present?
              contacts_ids << contact_names
            end
            contacts_ids.uniq!
            contacts = AgentContact.find(:all, :conditions => "id IN (#{contacts_ids.join(',')})") if contacts_ids.present?
            Log.create(:message => "EALERT SEND contacts_ids: #{contacts_ids.inspect}, contacts: #{contacts.inspect} ")
            if contacts.present?
              @emails = []
              contacts.each{|contact| 
                if contact.email.present? && !@emails.include?(contact.email)
                  send_mail_ealert(contact.id, sender_id, ealert)
                  @emails << contact.email
                end
              }
            end
          rescue Exception => ex
            Log.create(:message => "EALERT SEND ERRR : #{ex.inspect}, contacts_ids: #{contacts_ids.inspect}, contacts: #{contacts.inspect} ")
          end
#            contacts.each{|contact_id| send_mail_ealert(contact_id, sender_id, ealert)} unless contacts.blank?
#            contact_names.each{|contact_id| send_mail_ealert(contact_id, sender_id, ealert)} unless contact_names.blank?
        end
      end
    end
  end

  def send_mail_ealert(contact_id, sender_id, ealert)
    contact = AgentContact.find_by_id(contact_id)
    next if contact.blank?
    if contact.inactive != true
      ealert_track = EcampaignTrack.create(:type_ecampaign_id => ealert.id, :type_ecampaign => 'ealert', :agent_contact_id => contact_id)
      if ealert_track
        #send email for each contact
        sender = AgentUser.find_by_id(sender_id)
        begin
          EcampaignMailer.deliver_send_ealert("http://www.commercialloopcrm.com.au", sender, ealert, ealert_track, contact)
        rescue Net::SMTPServerBusy
          ActiveRecord::Base.connection.execute("UPDATE ecampaign_tracks SET email_bounced = 1 WHERE `id`=#{ealert_track.id}") unless ealert_track.blank?
        end
        ContactNote.create({:agent_contact_id => contact.id, :agent_user_id => sender_id, :note_type_id => 23, :description => "An ealert was sent to this user", :note_date => Time.now})
      end
    end
  end
  #========================= End Rake task for whenever ============================
end
