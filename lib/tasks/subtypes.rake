namespace :db do
  desc "Import business sale sub categories"
  task :subtypes => :environment do
    IO.readlines(File.join RAILS_ROOT, 'db/raw/Property_types_businessale.csv').each do |line|
      category,name = line.split(',')
      PropertyType.create :category => category, :name => name.strip
    end
  end
  
  desc "Import Property Types"
  task :property_types => :environment do
    IO.readlines(File.join RAILS_ROOT, 'db/raw/Property_types.csv').each do |line|
      category,name = line.split(',')
      PropertyType.create :category => category, :name => name
    end
  end
  
  desc "Import Business Sale Types"
  task :business_sales => :environment do
    IO.readlines(File.join RAILS_ROOT, 'db/raw/Property_types_businessale.csv').each do |line|
      category,name = line.split(',')
      PropertyType.create :category => category, :name => name.strip
    end
  end
end
