class PropertyApiScheduler < Struct.new(:host, :property_id)
  include Jobs::ScheduledJob

  run_every 10.seconds

  def property_processed
    property = Property.find_by_id(property_id.to_i)
    return property.processed
  end

  def perform
    property = Property.find_by_sql("SELECT * FROM properties WHERE `processed` = 0 AND (`count_api_sent` < 3 OR `count_api_sent` IS NULL) AND id = #{property_id.to_i}").first
    unless property.blank?
      unless property.office.blank?
        GLOBAL_ATTR["Host-#{property.office_id}"] = host.blank? ? "#{property.office.agent.developer.entry}.commercialloopcrm.com.au" : host
        tmp = property.count_api_sent.to_i + 1
        sql = ActiveRecord::Base.connection()
        sql.update "UPDATE properties SET count_api_sent = #{tmp} WHERE id = #{property.id}"
        sql.commit_db_transaction
        property.send_property_api
      else
        remove_delayed_jobs(property_id.to_i)
      end
    else
      remove_delayed_jobs(property_id.to_i)
    end
  end

  def remove_delayed_jobs(property_id)
    jobs = Delayed::Job.all
    id_jobs = []
    unless jobs.blank?
      jobs.each do |job|
        handler = job.handler.split
        id_jobs << job.id if handler[handler.size-2].include?("property_id") && property_id.to_i == handler.last.to_i
      end
    end
    Delayed::Job.destroy(id_jobs) unless id_jobs.blank?
  end
end