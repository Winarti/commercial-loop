class TriggerDomainApi < Struct.new(:domain_id, :developer_id)
  def perform
    domain = Domain.find(domain_id)
    if !domain.ipn.blank? && !domain.name.blank?
      domains = Domain.find(:all, :conditions => ["ipn LIKE ? AND office_id IS NOT NULL",domain.ipn])
      offices = []
      domains.each do |dm|
        of = Office.find_by_id(dm.office_id)
        unless of.blank?
          if of.agent.developer_id.to_i == developer_id.to_i
           offices << dm.office_id unless offices.include?(dm.office_id)
          end
        end
      end
      properties = Property.find(:all, :conditions => ["office_id IN (?) And status IN (1,5)", offices])
      properties.each do |property|
#        if domain.portal_export == true
#          property_portal_exports = property.property_portal_exports
#          unless property_portal_exports.blank?
#           property_portal_exports.each do |px|
#             next if px.portal.blank?
#             next if px.uncheck == true
#             portal = px.portal
#             next if portal.ftp_url.blank?
#             portal_url = portal.ftp_url.downcase.gsub('http://', '')
#             portal_url = portal_url.gsub('www.', '')
#             if domain.name.downcase.include?(portal_url)
#                 domain_id = domain.id
#                 if domain.name.downcase.include?("propertypoint.com.au")
#                   next if property.state.blank?
#                   if property.state.strip == "TAS"
#                     ipn_url = portal.ftp_directory
#                     break
#                   end
#                 elsif domain.name.downcase.include?("land.com.au")
#                   if property.type.to_s == "ProjectSale" || property.property_type.to_s == "Land"
#                     ipn_url = portal.ftp_directory
#                     break
#                   end
#                 elsif domain.name.downcase.include?("getrealty.com.au")
#                   next if property.state.blank?
#                   if property.state.strip == "ACT"
#                     ipn_url = portal.ftp_directory
#                     break
#                   end
#                 elsif domain.name.downcase.include?("virginhomes.com.au")
#                   include_feature = property.features.find(:all,:conditions => "`name` LIKE '%newly built property%' OR `name` LIKE '%new developer listing%'") unless property.features.blank?
#                   if !include_feature.blank? || property.type.to_s == "ProjectSale"
#                    ipn_url = portal.ftp_directory
#                    break
#                   end
#                 elsif domain.name.downcase.include?("propertyinvestor.com.au")
#                   if property.type.to_s == "ResidentialSale" || property.type.to_s == "ProjectSale" || property.type.to_s == "NewDevelopment"
#                    ipn_url = portal.ftp_directory
#                    break
#                   end
#                 else
#                   ipn_url = portal.ftp_directory
#                   break
#                 end
#             end
#           end
#          end
#        else
          unless domain.ipn.blank?
            ipn_url = domain.ipn
            domain_id = domain.id
          end
#        end
        property.trigger_domain(domain_id) unless ipn_url.blank?
      end
    end
  end
end
