class WorklingDelta < ThinkingSphinx::Deltas::DefaultDelta
  def index(model, instance = nil)
    return true unless ThinkingSphinx.updates_enabled? && ThinkingSphinx.deltas_enabled?
    return true if instance && !toggled(instance)
    update_delta_indexes model
    delete_from_core     model, instance if instance
    doc_id = instance ? instance.sphinx_document_id : nil
    WorklingDeltaWorker.asynch_index(:delta_index_name => delta_index_name(model), :core_index_name => core_index_name(model), :document_id => doc_id)

    return true
  end
end