require 'aws/s3'
require 'digest/sha1'

module AttachmentHelper
   def public_filename(thumbnail = nil)
     if thumbnail.present? && self.type == "Image" && self.attachable_type == "Property"
       attachment = Attachment.find(:first, :conditions => "parent_id = #{self.id} and thumbnail LIKE '#{thumbnail}' ")
       fname = full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), '')
       fname = fname.gsub(thumbnail_name_for(thumbnail),attachment.filename)
       s3_filename = Digest::SHA1.hexdigest(fname)
     else
       s3_filename = Digest::SHA1.hexdigest(full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), ''))
     end
      return "http://img.commercialloopcrm.com.au/" + s3_filename
   end

  def real_filename(thumbnail = nil)
    BASE_URL + full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), '')
  end
end

#module AttachmentHelper
#  def public_filename(thumbnail = nil)
#    BASE_URL + full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), '')
#  end
#end