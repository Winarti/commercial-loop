module RoleLevelSystem  
  
  def self.included(base)
    base.extend ClassMethods
  end
  
  module ClassMethods
    def access(level)
      case level
      when 1
        "Full Access"
      when 2
        "Client Only Access"
      when 3
        "Limited Client Access"
      else
        "Allow Property Owner Access"
      end
    end  
    
    ::CLASSES = [DeveloperUser, AgentUser , AdminUser].freeze
        
  end
  
  def role_gte?(klass)      
     CLASSES.index(self.class) >= CLASSES.index(klass)
  end
  
  # LEVEL 1: Full Access
  # LEVEL 2: Client Only Access
  # Level 3: Limited Client Access
  def access_level
    (has_role?('LEVEL 1') && 1) || (has_role?('LEVEL 2') && 2) || (has_role?('LEVEL 3') && 3) || 4
  end

  def access_level=(level)
    self.roles.delete(*Role.access_levels)
    self.roles << Role.find_by_name("LEVEL #{level}")
  end
    
  def access
    self.class.access access_level
  end
  
  def full_access?
    access_level == 1
  end
  
  def client_only_access?
    access_level == 2
  end
  
  def limited_client_access?
    access_level == 3
  end
  
  def allow_property_owner_access?
    access_level == 4
  end
  
end
