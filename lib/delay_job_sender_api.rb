class DelayJobSenderApi < Struct.new(:property_id, :type)
  def perform
    property = Property.find(property_id)
    if property
      property.updated_at = Time.now
      property.image_exist =true if type == "Image"
      property.save(false)
    end
  end
end
