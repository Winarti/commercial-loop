module DetailHelper

  def self.included(recipient)
    recipient.class_eval do
      #attr_writer :land_area_metric, :floor_area_metric

      #validates_presence_of :land_area_metric, :unless => lambda {|r| r.land_area.blank?} 
      #validates_presence_of :floor_area_metric, :unless => lambda {|r| r.floor_area.blank?}

      #before_save :area_translate

      #def land_area_metric
      #  @land_area_metric#||area_metric
      #end

      #def floor_area_metric
      #  @floor_area_metric#||area_metric
      #end

      protected

      def area_metric
        new_record? ? nil : 'Square Meters'
      end

      def length_metric
        new_record? ? nil : 'Meters'
      end

      def period
        new_record? ? nil : 'Per Quarter'
      end

      def price_per_period
        new_record? ? nil : 'Nightly'
      end

      def price_per_access
        new_record? ? nil : 1
      end

      # we use square meters as standard metric in db
      def area_translate
        self.floor_area = area_standarize floor_area_metric, floor_area
        self.land_area = area_standarize land_area_metric, land_area
      end

      def area_standarize(metric, area)
        return nil if area.nil?
        case metric
        when 'Acres' then from_acres(area)
        when 'Hectares' then from_hectares(area)
        when 'Square Feet' then from_square_feets(area)
        when 'Squares' then from_squares(area)
        when 'Square Yards' then from_square_yards(area)
        else area
        end
      end

      # return area in square meters
      def from_acres(area)
        area*4046.85642
      end

      def from_squares(area)
        area*9.29
      end

      def from_hectares(area)
        area*10000
      end

      def from_square_feets(area)
        area*0.3048*0.3048
      end

      def from_square_yards(area)
        area*0.9144*0.9144
      end
    end
  end

end
