class TblSuburb < ActiveRecord::Base
  establish_connection :adapter => "mysql", :host => 'localhost', :username => 'root', :password => 'nopassword', :database => 'suburbs'
  set_table_name "tblSuburbs"
end

class TblState < ActiveRecord::Base
  establish_connection :adapter => "mysql", :host => 'localhost', :username => 'root', :password => 'nopassword', :database => 'suburbs'
  set_table_name "tblState"
end

class TblCountry < ActiveRecord::Base
  establish_connection :adapter => "mysql", :host => 'localhost', :username => 'root', :password => 'nopassword', :database => 'suburbs'
  set_table_name "tblCountry"
end

class TransmitSuburbsData
  def self.transmit
    countries = Country.all
    countries.each do |c|
      states = TblState.find(:all, :conditions => ['strCountryCode = ?', c.code])
      states.each do |s|
        state = State.create(:name => s.strStateCode, :country => c)
        suburbs = TblSuburb.find(:all, :conditions => ['strState = ?', s.strStateCode])
        suburbs.each do |su|
          Suburb.create(:name => su.strSuburb, :postcode => su.strPostcode, :state => state, :country => c)
        end
      end
    end
  end
end
