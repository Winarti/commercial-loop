class AlterLeaseColumnsForBizsale < ActiveRecord::Migration
  def self.up
    change_column :business_sale_details, :lease_commencement, :date
    change_column :business_sale_details, :lease_end, :date
  end

  def self.down
    change_column :business_sale_details, :lease_commencement, :string
    change_column :business_sale_details, :lease_end, :strin
  end
end
