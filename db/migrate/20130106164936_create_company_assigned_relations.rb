class CreateCompanyAssignedRelations < ActiveRecord::Migration
  def self.up
    create_table :company_assigned_relations do |t|
      t.integer :agent_company_id
      t.integer :assigned_to
      t.timestamps
    end
  end

  def self.down
    drop_table :company_assigned_relations
  end
end
