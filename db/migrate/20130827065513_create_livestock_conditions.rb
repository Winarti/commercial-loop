class CreateLivestockConditions < ActiveRecord::Migration
  def self.up
    create_table :livestock_conditions do |t|
      t.string :name
      t.timestamps
    end

    LivestockCondition.create({:name => "Forward"})
    LivestockCondition.create({:name => "Medium"})
    LivestockCondition.create({:name => "Light"})
  end

  def self.down
    drop_table :livestock_conditions
  end
end
