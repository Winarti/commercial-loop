class CreateChangesLogs < ActiveRecord::Migration
  def self.up
    create_table :changes_logs do |t|
      t.integer :property_id
      t.integer :updated_child_id
      t.string :updated_child
      t.integer :operation, :limit => 2
      t.text :updated_attributes, :limit => 16777215
      t.integer :agent_user_id
      t.integer :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :changes_logs
  end
end
