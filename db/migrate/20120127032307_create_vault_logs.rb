class CreateVaultLogs < ActiveRecord::Migration
  def self.up
    create_table :vault_logs do |t|
      t.integer  :property_id
      t.integer  :agent_contact_id
      t.string   :filename
      t.timestamps
    end
  end

  def self.down
    drop_table :vault_logs
  end
end
