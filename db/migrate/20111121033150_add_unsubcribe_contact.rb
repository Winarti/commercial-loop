class AddUnsubcribeContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :inactive, :boolean, :default => false
  end

  def self.down
    remove_column :agent_contacts, :inactive
  end
end
