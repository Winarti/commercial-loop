class AddColumnFontColorActiveAndFontColorCurrentToOffices < ActiveRecord::Migration
  def self.up
  	add_column :offices, :font_color_active, :string
  	add_column :offices, :font_color_non_active, :string
  end

  def self.down
  	remove_column :offices, :font_color_active
  	remove_column :offices, :font_color_non_active
  end
end
