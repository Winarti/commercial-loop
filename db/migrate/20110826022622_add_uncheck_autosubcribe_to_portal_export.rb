class AddUncheckAutosubcribeToPortalExport < ActiveRecord::Migration
  def self.up
    add_column :property_portal_exports, :uncheck, :boolean, :default => false
  end

  def self.down
    remove_column(:property_portal_exports, :uncheck)
  end
end
