class CreateSubscriptionHistories < ActiveRecord::Migration
  def self.up
    create_table :subscription_histories do |t|
      t.integer :subscription_id      
      t.string  :gateway_reference
      t.string  :txn_type
      t.text    :payer_email
      t.string  :profile_status
      t.string  :payer_id
      t.string  :amount
      t.string  :currency_code      
      t.timestamps
    end
  end

  def self.down
    drop_table :subscription_histories
  end
end
