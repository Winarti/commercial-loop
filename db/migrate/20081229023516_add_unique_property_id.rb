class AddUniquePropertyId < ActiveRecord::Migration
  def self.up
    add_column :properties, :property_id, :string
  end

  def self.down
    remove_column :properties, :property_id
  end
end
