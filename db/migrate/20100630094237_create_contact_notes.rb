class CreateContactNotes < ActiveRecord::Migration
  def self.up
    create_table :contact_notes do |t|
      t.integer :agent_contact_id
      t.integer :agent_user_id
      t.integer :note_type_id
      t.string :address
      t.text :description
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_notes
  end
end
