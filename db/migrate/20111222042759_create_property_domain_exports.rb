class CreatePropertyDomainExports < ActiveRecord::Migration
  def self.up
    create_table :property_domain_exports do |t|
      t.integer  :property_id
      t.integer  :domain_id
      t.boolean  :send_api
      t.timestamps
    end
  end

  def self.down
    drop_table :property_domain_exports
  end
end
