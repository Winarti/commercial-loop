class RemoveAreaIdAndAreaTypeAndAddCountryIdAndStateIdInSuburbs < ActiveRecord::Migration
  def self.up
    remove_column :suburbs, :area_id
    remove_column :suburbs, :area_type
    add_column :suburbs, :country_id, :integer
    add_column :suburbs, :state_id, :integer
  end

  def self.down
    remove_column :suburbs, :state_id
    remove_column :suburbs, :country_id
    add_column :suburbs, :area_type
    add_column :suburbs, :area_id
  end
end
