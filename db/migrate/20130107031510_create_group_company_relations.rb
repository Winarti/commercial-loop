class CreateGroupCompanyRelations < ActiveRecord::Migration
  def self.up
    create_table :group_company_relations do |t|
      t.integer :agent_company_id
      t.integer :group_company_id
      t.timestamps
    end
  end

  def self.down
    drop_table :group_company_relations
  end
end
