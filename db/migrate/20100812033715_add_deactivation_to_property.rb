class AddDeactivationToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :deactivation, :boolean
  end

  def self.down
    remove_column(:properties, :deactivation)
  end
end
