class AddHeardToPropertyNote < ActiveRecord::Migration
  def self.up
    add_column :property_notes, :heard_from_id, :integer
  end

  def self.down
    remove_column(:property_notes, :heard_from_id)
  end
end
