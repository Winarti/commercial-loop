class CreateTestimonials < ActiveRecord::Migration
  def self.up
    create_table :testimonials do |t|
      t.integer :agent_user_id
      t.string :name
      t.text :content
      t.integer :position
      t.timestamps
    end
  end

  def self.down
    drop_table :testimonials
  end
end
