class AddAlertToPropertyNote < ActiveRecord::Migration
  def self.up
    add_column :property_notes, :alert, :boolean
    add_column :property_notes, :contract, :boolean
  end

  def self.down
    remove_column(:property_notes, :alert)
    remove_column(:property_notes, :contract)
  end
end
