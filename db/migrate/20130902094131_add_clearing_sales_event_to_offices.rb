class AddClearingSalesEventToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :clearing_sales_event, :boolean
  end

  def self.down
    remove_column :offices, :clearing_sales_event
  end
end
