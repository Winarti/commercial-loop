class RenameTableCategoryIndustry < ActiveRecord::Migration
  def self.up
    rename_table :category_industries, :industry_categories
  end

  def self.down
    rename_table :industry_categories, :category_industries
  end
end
