class AddColumnPropertyTypeIdOnAgentCompanies < ActiveRecord::Migration
  def self.up
    add_column :agent_companies, :property_type_id, :integer
  end

  def self.down
    remove_column :agent_companies, :property_type_id
  end
end
