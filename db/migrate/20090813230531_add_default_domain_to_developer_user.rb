class AddDefaultDomainToDeveloperUser < ActiveRecord::Migration
  def self.up
    change_column :developers, :domain, :string, :default => 'agentaccount.com'
  end

  def self.down
    change_column :developers, :domain, :string, :default => nil   
  end
end
