class AddEmbedCodeAndStatusToPropertyVideo < ActiveRecord::Migration
  def self.up
    add_column :property_videos, :title, :string
    add_column :property_videos, :embed_code, :text
    add_column :property_videos, :status, :boolean
  end

  def self.down
    remove_column(:property_videos, :title)
    remove_column(:property_videos, :embed_code)
    remove_column(:property_videos, :status)
  end
end
