class CreatePropertyVideos < ActiveRecord::Migration
  def self.up
    create_table :property_videos do |t|
      t.integer :property_id
      t.string :youtube_id
      t.timestamps
    end
  end

  def self.down
    drop_table :property_videos
  end
end
