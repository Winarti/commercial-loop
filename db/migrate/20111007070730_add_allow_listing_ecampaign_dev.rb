class AddAllowListingEcampaignDev < ActiveRecord::Migration
  def self.up
    add_column :developers, :allow_listing_office, :boolean
  end

  def self.down
    remove_column(:developers, :allow_listing_office)
  end
end
