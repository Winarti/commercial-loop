class FixErrorWhenPopulatePropertyTypes < ActiveRecord::Migration
  def self.up
  	change_column :commercial_details, :property_type2, :string, :default => ""
  	change_column :commercial_details, :property_type3, :string, :default => ""

  	nil_property_types2 = CommercialDetail.find(:all, :conditions => ["property_type2 IS NULL"])
  	unless nil_property_types2.blank?
  	  nil_property_types2.each do |ptype2|
  	  	ptype2.update_attributes!({:property_type2 => ""})
  	  end
  	end

  	nil_property_types3 = CommercialDetail.find(:all, :conditions => ["property_type3 IS NULL"])
  	unless nil_property_types3.blank?
  	  nil_property_types3.each do |ptype3|
  	  	ptype3.update_attributes!({:property_type3 => ""})
  	  end
  	end

  end

  def self.down
  end
end