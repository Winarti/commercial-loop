class AddXmlApiAndHttpApiEnable < ActiveRecord::Migration
  def self.up
    add_column :offices, :xml_api_enable, :boolean
    add_column :offices, :http_api_enable, :boolean
  end

  def self.down
    remove_column :offices, :xml_api_enable
    remove_column :offices, :http_api_enable
  end
end
