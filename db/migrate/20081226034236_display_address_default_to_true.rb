class DisplayAddressDefaultToTrue < ActiveRecord::Migration
  def self.up
    change_column_default :properties, :display_address, true
  end

  def self.down
    change_column_default :properties, :display_address, nil
  end
end
