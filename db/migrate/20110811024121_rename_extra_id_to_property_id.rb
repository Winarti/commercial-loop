class RenameExtraIdToPropertyId < ActiveRecord::Migration
  def self.up
    rename_column(:extra_details, :extra_id, :property_id)
  end

  def self.down
    rename_column(:extra_details, :property_id, :extra_id)
  end
end
