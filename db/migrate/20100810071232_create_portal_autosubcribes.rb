class CreatePortalAutosubcribes < ActiveRecord::Migration
  def self.up
    create_table :portal_autosubcribes do |t|
      t.integer :office_id
      t.integer :portal_id
      t.timestamps
    end
  end

  def self.down
    drop_table :portal_autosubcribes
  end
end
