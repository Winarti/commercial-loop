class CreateEmailConfigurationSearches < ActiveRecord::Migration
  def self.up
    create_table :email_configuration_searches do |t|
      t.integer :user_id
      t.integer :agent_id
      t.integer :office_id
      t.string :sender_email
      t.string :sender_name
      t.string :send_bcc
      t.string :contact_name
      t.string :title
      t.text :comments
      t.timestamps
    end
  end

  def self.down
    drop_table :email_configuration_searches
  end
end
