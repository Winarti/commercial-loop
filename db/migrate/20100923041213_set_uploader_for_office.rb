class SetUploaderForOffice < ActiveRecord::Migration
  def self.up
    add_column :offices,:uploader,:boolean
  end

  def self.down
    remove_column(:offices, :uploader)
  end
end
