class AddColumnIsHaveChildrenListingToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :is_children_listing, :boolean
    @parent_listings = Property.find(:all, :conditions => ["parent_category IS NOT NULL AND parent_listing_id IS NOT NULL"])
    @parent_listings.each do |parent|
      parent.update_attributes(:is_children_listing => "1")
    end
    add_column :properties, :is_have_children_listing, :boolean
    @child_listings = Property.find(:all, :conditions => ["is_children_listing = ?", "1"])
    @parent_ids = @child_listings.collect(&:parent_listing_id).uniq
    @parents = Property.find(:all, :conditions => ["id IN (?)", @parent_ids])
    @parents.each do |parent_property|
      parent_property.update_attributes(:is_have_children_listing => "1")
    end
  end

  def self.down
    remove_column :properties, :is_children_listing
    remove_column :properties, :is_have_children_listing
  end
end
