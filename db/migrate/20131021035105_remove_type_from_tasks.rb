class RemoveTypeFromTasks < ActiveRecord::Migration
  def self.up
    remove_column :tasks, :type
  end

  def self.down
    add_column :tasks, :type, :string
  end
end
