class AddColumnLandFrontageToCommercialBuildingDetails < ActiveRecord::Migration
  def self.up
  	add_column :commercial_building_details, :land_frontage, :float
    add_column :commercial_building_details, :land_depth_left, :float
    add_column :commercial_building_details, :land_depth_right, :float
    add_column :commercial_building_details, :land_depth_rear, :float
    add_column :commercial_building_details, :land_crossover, :string
    add_column :commercial_building_details, :land_frontage_metric, :string
    add_column :commercial_building_details, :land_depth_left_metric, :string
    add_column :commercial_building_details, :land_depth_right_metric, :string
    add_column :commercial_building_details, :land_depth_rear_metric, :string
  end

  def self.down
  	remove_column :commercial_building_details, :land_frontage
    remove_column :commercial_building_details, :land_depth_left
    remove_column :commercial_building_details, :land_depth_right
    remove_column :commercial_building_details, :land_depth_rear
    remove_column :commercial_building_details, :land_crossover
    remove_column :commercial_building_details, :land_frontage_metric
    remove_column :commercial_building_details, :land_depth_left_metric
    remove_column :commercial_building_details, :land_depth_right_metric
    remove_column :commercial_building_details, :land_depth_rear_metric
  end
end
