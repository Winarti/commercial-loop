class CreateProperties < ActiveRecord::Migration
  def self.up
    create_table :properties do |t|
      t.string :country
      t.string :state
      t.string :suburb
      t.string :town_village
      t.string :street
      t.string :street_number
      t.string :unit_number
      t.boolean :display_address
      t.string :property_type
      t.string :deal_type
      t.string :type
      t.string :headline
      t.text :description
      t.decimal :price, :precision => 16, :scale => 3
      t.boolean :display_price
      t.string :display_price_text
      t.integer :primary_contact_id
      t.integer :secondary_contact_id
      t.integer :status
      t.integer :vendor_id
      t.datetime :deleted_at
      t.timestamps
    end
  end

  def self.down
    drop_table :properties
  end
end
