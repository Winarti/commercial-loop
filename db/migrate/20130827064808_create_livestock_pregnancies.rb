class CreateLivestockPregnancies < ActiveRecord::Migration
  def self.up
    create_table :livestock_pregnancies do |t|
      t.string :name
      t.timestamps
    end

    LivestockPregnancy.create({:name => "Vet Pregnancy Tested in Calf (PTIC)"})
    LivestockPregnancy.create({:name => "Vet Pregnancy Tested Empty (PTE)"})
    LivestockPregnancy.create({:name => "Vet Speyed"})
    LivestockPregnancy.create({:name => "Vendor Pregnancy Tested in Cald (PTIC)"})
    LivestockPregnancy.create({:name => "Vendor Pregnancy Tested Empty (PTE)"})
    LivestockPregnancy.create({:name => "Vendor Speyed"})
  end

  def self.down
    drop_table :livestock_pregnancies
  end
end
