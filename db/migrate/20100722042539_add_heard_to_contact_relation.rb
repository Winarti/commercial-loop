class AddHeardToContactRelation < ActiveRecord::Migration
  def self.up
    add_column :contact_pastopentime_relations, :heard_from_id, :integer
  end

  def self.down
    remove_column(:contact_pastopentime_relations, :heard_from_id)
  end
end
