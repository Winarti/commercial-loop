class AddMessageIdAndReceivedAtToSmsMessages < ActiveRecord::Migration
  def self.up
    add_column :sms_messages, :message_id, :string
    add_column :sms_messages, :received_at, :datetime
  end

  def self.down
    remove_column :sms_messages, :message_id
    remove_column :sms_messages, :received_at
  end
end
