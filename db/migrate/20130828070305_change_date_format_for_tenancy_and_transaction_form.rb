class ChangeDateFormatForTenancyAndTransactionForm < ActiveRecord::Migration
  def self.up
    change_column :sales_records, :settlement_date, :string
    change_column :tenancy_records, :start_date, :string
    change_column :tenancy_records, :end_date, :string
    change_column :tenancy_records, :lease_signed, :string
    change_column :tenancy_records, :reminder_alert, :string
    change_column :sales_records, :date, :string
  end

  def self.down
    change_column :sales_records, :settlement_date, :date
    change_column :tenancy_records, :start_date, :date
    change_column :tenancy_records, :end_date, :date
    change_column :tenancy_records, :lease_signed, :date
    change_column :tenancy_records, :reminder_alert, :date
    change_column :sales_records, :date, :date
  end
end
