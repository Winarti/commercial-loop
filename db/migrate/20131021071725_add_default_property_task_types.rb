class AddDefaultPropertyTaskTypes < ActiveRecord::Migration
  def self.up
    PropertyTaskType.create({:name => "Take Property Photos", :type => "PropertyTaskType"})
    PropertyTaskType.create({:name => "Creating Web Advertisement", :type => "PropertyTaskType"})
    PropertyTaskType.create({:name => "Create Print Advertisement", :type => "PropertyTaskType"})
    PropertyTaskType.create({:name => "Create Sign Board", :type => "PropertyTaskType"})
  end

  def self.down
    PropertyTaskType.find_by_name("Take Property Photos").destroy
    PropertyTaskType.find_by_name("Creating Web Advertisement").destroy
    PropertyTaskType.find_by_name("Create Print Advertisement").destroy
    PropertyTaskType.find_by_name("Create Sign Board").destroy
  end
end
