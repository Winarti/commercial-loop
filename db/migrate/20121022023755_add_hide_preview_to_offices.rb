class AddHidePreviewToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :hide_preview, :boolean
  end

  def self.down
    remove_column :offices, :hide_preview
  end
end
