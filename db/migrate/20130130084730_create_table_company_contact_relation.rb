class CreateTableCompanyContactRelation < ActiveRecord::Migration
  def self.up
    create_table :company_contact_relations do |t|
      t.integer :agent_contact_id
      t.integer :agent_company_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_contact_relations
  end
end
