class RenameColumnCategoryContactIdOnAgentContacts < ActiveRecord::Migration
  def self.up
    rename_column :agent_companies, :category_contact_id, :category_company_id
  end

  def self.down
    rename_column :agent_companies, :category_company_id, :category_contact_id
  end
end
