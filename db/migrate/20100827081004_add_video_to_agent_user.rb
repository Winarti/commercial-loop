class AddVideoToAgentUser < ActiveRecord::Migration
  def self.up
    add_column :users, :video_url, :string
    add_column :users, :video_code, :string
  end

  def self.down
    remove_column(:users, :video_url)
    remove_column(:users, :video_code)
  end
end
