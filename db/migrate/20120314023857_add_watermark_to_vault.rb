class AddWatermarkToVault < ActiveRecord::Migration
  def self.up
    add_column :vaults, :enable_watermark, :boolean
    add_column :vaults, :watermark_text, :string
  end

  def self.down
    remove_column :vaults, :enable_watermark
    remove_column :vaults, :watermark_text
  end
end
