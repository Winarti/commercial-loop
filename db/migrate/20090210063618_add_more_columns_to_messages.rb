class AddMoreColumnsToMessages < ActiveRecord::Migration
  def self.up
    add_column :messages, :admin_unread, :boolean, :default => true
    add_column :messages, :rating, :integer, :default => 0
    add_column :messages, :pending_archive, :boolean, :default => false
    add_column :messages, :topic_id, :integer
    add_column :messages, :category_id, :integer
  end

  def self.down
    remove_column :messages, :category_id
    remove_column :messages, :topic_id
    remove_column :messages, :pending_archive
    remove_column :messages, :rating
    remove_column :messages, :admin_unread
  end
end
