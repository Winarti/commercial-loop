class CreateExtraOptions < ActiveRecord::Migration
  def self.up
    create_table :extra_options do |t|
      t.integer  :property_id
      t.string   :option_title
      t.string   :option_value
      t.timestamps
    end
  end

  def self.down
    drop_table :extra_options
  end
end
