class AddLoginReqToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :login_required, :boolean, :default => false
  end

  def self.down
    remove_column :agent_contacts, :login_required
  end
end
