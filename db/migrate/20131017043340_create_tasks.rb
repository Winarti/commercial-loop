class CreateTasks < ActiveRecord::Migration
  def self.up
    create_table :tasks do |t|
      t.integer :agent_contact_id
      t.integer :agent_user_id
      t.integer :task_type_id
      t.string :address
      t.text :description
      t.date :task_date
      t.boolean :email_reminder
      t.integer :property_id
      t.boolean :completed, :default => false
      t.timestamps
    end
  end

  def self.down
    drop_table :tasks
  end
end
