class AddColumnNumberOfFloorsToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :number_of_floors, :integer
  end

  def self.down
    remove_column :properties, :number_of_floors
  end
end
