class ChangeCountryDeviationType < ActiveRecord::Migration
  def self.up
    change_table :countries do |t|
      t.change :eer_deviation, :float
    end
  end

  def self.down
  end
end
