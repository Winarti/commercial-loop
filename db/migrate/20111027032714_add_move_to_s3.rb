class AddMoveToS3 < ActiveRecord::Migration
  def self.up
    add_column :attachments, :moved_to_s3, :boolean, :default => false
  end

  def self.down
    remove_column(:attachments, :moved_to_s3)
  end
end
