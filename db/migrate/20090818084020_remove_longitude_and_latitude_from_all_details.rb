class RemoveLongitudeAndLatitudeFromAllDetails < ActiveRecord::Migration
  def self.up
    remove_column :residential_sale_details, :longitude
    remove_column :residential_sale_details, :latitude
    remove_column :residential_lease_details, :longitude
    remove_column :residential_lease_details, :latitude
    remove_column :commercial_details, :longitude
    remove_column :commercial_details, :latitude
    remove_column :holiday_lease_details, :longitude
    remove_column :holiday_lease_details, :latitude
    remove_column :business_sale_details, :longitude
    remove_column :business_sale_details, :latitude
    remove_column :project_sale_details, :longitude
    remove_column :project_sale_details, :latitude
  end

  def self.down
    add_column :project_sale_details, :latitude, :string
    add_column :project_sale_details, :longitude, :string
    add_column :business_sale_details, :latitude, :string
    add_column :business_sale_details, :longitude, :string
    add_column :holiday_lease_details, :latitude, :string
    add_column :holiday_lease_details, :longitude, :string
    add_column :commercial_details, :latitude, :string
    add_column :commercial_details, :longitude, :string
    add_column :residential_lease_details, :latitude, :string
    add_column :residential_lease_details, :longitude, :string
    add_column :residential_sale_details, :latitude, :string
    add_column :residential_sale_details, :longitude, :string
  end
end
