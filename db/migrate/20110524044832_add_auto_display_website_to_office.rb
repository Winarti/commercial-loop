class AddAutoDisplayWebsiteToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :auto_display_website, :boolean
  end

  def self.down
    remove_column :offices, :auto_display_website
  end
end
