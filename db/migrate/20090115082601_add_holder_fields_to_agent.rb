class AddHolderFieldsToAgent < ActiveRecord::Migration
  def self.up
    add_column :agents, :holder_first_name, :string
    add_column :agents, :holder_last_name, :string
    add_column :agents, :holder_email, :string
    add_column :agents, :holder_phone, :string
    add_column :agents, :holder_mobile, :string
    add_column :agents, :holder_fax, :string
    Role.find_by_name('holder').destroy
  end

  def self.down
    remove_column :agents, :holder_first_name
    remove_column :agents, :holder_last_name
    remove_column :agents, :holder_email
    remove_column :agents, :holder_phone
    remove_column :agents, :holder_mobile
    remove_column :agents, :holder_fax
    Role.create(:name => 'holder')
  end
end
