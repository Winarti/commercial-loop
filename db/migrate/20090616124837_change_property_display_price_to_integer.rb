class ChangePropertyDisplayPriceToInteger < ActiveRecord::Migration
  def self.up
    change_column :properties, :display_price, :integer, :size => 1
  end

  def self.down
    change_column :properties, :display_price, :string
  end
end
