class RenameFilename < ActiveRecord::Migration
  def self.up
    rename_column(:vault_logs, :filename, :log_title)
  end

  def self.down
    rename_column(:vault_logs, :log_title, :filename)
  end
end
