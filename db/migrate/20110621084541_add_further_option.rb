class AddFurtherOption < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :lease_further_option, :string
    remove_column(:commercial_details, :lease_option)
    remove_column(:commercial_details, :lease_plus_another)
  end

  def self.down
    remove_column(:commercial_details, :lease_further_option)
    add_column :commercial_details, :lease_option, :string
    add_column :commercial_details, :lease_plus_another, :string
  end
end
