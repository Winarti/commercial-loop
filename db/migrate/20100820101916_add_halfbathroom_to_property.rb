class AddHalfbathroomToProperty < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :half_bathroom, :boolean
    add_column :residential_lease_details, :half_bathroom, :boolean
    add_column :project_sale_details, :half_bathroom, :boolean
    add_column :holiday_lease_details, :half_bathroom, :boolean
  end

  def self.down
    remove_column(:residential_sale_details, :half_bathroom)
    remove_column(:residential_lease_details, :half_bathroom)
    remove_column(:project_sale_details, :half_bathroom)
    remove_column(:holiday_lease_details, :half_bathroom)
  end
end
