class CreatePropertyViewers < ActiveRecord::Migration
  def self.up
    create_table :property_viewers do |t|
      t.integer :property_id
      t.integer :property_viewer_type_id
      t.string  :ip_address
      t.date    :date
      t.timestamps
    end
  end

  def self.down
    drop_table :property_viewers
  end
end
