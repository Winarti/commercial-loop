class CreatePropertySummaries < ActiveRecord::Migration
  def self.up
    create_table :property_summaries do |t|
      t.string :country
      t.integer :developer_a
      t.integer :developer_i
      t.integer :developer_s
      t.integer :client_a
      t.integer :client_i
      t.integer :client_s
      t.integer :office_a
      t.integer :office_i
      t.integer :office_p
      t.integer :office_s
      t.integer :team
      t.integer :listing_a
      t.integer :listing_i

      t.timestamps
    end
    
    add_index :property_summaries, :country
  end

  def self.down
    drop_table :property_summaries
  end
end
