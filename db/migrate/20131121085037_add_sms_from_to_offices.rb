class AddSmsFromToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :sms_from, :string
  end

  def self.down
    remove_column :offices, :sms_from
  end
end
