class AddDisplayNameToPortals < ActiveRecord::Migration
  def self.up
    add_column :portals, :display_name, :string
  end

  def self.down
    remove_column :portals, :display_name
  end
end
