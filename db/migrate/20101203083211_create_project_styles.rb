class CreateProjectStyles < ActiveRecord::Migration
  def self.up
    create_table :project_styles do |t|
      t.string   :name
      t.integer  :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :project_styles
  end
end
