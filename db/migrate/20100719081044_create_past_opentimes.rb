class CreatePastOpentimes < ActiveRecord::Migration
  def self.up
    create_table :past_opentimes do |t|
      t.integer :opentime_id
      t.integer :property_id
      t.date :date
      t.time :start_time
      t.time :end_time
      t.integer :total_attended
      t.timestamps
    end
  end

  def self.down
    drop_table :past_opentimes
  end
end
