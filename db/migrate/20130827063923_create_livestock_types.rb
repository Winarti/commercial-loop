class CreateLivestockTypes < ActiveRecord::Migration
  def self.up
    create_table :livestock_types do |t|
      t.string :name
      t.integer :parent_id
      t.timestamps
    end

    LivestockType.create({:id => 1, :name => "BRITISH"})
    LivestockType.create({:id => 2, :name => "EUROPEAN"})
    LivestockType.create({:id => 3, :name => "BOS INDICUS"})
    LivestockType.create({:id => 4, :name => "ADAPTED TAURINE / SANGA"})
    LivestockType.create({:id => 5, :name => "DAIRY"})
    LivestockType.create({:id => 6, :name => "JAPANESE"})
    LivestockType.create({:id => 7, :name => "COMPOSITE"})
    LivestockType.create({:id => 8, :name => "BOS INDICUS COMPOSITE"})

    LivestockType.create({:name => "Angus", :parent_id => 1})
    LivestockType.create({:name => "Hereford", :parent_id => 1})
    LivestockType.create({:name => "Shorthorn", :parent_id => 1})
    LivestockType.create({:name => "Galloway", :parent_id => 1})
    LivestockType.create({:name => "Murray Grey", :parent_id => 1})
    LivestockType.create({:name => "Devon", :parent_id => 1})

    LivestockType.create({:name => "Simmental", :parent_id => 2})
    LivestockType.create({:name => "Gelbvieh", :parent_id => 2})
    LivestockType.create({:name => "Maine Anjou", :parent_id => 2})
    LivestockType.create({:name => "Brown Swiss Charolais", :parent_id => 2})
    LivestockType.create({:name => "Romagnola", :parent_id => 2})
    LivestockType.create({:name => "Chianina", :parent_id => 2})
    LivestockType.create({:name => "Limousin", :parent_id => 2})
    LivestockType.create({:name => "Blonde Aquitaine", :parent_id => 2})
    LivestockType.create({:name => "Belgian Blue", :parent_id => 2})
    LivestockType.create({:name => "Piedmontese", :parent_id => 2})

    LivestockType.create({:name => "Brahman", :parent_id => 3})
    LivestockType.create({:name => "Sahiwal", :parent_id => 3})

    LivestockType.create({:name => "Senepol", :parent_id => 4})
    LivestockType.create({:name => "Tuli", :parent_id => 4})

    LivestockType.create({:name => "Holstein", :parent_id => 5})
    LivestockType.create({:name => "Jersey", :parent_id => 5})

    LivestockType.create({:name => "Black Wagyu", :parent_id => 6})
    LivestockType.create({:name => "Red Wagyu", :parent_id => 6})
    
    LivestockType.create({:name => "Santa Gertrudis", :parent_id => 7})
    LivestockType.create({:name => "Braford", :parent_id => 7})

    LivestockType.create({:name => "Brangus", :parent_id => 8})
    LivestockType.create({:name => "Droughtmaster", :parent_id => 8})
    LivestockType.create({:name => "Belmont Red", :parent_id => 8})
  end

  def self.down
    drop_table :livestock_types
  end
end
