class AddAppearInDeveloperDirectoryforDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :appear_in_directory, :boolean
  end

  def self.down
    remove_column :developers, :appear_in_directory    
  end
end
