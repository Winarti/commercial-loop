class AddColumnToCommercialBuildingDetail < ActiveRecord::Migration
  def self.up
    add_column :commercial_building_details, :land_area, :integer
    add_column :commercial_building_details, :land_area_metric, :string
    add_column :commercial_building_details, :floor_area, :string
  end

  def self.down
    remove_column :commercial_building_details, :land_area
    remove_column :commercial_building_details, :land_area_metric
    remove_column :commercial_building_details, :floor_area
  end
end
