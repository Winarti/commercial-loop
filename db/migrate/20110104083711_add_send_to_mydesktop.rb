class AddSendToMydesktop < ActiveRecord::Migration
  def self.up
    add_column :offices, :send_to_mydesktop, :boolean
    add_column :offices, :mydesktop_group_id, :string
  end

  def self.down
    remove_column :offices, :send_to_mydesktop
    remove_column :offices, :mydesktop_group_id
  end
end
