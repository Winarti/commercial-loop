class CreateTenancyRecords < ActiveRecord::Migration
  def self.up
    create_table :tenancy_records do |t|
      t.integer :property_id
      t.string :company
      t.string :contact
      t.string :phone
      t.string :mobile
      t.string :email
      t.string :rent
      t.string :net_gross
      t.string :outgoing
      t.string :total_rental_psm
      t.string :total_rental_pa
      t.string :lease_term
      t.string :option
      t.date :start_date
      t.date :end_date
      t.string :fee
      t.date :lease_signed
      t.text :office_note
      t.string :transaction_sales_person
      t.string :external_agent_company
      t.string :external_agent_sales_person
      t.string :conjunction_fee
      t.string :scale_of_fee
      t.date :reminder_alert
      t.string :lease_expiry_sales_person
      t.timestamps
    end
  end

  def self.down
    drop_table :tenancy_records
  end
end
