class RemoveColumnPrice2NOutgoingForCommercialProperty < ActiveRecord::Migration
  def self.up
    remove_column(:commercial_details, :current_outgoings)
    remove_column(:commercial_details, :current_outgoings_include_tax)
    remove_column(:properties, :price2)
    remove_column(:properties, :price2_include_tax)
  end

  def self.down
    add_column :commercial_details,:current_outgoings, :precision => 16, :scale => 3
    add_column :commercial_details, :current_outgoings_include_tax, :boolean
    add_column :properties, :price2, :decimal, :precision => 16, :scale => 3
    add_column :properties, :price2_include_tax, :boolean
  end
end
