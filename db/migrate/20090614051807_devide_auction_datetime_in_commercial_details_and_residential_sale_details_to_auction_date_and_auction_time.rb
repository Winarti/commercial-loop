class DevideAuctionDatetimeInCommercialDetailsAndResidentialSaleDetailsToAuctionDateAndAuctionTime < ActiveRecord::Migration
  def self.up
    remove_column :residential_sale_details, :auction_datetime
    remove_column :commercial_details, :auction_datetime
    add_column :residential_sale_details, :auction_date, :date
    add_column :residential_sale_details, :auction_time, :time
    add_column :commercial_details, :auction_date, :date
    add_column :commercial_details, :auction_time, :time
  end

  def self.down
    remove_column :commercial_details, :auction_time
    remove_column :commercial_details, :auction_date
    remove_column :residential_sale_details, :auction_time
    remove_column :residential_sale_details, :auction_date
    add_column :commercial_details, :auction_datetime, :datetime
    add_column :residential_sale_details, :auction_datetime, :datetime
  end
end
