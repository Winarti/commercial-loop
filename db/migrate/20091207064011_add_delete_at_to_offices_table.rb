class AddDeleteAtToOfficesTable < ActiveRecord::Migration
  def self.up
    add_column :offices, :deleted_at, :date
  end

  def self.down
    remove_column :offices, :deleted_at
  end
end
