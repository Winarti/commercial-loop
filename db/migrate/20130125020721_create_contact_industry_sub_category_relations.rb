class CreateContactIndustrySubCategoryRelations < ActiveRecord::Migration
  def self.up
    create_table :contact_industry_sub_category_relations do |t|
      t.integer :agent_contact_id
      t.integer :industry_sub_category_id
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_industry_sub_category_relations
  end
end
