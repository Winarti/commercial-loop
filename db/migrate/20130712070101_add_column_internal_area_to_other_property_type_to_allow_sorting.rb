class AddColumnInternalAreaToOtherPropertyTypeToAllowSorting < ActiveRecord::Migration
  def self.up
    add_column :business_sale_details, :internal_area, :string
    add_column :commercial_details, :internal_area, :string
    add_column :commercial_building_details, :internal_area, :string
    add_column :holiday_lease_details, :internal_area, :string
    add_column :new_development_details, :internal_area, :string
    add_column :project_sale_details, :internal_area, :string
    add_column :residential_lease_details, :internal_area, :string
    add_column :land_release_details, :internal_area, :string
  end

  def self.down
    remove_column :business_sale_details, :internal_area
    remove_column :commercial_details, :internal_area
    remove_column :commercial_building_details, :internal_area
    remove_column :holiday_lease_details, :internal_area
    remove_column :new_development_details, :internal_area
    remove_column :project_sale_details, :internal_area
    remove_column :residential_lease_details, :internal_area
    remove_column :land_release_details, :internal_area
  end
end
