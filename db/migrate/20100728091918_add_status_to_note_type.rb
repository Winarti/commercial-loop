class AddStatusToNoteType < ActiveRecord::Migration
  def self.up
    add_column :note_types, :status, :string
    NoteType.destroy_all
    NoteType.create(:name => "Alert - Sent", :status => "system")

    NoteType.create(:name => "Sales Appraisal", :status => "contact_note")
    NoteType.create(:name => "Rental Appraisal", :status => "contact_note")
    NoteType.create(:name => "Alert - Created", :status => "contact_note")
    NoteType.create(:name => "Open Inspection Atendee", :status => "contact_note")
    NoteType.create(:name => "Property Enquiry", :status => "contact_note")
    NoteType.create(:name => "Submit Offer", :status => "contact_note")
    NoteType.create(:name => "Purchase Property", :status => "contact_note")
    NoteType.create(:name => "Lease Property", :status => "contact_note")
    NoteType.create(:name => "Attend Auction", :status => "contact_note")
    NoteType.create(:name => "Testimonial", :status => "contact_note")
    NoteType.create(:name => "Contract Provided", :status => "contact_note")
    NoteType.create(:name => "General", :status => "contact_note")

    NoteType.create(:name => "Property Enquiry", :status => "property_note")
    NoteType.create(:name => "Attend Auction", :status => "property_note")
    NoteType.create(:name => "Contract Provided", :status => "property_note")
    NoteType.create(:name => "Purchase Property", :status => "property_note")
    NoteType.create(:name => "General", :status => "property_note")
  end

  def self.down
    remove_column(:note_types, :status)
  end
end
