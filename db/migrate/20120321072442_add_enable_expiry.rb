class AddEnableExpiry < ActiveRecord::Migration
  def self.up
    add_column :vaults, :enable_expiry, :boolean
    add_column :vaults, :expiry_hour, :integer
  end

  def self.down
    remove_column :vaults, :enable_expiry
    remove_column :vaults, :expiry_hour
  end
end
