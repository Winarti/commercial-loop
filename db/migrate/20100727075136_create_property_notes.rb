class CreatePropertyNotes < ActiveRecord::Migration
  def self.up
    create_table :property_notes do |t|
      t.integer :property_id
      t.integer :agent_contact_id
      t.integer :agent_user_id
      t.integer :note_type_id
      t.string :address
      t.text :description
      t.date :note_date
      t.timestamps
    end
  end

  def self.down
    drop_table :property_notes
  end
end
