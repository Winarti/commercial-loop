class AddUrlRedirectColumnToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :url_redirect, :string
  end

  def self.down
    remove_column :users, :url_redirect
  end
end
