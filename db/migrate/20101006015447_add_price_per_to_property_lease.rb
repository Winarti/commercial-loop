class AddPricePerToPropertyLease < ActiveRecord::Migration
  def self.up
    add_column :properties, :price_per, :string
  end

  def self.down
    remove_column :properties, :price_per
  end
end
