class CreateVaults < ActiveRecord::Migration
  def self.up
    create_table :vaults do |t|
      t.integer  :office_id
      t.integer  :property_id
      t.string   :contact_search
      t.string   :contact_access
      t.integer   :access_time
      t.boolean  :required_password
      t.string   :password
      t.timestamps
    end
  end

  def self.down
    drop_table :vaults
  end
end
