class AddHideBrochureToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :hide_brochure, :boolean
  end

  def self.down
    remove_column :offices, :hide_brochure
  end
end
