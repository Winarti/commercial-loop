class AddColumnForAdvancedSearchFieldToContactAlerts < ActiveRecord::Migration
  def self.up
    add_column :contact_alerts, :keyword, :text
    add_column :contact_alerts, :commercial_type, :text
    add_column :contact_alerts, :availiability_type, :string
    add_column :contact_alerts, :listing_select_status_commercial, :string
    add_column :contact_alerts, :listing_select_status_residential, :string
    add_column :contact_alerts, :commercial_property_type, :string
    add_column :contact_alerts, :residential_type, :text
    add_column :contact_alerts, :residential_building, :string
    add_column :contact_alerts, :houseland_property_type, :string
    add_column :contact_alerts, :price_range_from, :string
    add_column :contact_alerts, :price_range_to, :string
    add_column :contact_alerts, :houseland_status, :string
    add_column :contact_alerts, :suites, :string
    add_column :contact_alerts, :sqm_range_from, :string
    add_column :contact_alerts, :sqm_range_to, :string
    add_column :contact_alerts, :state, :string
    add_column :contact_alerts, :region, :string
    add_column :contact_alerts, :internal_agent, :string
    add_column :contact_alerts, :external_company, :string
    add_column :contact_alerts, :external_agent, :string
    add_column :contact_alerts, :yield_from, :string
    add_column :contact_alerts, :yield_to, :string
    add_column :contact_alerts, :building_area_from, :string
    add_column :contact_alerts, :building_area_to, :string
    add_column :contact_alerts, :land_area_from, :string
    add_column :contact_alerts, :land_area_to, :string
    add_column :contact_alerts, :asking_price_from, :string
    add_column :contact_alerts, :asking_price_to, :string
    add_column :contact_alerts, :annual_rental_from, :string
    add_column :contact_alerts, :annual_rental_to, :string
    add_column :contact_alerts, :total_floor_area_from, :string
    add_column :contact_alerts, :total_floor_area_to, :string
    add_column :contact_alerts, :office_from, :string
    add_column :contact_alerts, :office_to, :string
    add_column :contact_alerts, :warehouse_from, :string
    add_column :contact_alerts, :warehouse_to, :string
    add_column :contact_alerts, :rent_from, :string
    add_column :contact_alerts, :rent_to, :string
    add_column :contact_alerts, :lease_expiry_from, :date
    add_column :contact_alerts, :lease_expiry_to, :date
    add_column :contact_alerts, :tenant, :string
  end

  def self.down
    remove_column :contact_alerts, :keyword
    remove_column :contact_alerts, :commercial_type
    remove_column :contact_alerts, :availiability_type
    remove_column :contact_alerts, :listing_select_status_commercial
    remove_column :contact_alerts, :listing_select_status_residential
    remove_column :contact_alerts, :commercial_property_type
    remove_column :contact_alerts, :residential_type
    remove_column :contact_alerts, :residential_building
    remove_column :contact_alerts, :houseland_property_type
    remove_column :contact_alerts, :price_range_from
    remove_column :contact_alerts, :price_range_to
    remove_column :contact_alerts, :houseland_status
    remove_column :contact_alerts, :suites
    remove_column :contact_alerts, :sqm_range_from
    remove_column :contact_alerts, :sqm_range_to
    remove_column :contact_alerts, :state
    remove_column :contact_alerts, :region
    remove_column :contact_alerts, :internal_agent
    remove_column :contact_alerts, :external_company
    remove_column :contact_alerts, :external_agent
    remove_column :contact_alerts, :yield_from
    remove_column :contact_alerts, :yield_to
    remove_column :contact_alerts, :building_area_from
    remove_column :contact_alerts, :building_area_to
    remove_column :contact_alerts, :land_area_from
    remove_column :contact_alerts, :land_area_to
    remove_column :contact_alerts, :asking_price_from
    remove_column :contact_alerts, :asking_price_to
    remove_column :contact_alerts, :annual_rental_from
    remove_column :contact_alerts, :annual_rental_to
    remove_column :contact_alerts, :total_floor_area_from
    remove_column :contact_alerts, :total_floor_area_to
    remove_column :contact_alerts, :office_from
    remove_column :contact_alerts, :office_to
    remove_column :contact_alerts, :warehouse_from
    remove_column :contact_alerts, :warehouse_to
    remove_column :contact_alerts, :rent_from
    remove_column :contact_alerts, :rent_to
    remove_column :contact_alerts, :lease_expiry_from
    remove_column :contact_alerts, :lease_expiry_to
    remove_column :contact_alerts, :tenant
  end
end
