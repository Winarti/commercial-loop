class CreateCompanyPasteopentimeRelations < ActiveRecord::Migration
  def self.up
    create_table :company_pasteopentime_relations do |t|
      t.integer :past_opentime_id
      t.integer :agent_company_id
      t.integer :company_note_id
      t.boolean :alert
      t.boolean :contract
      t.integer :company_alert_id
      t.integer :company_alertnote_id
      t.integer :heard_from_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_pasteopentime_relations
  end
end
