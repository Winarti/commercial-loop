class RenameTableCompanyIndustryCategoryRelation < ActiveRecord::Migration
  def self.up
    rename_table :company_industry_category_relations, :company_industry_sub_category_relations
  end

  def self.down
    rename_table :company_industry_sub_category_relations, :company_industry_category_relations
  end
end
