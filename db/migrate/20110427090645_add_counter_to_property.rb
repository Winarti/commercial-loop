class AddCounterToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :counter, :integer
  end

  def self.down
    remove_column :properties, :counter
  end
end
