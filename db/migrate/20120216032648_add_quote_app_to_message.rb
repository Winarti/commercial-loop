class AddQuoteAppToMessage < ActiveRecord::Migration
  def self.up
    add_column :messages, :quote_approved, :boolean, :default => false
  end

  def self.down
    remove_column :messages, :quote_approved
  end
end
