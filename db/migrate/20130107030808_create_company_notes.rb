class CreateCompanyNotes < ActiveRecord::Migration
  def self.up
    create_table :company_notes do |t|
      t.integer :agent_company_id
      t.integer :agent_user_id
      t.integer :note_type_id
      t.string :address
      t.text :description
      t.date :note_date
      t.boolean :email_reminder
      t.integer :property_id
      t.timestamps
    end
  end

  def self.down
    drop_table :company_notes
  end
end
