class AddRatingToMessageComment < ActiveRecord::Migration
  def self.up
    add_column :message_comments, :rating, :integer
  end

  def self.down
    remove_column :message_comments, :rating
  end
end
