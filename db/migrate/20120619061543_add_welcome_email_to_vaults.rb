class AddWelcomeEmailToVaults < ActiveRecord::Migration
  def self.up
    add_column :vaults, :welcome_email, :boolean
  end

  def self.down
    remove_column :vaults, :welcome_email
  end
end
