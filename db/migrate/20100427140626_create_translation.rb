class CreateTranslation < ActiveRecord::Migration
  def self.up
    create_table "languages" do |t|
      t.column :name, :string, :null => false, :limit => 15, :unique => true
    end
    create_table "translations" do |t|
      t.column :language_id, :integer, :null => false
      t.column :key, :string, :null => false, :limit => 100 #max length for a column used in an index in mysql
      t.column :value, :text
      t.column :format, :string, :null => false, :default => "inline"
    end
    add_index :translations, [:language_id, :key], :name => :one_translation_per_language, :unique => true
  end

  def self.down
    drop_table "languages"
    drop_table "translations"
  end

end
