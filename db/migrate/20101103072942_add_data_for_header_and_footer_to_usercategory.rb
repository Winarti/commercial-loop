class AddDataForHeaderAndFooterToUsercategory < ActiveRecord::Migration
  def self.up
    UserFileCategory.create(:name => "Brochure Header")
    UserFileCategory.create(:name => "Brochure Footer")
  end

  def self.down
  end
end
