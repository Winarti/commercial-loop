class AddOutgoingPaidBy < ActiveRecord::Migration
  def self.up
    add_column :commercial_details, :outgoings_paid_by, :integer
  end

  def self.down
    remove_column(:commercial_details, :outgoings_paid_by)
  end
end
