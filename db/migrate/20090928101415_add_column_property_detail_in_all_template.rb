class AddColumnPropertyDetailInAllTemplate < ActiveRecord::Migration
  def self.up
    add_column(:residential_sale_details, :property_url, :string)
    add_column(:residential_lease_details, :property_url, :string)
    add_column(:holiday_lease_details, :property_url, :string)
    add_column(:commercial_details, :property_url, :string)
    add_column(:project_sale_details, :property_url, :string)
    add_column(:business_sale_details, :property_url, :string)
  end

  def self.down
    remove_column(:residential_sale_details, :property_url)
    remove_column(:residential_lease_details, :property_url)
    remove_column(:holiday_lease_details, :property_url)
    remove_column(:commercial_details, :property_url)
    remove_column(:project_sale_details, :property_url)
    remove_column(:business_sale_details, :property_url)
  end
end
