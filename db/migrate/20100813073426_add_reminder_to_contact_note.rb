class AddReminderToContactNote < ActiveRecord::Migration
  def self.up
    add_column :contact_notes, :email_reminder, :boolean
  end

  def self.down
    remove_column(:contact_notes, :email_reminder)
  end
end
