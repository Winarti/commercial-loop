class AddMydesktopUpdatedAtToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :mydesktop_agent_update_at, :date
  end

  def self.down
    remove_column :users, :mydesktop_agent_update_at
  end
end
