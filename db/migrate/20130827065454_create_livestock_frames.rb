class CreateLivestockFrames < ActiveRecord::Migration
  def self.up
    create_table :livestock_frames do |t|
      t.string :name
      t.timestamps
    end

    LivestockFrame.create({:name => "Small"})
    LivestockFrame.create({:name => "Medium"})
    LivestockFrame.create({:name => "Large"})
  end

  def self.down
    drop_table :livestock_frames
  end
end
