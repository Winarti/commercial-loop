class CreateEalerts < ActiveRecord::Migration
  def self.up
    create_table :ealerts do |t|
      t.integer  :office_id
      t.string   :title
      t.datetime :sent_at
      t.timestamps
    end
  end

  def self.down
    drop_table :ealerts
  end
end
