class CreatePortals < ActiveRecord::Migration
  def self.up
    create_table :portals do |t|
      t.string :portal_name
      t.string :portal_url
      t.string :country
      t.string :suburb
      t.string :state
      t.string :postcode
      t.string :unit_number
      t.string :street_number
      t.string :street_name
      t.string :portal_type
      t.string :portal_country_id
      t.string :properties_updated
      t.string :timeframe
      t.string :sign_up_url
      t.string :terms_url
      t.string :warnings_url
      t.string :client_support_phone
      t.string :client_support_email
      t.string :tech_support_phone
      t.string :tech_support_email
      t.string :ftp_url
      t.string :ftp_directory
      t.string :username
      t.string :password
      t.string :description
      t.text :notes
      t.integer :require_account_id
      t.string :default
      t.timestamps
    end
  end

  def self.down
    drop_table :portals
  end
end
