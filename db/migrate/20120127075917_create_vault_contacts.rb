class CreateVaultContacts < ActiveRecord::Migration
  def self.up
    create_table :vault_contacts do |t|
      t.integer  :vault_id
      t.integer  :property_id
      t.integer  :agent_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :vault_contacts
  end
end
