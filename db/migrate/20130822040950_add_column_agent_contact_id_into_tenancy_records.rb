class AddColumnAgentContactIdIntoTenancyRecords < ActiveRecord::Migration
  def self.up
    add_column :tenancy_records, :agent_contact_id, :integer
    add_column :tenancy_records, :contact_note_id, :integer
  end

  def self.down
    remove_column :tenancy_records, :agent_contact_id
    remove_column :tenancy_records, :contact_note_id
  end
end
