class RecreateFeaturePropertyJoinTable < ActiveRecord::Migration
  def self.up
    drop_table :features_properties
    create_table :features_properties, :id => false do |t|
      t.integer :feature_id, :null => false
      t.integer :property_id, :null => false
    end
  end

  def self.down
    drop_table :features_properties
    create_table :features_properties do |t|
      t.integer :feature_id
      t.integer :property_id
    end
  end
end
