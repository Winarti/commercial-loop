class CreatePortalCountries < ActiveRecord::Migration
  def self.up
    create_table :portal_countries do |t|
      t.integer :portal_id
      t.integer :country_id
      t.timestamps
    end
  end

  def self.down
    drop_table :portal_countries
  end
end
