class CreatePortalCombineds < ActiveRecord::Migration
  def self.up
    create_table :portal_combineds do |t|
      t.integer  :developer_id
      t.integer  :portal_id
      t.boolean  :active
      t.timestamps
    end
  end

  def self.down
    drop_table :portal_combineds
  end
end
