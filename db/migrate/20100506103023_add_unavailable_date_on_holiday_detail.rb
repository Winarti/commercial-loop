class AddUnavailableDateOnHolidayDetail < ActiveRecord::Migration
  def self.up
    add_column :holiday_lease_details, :unavailable_date,:string, :limit => 1000
  end

  def self.down
    remove_column(:holiday_lease_details, :unavailable_date)
  end
end
