class AddTimezoneToDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :time_zone, :string
  end

  def self.down
    remove_column :developers, :time_zone
  end
end
