class ChangeExtra < ActiveRecord::Migration
  def self.up
    add_column :extra_options, :option_order, :integer
    remove_column :extra_options, :option_value
  end

  def self.down
    remove_column :extra_options, :option_order
    add_column :extra_options, :option_value, :string
  end
end
