class AddHalfBedroomToProperty < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :half_bedroom, :boolean
    add_column :residential_lease_details, :half_bedroom, :boolean
    add_column :project_sale_details, :half_bedroom, :boolean
    add_column :holiday_lease_details, :half_bedroom, :boolean
  end

  def self.down
    remove_column(:residential_sale_details, :half_bedroom)
    remove_column(:residential_lease_details, :half_bedroom)
    remove_column(:project_sale_details, :half_bedroom)
    remove_column(:holiday_lease_details, :half_bedroom)
  end
end
