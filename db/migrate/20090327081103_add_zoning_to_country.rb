class AddZoningToCountry < ActiveRecord::Migration
  def self.up
    add_column :countries, :zoning, :text
  end

  def self.down
    remove_column :countries, :zoning
  end
end
