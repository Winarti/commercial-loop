class AddZipcodeToProperty < ActiveRecord::Migration
  def self.up
    add_column :properties, :zipcode, :string, :limit => 30
  end

  def self.down
  end
end
