class AddAreaForProjectSale < ActiveRecord::Migration
  def self.up
    add_column :project_sale_details, :project_development_id, :integer
    add_column :project_sale_details, :project_design_type_id, :integer
    add_column :project_sale_details, :project_style_id, :integer
  end

  def self.down
    remove_column :project_sale_details, :project_development_id
    remove_column :project_sale_details, :project_design_type_id
    remove_column :project_sale_details, :project_style_id
  end
end
