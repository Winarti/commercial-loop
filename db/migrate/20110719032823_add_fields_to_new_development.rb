class AddFieldsToNewDevelopment < ActiveRecord::Migration
  def self.up
    add_column :new_development_details, :architect_name, :string
    add_column :new_development_details, :start_date, :string
    add_column :new_development_details, :completion_date, :string
    add_column :new_development_details, :distance, :string
    add_column :new_development_details, :bedroom_from, :integer
    add_column :new_development_details, :bedroom_to, :integer
    add_column :new_development_details, :bathroom_from, :integer
    add_column :new_development_details, :bathroom_to, :integer
    add_column :new_development_details, :carspace_from, :integer
    add_column :new_development_details, :carspace_to, :integer
    add_column :new_development_details, :size_from, :integer
    add_column :new_development_details, :size_to, :integer
    add_column :new_development_details, :listing_privacy, :boolean

  end

  def self.down
    remove_column :new_development_details, :architect_name
    remove_column :new_development_details, :start_date
    remove_column :new_development_details, :completion_date
    remove_column :new_development_details, :distance
    remove_column :new_development_details, :bedroom_from
    remove_column :new_development_details, :bedroom_to
    remove_column :new_development_details, :bathroom_from
    remove_column :new_development_details, :bathroom_to
    remove_column :new_development_details, :carspace_from
    remove_column :new_development_details, :carspace_to
    remove_column :new_development_details, :size_from
    remove_column :new_development_details, :size_to
    remove_column :new_development_details, :listing_privacy
  end
end
