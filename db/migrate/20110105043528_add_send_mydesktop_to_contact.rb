class AddSendMydesktopToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :sent_mydesktop, :boolean
  end

  def self.down
    remove_column :agent_contacts, :sent_mydesktop
  end
end
