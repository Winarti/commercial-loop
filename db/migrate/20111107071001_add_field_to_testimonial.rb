class AddFieldToTestimonial < ActiveRecord::Migration
  def self.up
    add_column :testimonials, :mydesktop_testimonial_id, :integer
  end

  def self.down
    remove_column :testimonials, :mydesktop_testimonial_id
  end
end
