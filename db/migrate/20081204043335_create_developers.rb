class CreateDevelopers < ActiveRecord::Migration
  def self.up
    create_table :developers do |t|
      t.string :code
      t.string :entry
      t.string :name
      t.string :company
      t.string :unit_number
      t.string :street_number
      t.string :street
      t.string :street_type
      t.string :suburb
      t.string :state
      t.string :country
      t.string :phone_number
      t.string :fax_number
      t.string :email
      t.string :domain
      t.datetime :deleted_at

      t.timestamps
    end
  end

  def self.down
    drop_table :developers
  end
end
