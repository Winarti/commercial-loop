class AddOfficeForGroup < ActiveRecord::Migration
  def self.up
    add_column :group_contacts, :office_id, :integer
  end

  def self.down
    remove_column(:group_contacts, :office_id)
  end
end
