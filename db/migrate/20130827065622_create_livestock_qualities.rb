class CreateLivestockQualities < ActiveRecord::Migration
  def self.up
    create_table :livestock_qualities do |t|
      t.string :name
      t.timestamps
    end

    LivestockQuality.create({:name => "Very Good"})
    LivestockQuality.create({:name => "Good"})
    LivestockQuality.create({:name => "Average"})
    LivestockQuality.create({:name => "Low"})
  end

  def self.down
    drop_table :livestock_qualities
  end
end
