class AddFieldToExtras < ActiveRecord::Migration
  def self.up
    add_column :extras, :field1, :string
    add_column :extras, :field2, :string
    add_column :extras, :field3, :string
    add_column :extras, :field4, :string
    add_column :extras, :field5, :string
    add_column :extras, :field6, :string
    add_column :extras, :field7, :string
    add_column :extras, :field8, :string
    add_column :extras, :field9, :string
  end

  def self.down
    remove_column :extras, :field1
    remove_column :extras, :field2
    remove_column :extras, :field3
    remove_column :extras, :field4
    remove_column :extras, :field5
    remove_column :extras, :field6
    remove_column :extras, :field7
    remove_column :extras, :field8
    remove_column :extras, :field9
  end
end
