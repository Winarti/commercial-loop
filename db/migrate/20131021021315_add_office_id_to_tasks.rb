class AddOfficeIdToTasks < ActiveRecord::Migration
  def self.up
    add_column :tasks, :office_id, :integer
  end

  def self.down
    add_column :tasks, :office_id
  end
end
