class AddColumnContactPersonToDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :contact_person, :integer
  end

  def self.down
    remove_column :developers, :contact_person
  end
end
