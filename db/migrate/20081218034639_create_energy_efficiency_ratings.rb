class CreateEnergyEfficiencyRatings < ActiveRecord::Migration
  def self.up
    create_table :energy_efficiency_ratings do |t|
      t.string :name
      t.integer :country_id
      t.integer :order
    end
  end

  def self.down
    drop_table :energy_efficiency_ratings
  end
end
