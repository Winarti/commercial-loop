class AddUpdateAlertToVaults < ActiveRecord::Migration
  def self.up
    add_column :vaults, :update_alert, :boolean
  end

  def self.down
    remove_column :vaults, :update_alert
  end
end
