class CreateGroupCompanies < ActiveRecord::Migration
  def self.up
    create_table :group_companies do |t|
      t.string :name
      t.integer :creator_id
      t.integer :office_id
      t.timestamps
    end
  end

  def self.down
    drop_table :group_companies
  end
end
