class AddHideExportsToOffices < ActiveRecord::Migration
  def self.up
    add_column :offices, :hide_exports, :boolean
  end

  def self.down
    remove_column :offices, :hide_exports
  end
end
