class AddHttpEnableApiToPortal < ActiveRecord::Migration
  def self.up
    add_column :portals, :http_api_enable, :boolean
  end

  def self.down
    remove_column :portals, :http_api_enable
  end
end
