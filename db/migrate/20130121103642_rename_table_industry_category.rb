class RenameTableIndustryCategory < ActiveRecord::Migration
  def self.up
    rename_table :industry_categories, :industry_sub_categories
  end

  def self.down
    rename_table :industry_sub_categories, :industry_categories
  end
end
