class CreateLivestockSaleTypes < ActiveRecord::Migration
  def self.up
    create_table :livestock_sale_types do |t|
      t.string :name
      t.timestamps
    end

    LivestockSaleType.create({:name => "For Sale"})
    LivestockSaleType.create({:name => "Auction"})
  end

  def self.down
    drop_table :livestock_sale_types
  end
end
