class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.integer :sender_id
      t.integer :recipient_id
      t.string :title
      t.text :message
      t.integer :kind, :default => 0
      t.integer :message_comments_count, :default => 0
      
      t.boolean :sender_unread, :default => false
      t.boolean :recipient_unread, :default => true

      t.timestamps
    end
    
    add_index :messages, :kind
    add_index :messages, :sender_id
    add_index :messages, :recipient_id
  end

  def self.down
    drop_table :messages
  end
end
