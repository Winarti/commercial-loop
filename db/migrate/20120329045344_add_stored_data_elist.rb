class AddStoredDataElist < ActiveRecord::Migration
  def self.up
    add_column :elists, :stored_layout, :longblob
    add_column :elists, :stored_data, :longblob
  end

  def self.down
    remove_column :elists, :stored_layout
    remove_column :elists, :stored_data
  end
end
