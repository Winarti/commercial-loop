class AddColumnPropertyTypeIdToAgentContacts < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :property_type_id, :integer
  end

  def self.down
    remove_column :agent_contacts, :property_type_id
  end
end
