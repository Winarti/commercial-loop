class AddEstTimeToMessage < ActiveRecord::Migration
  def self.up
    add_column :messages, :estimation_day, :integer, :default => 0
    add_column :messages, :estimation_hour, :integer, :default => 0
    add_column :messages, :estimation_min, :integer, :default => 0
  end

  def self.down
    remove_column :messages, :estimation_day
    remove_column :messages, :estimation_hour
    remove_column :messages, :estimation_min
  end
end
