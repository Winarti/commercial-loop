class AddDeveloperContactsToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :developer_contact_id, :integer
    add_column :offices, :support_contact_id, :integer
    add_column :offices, :upgrade_contact_id, :integer
  end

  def self.down
    remove_column :offices, :developer_contact_id
    remove_column :offices, :support_contact_id
    remove_column :offices, :upgrade_contact_id
  end
end
