class UpdatePropertyViewerForContract < ActiveRecord::Migration
  def self.up
    PropertyViewerType.update_all( "name = 'Contract Provided'", "id = 8" )
  end

  def self.down
    PropertyViewerType.update_all( "name = 'Contracts Taken'", "id = 8" )
  end
end
