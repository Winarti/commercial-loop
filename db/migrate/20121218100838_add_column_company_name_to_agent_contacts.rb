class AddColumnCompanyNameToAgentContacts < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :website, :string
    add_column :agent_contacts, :town_village, :string
  end

  def self.down
    remove_column :agent_contacts, :website
    remove_column :agent_contacts, :town_village
  end
end
