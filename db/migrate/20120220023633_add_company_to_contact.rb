class AddCompanyToContact < ActiveRecord::Migration
  def self.up
    add_column :agent_contacts, :company, :string
  end

  def self.down
    remove_column :agent_contacts, :company
  end
end
