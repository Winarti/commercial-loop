class AddColumnTypeToAgentCompanies < ActiveRecord::Migration
  def self.up
    add_column :agent_companies, :company_type, :string
  end

  def self.down
    remove_column :agent_companies, :company_type
  end
end
