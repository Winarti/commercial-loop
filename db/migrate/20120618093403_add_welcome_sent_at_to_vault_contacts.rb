class AddWelcomeSentAtToVaultContacts < ActiveRecord::Migration
  def self.up
    add_column :vault_contacts, :welcome_sent_at, :datetime
  end

  def self.down
    remove_column :vault_contacts, :welcome_sent_at
  end
end
