class MessageCategoryData < ActiveRecord::Migration
  def self.up
    cate = MessageCategory.find_by_name("Systems")
    cate.update_attribute(:name, "System") if cate
    topic = MessageTopic.find_by_name("Systems")
    topic.update_attribute(:name, "System") if topic
    
    cate = MessageCategory.first
    Message.update_all("category_id = #{cate.id}") if cate
  end

  def self.down
  end
end
