class ChangePortalId < ActiveRecord::Migration
  def self.up
    rename_column(:export_reports, :portal_id, :portal_property_id)
  end

  def self.down
    rename_column(:export_reports, :portal_property_id, :portal_id)
  end
end
