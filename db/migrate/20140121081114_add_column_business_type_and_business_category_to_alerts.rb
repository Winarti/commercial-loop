class AddColumnBusinessTypeAndBusinessCategoryToAlerts < ActiveRecord::Migration
  def self.up
  	add_column :contact_alerts, :business_type, :string
  	add_column :contact_alerts, :business_category, :string
  end

  def self.down
  	remove_column :contact_alerts, :business_type
  	remove_column :contact_alerts, :business_category
  end
end
