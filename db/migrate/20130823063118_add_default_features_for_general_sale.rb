class AddDefaultFeaturesForGeneralSale < ActiveRecord::Migration
  def self.up
    GeneralFeature.create({:name => "Bull Sale", :property_type => "GeneralSale"})
    GeneralFeature.create({:name => "Auction Plus", :property_type => "GeneralSale"})
    GeneralFeature.create({:name => "Weekly Sale", :property_type => "GeneralSale"})
  end

  def self.down
    Feature.find_by_name_and_property_type('Bull Sale', 'GeneralSale').destroy
    Feature.find_by_name_and_property_type('Auction Plus', 'GeneralSale').destroy
    Feature.find_by_name_and_property_type('Weekly Sale', 'GeneralSale').destroy
  end
end
