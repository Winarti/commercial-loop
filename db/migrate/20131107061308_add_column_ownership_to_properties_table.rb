class AddColumnOwnershipToPropertiesTable < ActiveRecord::Migration
  def self.up
  	add_column :properties, :ownership, :string
  end

  def self.down
  	remove_column :properties, :ownership
  end
end
