class AddFieldsToDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :website, :string
    add_column :developers, :description, :text
  end

  def self.down
    remove_column :developers, :description
    remove_column :developers, :website
  end
end
