class ChangeVendorIdInPropertiesToAgentUserId < ActiveRecord::Migration
  def self.up
    remove_column :properties, :vendor_id
    add_column :properties, :agent_user_id, :integer
  end

  def self.down
    remove_column :properties, :agent_user_id
    add_column :properties, :vendor_id, :integer
  end
end
