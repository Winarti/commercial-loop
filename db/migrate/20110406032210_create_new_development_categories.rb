class CreateNewDevelopmentCategories < ActiveRecord::Migration
  def self.up
    create_table :new_development_categories do |t|
      t.string  :name
      t.integer :property_id
      t.timestamps
    end
  end

  def self.down
    drop_table :new_development_categories
  end
end
