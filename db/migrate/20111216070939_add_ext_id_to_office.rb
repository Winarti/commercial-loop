class AddExtIdToOffice < ActiveRecord::Migration
  def self.up
    add_column :offices, :ext_id, :string
  end

  def self.down
    remove_column :offices, :ext_id
  end
end
