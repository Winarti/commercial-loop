class AddDomainToProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :domain_id, :string
  end

  def self.down
    remove_column :properties, :domain_id
  end
end
