class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string :first_name, :limit => 50, :default => '', :null => true
      t.string :last_name, :limit => 50, :default => '', :null => true
      t.string :phone
      t.string :mobile
      t.string :email, :limit => 100
      t.string :login, :limit => 40
      t.string :code
      t.string :crypted_password, :limit => 40
      t.string :salt, :limit => 40
      t.string :remember_token, :limit => 40
      t.datetime :remember_token_expires_at
      t.string :activation_code, :limit => 40
      t.datetime :activated_at
      t.text :description
      t.integer :developer_id
      t.integer :office_id
      t.string :type
      t.datetime :deleted_at

      t.timestamps
    end
    add_index :users, :login, :unique => true
  end

  def self.down
    drop_table :users
  end
end
