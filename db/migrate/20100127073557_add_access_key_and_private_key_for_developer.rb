class AddAccessKeyAndPrivateKeyForDeveloper < ActiveRecord::Migration
  def self.up
    add_column :developers, :access_key, :string
    add_column :developers, :private_key, :string
  end

  def self.down
    remove_column :developers, :private_key
    remove_column :developers, :access_key
  end
end
