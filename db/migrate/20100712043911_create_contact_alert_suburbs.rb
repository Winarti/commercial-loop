class CreateContactAlertSuburbs < ActiveRecord::Migration
  def self.up
    create_table :contact_alert_suburbs do |t|
      t.integer :contact_alert_id
      t.string :suburb
      t.timestamps
    end
  end

  def self.down
    drop_table :contact_alert_suburbs
  end
end
