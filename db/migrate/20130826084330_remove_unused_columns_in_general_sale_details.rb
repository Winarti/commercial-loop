class RemoveUnusedColumnsInGeneralSaleDetails < ActiveRecord::Migration
  def self.up
    remove_column :general_sale_details, :year_built
    remove_column :general_sale_details, :carport_spaces
    remove_column :general_sale_details, :garage_spaces
    remove_column :general_sale_details, :off_street_spaces
    remove_column :general_sale_details, :bedrooms
    remove_column :general_sale_details, :bathrooms
    remove_column :general_sale_details, :energy_efficiency_rating
    remove_column :general_sale_details, :energy_star_rating
    remove_column :general_sale_details, :land_area_metric
    remove_column :general_sale_details, :floor_area_metric
  end

  def self.down
    add_column :general_sale_details, :year_built, :integer
    add_column :general_sale_details, :carport_spaces, :integer
    add_column :general_sale_details, :garage_spaces, :integer
    add_column :general_sale_details, :off_street_spaces, :integer
    add_column :general_sale_details, :bedrooms, :integer
    add_column :general_sale_details, :bathrooms, :integer
    add_column :general_sale_details, :energy_efficiency_rating, :string
    add_column :general_sale_details, :energy_star_rating, :string
    add_column :general_sale_details, :land_area_metric, :string
    add_column :general_sale_details, :floor_area_metric, :string
  end
end
