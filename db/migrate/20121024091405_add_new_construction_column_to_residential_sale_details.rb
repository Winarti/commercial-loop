class AddNewConstructionColumnToResidentialSaleDetails < ActiveRecord::Migration
  def self.up
    add_column :residential_sale_details, :new_construction, :boolean
  end

  def self.down
    remove_column :residential_sale_details, :new_construction
  end
end
