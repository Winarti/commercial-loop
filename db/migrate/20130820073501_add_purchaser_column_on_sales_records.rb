class AddPurchaserColumnOnSalesRecords < ActiveRecord::Migration
  def self.up
    add_column :sales_records, :agent_contact_id, :integer
    add_column :sales_records, :date, :date
    add_column :sales_records, :amount, :integer
    add_column :sales_records, :display_price, :boolean, :default => true
    add_column :sales_records, :contact_note_id, :integer
    add_column :sales_records, :is_deleted, :boolean, :default => false
  end

  def self.down
    remove_column :sales_records, :agent_contact_id
    remove_column :sales_records, :date
    remove_column :sales_records, :amount
    remove_column :sales_records, :display_price
    remove_column :sales_records, :contact_note_id
    remove_column :sales_records, :is_deleted
  end
end
