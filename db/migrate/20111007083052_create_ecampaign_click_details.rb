class CreateEcampaignClickDetails < ActiveRecord::Migration
  def self.up
    create_table :ecampaign_click_details do |t|
      t.integer  :ecampaign_click_id
      t.integer  :agent_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :ecampaign_click_details
  end
end
