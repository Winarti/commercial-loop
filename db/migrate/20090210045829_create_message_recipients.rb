class CreateMessageRecipients < ActiveRecord::Migration
  def self.up
    create_table :message_recipients do |t|
      t.integer :message_id
      t.integer :user_id
      t.boolean :unread, :default => true
    
      t.timestamps
    end
    
    add_index :message_recipients, :message_id
    add_index :message_recipients, :user_id
    
    remove_index :messages, :kind
    remove_column :messages, :kind
    
    remove_index :messages, :recipient_id
    remove_column :messages, :recipient_id
    
    add_column :messages, :type, :string
    add_column :messages, :message_recipients_count, :integer, :default => 0
  end

  def self.down
    remove_column :messages, :message_recipients_count
    remove_column :messages, :type
    
    add_column :messages, :kind, :integer
    add_index :messages, :kind
    
    add_column :messages, :recipient_id, :integer
    add_index :messages, :recipient_id
    
    remove_index :message_recipients, :user_id
    remove_index :message_recipients, :message_id
    drop_table :message_recipients
  end
end
