class AddColumnParentCategoryAndParentListingIdIntoProperties < ActiveRecord::Migration
  def self.up
  	add_column :properties, :parent_category, :string
  	add_column :properties, :parent_listing_id, :integer
  end

  def self.down
  	remove_column :properties, :parent_category
  	remove_column :properties, :parent_listing_id
  end
end
