class AlterStaffColumnsForBizsale < ActiveRecord::Migration
  def self.up
    change_column :business_sale_details, :staff_casual, :integer
    change_column :business_sale_details, :staff_part_time, :integer
    change_column :business_sale_details, :staff_full_time, :integer
  end

  def self.down
    change_column :business_sale_details, :staff_casual, :string
    change_column :business_sale_details, :staff_part_time, :string
    change_column :business_sale_details, :staff_full_time, :string
  end
end
