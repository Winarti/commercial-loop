class AddLatitudeAndLongitudeInProperties < ActiveRecord::Migration
  def self.up
    add_column :properties, :latitude, :string, :limit => 50
    add_column :properties, :longitude, :string, :limit => 50
  end

  def self.down
    remove_column :properties, :longitude
    remove_column :properties, :latitude
  end
end
