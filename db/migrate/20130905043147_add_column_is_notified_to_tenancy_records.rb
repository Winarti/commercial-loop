class AddColumnIsNotifiedToTenancyRecords < ActiveRecord::Migration
  def self.up
    add_column :tenancy_records, :is_notified, :boolean, :default => false
  end

  def self.down
    remove_column :tenancy_records, :is_notified
  end
end
