class CreateEcampaigns < ActiveRecord::Migration
  def self.up
    create_table :ecampaigns do |t|
      t.integer :office_id
      t.string  :title
      t.timestamps
    end
  end

  def self.down
    drop_table :ecampaigns
  end
end
