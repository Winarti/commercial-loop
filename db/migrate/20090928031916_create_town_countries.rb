class CreateTownCountries < ActiveRecord::Migration
  def self.up
    create_table :town_countries do |t|
      t.string :name
      t.integer :country_id
      t.integer :state_id
      t.timestamps
    end
  end

  def self.down
    drop_table :town_countries
  end
end
