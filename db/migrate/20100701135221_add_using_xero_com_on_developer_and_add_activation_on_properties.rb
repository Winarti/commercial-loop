class AddUsingXeroComOnDeveloperAndAddActivationOnProperties < ActiveRecord::Migration
  def self.up
    add_column :developers, :using_xero,:boolean
    add_column :properties, :activation,:boolean
  end

  def self.down
    remove_column(:developers, :using_xero)
    remove_column(:properties, :activation)
  end
end
