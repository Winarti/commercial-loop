class CreateAgentDeveloperAccesses < ActiveRecord::Migration
  def self.up
    create_table :agent_developer_accesses do |t|
      t.integer :agent_id
      t.integer :developer_user_id
    end
    
    add_index :agent_developer_accesses, [:agent_id, :developer_user_id]
  end

  def self.down
    drop_table :agent_developer_accesses
  end
end
