class AddFaxToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :fax, :string
  end

  def self.down
    remove_column :users, :fax
  end
end
