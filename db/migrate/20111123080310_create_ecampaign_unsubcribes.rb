class CreateEcampaignUnsubcribes < ActiveRecord::Migration
  def self.up
    create_table :ecampaign_unsubcribes do |t|
      t.integer  :ecampaign_id
      t.integer  :agent_contact_id
      t.timestamps
    end
  end

  def self.down
    drop_table :ecampaign_unsubcribes
  end
end
