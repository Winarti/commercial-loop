class CreatePropertyCompanyEnquiries < ActiveRecord::Migration
  def self.up
    create_table :property_company_enquiries do |t|
      t.integer :property_id
      t.integer :agent_company_id
      t.timestamps
    end
  end

  def self.down
    drop_table :property_company_enquiries
  end
end
