# Use this file to easily define all of your cron jobs.
#
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :cron_log, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

# every 5.minutes do
#  rake "db:whn_check_property_5min", :environment => :production
# end

# every 6.minutes do
#  rake "db:whn_check_user_6min", :environment => :production
# end

every :hour do
  rake "reminder:whn_check_for_lease_reminder", :environment => :production
end

every :hour do
  rake "db:whn_check_eproperty_alert", :environment => :production
end

every 1.day, :at => '00:00' do
  rake "db:whn_scanning_office_trial_at00", :environment => :production
end

every 1.day, :at => '00:05' do
  rake "ts:index", :environment => :production
end

every 1.day, :at => '00:30' do
  rake "db:whn_scanning_export_errors", :environment => :production
end

every 1.day, :at => '01:00' do
  command "cd #{RAILS_ROOT} && RAILS_ENV=production /usr/bin/env script/delayed_job -n 2 start", :environment => :production
end

every 1.day, :at => '01:15' do
  rake "db:whn_update_cron_checked", :environment => :production
end

every 1.day, :at => '01:30' do
  command "rm -rf /home/comlpcrm/public/page_cache/views", :environment => :production
end

every 1.day, :at => '02:00' do
  rake "db:whn_check_for_delayed_jobs", :environment => :production
end

every 1.day, :at => '02:20' do
  rake "db:whn_check_for_sphinx_search", :environment => :production
end

every 1.day, :at => '02:30' do
  rake "db:whn_update_image_location", :environment => :production
end

every 1.day, :at => '04:30' do
  rake "db:whn_scanning_office_trial_at430", :environment => :production
end
 
every 1.day, :at => '07:00' do
  rake "db:whn_scanning_note_notifications", :environment => :production
end

every 1.day, :at => '12:00' do
  rake "db:whn_scanning_office_trial_at1200", :environment => :production
end

every 1.day, :at => '18:00' do
  rake "db:whn_scanning_office_trial_at1800", :environment => :production
end

every :saturday, :at => '23:00' do
  rake "db:whn_update_properties_weekly", :environment => :production
end

every 1.day, :at => '23:45' do
  rake "db:whn_scanning_opentimes", :environment => :production
end
 
every :reboot do
  runner "Log.logger_reboot", :environment => :production
  rake "ts:start", :environment => :production
  command "cd #{RAILS_ROOT} && RAILS_ENV=production /usr/bin/env script/delayed_job -n 10 start", :environment => :production
  # command "php /var/www/project/shared/public/xml_file/xmlimport/reset.php", :environment => :production
end
