# Settings specified here will take precedence over those in config/environment.rb

# The production environment is meant for finished, "live" apps.
# Code is not reloaded between requests
config.cache_classes = true

# Full error reports are disabled and caching is turned on
config.action_controller.consider_all_requests_local = false
config.action_controller.perform_caching             = true

# See everything in the log (default is :info)
# config.log_level = :debug

# Use a different logger for distributed setups
# config.logger = SyslogLogger.new

# Use a different cache store in production
# config.cache_store = :mem_cache_store

# Enable serving of images, stylesheets, and javascripts from an asset server
# config.action_controller.asset_host = "http://assets.example.com"

# Disable delivery errors, bad email addresses will be ignored
 config.action_mailer.raise_delivery_errors = true


# Don't care if the mailer can't send
config.action_mailer.perform_deliveries = true
config.action_mailer.delivery_method = :smtp
config.action_mailer.smtp_settings = {
  :enable_starttls_auto => true,
  :address              => "smtp.gmail.com",
  :port                 => 587,
  :domain               => 'commercialloopcrm.com.au',
  :user_name            => 'commercialloopcrm2@gmail.com.com',
  :password             => 'kiranatama1234',
  :authentication       => 'plain'}
config.action_mailer.default_url_options = { :host => 'commercialloopcrm.com.au' }
# Enable threaded mode

# config.threadsafe!

DOMAIN_NAMES = %w(commercialloopcrm.com.au commercialloopcrm.com.au)
DOMAIN_NAME = 'commercialloopcrm.com.au'
BASE_URL    = 'http://www.commercialloopcrm.com.au'

if `hostname`.include? "commercialloopcrm"
  # commercialloopcrm.com.au
  GOOGLE_MAPS_KEY = "ABQIAAAAlrI_wKD-_oa8tslZ6SN_oxSwrosQPVFEGKbzOiFq-SRuABKUHxSsDe6JQaUZn53sGuTxnJSLt-ghlw"
else
  # rorcraft.com
  GOOGLE_MAPS_KEY = 'ABQIAAAAlrI_wKD-_oa8tslZ6SN_oxSwrosQPVFEGKbzOiFq-SRuABKUHxSsDe6JQaUZn53sGuTxnJSLt-ghlw'
end
# Paperclip.options[:image_magick_path] = "/opt/local/bin"

# also config in payment_gateways.yml for servicemerchant
config.after_initialize do
  # ActiveMerchant::Billing::Base.mode = :test
  paypal_options = {
    :login => "peter_api1.agentpoint.com.au",
    :password => "AAZRHDNGTVM826U5",
    :signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31A3DM-beP7cgAIPuDye40.KVbdbny"
  }

#config.after_initialize do
#  ActiveMerchant::Billing::Base.mode = :test
#  paypal_options = {
    #:login => "rex_1239802854_biz_api1.rorcraft.com",
    #:password => "1239802866",
    #:signature => "AFcWxV21C7fd0v3bYYYRCpSSRl31AVB8-9Wp3USt468hAz8mrf7xJepR "
#    :login => "sel__1255597104_biz_api1.yahoo.com",
#    :password => "1255597115",
#    :signature => "Awsr.tgJlF4hQJCs02xOeXE5GZq7A71Y01ihMRinfsgHZ.Eeb1VuER9Z"
#  }

  ::EXPRESS_GATEWAY = ActiveMerchant::Billing::PaypalExpressRecurringGateway.new( paypal_options )
end

PAYPAL_TIMEZONE = 'Etc/GMT-10'
