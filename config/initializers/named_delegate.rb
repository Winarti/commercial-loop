require 'rubygems'
require 'active_support'

class Module
  def named_delegate(*methods)
    options = methods.pop
    unless options.is_a?(Hash) && to = options[:to]
      raise ArgumentError, "Delegation needs a target. Supply an options hash with a :to key as the last argument (e.g. delegate :hello, :to => :greeter)."
    end

    methods.each do |method|
      module_eval(<<-EOS, "(__DELEGATION__)", 1)
       def #{to}_#{method}(*args, &block)
         #{to}.__send__(#{method.inspect}, *args, &block) if #{to}
       end
     EOS
    end
  end
end

# class Photo
#   def name()
#     "picture of my cats"
#   end
# end
# 
# class User
#   attr_accessor :photo
#   named_delegate :name, :to => :photo
# end
# 
# photo = Photo.new
# user = User.new
# 
# user.photo = photo
# 
# user.photo.name # => "picture of my cats"
# user.photo_name # => "picture of my cats"