#!/usr/bin/env ruby
#env = ['development', 'production']
#RAILS_ENV = 'development'#env.include?("#{ARGV[0]}") ? ARGV[0] : 'production'

#require File.dirname(__FILE__) + '/../config/environment'
#require 'rubygems'
#require 'rufus/scheduler'

=begin
scheduler = Rufus::Scheduler.start_new

scheduler.every '5m', :timeout => '4m' do
  begin
    properties = Property.find_by_sql("SELECT * FROM properties WHERE processed =0 AND (cron_checked = 0 OR cron_checked IS NULL) AND status != 6 AND ( api_is_sent = 1 OR api_is_sent IS NULL)")
    unless properties.blank?
      properties.each do |property|
        sql = ActiveRecord::Base.connection()
        sql.update "UPDATE properties SET cron_checked = 1 WHERE id = #{property.id}"
        sql.commit_db_transaction
        send_property_api(property)
      end
    end
  rescue
  end
end

scheduler.every '6m', :timeout => '5m' do
begin
  agent_users = AgentUser.find_by_sql("SELECT * FROM users WHERE (cron_checked = 0 OR cron_checked IS NULL) AND type LIKE 'AgentUser' AND processed = 0")
  unless agent_users.blank?
    agent_users.each do |user|
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET cron_checked = 1 WHERE id = #{user.id}"
      sql.commit_db_transaction
      send_team_member_api(user)
    end
  end
rescue
end
end

scheduler.cron '15 1 * * *' do
begin
  properties = Property.find_by_sql("SELECT * FROM properties WHERE processed = 0 AND cron_checked = 1")
  unless properties.blank?
    properties.each do |property|
      property.update_attributes({:updated_at => Time.now, :cron_checked => false})
    end
  end
rescue
end
end

scheduler.cron '15 20 * * 0-6' do
  Log.create(:message => "Check scheduler running every day!!! (#{Time.zone.now})")
end

scheduler.cron '45 23 * * 0-6', :timeout => '40m' do
  # now we don't use end time parameter for scaning opentime
  # because if we run this script in 23:45, it will not be found opentime more than 23:45.
  # so we only need date parameter

  # change from 24 hour to 15 hour in the past
  # 23:45 - 15 = 13:45

  Log.create(:message => "Scanning Opentime more then 15 hour (#{Time.zone.now})")
  logs = ""
  logs += "------------------------Scanning Opentime more then 15 hour (#{Time.zone.now})-------------------------------<br/>"
  # SELECT * FROM `opentimes` WHERE (date <= '2011-01-12') OR (date = '2011-01-13' AND `end_time` < '13:45:00')
  # opentimes = Opentime.find(:all, :conditions => ["date <= (?)", (Time.zone.now - 24.hour).strftime("%Y-%m-%d")])

  opentimes = Opentime.find(:all, :conditions => ["(date <= ? ) OR (date = ? AND `end_time` < ?) ", (Time.zone.now - 24.hour).strftime("%Y-%m-%d"), Time.zone.now.strftime("%Y-%m-%d"), "13:45:00"])
  opentimes.each{|opentime|
   next if opentime.blank?
   tmp_property = opentime.property unless opentime.property.blank?
   logs += "Remove open time for #{opentime.id.to_s} #{opentime.date.to_s} <br/>"
   opentime.destroy
   unless tmp_property.blank?
     logs += "Update property #{tmp_property.id} and send api, Time:#{Time.zone.now} <br/>"
     # tmp_property.update_attribute(:updated_at,Time.zone.now)
   end
  } unless opentimes.blank?
  logs += "------------------------End Scanning Opentime more then 15 hour (#{Time.zone.now})------------------------------- <br/>"
  logs += "Finished scanning #{opentimes.size} opentimes" unless opentimes.blank?
  Log.create(:message => logs) unless logs.blank?
end

scheduler.cron '0 7 * * 0-6' do
  # when 07:00 will scan notification for note

  contact_notes = ContactNote.find(:all, :conditions => ["note_date = (?) and email_reminder = 1", (Time.zone.now).strftime("%Y-%m-%d")])
  contact_notes.collect{|contact_note|
    user = AgentUser.find_by_id(contact_note.agent_user_id)
    NoteNotification.deliver_email_reminder(user,contact_note.description,contact_notes.agent_contact,user.first_name)
  } unless contact_notes.blank?
end

scheduler.cron '0 0 0 * * 0-6' do
  inactivate_office_whose_trial_date_is_over
  update_office_whose_subscription_is_canceled
end

scheduler.cron '0 30 4 * * 0-6' do
  inactivate_office_whose_trial_date_is_over
  update_office_whose_subscription_is_canceled
end

scheduler.cron '0 0 12 * * 0-6' do
  inactivate_office_whose_trial_date_is_over
  update_office_whose_subscription_is_canceled
end

scheduler.cron '0 0 18 * * 0-6' do
  inactivate_office_whose_trial_date_is_over
  update_office_whose_subscription_is_canceled
end

def inactivate_office_whose_trial_date_is_over
  offices = Office.find_by_sql("SELECT * FROM `subscriptions` , `offices` WHERE offices.status = 'active' AND offices.id = subscriptions.office_id")
  f = File.open(File.join(RAILS_ROOT, 'log/cron_log_' "#{Time.now.strftime("%d-%m-%y_%H-%M")}"), "w+")
  offices.each do |office|
		sub = office.subscription
		sp = office.subscription_plan
    if Time.now >= sp['trial_end_date'] && sub.status == 'pending'
      office.update_attribute(:status, 'inactive')
      f.write("#{office.name} --- #{office.status} --- #{office.created_at} \n")
      User.find(:all,:conditions =>["office_id = ?",office.id]).each do |user|
        if user['type'] == "DeveloperUser" && user.full_access?
          MessageNotification.deliver_notify_inactive(user,office)
        end
      end
    end
  end
  f.close
end

def update_office_whose_subscription_is_canceled
  subscriptions = Subscription.all
  subscriptions.each do |subscription|
		office = subscription.office
    if !subscription.canceled_at.blank? && subscription.status == 'canceled' && Time.now >= subscription.ends_on
      office.update_attribute(:status, "inactive")
    end
  end
end

def send_property_api(pr)
    return if pr.office.blank? || pr.office.agent.blank?
    url_domain = "#{pr.office.agent.developer.entry}.commercialloopcrm.com.au"
    domains = pr.office.domains
    is_http_api = pr.office.http_api_enable
    is_office_active = pr.office.status == 'active' ? true : false
    return if domains.blank? || (is_http_api == false || is_http_api.blank?) || is_office_active == false
    domains.each do |domain|
      ipn_url = nil
      if domain.portal_export == true
        next if domain.name.blank?
        property_portal_exports = pr.property_portal_exports
        unless property_portal_exports.blank?
         property_portal_exports.each do |px|
           next if px.portal.blank?
           portal = px.portal
           next if portal.ftp_url.blank?
           portal_url = portal.ftp_url.downcase.gsub('http://', '')
           portal_url = portal_url.gsub('www.', '')
           if domain.name.downcase.include?(portal_url)
             # ========= begin special condition ==========
             if domain.name.downcase.include?("propertypoint.com.au")
               next if pr.state.blank?
               if pr.state.strip == "TAS"
                 ipn_url = portal.ftp_directory
                 break
               end
             elsif domain.name.downcase.include?("land.com.au")
               if pr.type == "ProjectSale" || pr.property_type == "Land"
                 ipn_url = portal.ftp_directory
                 break
               end
             elsif domain.name.downcase.include?("getrealty.com.au")
               next if pr.state.blank?
               if pr.state.strip == "ACT"
                 ipn_url = portal.ftp_directory
                 break
               end
             elsif domain.name.downcase.include?("virginhomes.com.au")
               include_feature = pr.features.find(:all,:conditions => "`name` LIKE '%newly built property%' OR `name` LIKE '%new developer listing%'") unless pr.features.blank?
               if !include_feature.blank? || pr.type == "ProjectSale"
                ipn_url = portal.ftp_directory
                break
               end
             else
               ipn_url = portal.ftp_directory
               break
             end
             # ========= end special condition ==========
           end
         end
        end
      else
        next if domain.ipn.blank?
        ipn_url = domain.ipn
      end
      next if ipn_url.blank?

      begin
        token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
        GLOBAL_ATTR["#{pr.office_id}-#{pr.agent_id}-#{pr.id}"] = token
        property = pr.attributes
        encoded_property = {}
        property.each do |prop|
          if prop.last.is_a?(String)
            encoded_property.merge!(prop.first => Base64.encode64(prop.last))
          else
            encoded_property.merge!(prop.first => prop.last)
          end
        end

        rental_seasons = pr.is_a?(HolidayLease) ? pr.rental_seasons : nil
        rent_seasons = {}
        unless rental_seasons.blank?
          rental_seasons.each_with_index do |rent, i|
            rent_seasons.merge!(i.to_s => rent.attributes)
          end
        end

        property_detail = pr.detail.attributes.delete_if { |x,y| ["residential_sale_id","residential_lease_id","business_sale_id","commercial_id","project_sale_id","holiday_lease_id","id"].include?(x)  }
        property_detail["bedrooms"] = property_detail["bedrooms"] + 0.5 if property_detail["half_bedroom"] == true
        property_detail["bathrooms"] = property_detail["bathrooms"] + 0.5 if property_detail["half_bathroom"] == true
        encoded_property_detail ={}
        property_detail.each do |prop_detail|
          if prop_detail.last.is_a?(String)
            encoded_property_detail.merge!(prop_detail.first => Base64.encode64(prop_detail.last))
          else
            encoded_property_detail.merge!(prop_detail.first => prop_detail.last)
          end
        end

        users = {}
        features = {}
        opentimes = {}

        unless pr.primary_contact.blank?
          primary_attr = pr.primary_contact.attributes
          encoded_primary = {}
          primary_attr.each do |prim_user|
            if prim_user.last.is_a?(String)
              encoded_primary.merge!(prim_user.first => Base64.encode64(prim_user.last))
            else
              encoded_primary.merge!(prim_user.first => prim_user.last)
            end
          end

          encoded_primary.merge!({ "landscape_images"=>{"1"=>( pr.primary_contact.landscape_image.first.blank? ? "" : pr.primary_contact.landscape_image.first.public_filename ),"2"=>( pr.primary_contact.landscape_image.second.blank? ? "" : pr.primary_contact.landscape_image.second.public_filename )},"portrait_images"=>{"1"=>( pr.primary_contact.portrait_image.first.blank? ? "" : pr.primary_contact.portrait_image.first.public_filename ),"2"=>( pr.primary_contact.portrait_image.second.blank? ? "" : pr.primary_contact.portrait_image.second.public_filename )}}) unless encoded_primary.blank?
          users.merge!({"primary" => encoded_primary}) unless encoded_primary.blank?
        end

        unless pr.secondary_contact.blank?
          secondary_attr = pr.secondary_contact.attributes
          encoded_secondary = {}
          secondary_attr.each do |sec_user|
            if sec_user.last.is_a?(String)
              encoded_secondary.merge!(sec_user.first => Base64.encode64(sec_user.last))
            else
              encoded_secondary.merge!(sec_user.first => sec_user.last)
            end
          end

          encoded_secondary.merge!("landscape_images"=>{"1"=>( pr.secondary_contact.landscape_image.first.blank? ? "" : pr.secondary_contact.landscape_image.first.public_filename ),"2"=>( pr.secondary_contact.landscape_image.second.blank? ? "" : pr.secondary_contact.landscape_image.second.public_filename )},"portrait_images"=>{"1"=>( pr.secondary_contact.portrait_image.first.blank? ? "" : pr.secondary_contact.portrait_image.first.public_filename ),"2"=>( pr.secondary_contact.portrait_image.second.blank? ? "" : pr.secondary_contact.portrait_image.second.public_filename )})  unless encoded_secondary.blank?
          users.merge!({"secondary" => encoded_secondary}) unless encoded_secondary.blank?
        end

        unless pr.third_contact.blank?
          third_attr = pr.third_contact.attributes
          encoded_third = {}
          third_attr.each do |third_user|
            if third_user.last.is_a?(String)
              encoded_third.merge!(third_user.first => Base64.encode64(third_user.last))
            else
              encoded_third.merge!(third_user.first => third_user.last)
            end
          end

          encoded_third.merge!("landscape_images"=>{"1"=>( pr.third_contact.landscape_image.first.blank? ? "" : pr.third_contact.landscape_image.first.public_filename ),"2"=>( pr.third_contact.landscape_image.second.blank? ? "" : pr.third_contact.landscape_image.second.public_filename )},"portrait_images"=>{"1"=>( pr.third_contact.portrait_image.first.blank? ? "" : pr.third_contact.portrait_image.first.public_filename ),"2"=>( pr.third_contact.portrait_image.second.blank? ? "" : pr.third_contact.portrait_image.second.public_filename )})  unless encoded_third.blank?
          users.merge!({"third" => encoded_third}) unless encoded_third.blank?
        end

        pr.opentimes.each_with_index{|o,i| opentimes.merge!(i.to_s => {"date"=> o.date, "property_id"=> o.property_id, "id"=> o.id, "start_time"=> o.start_time.strftime("%H:%M"), "end_time"=> o.end_time.strftime("%H:%M")})} unless pr.opentimes.blank?

        # pr.images.each_with_index{|im,i| images.merge!(i.to_s => im.attributes)} unless pr.images.blank?
        encoded_images = {}
        unless pr.images.blank?
          pr.images.map{|x| x.attributes}.each_with_index do |img, i|
            img.keys.each{|t| img[t] = Base64.encode64(img[t]) if img[t].is_a?(String)}
            encoded_images.merge!(i.to_s => img)
          end
        end

        # pr.brochure.each_with_index{|b,i| brochures.merge!(i.to_s => b.attributes)} unless pr.brochure.blank?
        encoded_brochures = {}
        unless pr.brochure.blank?
          pr.brochure.map{|x| x.attributes}.each_with_index do |bro, i|
            bro.keys.each{|t| bro[t] = Base64.encode64(bro[t]) if bro[t].is_a?(String) }
            encoded_brochures.merge!(i.to_s => bro)
          end
        end

        # pr.floorplans.each_with_index{|f,i| floorplans.merge!(i.to_s => f.attributes)} unless pr.floorplans.blank?
        encoded_floorplans = {}
        unless pr.floorplans.blank?
          pr.floorplans.map{|x| x.attributes}.each_with_index do |fp, i|
            fp.keys.each{|t| fp[t] = Base64.encode64(fp[t]) if fp[t].is_a?(String)}
            encoded_floorplans.merge!(i.to_s => fp)
          end
        end

        pr.features.each_with_index { |f,i| features.merge!({i.to_s => Base64.encode64(f.name)}) } unless pr.features.blank?

        # pr.videos.each_with_index{|b,i| videos.merge!(i.to_s => b.attributes)} unless pr.videos.blank?
        encoded_videos = {}
        unless pr.property_videos.blank?
          pr.property_videos.map{|x| x.attributes}.each_with_index do |vd, i|
            vd.keys.each{|t| vd[t] = Base64.encode64(vd[t]) if vd[t].is_a?(String)}
            encoded_videos.merge!(i.to_s => vd)
          end
        end

        audio = pr.mp3.blank? ? nil : {"0" => pr.mp3.attributes}

        ["updated_at","created_at","deleted_at"].each do |date|
          property[date] = property[date].to_formatted_s(:db_format) unless property[date].blank?
          property_detail[date] = property_detail[date].to_formatted_s(:db_format) unless property_detail[date].blank?
        end
        system("curl -i -X POST -d 'property=#{encoded_property.to_json}&details=#{encoded_property_detail.to_json}&rental_seasons=#{rent_seasons.to_json}&contacts=#{users.to_json}&brochures=#{encoded_brochures.to_json}&opentimes=#{opentimes.to_json}&images=#{encoded_images.to_json}&floorplans=#{encoded_floorplans.to_json}&features=#{features.to_json}&videos=#{encoded_videos.to_json}&audio=#{audio.to_json}&callback_url=http://#{url_domain}/agents/#{pr.agent_id}/offices/#{pr.office_id}/properties/#{pr.id}/property_notification?token=#{token}&type=property' #{ipn_url}")
        pr.api_is_sent = true
      rescue Exception => ex
        Log.create(:message => "property ---> #{pr.errors.inspect} ::: ---> errors : #{ex.inspect} ")
        pr.api_is_sent = false
      end
      # could not save with method save active record
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE properties SET api_is_sent = #{pr.api_is_sent} WHERE id = #{pr.id}"
      sql.commit_db_transaction
    end
end


def send_team_member_api(a_user)
    return if a_user.office.blank? || a_user.office.agent.blank?
    url_domain = "#{a_user.office.agent.developer.entry}.commercialloopcrm.com.au"
    domains = a_user.office.domains
    is_http_api = a_user.office.http_api_enable
    is_office_active = a_user.office.status == 'active' ? true : false
    return if domains.blank? || (is_http_api == false || is_http_api.blank?) || is_office_active == false
    domains.each do |domain|
      next if domain.ipn.blank?
      begin
        team_member = a_user.attributes
        encoded_team_member = {}
        team_member.each do |tm|
          if tm.last.is_a?(String)
            encoded_team_member.merge!(tm.first => Base64.encode64(tm.last))
          else
            encoded_team_member.merge!(tm.first => tm.last)
          end
        end

        encoded_testimonials = {}
        unless a_user.testimonials.blank?
          a_user.testimonials.map{|x| x.attributes}.each_with_index do |testi, i|
            testi.keys.each do |t|
              if testi[t].is_a?(String)
                testi[t] = Base64.encode64(testi[t])
              end
            end
            encoded_testimonials.merge!(i.to_s => testi)
          end
        end

        audio = a_user.mp3.blank? ? nil : a_user.mp3.attributes
        img_1 = img_2 = img_portrait_1 = img_portrait_2 = nil

        landscapes = a_user.landscape_image
        unless landscapes.blank?
          landscapes.each do |img|
            if img.position == 1
              img_1 = img
            else
              img_2 = img
            end
          end
        end

        portraits = a_user.portrait_image
        unless portraits.blank?
          portraits.each do |img_portrait|
            if img_portrait.position == 1
              img_portrait_1 = img_portrait
            else
              img_portrait_2 = img_portrait
            end
          end
        end
        encoded_team_member.merge!({ "landscape_images"=>{"1"=>( img_1.blank? ? "" : img_1.public_filename ),"2"=>( img_2.blank? ? "" : img_2.public_filename )},"portrait_images"=>{"1"=>( img_portrait_1.blank? ? "" : img_portrait_1.public_filename ),"2"=>( img_portrait_2.blank? ? "" : img_portrait_2.public_filename )}}).merge!("testimonials" => encoded_testimonials).merge!("vcard" => (a_user.vcard.blank? ? "" : BASE_URL + a_user.vcard.public_filename)).merge!("audio" => audio)

        token = Digest::SHA1.hexdigest("#{Time.now}")[8..50]
        GLOBAL_ATTR["#{a_user.office_id}-#{a_user.developer_id}-#{a_user.id}"] = token

        system("curl -i -X POST -d 'contacts=#{encoded_team_member.to_json}&callback_url=http://#{url_domain}/agents/#{a_user.agent.id}/offices/#{a_user.office_id}/agent_users/#{a_user.id}/team_notification?token=#{token}&type=team_member' #{domain.ipn}")

        # update api_is_sent field
        a_user.api_is_sent = true
      rescue Exception => ex
        Log.create(:message => "team member ---> #{a_user.inspect} :: ---> errors : #{ex.inspect}")
        a_user.api_is_sent = false
      end
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE users SET api_is_sent = #{a_user.api_is_sent} WHERE id = #{a_user.id}"
      sql.commit_db_transaction
    end
end
=end