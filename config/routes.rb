ActionController::Routing::Routes.draw do |map|

  map.root :controller => 'sessions', :action => 'new'
  map.connect "/announcements/:action", :controller => "announcements"
  map.connect "/paypal_ipn", :controller => "paypal_ipn", :action => "notify"
  map.connect "/paypal_ipn", :controller => "paypal_ipn", :action => "paypal_ipn"
  map.connect '/spellcheck', :controller => 'spell', :action => 'spellcheck'
  map.sms_status '/sms_status', :controller => 'sms', :action => 'update_status'

  #--------------------------------------------------
  #   admin routes
  #--------------------------------------------------
  map.namespace :admin do |admin|
    # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
    admin.resources :countries, :member => {:delete_flag => :delete, :suburbs => :get, :bulk_upload => :post }
    admin.resource :active_help, :member => {:update => :put}
    admin.resource :active_help_pages, :member => {:index => :get}
    admin.resource :active_help_page_agents, :member => {:index => :get}
    admin.resource :updates, :member => {:update => :put}
    admin.resource :exports, :member => {:update => :put}
    admin.resource :agent_quicklinks, :member => {:update => :put}
    admin.resource :developer_quicklinks, :member => {:update => :put}
    admin.resource :languages, :member => {:index => :get}
  end
  map.admin '/admin', :controller => 'admin'
  map.resources :admin_users
  map.resources :agent_users
  map.resources :admin, :collection => {:clients => :get, :offices => :get, :search => :post, :toggle_office_status => :put, :trigger => :get}

  map.resources :messages, :collection => { :unread => :get, :archives => :get },
    :member => { :assign_recipients => :put } do |message|
    # comments controller is used by developer and agent version also
    message.resources :comments, :controller => 'message_comments',
      :collection => { :request_archive => :put, :approve_archive => :get, :archive => :put}
  end

  map.resources :message_categories do |cate|
    cate.resources :topics, :controller => 'message_topics'
  end
  map.internal_chat "/messages/internal/:id", :controller => "messages", :action => "internal"
  map.developer_internal_chat "developers/:developer_id/messages/internal/:id", :controller => "messages_d", :action => "internal"


  #--------------------------------------------------
  #   developer version routes
  #--------------------------------------------------
  map.resources :developers, :collection => {:search => :post,:account_statistic => :get }, :member => { :api_details => :get, :dev_summary => :get, :client_messages => :get, :commercial_loop_messages => :get, :subscriptions => :get, :updates => :get, :clients => :get, :quick_links => :get, :update_developer_api => :put, :delete_developer_domain => :any, :combined_exports => :get, :update_combined_exports => :put, :ecampaign => :get, :update_ecampaign => :put, :trigger_domain => :any, :add_conjunctional_office => :any, :create_conjunctional_office => :post} do |developer|
    developer.resources :developer_users, :member => {:toggle_active_help => :post, :close_button_per_page => :post}
    developer.resource :developer_account, :member => {:billing => :any, :credit_card => :any, :plans => :any, :subscribe => :any, :unsubscribe => :post, :complete => :any, :cancel => :any}
    developer.resources :agents, :collection => { :search => :post }, :member => {:delete_agent => :any} do |agent|
      agent.resources :offices, :controller => 'offices_d', :except => [:index],
        :member => {
          :set_hostname => :any,:office_notification => :any, :logo => :any, :update_logo => :any, :change_logo => :any,
          :delete_logo => :any, :create_domain =>:any,:delete_domain => :any, :delete_website => :any, :trigger_http_api => :any,
          :send_properties => :any, :send_contacts => :any, :send_office => :any
        }, :collection => { :create_office => :post }
    end
    # developer could access offices across multiple agents
    developer.resources :offices, :controller => 'offices_d', :only => [:index, :edit_export_setting, :update_export_setting, :api_details, :delete_office, :send_export, :send_export_to_all, :update_office_api], :member => {:edit_export_setting => :any, :update_export_setting => :put, :api_details => :any, :delete_office => :any, :send_export => :any, :send_export_to_all => :any, :update_office_api => :put, :populate_specialities => :any, :trigger => :any, :update_domain_projects => :any, :delete_domain_project => :any}
    developer.resources :updates

    developer.resources :messages, :controller => 'messages_d',
      :collection => { :update_priority => :post, :update_quote_approved => :get, :update_quote => :get,:add_estimation => :get, :update_estimation => :post, :client => :get, :new_client => :get, :create_client => :post, :unread_client => :get, :archives_client => :get, :quote_required => :get, :quote_approved => :get,
      :commercial_loop_crm => :get, :new_commercial_loop_crm => :get, :create_commercial_loop_crm => :post, :unread_commercial_loop_crm => :get, :archives_commercial_loop_crm => :get, :topic_commercial_loop_crm => :get },
      :member => { :assign_recipients => :put, :edit_client => :get, :update_client => :put, :edit_commercial_loop_crm => :get, :update_commercial_loop_crm => :put,  }
    developer.resources :message_categories, :controller => 'message_categories_d'
  end
  map.active_offices_status '/developers/:id/active_offices/:status', :controller => "developers", :action => 'active_offices'
  map.support_status '/developers/:id/support/:status', :controller => "developers", :action => 'support'
  map.send_properties '/developers/:developer_id/agents/:agent_id/offices/:id/send_properties/:so_id', :controller => "offices_d", :action => 'send_properties'
  map.send_contacts '/developers/:developer_id/agents/:agent_id/offices/:id/send_contacts/:so_id', :controller => "offices_d", :action => 'send_contacts'
  map.send_office '/developers/:developer_id/agents/:agent_id/offices/:id/send_office/:so_id', :controller => "offices_d", :action => 'send_office'
#  map.update_child_property_address '/agents/:agent_id/offices/:id/properties/:property_id/edit/update_child_property_address', :controller => "properties", :action => 'update_child_property_address'
  #--------------------------------------------------
  #   agent version routes
  #--------------------------------------------------

  # levels is more clearly than :has_many
  map.resources :agents, :member => { :api_details => :any } do |agent|
    agent.resources :offices, :controller => 'offices_a',
      :member => {:logo => :any, :agent_api_details => :get, :update_office_api => :put, :send_properties => :any, :send_contacts => :any,
        :send_office => :any, :delete_website => :any, :delete_domain => :any, :trigger => :any, :agent_enable_exports => :any
      }, :collection => {:account => :get , :api_details =>:get} do |offices|
      offices.resources :agent_users,
        :member => {
          :toggle_active_help => :post, :close_button_per_page => :post, :load_photo => :any, :crop_photo => :any, :exec_crop => :any,
          :team_notification => :get, :edit_conjunctional => :get
        },
        :collection => {
          :preference => :any, :conjunctional => :any, :sort_users => :post, :create_conjunctional => :post, :create_level3 => :post,
          :create_level4 => :post,:check_user => :any,:delete_photo => :any, :search => :post
        }
      offices.resources :testimonials,:new =>{:new => :any},:edit =>{:edit =>:any},:delete =>{:delete =>:any},:index=>{:index=>:any},:update =>{:update =>:any}, :collection => { :open => :any }
      offices.resources :sms, :collection => { :auto_complete => :post }
      offices.resources :mail_merges, :collection => { :upload_doc => :any }
      offices.resources :changes_logs
      offices.resources :files
      offices.resources :configurations
      offices.resources :pdf_configurations, :collection => { :template_view => :any }
      offices.resources :sharing_stocks
      offices.resources :team_assigns, :collection => {:psize => :any}
      offices.resources :contact_listeners, :collection => {:new_group_contact => :any,:add_group_contact => :post, :new_heard_about_us => :any,:add_heard_about_us => :post, :check_username => :post, :heard_about_us_list => :any, :group_contact_list => :any}
      offices.resources :project_developments, :collection => {:refresh => :any}
      offices.resources :project_design_types, :collection => {:refresh => :any}
      offices.resources :project_styles, :collection => {:refresh => :any}
      offices.resources :stock_lists, :collection => { :list => :any, :multiple_delete => :post, :choose_layout => :any, :choose_data => :any, :choose_properties => :any, :choose_colour => :any, :preview => :any, :generate_pdf => :any, :populate_property_type => :any, :search_property => :any}, :member => {:download => :get, :delete_stock => :delete}
      offices.resources :ecampaigns, :collection => {:eproperty_update => :any, :search_contact => :post, :search => :post, :choose_layout => :any, :campaign_copy => :any, :choose_colour => :any, :select_recipient => :any, :view_sample => :any, :get_image => :any, :send_ecampaign => :any}, :member => {:delete_campaign => :delete, :stats => :any, :edit_ecampaign => :any, :view_and_send => :any, :duplicate_ecampaign => :any, :visit_link => :any, :visit_property_link => :any, :preview => :any, :unsubcribe => :any, :forward => :any, :send_forward => :post, :sample => :any, :send_sample => :any}
      offices.resources :ebrochures, :collection => {:populate_prop => :any, :search_contact => :post, :search => :post, :choose_layout => :any, :campaign_copy => :any, :choose_colour => :any, :select_recipient => :any, :view_sample => :any, :get_image => :any, :send_ebrochure => :any}, :member => {:delete_ebrochure => :delete, :stats => :any, :edit_ebrochure => :any, :view_and_send => :any, :duplicate_ebrochure => :any, :visit_link => :any, :visit_property_link => :any, :preview => :any, :unsubcribe => :any, :forward => :any, :send_forward => :post, :sample => :any, :send_sample => :any, :continue_send => :any}
      offices.resources :ealerts, :collection => {:search_contact => :post, :search => :post, :choose_layout => :any, :campaign_copy => :any, :choose_colour => :any, :select_recipient => :any, :schedule_alert => :any, :view_sample => :any, :get_image => :any, :send_ealert => :any}, :member => {:delete_alert => :delete, :stats => :any, :edit_ealert => :any, :view_and_send => :any, :duplicate_ealert => :any, :visit_link => :any, :visit_property_link => :any, :preview => :any, :unsubcribe => :any, :forward => :any, :send_forward => :post, :sample => :any, :send_sample => :any}
      offices.resources :emarketings, :collection => {:search_contact => :post, :search => :post, :choose_layout => :any, :campaign_copy => :any, :choose_colour => :any, :select_recipient => :any, :view_sample => :any, :get_image => :any, :send_emarketing => :any, :delete_image => :any, :delete_emarketing_image => :any}, :member => {:delete_emarketing => :delete, :stats => :any, :edit_emarketing => :any, :view_and_send => :any, :duplicate_emarketing => :any, :visit_link => :any, :visit_property_link => :any, :preview => :any, :unsubcribe => :any, :forward => :any, :send_forward => :post, :sample => :any, :send_sample => :any}
      offices.resources :elists, :collection => {:search_property => :post, :search_contact => :post, :search => :post, :choose_layout => :any, :choose_properties => :any, :choose_data => :any, :choose_colour => :any, :select_recipient => :any, :view_sample => :any, :get_image => :any, :send_elist => :any}, :member => {:delete_elist => :delete, :stats => :any, :edit_elist => :any, :view_and_send => :any, :duplicate_elist => :any, :visit_link => :any, :visit_property_link => :any, :preview => :any, :unsubcribe => :any, :forward => :any, :send_forward => :post, :sample => :any, :send_sample => :any}
      offices.resources :csv_export_import, :collection => {:import_residential_sale_price_list_csv => :any,:import_residential_sale_summary_csv => :any, :import_contact_csv => :post, :export_contact_csv => :any, :import_property_csv => :post, :export_property_csv => :any}
      offices.resources :auto_responder, :collection => { :update_auto_responder => :any }
      offices.resources :reports, :collection => {:export_to_csv => :any, :preview => :get, :choose_data => :any, :populate_property_type => :any, :choose_properties => :any}
      offices.resources :export_errors

      offices.resources :properties, :new => {
        :residential_lease => :get,
        :residential_sale => :get,
        :holiday_lease => :get,
        :commercial => :get,
        :commercial_building => :get,
        :project_sale => :get,
        :business_sale => :get,
        :new_development => :get,
        :land_release => :get,
        :general_sale => :get,
        :livestock_sale => :get,
        :clearing_sales_event => :get,
        :clearing_sale => :get
      }, :collection => {
        :get_agent_contact_purchaser => :any,
        :update_child_property_address => :any,
        :auto_complete => :post,
        :auto_complete_sale_property => :post,
        :auto_complete_lease_property => :post,
        :auto_complete_external_agent => :post,
        :populate_agent => :post,
        :residential_lease => :post,
        :residential_sale => :post,
        :holiday_lease => :post,
        :commercial => :post,
        :commercial_building => :post,
        :project_sale => :post,
        :business_sale => :post,
        :new_development => :post,
        :land_release => :post,
        :update_area => :get,
        :search => :post,
        :sql_search => :post,
        :sub_categories => :post,
        :delete_listing => :delete,
        :refresh_features => :post,
        :update_listing => :get,
        :view_listing => :get,
        :populate_combobox => :get,
        :update_price => :any,
        :update_assign => :any,
        :update_exports => :any,
        :preparing_changes => :any,
        :check_color => :any,
        :update_portal_exports2 => :any,
        :general_sale => :post,
        :livestock_sale => :post,
        :clearing_sales_event => :post,
        :clearing_sale => :post
      }, :member => {
        :assign => :any,
        :gmap => :any,
        :property_notification => :get,
        :import_callback => :get,
        :property_ipn => :any,
        :photos => :any,
        :floorplans => :any,
        :brochure => :any,
        :price => :any,
        :ticket => :any,
        :status => :any,
        :update_status=>:any,
        :update_deal_type => :any,
        :exports2 => :any,
        :export_portals => :any,
        :share_on_facebook => :any,
        :share_on_twitter => :any,
        :tweet =>:post,
        :preview => :any,
        :vendor_detail =>:any,
        :duplication => :any
      } do |property|
        property.resources :campaigns
        property.resources :opentimes, :collection => { :form_api => :any, :api => :post, :end_times => :post, :search => :post, :sub_categories => :post, :delete_listing =>:delete, :refresh_features =>:post, :update_listing =>:get,:view_listing =>:get,:populate_combobox => :get}
        property.resources :property_notes, :collection => {:auto_complete_for_contact_name => :any, :search => :post, :sub_categories => :post, :delete_listing =>:delete, :refresh_features =>:post, :update_listing =>:get,:view_listing =>:get,:populate_combobox => :get, :get_viewer => :get, :set_viewer => :get}
        property.resources :past_opentimes  do |past|
          past.resources :contact_pastopentime_relations
        end
        property.resources :tenancies, :collection => {:contact_auto_complete => :post, :populate_combobox => :get, :view_listing => :get}, :member => {:update_status=>:any, :view_tenancy => :any}
        property.resources :transactions, :collection => {:populate_combobox => :get, :view_listing => :get}, :member => {:update_status=>:any,:view_transaction => :any}
        property.resources :property_tasks, :collection => {
          :populate_task_types => :get
        }, :member => {
          :update_task_status => :put
        }

        property.resources :offers, :collection => {:auto_complete_for_offer_name => :any}
        property.resources :add_tenancies
        property.resources :add_transactions
        property.resources :purchasers, :collection => {:auto_complete_for_offer_name => :any}
        property.resources :mp3s, :collection => {:download => :get}
        property.resources :brochures, :collection => { :list => :any, :update_brochure_list => :any, :choose_layout => :any, :choose_images => :any, :crop_images => :any, :crop => :any, :manage_brochure => :any, :brochure_back => :any, :preview => :any, :generate_pdf => :any}, :member => {:download => :get, :delete_brochure => :delete}
        property.resources :ebrochures, :collection => {:populate_prop => :any, :search_contact => :post, :search => :post, :choose_layout => :any, :campaign_copy => :any, :choose_colour => :any, :select_recipient => :any, :view_sample => :any, :get_image => :any, :send_ebrochure => :any}, :member => {:delete_ebrochure => :delete, :stats => :any, :edit_ebrochure => :any, :view_and_send => :any, :duplicate_ebrochure => :any, :visit_link => :any, :visit_property_link => :any, :preview => :any, :unsubcribe => :any, :forward => :any, :send_forward => :post, :sample => :any, :send_sample => :any, :continue_send => :any}
        property.resources :videos, :member => {:show_video => :get}
#        property.resources :media, :collection => { :simple_media => :get, :update_simple_plans => :post, :update_simple_images => :any, :update_simple_brochures => :any, :update_description => :any, :update_mp3 => :post, :destroy_mp3 => :delete, :update_logo => :post, :destroy_logo => :delete,:update_brochure => :post, :destroy_brochure => :delete,:update_images => :post, :destroy_image => :delete, :update_plans => :post, :destroy_plan => :delete, :request_token => :any, :video_ipn => :any, :create_video => :post, :sort_images => :post, :sort_plans => :post, :sort_brochure => :post}, :only => [:index]
        property.resources :media, :collection => { :simple_media => :get, :update_simple_plans => :post, :update_simple_images => :any, :update_simple_brochures => :any, :update_simple_uploaded_video => :any, :update_description => :any, :update_mp3 => :post, :destroy_mp3 => :delete, :update_logo => :post, :destroy_logo => :delete,:update_brochure => :post, :destroy_brochure => :delete,:update_images => :post, :destroy_image => :delete, :update_plans => :post, :destroy_plan => :delete, :update_uploaded_video => :post, :destroy_uploaded_video => :delete, :request_token => :any, :video_ipn => :any, :create_video => :post, :sort_images => :post, :sort_plans => :post, :sort_brochure => :post}, :only => [:index]
        property.resources :listeners, :collection => { :form_export_portal => :any, :form_status => :any, :status => :post, :form_opentime => :any, :opentime => :post, :export_report => :post}
        property.resources :extras, :collection => { :update_extra => :any, :option_form => :get }
        property.resources :commercial_building_children_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
        property.resources :land_release_children_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
        property.resources :residential_building_children_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
        property.resources :parent_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
        property.resources :vaults, :collection => { :add_contact => :get, :create_contact => :post, :search_contact => :post, :update_vault => :any, :update_simple_brochures => :any, :update_brochure => :post, :update_description => :any, :destroy_brochure => :delete , :vault_logs => :any, :vault_settings => :any, :vault_files => :any, :vault_contacts => :any}, :member => {:delete_contact => :delete, :extend_access => :get, :send_welcome_mail => :get}
        property.resources :extra_options, :collection => { :populate => :any }
        property.resources :new_development_categories, :collection => { :populate => :any }
        property.resources :match_contacts, :collection => {:populate_regions => :get, :populate_suburbs => :get, :auto_complete => :post, :populate_combobox_contact => :get, :view_contact_listing => :get, :populate_combobox => :get, :view_listing => :get, :create_match_contacts => :post}
        property.resources :residential_building_reports, :collection => {:generate_building_data_pdf => :any, :print_building_data_pdf => :any, :generate_price_list_pdf => :any, :print_price_list_pdf => :any, :export_residential_sale_csv => :any, :export_residential_sale_csv2 => :any, :export_residential_sale_xls => :any, :export_residential_sale_xls2 => :any}
        property.resources :general_sale_children_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
        property.resources :livestock_sale_children_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
        property.resources :clearing_sales_event_children_listings, :collection => { :populate_combobox => :get, :view_listing => :get }
      end
      offices.resources :property_listeners, :new => {:residential_lease => :get, :residential_sale => :get, :holiday_lease => :get, :commercial => :get, :project_sale => :get, :business_sale => :get}, :collection => {:residential_lease => :post, :residential_sale => :post, :holiday_lease => :post, :commercial => :post, :project_sale => :post, :business_sale => :post, :update_property => :post, :new_property => :post}
      offices.resources :user_defined_features
      offices.resources :user_file_categories
      offices.resources :updates
      offices.resources :category_contacts
      offices.resources :company_contacts
      offices.resources :group_contacts
      offices.resources :group_companies
      offices.resources :heard_froms
      offices.resources :note_types
      offices.resources :contact_task_types
      offices.resources :property_task_types
      offices.resources :task_types
      offices.resources :tasks, :collection => {
        :populate_task_types => :get,
        :populate_combobox => :get,
        :view_listing => :get
      }, :member => {
        :update_task_status => :put
      }

      offices.resources :search_results, :new => {:email_configuration => :get, :email_configuration_process => :post, :email_preview => :get, :email_body_preview => :get}, :collection => {:change_property_types => :get, :populate_suburbs => :get, :populate_regions => :get, :auto_complete_tenant => :post, :auto_complete => :post, :auto_complete_region => :post, :auto_complete_suburb => :post, :populate_combobox_contact => :get, :view_contact_listing => :get, :notification => :get, :print_pdf => :any, :generate_pdf => :any, :save_email_configuration => :post, :create_match_properties => :post, :generate_pdf => :any, :view_history_listing => :get, :exact_match_listing => :get, :similiar_match_listing => :get, :create_search_history => :get, :history => :get, :populate_combobox => :get, :download => :get}, :member => {:preview => :any, :share_on_facebook => :any, :share_on_twitter => :any, :tweet => :post}
      offices.resources :agent_contacts, :collection => {:tenants => :get, :populate_combobox_tenant => :get, :view_listing_tenant => :get, :auto_complete => :post, :search => :post, :sub_categories => :post, :delete_listing =>:delete, :refresh_features =>:post, :update_listing =>:get,:view_listing =>:get,:populate_combobox => :get, :update_price=>:any,:update_assign =>:any,:update_status=>:any,:preparing_changes =>:any,:populate_property_types => :any,:populate_categories =>:any,:populate_groups =>:any, :populate_heards =>:any, :populate_notes =>:any, :update_duplicate => :post}, :member => {:listings => :get, :populate_combobox_property_listings => :get,
          :view_property_listings => :get} do |contact|
        contact.resources :alerts, :member => {:populate_regions => :get, :populate_suburbs => :get, :populate_prop => :any, :edit_property_types => :get}, :collection => {:change_property_types => :get}
        contact.resources :notes
        contact.resources :contact_tasks, :collection => {
          :populate_task_types => :get
        }, :member => {
          :update_task_status => :put
        }
        # contact.resources :listings, :collection => {
        #   :populate_combobox_property_listings => :get,
        #   :view_property_listings => :get
        # }
      end
      offices.resources :agent_companies, :collection => {:search => :post, :sub_categories => :post, :delete_listing =>:delete, :refresh_features =>:post, :update_listing =>:get,:view_listing =>:get,:populate_combobox => :get, :update_price=>:any,:update_assign =>:any,:update_status=>:any,:preparing_changes =>:any,:populate_categories =>:any,:populate_property_types => :any,:populate_groups =>:any, :populate_heards =>:any, :populate_notes =>:any, :update_duplicate => :post} do |company|
        company.resources :company_alerts, :collection => {:populate_prop => :any}
        company.resources :company_notes
      end
      offices.resources :livestock_types, :collection => {:refresh_types => :post}
      offices.resources :livestock_type_parents
    end
    agent.resources :messages, :controller => 'messages_a', :collection => {:unread => :get, :archives => :get}
  end
  map.listeners_export "/listeners/export_portal/:property_id", :controller => "listeners", :action => "export_portal", :method => :post
  map.listing '/agents/:agent_id/offices/:office_id/properties/listing/:type', :controller => 'properties', :action => 'listing'
  map.destroy_past_opentime '/agents/:agent_id/offices/:office_id/properties/:property_id/opentimes/destroy_past_opentime/:id', :controller => 'opentimes', :action => 'destroy_past_opentime'
  map.destroy_attendee '/agents/:agent_id/offices/:office_id/properties/:property_id/contact_pastopentime_relations/destroy_attendee/:id', :controller => 'contact_pastopentime_relations', :action => 'destroy_attendee'
  map.destroy_note '/agents/:agent_id/offices/:office_id/properties/:property_id/property_notes/destroy_note/:id', :controller => 'property_notes', :action => 'destroy_note'
  map.destroy_youtube_video '/agents/:agent_id/offices/:office_id/properties/:property_id/media/destroy_youtube_video/:id', :controller => 'media', :action => 'destroy_youtube_video'
  map.update_duplicate_contact_pastopentime '/agents/:agent_id/offices/:office_id/properties/:property_id/contact_pastopentime_relations/past_opentimes/:past_opentime_id/update_duplicate', :controller => 'contact_pastopentime_relations', :action => 'update_duplicate'
  map.update_duplicate_property_note '/agents/:agent_id/offices/:office_id/properties/:property_id/property_notes/update_duplicate', :controller => 'property_notes', :action => 'update_duplicate'
  map.update_duplicate_offer '/agents/:agent_id/offices/:office_id/properties/:property_id/offers/update_duplicate', :controller => 'offers', :action => 'update_duplicate'
  map.update_duplicate_purchaser '/agents/:agent_id/offices/:office_id/properties/:property_id/purchasers/update_duplicate', :controller => 'purchasers', :action => 'update_duplicate'
  map.load_embed_video '/agents/:agent_id/offices/:office_id/properties/:property_id/media/load_embed_video/:id', :controller => 'media', :action => 'load_embed_video'


  #--------------------------------------------------
  #   API
  #--------------------------------------------------
  map.resources :properties , :controller => "api/properties", :member => {:photos => :get, :floorplans => :get, :brochure => :get }
  map.resources :agent_users , :controller => "api/agents"
  map.resources :offices , :controller => "api/offices"
  map.api_login '/api_login', :controller => 'sessions', :action => 'api_login'


  #--------------------------------------------------
  #   misc routes
  #--------------------------------------------------
  map.resources :suburbs, :collection => { :search => :post }
  map.resources :country_property_types, :collection => {:geosearch_in => :get,:search_ptype =>:get, :search_eer =>:get, :search_label =>:get, :search_zoning =>:get}
  map.service_property '/services/:office_id/properties', :controller => 'services', :action => 'properties'
  map.service_property '/services/:office_id/opentimes', :controller => 'services', :action => 'opentimes'
  map.service_property '/services/:office_id/auction', :controller => 'services', :action => 'auction'
  map.service_property '/services/:office_id/search', :controller => 'services', :action => 'search'
  map.service_detail '/services/:office_id/property/:id/detail', :controller => 'services', :action => 'detail'
  map.service_detail '/services/:office_id/suburbs', :controller => 'services', :action => 'suburb'
  map.service_detail '/services/:office_id/agents', :controller => 'services', :action => 'agents'
  map.service_detail '/services/:office_id/agent_property', :controller => 'services', :action => 'agent_property'
  map.service_detail '/services/:office_id/agents/:agent_id/testimonial', :controller => 'services', :action => 'testimonial'
  map.service_detail '/services/:office_id/property_types', :controller => 'services', :action => 'property_types'
  map.service_send_property '/services/:office_id/send_property/:property_id', :controller => 'services', :action => 'send_property'
  map.service_send_team '/services/:office_id/send_team/:user_id', :controller => 'services', :action => 'send_team'
  map.service_send_office '/services/:office_id/send_office', :controller => 'services', :action => 'send_office'
  map.continue'/continue', :controller => 'sessions', :action => 'continue'
  map.success'/success', :controller => 'sessions', :action => 'success'

  #--------------------------------------------------
  #   authentication routes
  #--------------------------------------------------

  map.resources :users

  map.resource :session
  map.contact_vault '/contact_vault', :controller => 'vaults', :action => 'contact_vault'
  map.resources :residential_projects, :collection => {:view_listing => :get, :populate_combobox => :get, :auto_complete => :post}, :member => {:tweet => :post, :preview => :any, :status => :any, :update_status => :any} do |residential_project|
    residential_project.resources :children_listings, :collection => {:view_listing =>:get,:populate_combobox => :get}, :member => {:preview => :any}
    residential_project.resources :available_children_listings, :collection => {:view_listing =>:get,:populate_combobox => :get}, :member => {:preview => :any}
    residential_project.resources :sold_children_listings, :collection => {:view_listing =>:get,:populate_combobox => :get}, :member => {:preview => :any}
    residential_project.resources :on_hold_children_listings, :collection => {:view_listing =>:get,:populate_combobox => :get}, :member => {:preview => :any}
    residential_project.resources :documents
  end
  map.vault_documents '/vault_documents/:vault_id', :controller => 'vaults', :action => 'vault_documents'
  map.check_vault '/check_vault', :controller => 'vaults', :action => 'check_vault', :method => :post
  map.vault_download '/vault_download/:vault_id/:id', :controller => 'vaults', :action => 'vault_download'
  map.logout '/logout', :controller => 'sessions', :action => 'destroy'
  map.login '/login', :controller => 'sessions', :action => 'new'
  map.register '/register', :controller => 'users', :action => 'create'
  map.signup '/signup', :controller => 'users', :action => 'new'
  map.forgot_password '/forgot_password', :controller => 'users', :action => 'forgot_password'
  map.reset 'reset/:reset_code', :controller => 'users', :action => 'reset'
  map.check_domain '/check_domain', :controller => 'users', :action => 'check_domain', :method => :get
  map.check_login '/check_login', :controller => 'users', :action => 'check_login', :method => :get
  map.check_email '/check_email', :controller => 'users', :action => 'check_email', :method => :get
  map.rails_services '/rails_services', :controller => 'contact_listeners', :action => 'rails_services'

  map.get_token '/get_token', :controller => 'sessions', :action => 'get_token', :method => :post
  map.do_login '/do_login', :controller => 'sessions', :action => 'do_login'
  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller

  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"
  #map.from_plugin 'i18n_backend_database'
  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.
  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
  map.resources :google_maps
end
