require 'test/unit'
require 'rubygems'
require 'action_pack'
require 'mocha'
require 'action_controller'
require 'action_view'
require 'view_extensions'
require 'active_record'
class ActsAsCachedTest < Test::Unit::TestCase
 
  class MockActiveRecord
    attr_accessor :id
    def initialize(id)
      @id = id
    end

  end

  def setup
    @action_view = ActionView::Base.new
    @action_view.stubs(:render).returns("yoyo")
    @action_view.stubs(:logger).returns(stub_everything)
  end
  
  def test_render_object_cached
    obj = MockActiveRecord.new(1)
    MockActiveRecord.expects(:find).never
    cached_value(@action_view, 'yoyo')

    assert_equal "yoyo", @action_view.render_object(obj, 'view')
  end

  def test_render_object_uncached
    obj = MockActiveRecord.new(1)
    MockActiveRecord.expects(:find).never
    uncached_value(@action_view)
    
    assert_equal "yoyo", @action_view.render_object(obj, 'view')
  end


  def test_render_object_klass_id_cached
    obj = MockActiveRecord.new(1)
    MockActiveRecord.expects(:find).never
    cached_value(@action_view, 'yoyo')
    assert_equal "yoyo", @action_view.render_object_klass_id(MockActiveRecord, 1, 'view')
  end

  def test_render_object_klass_id_cached_uncached
    obj = MockActiveRecord.new(1)
    MockActiveRecord.expects(:find).with(1).once.returns(obj)
    uncached_value(@action_view)

    assert_equal "yoyo", @action_view.render_object_klass_id(MockActiveRecord, 1, 'view')

  end

  def test_render_object_klass_id_missing_object
    obj = MockActiveRecord.new(1)
    MockActiveRecord.expects(:find).with(1).once.raises(ActiveRecord::RecordNotFound)
    uncached_value(@action_view)

    assert_equal "", @action_view.render_object_klass_id(MockActiveRecord, 1, 'view')

  end

  def cached_value(obj, value)
    obj.class.class_eval <<-EOS
      def cache_value(*args)
         '#{value}'
      end
    EOS
  end

  def uncached_value(obj)
    obj.class.class_eval do 
      def cache_value(namespace, expire, &proc)
        proc.call
      end
    end
  end

end
