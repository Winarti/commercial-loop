module TasksHelper

  def build_monthly_date_range(start_date, end_date)
    date_range_start, date_range_end = [], []

    date_range_start << {
      :date => start_date.beginning_of_month.strftime("%d-%m-%Y"),
      :dateText => start_date.beginning_of_month.strftime("%B %Y")
    }

    date_range_end << {
      :date => start_date.end_of_month.strftime("%d-%m-%Y"),
      :dateText => start_date.end_of_month.strftime("%B %Y")
    }

    start_date.month.upto(end_date.month - 2) do |m|
      date = date_range_start.last[:date].to_date + 1.month

      date_range_start << {
        :date => date.beginning_of_month.strftime("%d-%m-%Y"),
        :dateText => date.beginning_of_month.strftime("%B %Y")
      }

      date_range_end << {
        :date => date.end_of_month.strftime("%d-%m-%Y"),
        :dateText => date.end_of_month.strftime("%B %Y")
      }
    end

    date_range_start << {
      :date => end_date.beginning_of_month.strftime("%d-%m-%Y"),
      :dateText => end_date.beginning_of_month.strftime("%B %Y")
    } unless start_date.month == end_date.month

    date_range_end << {
      :date => end_date.end_of_month.strftime("%d-%m-%Y"),
      :dateText => end_date.end_of_month.strftime("%B %Y")
    } unless start_date.month == end_date.month

    return date_range_start, date_range_end

  end
end
