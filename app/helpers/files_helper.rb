module FilesHelper

  def content_type_image(file)
    case file.content_type
    when /image/ then image_tag 'icons/document_types/JPEG32X32.png', :alt => 'Picture'
    when /msword/ then image_tag 'icons/document_types/Word32X32.png', :alt => 'Document'
    else image_tag 'icons/document_types/Message32X32.png', :alt => 'File'
    end
  end

end
