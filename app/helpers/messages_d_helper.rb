module MessagesDHelper
  def message_topic_select_options(category = nil)
    unless category
      [['Please select a category first', nil]]
    else
      MessageCategory.find(category).message_topics.map{|t| [t.name, t.id]}.unshift(['Please select', nil])
    end
  end
end
