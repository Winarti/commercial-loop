module VaultHelper
  def check_access_limitation(vc)
    return true if vc.vault.access_time.blank?
    # 0 = No Limit, 1 = 1 Day, 2 = 1 Week, 3 = 1 Month
    if vc.vault.access_time != 0
      last_update = (Time.now.utc.day + (Time.now.utc.month * 30))
      last_assign = (vc.updated_at.utc.day + (vc.updated_at.utc.month * 30))
      diff = last_update - last_assign

      allowed_to_access = false
      allowed_to_access = true if (diff <= 1) && (vc.vault.access_time == 1)
      allowed_to_access = true if (diff <= 7) && (vc.vault.access_time == 2)
      allowed_to_access = true if (diff <= 31) && (vc.vault.access_time == 3)

      return allowed_to_access
    else
      return true
    end
  end

  def vault_access(vc)
    return "active" if vc.vault.access_time.blank?
    if vc.vault.access_time != 0
       last_update = (Time.now.day + (Time.now.utc.month * 30))
       last_assign = (vc.updated_at.utc.day + (vc.updated_at.utc.month * 30))
       diff = last_update - last_assign

       allowed_to_access = "<span style='color:red;'>expired</span>"
       allowed_to_access = "active" if (diff <= 1) && (vc.vault.access_time == 1)
       allowed_to_access = "active" if (diff <= 7) && (vc.vault.access_time == 2)
       allowed_to_access = "active" if (diff <= 31) && (vc.vault.access_time == 3)
       status = allowed_to_access
     else
      status = "active"
     end
     return status
  end

  def random_id(id)
    "#{Time.now.strftime('%H%I%S%d%m%Y')}DYSDN#{Time.now.strftime('%d%m%Y%H%I%S')}54EUKkuhsydNAKSDnDYSDNS45#{id}"
  end

  def unrandom_id(scr)
    scr_arr = scr.split("54EUKkuhsydNAKSDnDYSDNS45")
    scr_arr[(scr_arr.size - 1)]
  end

  def welcome_mail_sent(agent, office, property, vault, agent_contact_id)
    str = ''
    vault_contact = VaultContact.find(:first, :conditions=>"vault_id = #{vault.id} && property_id = #{property.id} && agent_contact_id = #{agent_contact_id}")
    wmail_path = send_welcome_mail_agent_office_property_vault_path(agent, office, property, vault)+"?contact_id=#{agent_contact_id}"
    if vault_contact.present?
      if vault_contact.welcome_sent_at.present?
        str = "Welcome email sent (#{vault_contact.welcome_sent_at.strftime("%m/%d/%Y")} #{vault_contact.welcome_sent_at.strftime("%H:%M:%S")}) #{link_to("Resend welcome email",wmail_path)}"
      else
        str = "#{link_to("Send welcome email",wmail_path)}"
      end
    end
    return str
  end

end
