module OfficesHelper

  def logo_image(office, type, format = 0)
    logo = office.logos.find_by_order(type)
    if logo
      path = logo.thumbnails.blank? ? 'small_logo.jpg' : logo.thumbnails.first.public_filename
    else
      path = 'small_logo.jpg'
    end
    if format == 0
      image_tag path
    else
      image_tag(path, :size => (type == 1? "200x150" : "200x200"), :style => "border:2px solid #E1EBF5")
    end
  end

  def logo_img_path(office, type, format = 0)
    logo = office.logos.find_by_order(type)
    if logo
      path = logo.thumbnails.blank? ? 'small_logo.jpg' : logo.thumbnails.first.public_filename
    else
      path = 'small_logo.jpg'
    end
    path
  end
end
