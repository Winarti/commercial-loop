module OfficesAHelper
  def check_image(row)
    if !row.logos.blank?
      row.logos.each do |logo|
        if  !logo.blank?
          if logo.order == 1
            @logo_1 = true
          end
          if logo.order == 2
            @logo_2 = true
          end
          if logo.order == 3
            @logo_3 = true
          end
        end
      end
    end
  end

  def quick_link(url,agent,office)
    tmp = url.gsub("{subdomain}",RAILS_ENV == 'development' ? "development" : request.env["HTTP_HOST"].split(".").first) if url.include?("{subdomain}")
    tmp = tmp.gsub("{agent_id}",agent) if tmp.include?("{agent_id}")
    tmp = tmp.gsub("{office_id}",office) if tmp.include?("{office_id}")
    tmp
  end
end
