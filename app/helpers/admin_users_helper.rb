module AdminUsersHelper
  def admin_user_access_level
    [1, 2].map {|level| [AdminUser.access(level), level]}
  end
end
