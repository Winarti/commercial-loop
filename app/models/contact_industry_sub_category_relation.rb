class ContactIndustrySubCategoryRelation < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :industry_sub_category
end
