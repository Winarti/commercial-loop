class NoteNotification < ActionMailer::Base
  include ActionController::UrlWriter

  def email_reminder(user, message, contact, developer_name)
    setup_email(user)
    @subject = "Reminder! - Contact Note From: noreply@commercialloopcrm.com.au"
    @body[:contact] = contact
    @body[:message] = message
    @body[:developer_name] = developer_name
  end

  protected
  def setup_email(user, sender = "Commercial Loop")
    @recipients  = "#{user.email}"
    @from        = "#{sender} <noreply@commercialloopcrm.com.au>"
    @subject     = "[Notification] "
    @sent_on     = Time.now
    @body[:user] = user
    @content_type = "text/html"
  end
end
