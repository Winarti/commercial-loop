class ContactMailer < ActionMailer::Base
  def send_vault(property, sender, vault_template, contact)
    require "base64"
    @recipients  = contact.email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = vault_template.headline.gsub("%address%", property.address.to_s)
    @sent_on     = Time.now
    auction = ResidentialSaleDetail.find(:first,:conditions=>["residential_sale_id=?",property.id])
    auction_datetime = "#{auction.auction_time} #{auction.auction_date.strftime("%A, %-d %B, %Y")}" if auction.present? && auction.auction_date.present?
    email_layout = vault_template.content.gsub("%Name%", contact.first_name.capitalize.gsub("\\", ""))
    email_layout = email_layout.gsub("%username%", contact.username)
    email_layout = email_layout.gsub("%password%", Base64.decode64(contact.password))
    if sender.present?
      email_layout = email_layout.gsub("%agentname%", "#{sender.first_name.to_s.capitalize.gsub("\\", "")} #{sender.last_name.to_s.capitalize.gsub("\\", "")}")
      email_layout = email_layout.gsub("%office%", sender.office.name)
      email_layout = sender.phone.present? ? email_layout.gsub("%telephone%", sender.phone) : email_layout.gsub("%telephone%", "")
      email_layout = sender.mobile.present? ? email_layout.gsub("%mobile%", sender.mobile) : email_layout.gsub("%mobile%", "")
    else
      email_layout = email_layout.gsub("%agentname%", "Admin <noreply@commercialloopcrm.com.au>")
      email_layout = email_layout.gsub("%office%", '')
      email_layout = email_layout.gsub("%telephone%", '')
      email_layout = email_layout.gsub("%mobile%", '')
    end
    email_layout = email_layout.gsub("%address%", property.address.to_s)
    email_layout = email_layout.gsub("%mailsender%", "commercialloop@agentemail.com.au")
    email_layout = auction_datetime.present? ? email_layout.gsub("%auction_datetime%", auction_datetime) : email_layout.gsub("%auction_datetime%", '')
    ed = ExtraDetail.find(:first, :conditions => "property_id='#{property.id}'")
    email_layout = email_layout.gsub("%Field10%", ed.blank? ? "" : ed.field_value)
    @body[:email_layout] =  email_layout
  end

  def send_update_notice(property, sender, contact)
    @recipients  = contact.email
    @content_type = "text/html"
    @from        = (sender.blank? ? "Admin <noreply@commercialloopcrm.com.au>" : "#{sender.full_name} <#{sender.email}>")
    @subject     = "Vault files update notice. Property : #{property.address.to_s}"
    @sent_on     = Time.now
    @body[:property_address] = property.address.to_s
    @body[:name] = "#{contact.first_name} #{contact.last_name}"
  end

end
