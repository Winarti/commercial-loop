# == Schema Info
# Schema version: 20090331151240
#
# Table name: message_categories
#
#  id           :integer(4)      not null, primary key
#  developer_id :integer(4)
#  name         :string(255)
#  created_at   :datetime
#  updated_at   :datetime

class MessageCategory < ActiveRecord::Base
  validates_presence_of :name
  validates_uniqueness_of :name, :scope => :developer_id, :case_sensitive => false
  has_many :message_topics, :foreign_key => "category_id", :dependent => :delete_all
  has_many :messages, :foreign_key => "category_id"
  belongs_to :developer

  def system_category?
    self.developer.nil?
  end

  named_scope :system_categories, :conditions => { :developer_id => nil }

  def editable_by?(user)
    (self.system_category? && AdminUser === user) ||
      (DeveloperUser === user && self.developer_id == user.developer_id)
  end

  def can_delete?
    messages.size == 0
  end

  def self.per_page
    20
  end
end
