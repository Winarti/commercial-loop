# == Schema Info
# Schema version: 20090331151240
#
# Table name: messages
#
#  id                       :integer(4)      not null, primary key
#  category_id              :integer(4)
#  office_id                :integer(4)
#  sender_id                :integer(4)
#  topic_id                 :integer(4)
#  admin_unread             :boolean(1)      default(TRUE)
#  archive_state            :integer(4)      default(0)
#  message                  :text
#  message_comments_count   :integer(4)      default(0)
#  message_recipients_count :integer(4)      default(0)
#  pending_archive          :boolean(1)
#  rating                   :integer(4)      default(0)
#  title                    :string(255)
#  type                     :string(255)
#  created_at               :datetime
#  updated_at               :datetime

class Message < ActiveRecord::Base
  validates_presence_of :message
  validates_presence_of :title
  validates_presence_of :category, :message => "must be selected"

  white_list :only => [:message, :title]

  ArchiveNo         = 0
  ArchivePending    = 1
  ArchiveYes        = 2

  RatingIncomplete  = 0
  RatingPoor        = 1
  RatingReasonable  = 2
  RatingGood        = 3
  RatingExcellent   = 4

  before_create do |m|
    # assign office
    office_id = m.sender.office_id
    unless office_id
      office = m.sender.developer.offices.first
      office_id = office.id if office
    end
    m.office_id = office_id

    # add sender as recipient
    m.message_recipients.build(:user_id => m.sender_id, :unread => false)
  end

  belongs_to :sender, :class_name => "User"
  belongs_to :office
  has_many :message_comments, :dependent => :delete_all
  has_many :message_recipients, :dependent => :delete_all

  belongs_to :category, :class_name => "MessageCategory", :foreign_key => "category_id"

  has_one :file, :class_name => 'MessageFile', :as => :attachable, :dependent => :destroy
  attr_accessor :uploaded_file_data

  def save_with_file
    file = MessageFile.new
    begin
      self.transaction do
        if uploaded_file_data && uploaded_file_data.size > 0
          file.uploaded_data = uploaded_file_data
          file.thumbnails.clear
          file.save!
          self.file = file
        end
        save!
      end
    rescue
      if file.errors.on(:size)
        errors.add_to_base("Uploaded image is too big (100 MB max).")
      end
      if file.errors.on(:content_type)
        errors.add_to_base("Uploaded image content-type is not valid.")
      end
      false
    end
  end

  def unread_for_user?(user)
    message_recipients.find_by_user_id(user.id).unread
  end

  def mark_read_for!(user)
    set_read_status(user, true)
  end

  def mark_unread_for!(user)
    set_read_status(user, false)
  end

  named_scope :order_by, lambda { |field, user|
    case field
    when 'title'
      { :order => 'title asc' }
    when 'status'
      if AdminUser === user
        { :order => 'admin_unread desc' }
      else
        { :include => :message_recipients, :conditions => ['message_recipients.user_id = ?', user.id], :order => "message_recipients.unread desc" }
      end
    when 'offices'
      { :include => :office, :order => 'offices.name asc' }
    when 'from'
      { :include => :sender, :order => 'users.first_name asc' }
    else
      { :order => 'updated_at desc' }
    end
  }

  named_scope :filter_by_category, lambda { |category|
    if (category || 0).to_i > 0
      { :conditions => ['messages.category_id = ?', category.to_i] }
    else
      {}
    end
  }

  named_scope :filter_by_office, lambda { |office|
    if (office || 0).to_i > 0
      { :conditions => ['messages.office_id = ?', office.to_i] }
    else
      {}
    end
  }

  named_scope :unquote, :conditions => 'messages.quote = 0 OR messages.quote IS NULL'
  named_scope :archives, :conditions => ['archive_state != ?', Message::ArchiveNo]
  named_scope :inbox, :conditions => ['archive_state = ?', Message::ArchiveNo]
  named_scope :quote_required, :conditions => ['messages.quote = ? And messages.quote_approved = ?', 1, 0]
  named_scope :quote_approved, :conditions => ['messages.quote_approved = ?', 1]
  named_scope :inbox_with_admin_unread, :conditions => ['archive_state = ? OR admin_unread = ?', Message::ArchiveNo, true]
  named_scope :inbox_with_client_unread, :conditions => ['archive_state = ? OR `message_recipients`.unread = ?', Message::ArchiveNo, true]
  named_scope :archives_with_client_read, :conditions => ['archive_state != ? AND `message_recipients`.unread = ?', Message::ArchiveNo, false]

  def self.per_page
    15
  end

  def archive(rating, remarks, user)
    if rating == RatingIncomplete
      self.archive_state = ArchiveNo
    else
      self.archive_state = ArchiveYes
      self.rating = rating
    end
    self.add_comment(MessageComment.new(:commenter_id => user.id, :comment => (remarks.blank? ? "No Feedback Provided" : remarks), :rating => rating))
    self.save
  end

  def archived?
    self.archive_state == ArchiveYes
  end

  def pending_archive?
    self.archive_state === ArchivePending
  end

  def editable_by?(user)
    # creator can edit the message if they have not submitted a comment
    self.sender == user && self.message_comments_count == 0
  end

  def peding_archivable_by?(user)
    if self.archive_state == ArchiveNo
      # message sent from developer to agent user could be archived by sender
      if AgentMessage === self
        return self.sender == user
      else
        return self.sender != user && message_recipients.map(&:user_id).include?(user.id)
      end
    else
      return false
    end
  end

  def archivable_by?(user)
    self.sender == user
  end

  def viewable_by?(user)
    AdminUser === user || self.sender == user || message_recipients.map(&:user_id).include?(user.id)
  end

  def add_comment(comment, status = nil)
    message_comments << comment
    self.increment!(:message_comments_count)

    notify_recipients(comment, status)
  end

  def notify_recipients(comment = nil, status = nil)
    # notify when
    # 1, a new message is created
    # 2, a new comment is added
    # 3, comment is added, and rating is not zero (message archived)
    spawn(:kill => true) do
      if comment
        # developer comments on a message sent to commercial loop crm
        if AdminMessage === self && !(AdminUser === comment.commenter)
          self.update_attribute(:admin_unread, true)
        end
        if status == "internal"
          target_recipients1 = message_recipients.select { |r| r.user_id != self.sender_id }
          target_recipients = target_recipients1.select { |r| r.user_id != comment.commenter_id }
        else
          target_recipients = message_recipients.select { |r| r.user_id != comment.commenter_id }
        end

        target_recipients.each do |recipient|
          recipient.update_attribute(:unread, true)
          #TODO: delay mail notification
          MessageNotification.deliver_new_comment(recipient.user, self, comment)
        end
      else #new message
        message_recipients.each do |recipient|
          if recipient.user != self.sender
            #TODO: delay mail notification
            MessageNotification.deliver_new_message(recipient.user, self)
          end
        end
      end
    end
  end

  def assign_recipients(recipient_ids)
    recipient_ids ||= []
    old_user_ids = message_recipients.map(&:user_id)
    old_user_ids.delete(self.sender_id)
    new_user_ids = recipient_ids.map { |i| i.to_i }

    to_delete = old_user_ids - new_user_ids
    to_add = new_user_ids - old_user_ids

    MessageRecipient.transaction do
      message_recipients.all(:conditions => { :user_id => to_delete }).each(&:destroy)
      to_add.each do |id|
        message_recipients.create(:user_id => id.to_i)
      end
    end
  end

  private
  def set_read_status(user, is_read)
    if AdminUser === user
      update_attribute(:admin_unread, !is_read)
    else
      mr = message_recipients.find_by_user_id(user.id)
      mr.update_attribute(:unread, !is_read) if mr
    end
  end

end
