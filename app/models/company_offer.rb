class CompanyOffer < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :property
  attr_accessor :name, :agent_id, :office_id, :duplicate
  before_destroy :delete_relate_records
  after_create :add_viewer
  validates_presence_of :agent_company_id, :date, :detail

  def delete_relate_records
   CompanyNote.destroy_all({:id => self.company_note_id})
  end

  def add_viewer
    PropertyViewer.create({:property_id => self.property_id, :property_viewer_type_id => 9 ,:date => Time.now})
  end
end
