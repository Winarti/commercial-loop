# == Schema Info
# Schema version: 20090614051807
#
# Table name: rental_seasons
#
#  id               :integer(4)      not null, primary key
#  holiday_lease_id :integer(4)
#  end_date         :date
#  minimum_stay     :string(10)
#  season           :string(10)
#  start_date       :date

class RentalSeason < ActiveRecord::Base
  belongs_to :holiday_lease
  has_many :ordered_days
  validates_presence_of :start_date , :if => proc{|obj| obj.position == 0}
  validates_presence_of :end_date, :if => proc{|obj| obj.position == 0}
end
