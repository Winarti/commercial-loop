class CommercialBuilding < Property
  has_one :detail, :class_name => 'CommercialBuildingDetail'
  validates_associated :detail

  # validates_numericality_of :price, :greater_than => 0
  #validates_presence_of :price, :if => :sale_deal?
  #validates_presence_of :price_per, :if => :lease_deal?

#  validates_presence_of :building_name, :message =>": Building Name can't be blank"
end
