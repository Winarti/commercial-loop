class PdfConfiguration < ActiveRecord::Base
	belongs_to :office
	before_save :check_params

	def check_params
		self.heading_color = '#000000' if self.heading_color.blank?
		self.heading_font_color = '#FFFFFF' if self.heading_font_color.blank?
		self.font_family = "'arial'" if self.font_family.blank?
		self.font_color = '#000000' if self.font_color.blank?
		self.sidebar_color = '#FFFFFF' if self.sidebar_color.blank? && self.theme == "theme8"
		self.main_content_color = '#FFFFFF' if self.main_content_color.blank? && self.theme == "theme8"
	end
end
