class SalesRecord < ActiveRecord::Base
  belongs_to :property
  belongs_to :agent_contact
  # validates_presence_of :transaction_sales_person, :on => :create, :message => "can't be blank"
  validates_presence_of :date
  validates_presence_of :amount

  attr_accessor :skip_validation

  named_scope :valid, :conditions => ["sales_records.is_deleted = ?", false]

  def self.build_sorting(sort, dir)
  	case sort
  	  when 'id'
  	  	return 'sales_records.id '+dir
  	  when 'sold_date'
  	  	return 'sales_records.date '+dir
  	  when 'amount'
  	  	return 'sales_records.amount '+dir
      when 'purchaser'
        return 'agent_contacts.first_name '+dir
  	end
  end

end
