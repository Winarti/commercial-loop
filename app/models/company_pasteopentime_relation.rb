class CompanyPasteopentimeRelation < ActiveRecord::Base
  attr_accessor :agent_id, :office_id, :duplicate
  belongs_to :agent_contact
  belongs_to :past_opentime
  belongs_to :heard_from
  before_destroy :delete_relate_records
  after_create :add_viewer
  validates_presence_of :agent_contact_id, :message => " error, Contact must include a First Name and Last Name. Please try again"

  def self.build_sorting(sort,dir)
    case sort
    when 'date'
      return 'past_opentimes.date '+dir
    when 'first_name'
      return 'agent_contacts.first_name '+dir
    when 'last_name'
      return 'agent_contacts.last_name '+dir
    when 'contract'
      return 'contact_pastopentime_relations.contract '+dir
    when 'heard'
      return 'heard_froms.name '+dir
    when 'alert'
      return 'contact_pastopentime_relations.alert '+dir
    end
  end

  def delete_relate_records
    ContactNote.destroy_all({:id => self.contact_note_id})
    unless self.contact_alert_id.nil?
      ContactNote.destroy_all({:id => self.contact_alertnote_id})
      ContactAlert.destroy_all({:id => self.contact_alert_id})
    end
  end

  def add_viewer
    PropertyViewer.create({:property_id => self.past_opentime.property_id, :property_viewer_type_id => 7 ,:date => Time.now})
  end
end
