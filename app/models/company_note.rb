class CompanyNote < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :agent_user
  belongs_to :note_type, :class_name => 'NoteType', :foreign_key => "note_type_id"
  validates_presence_of :note_type_id

  def create_offer
    CompanyOffer.create(:property_id => self.property_id, :agent_company_id => self.agent_company_id, :date => self.note_date, :detail => self.description, :company_note_id => self.id, :duplicate => 0) if self.note_type_id == 7 && self.property_id != nil
  end
end
