class BrochureImage < Attachment
  has_attachment  :storage => :file_system,
                  :path_prefix => 'public/attachments/brochure_images',
                  :max_size => 10.megabytes
  validates_as_attachment
  include AttachmentHelper

  def upload_to_s3(temp_file)
    thumbnail = self.thumbnail
    s3_filename = Digest::SHA1.hexdigest(full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), ''))
    if thumbnail.blank?
      img = self
    else
      img = self.thumbnails.find(:first, :conditions => "thumbnail = '#{thumbnail.to_s}'")
    end
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(temp_file),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          sql = ActiveRecord::Base.connection()
          sql.insert "UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}"
          sql.commit_db_transaction
          return true
#          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
        rescue Exception => ex #Errno::ENOENT
          return false
          #return BASE_URL + full_filename(thumbnail).gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end
end
