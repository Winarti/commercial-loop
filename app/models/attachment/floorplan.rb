# == Schema Info
# Schema version: 20090331151240
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime
class Floorplan < Attachment
  attr_accessor :dimension
  attr_accessor :thumb
  attr_accessor :medium
  FLOORPLAN = [800,600]

  has_attachment :storage => :file_system,
    :path_prefix => 'public/attachments/images',
    :content_type => :image,
    :resize_to => :check_dimension,
    :thumbnails => {:thumb => :check_thumb  , :medium => :check_medium, :original => :check_original},
    :max_size => 1.megabyte,
    :processor => :Rmagick
  #  validates_as_attachment
  attr_accessible :order, :description, :position
  acts_as_list :scope => 'attachable_id = #{attachable_id} AND attachable_type = \'Property\' AND parent_id IS NULL'#, :column => "order"

  before_validation :attach_thumbnail
  include AttachmentHelper

  def validate_floorplan
    begin
      a = Magick::Image::read(self.temp_path.to_s).first
      errors.add_to_base("You must choose a file to upload") and return false unless self.filename
      errors.add_to_base("#{self.filename}") and return false unless /(jpeg|jpg|gif)/.match(a.format.to_s.downcase)
      return true
    rescue Exception => ex
      errors.add_to_base("#{self.filename}") and return false
    end
  end

  def check_dimension
    w,h = width,height
    with_image do |img|
      w, h = img.columns, img.rows
      img.strip!
    end

    GLOBAL_ATTR["image_dimension"] = [w,h]

    d = Array.new(2)
    d[0]  = 800
    d[1]  = (h/(w/800.to_f))
    return d
  end

  def check_thumb
    thumb = Array.new(2)
    w,h = width,height
    with_image do |img|
      w, h = img.columns, img.rows
      img.strip!
    end

    thumb = Array.new(2)
    thumb[0]  = 200
    thumb[1]  = (h/(w/200.to_f))
    return thumb
  end

  def check_medium
    medium = Array.new(2)
    w,h = width,height
    with_image do |img|
      w, h = img.columns, img.rows
      img.strip!
    end
    medium[0]  = 400
    medium[1]  = (h/(w/400.to_f))
    return medium
  end

  def check_original
    original = Array.new(2)
    original[0]  = GLOBAL_ATTR["image_dimension"][0]
    original[1]  = GLOBAL_ATTR["image_dimension"][1]
    return original
  end

  def upload_to_s3(temp_file)
    s3_filename = Digest::SHA1.hexdigest(full_filename.gsub(%r(^#{Regexp.escape(base_path)}), ''))
    img = self
    if !img.blank?
      if img.moved_to_s3 == false
        begin
          AWS::S3::DEFAULT_HOST.replace "s3-ap-southeast-2.amazonaws.com"
          AWS::S3::Base.establish_connection!(
             :access_key_id     => 'AKIAIXGPCECXGHB56OLQ',
             :secret_access_key => 'dQRmYVF4HSa8Nh8WKKvQw2gc7Z5WALS8uQ52/TUX'
          )
          AWS::S3::S3Object.store(
            s3_filename,
            open(temp_file),
            "img.commercialloopcrm.com.au",
            :access => :public_read,
            :content_type => img.content_type
          )
          ActiveRecord::Base.connection.execute("UPDATE attachments SET moved_to_s3=1 WHERE id = #{img.id}")
          return true
        rescue Exception => ex #Errno::ENOENT
          Log.create({:message => "Floorplan model UPLOAD ERROR. full_filename:#{full_filename}, base_path:#{base_path}. ERROR:#{ex.inspect}"})
          return false
#          return BASE_URL + full_filename.gsub(%r(^#{Regexp.escape(base_path)}), '')
        end
      end
    end
  end

  private

  def attach_thumbnail
    self.attachable = self.parent.attachable if attribute_present?('thumbnail')
  end



end
