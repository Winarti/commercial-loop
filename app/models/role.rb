# == Schema Info
# Schema version: 20090614051807
#
# Table name: roles
#
#  id   :integer(4)      not null, primary key
#  name :string(255)

class Role < ActiveRecord::Base

  has_and_belongs_to_many :users

  def self.access_levels
    result = []
    (1..3).each{|i| result << Role.find_by_name("LEVEL #{i}")}
    result
  end

end
