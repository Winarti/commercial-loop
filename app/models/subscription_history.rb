# == Schema Info
# Schema version: 20090614051807
#
# Table name: subscription_histories
#
#  id                :integer(4)      not null, primary key
#  payer_id          :string(255)
#  subscription_id   :integer(4)
#  amount            :string(255)
#  currency_code     :string(255)
#  gateway_reference :string(255)
#  payer_email       :text
#  profile_status    :string(255)
#  txn_type          :string(255)
#  created_at        :datetime
#  updated_at        :datetime

class SubscriptionHistory < ActiveRecord::Base
  belongs_to :subscription
  validates_presence_of :gateway_reference, :profile_status
  validates_associated :subscription

  default_scope :order => "created_at DESC"
  named_scope :payments, :conditions => ["txn_type = ?", "recurring_payment" ]

  # txn_types:
  # recurring_payment
  # recurring_payment_profile_created
  # recurring_payment_profile_cancel
  def self.new_from_notification(notify, recurring_payment_id)
    profile = RecurringPaymentProfile.find_by_gateway_reference(recurring_payment_id)
    unless profile.blank?
      begin
        subscription = profile.subscriptions.first #could optimise this
        sh = self.new
        sh.subscription_id = subscription.id
        sh.gateway_reference = recurring_payment_id
        sh.txn_type = notify.type
        sh.txn_id = notify.transaction_id
        #%w(txn_type payer_email profile_status payer_id amount currency_code).each do |attribute|
        #  eval("sh.#{attribute} = notify.#{attribute}")   rescue false
        #end
        return sh
      rescue Exception => ex
        return nil
      end
    end
  end
  #<ActiveMerchant::Billing::PaypalExpressResponse:0x604ec2c @test=true, @authorization=nil,
  #@params={"timestamp"=>"2009-04-25T07:32:45Z", "correlation_id"=>"c64ac6a066664", "build"=>"882966",
  # "version"=>"52.0", "ack"=>"Success", "profile_id"=>"I-UYRTLD4W94LD"}, @success=true, @cvv_result={"message"=>nil, "code"=>nil},
  # @fraud_review=false, @message="Success", @avs_result={"message"=>nil, "code"=>nil, "street_match"=>nil, "postal_match"=>nil}>
  def self.new_cancel_history(profile_id)
    sh = self.new
    profile = RecurringPaymentProfile.find_by_gateway_reference(profile_id)
    subscription = profile.subscriptions.first #could optimise this

    sh.subscription_id = subscription.id
    sh.gateway_reference = profile_id
    sh.txn_type = "recurring_payment_profile_cancel"
    sh.profile_status = "Cancel"

    return sh
  end

  def payment?
    txn_type == "recurring_payment"
  end

  def action
    case txn_type
    when "recurring_payment"
      "paid"
    when "recurring_payment_profile_created"
      "subscribed"
    when "recurring_payment_profile_cancel"
      "canceled"
    else
      "unknown"
    end
  end

end
