class Log < ActiveRecord::Base
  def self.logger_reboot
    Log.create(:message => "Whenever log reboot at (#{Time.zone.now})")
  end

  def self.general_log(message)
    Log.create(:message => message)
  end
end
