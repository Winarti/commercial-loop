# == Schema Info
# Schema version: 20090614051807
#
# Table name: agents
#
#  id                   :integer(4)      not null, primary key
#  developer_contact_id :integer(4)
#  developer_id         :integer(4)
#  support_contact_id   :integer(4)
#  upgrade_contact_id   :integer(4)
#  access_key           :string(255)
#  afterhrs_phone       :string(255)
#  business_phone       :string(255)
#  code                 :string(255)
#  country              :string(255)
#  email                :string(255)
#  fax_number           :string(255)
#  holder_email         :string(255)
#  holder_fax           :string(255)
#  holder_first_name    :string(255)
#  holder_last_name     :string(255)
#  holder_mobile        :string(255)
#  holder_phone         :string(255)
#  mobile               :string(255)
#  name                 :string(255)
#  private_key          :string(255)
#  state                :string(255)
#  status               :string(255)
#  street               :string(255)
#  street_number        :string(255)
#  street_type          :string(255)
#  suburb               :string(255)
#  unit_number          :string(255)
#  website              :string(255)
#  zipcode              :string(10)
#  created_at           :datetime
#  deleted_at           :datetime
#  updated_at           :datetime

require 'digest/sha1'
class Agent < ActiveRecord::Base
  define_index do
    indexes :name
  end

  has_many :searches
  has_many :offices
  has_many :active_offices, :class_name => 'Office', :conditions => "status = 'active'"
  has_many :inactive_offices, :class_name => 'Office', :conditions => "status = 'inactive'"
  has_many :pending_offices, :class_name => 'Office', :conditions => "status = 'pending'"
  has_many :suspended_offices, :class_name => 'Office', :conditions => "status = 'suspended'"
  has_many :agent_users, :through => :offices
  belongs_to :developer
  belongs_to :developer_contact, :class_name => 'DeveloperUser'
  belongs_to :support_contact, :class_name => 'DeveloperUser'
  belongs_to :upgrade_contact, :class_name => 'DeveloperUser'

  has_many :properties, :finder_sql => "SELECT properties.* FROM `properties` INNER JOIN `users` ON `properties`.agent_user_id = `users`.id INNER JOIN `offices` ON `users`.office_id = `offices`.id WHERE (((offices.agent_id = \#{id}) AND (users.deleted_at IS NULL OR users.deleted_at > now()) AND (( (`users`.`type` = 'AgentUser' ) ))) AND (properties.deleted_at IS NULL OR properties.deleted_at > now()));"

  has_many :agent_developer_accesses
  has_many :developer_accesses, :through => :agent_developer_accesses, :source => :developer_user

  has_many :agent_contacts
  has_many :agent_companies
  has_many :match_properties, :dependent => :destroy
  has_many :email_configuration_searches, :dependent => :destroy
   validates_presence_of :developer_id, :status, :developer_contact_id, :support_contact_id, :upgrade_contact_id, :name, :country
  validates_uniqueness_of :code, :name, :scope => :developer_id
  validates_uniqueness_of :access_key
  validates_format_of :code,
    :with => /^\d{5}$/i,
    :message => "agent must have a 5 digits code"

  before_validation_on_create :generate_random_code
  before_validation_on_create :generate_access_keys
  before_validation :set_status

  after_save :update_offices_status, :if => :status_changed? #built in dirty? method from Rails http://caboo.se/doc/classes/ActiveRecord/Dirty.html

  STATUS = ["active", "suspended"]

  named_scope :active, :conditions => ['agents.status = "active"']
  named_scope :inactive, :conditions => ['agents.status = "inactive"']
  named_scope :pending, :conditions => ['agents.status = "pending"']
  named_scope :suspended, :conditions => ['agents.status = "suspended"']

  acts_as_paranoid

  extend ActiveSupport::Memoizable

  def delete_agent
    self.deleted_at = Time.now
    if self.save_with_validation(false)
      offices.each do |office|
        office.delete_office
      end
    end
  end

  def full_code
    "#{developer.full_code}-#{code}"
  end

  def head_office
    offices.find_by_head true
  end

  def holder
    @holder = Contact.new :first_name => holder_first_name, :last_name => holder_last_name, :email => holder_email, :phone_number => :holder_phone, :mobile => holder_mobile, :fax_number => holder_fax
  end
  memoize :holder

  def holder=(h)
    self.holder_first_name = h.first_name
    self.holder_last_name = h.last_name
    self.holder_email = h.email
    self.holder_phone = h.phone_number
    self.holder_mobile = h.mobile
    self.holder_fax = h.fax_number
  end

  # composed_of is useless :(
  def contact
    @contact = Contact.new :country => country, :state => state, :suburb => suburb, :street_type => street_type, :street => street, :street_number => street_number, :unit_number => unit_number, :fax_number => fax_number, :phone_number => business_phone, :afterhrs_phone => afterhrs_phone, :email => email, :mobile => mobile
  end
  memoize :contact

  def contact=(ct)
    self.country = ct.country
    self.state = ct.state
    self.suburb = ct.suburb
    self.street_type = ct.street_type
    self.street_number = ct.street_number
    self.street = ct.street
    self.unit_number = ct.unit_number
    self.business_phone = ct.phone_number
    self.fax_number = ct.fax_number
    self.email = ct.email
    self.afterhrs_phone = ct.afterhrs_phone
    self.mobile = ct.mobile
  end

  # over-write offices developer access settings
  def apply_to_offices
    offices.each do |office|
      office.apply_agent_settings
      office.save
    end
  end

  def accessible_by?(user)
    case user
    when AdminUser
      true
    when DeveloperUser
      user.developer.agents.find(self.id)
    when AgentUser
      user.office.agent == self
    else
      false
    end
  end


  private


  def set_status
    if status.blank?
      self.status = 'active'
    end
  end

  def generate_random_code
    return true unless code.blank?
    begin
      self.code = "%05d" % (rand*100000)
    end while Agent.find_by_developer_id_and_code(developer_id, code)
  end

  def generate_access_keys
    return true unless access_key.blank?
    begin
      self.access_key = Digest::SHA1.hexdigest("#{self.code}-#{Time.now}-#{self.developer_id}")
      self.private_key = Digest::SHA1.hexdigest(self.access_key)
    end
  end

  def update_offices_status
    if status != "active" && status_changed?
      self.offices.update_all(:status => self.status)
    end
  end


end
