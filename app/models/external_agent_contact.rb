class ExternalAgentContact < AgentContact
  has_many :properties

  def full_name
    "#{first_name} #{last_name}"
  end
end