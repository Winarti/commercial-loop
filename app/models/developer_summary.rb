# == Schema Info
# Schema version: 20090614051807
#
# Table name: developer_summaries
#
#  id           :integer(4)      not null, primary key
#  developer_id :integer(4)
#  client_a     :integer(4)
#  client_i     :integer(4)
#  client_s     :integer(4)
#  country      :string(255)
#  listing_a    :integer(4)
#  listing_i    :integer(4)
#  office_a     :integer(4)
#  office_i     :integer(4)
#  office_p     :integer(4)
#  office_s     :integer(4)
#  team         :integer(4)
#  created_at   :datetime
#  updated_at   :datetime

class DeveloperSummary < ActiveRecord::Base
  belongs_to :developer

  def client_t
    client_a + client_i + client_s
  end

  def office_t
    office_a + office_i + office_p + office_s
  end

  def listing_t
    listing_a + listing_i
  end

  named_scope :countries, :conditions => ["country <> ?", "Total"], :order => "country asc"
  named_scope :total, :conditions => { :country => "Total" }
end
