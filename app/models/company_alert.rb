class CompanyAlert < ActiveRecord::Base
  belongs_to :agent_company
  has_many :company_alert_suburbs
  validates_presence_of :alert_type
  validates_numericality_of :min_price, :max_price, :allow_nil => true

  def add_suburbs(ids)
    ids ||= []
    old_ids = company_alert_suburbs.map(&:suburb)
    new_ids = ids

    to_delete = old_ids - new_ids
    to_add = new_ids - old_ids

    CompanyAlertSuburb.transaction do
      company_alert_suburbs.all(:conditions => { :suburb => to_delete }).each(&:destroy)
      to_add.each do |id|
        company_alert_suburbs.create(:suburb => id) unless id.blank?
      end
    end
  end
end
