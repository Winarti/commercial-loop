class CompanyAssignedRelation < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :agent_user
  belongs_to :assigned, :class_name => 'AgentUser', :foreign_key => "assigned_to"
end
