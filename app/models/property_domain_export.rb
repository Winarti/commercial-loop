class PropertyDomainExport < ActiveRecord::Base
  belongs_to :property
  belongs_to :domain

  def self.synchronize_domain_export(p)
    unless p.blank?
      unless p.office.blank?
        domains = p.office.domains
        unless domains.blank?
          domains.each do |domain|
            property_domain_export = PropertyDomainExport.find_by_property_id_and_domain_id(p.id,domain.id)
            PropertyDomainExport.create(:property_id => p.id, :domain_id => domain.id, :send_api => domain.enable_export) if property_domain_export.blank?
          end
        end
      end
    end
  end
end
