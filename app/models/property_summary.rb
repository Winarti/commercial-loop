# == Schema Info
# Schema version: 20090614051807
#
# Table name: property_summaries
#
#  id          :integer(4)      not null, primary key
#  client_a    :integer(4)
#  client_i    :integer(4)
#  client_s    :integer(4)
#  country     :string(255)
#  developer_a :integer(4)
#  developer_i :integer(4)
#  developer_s :integer(4)
#  listing_a   :integer(4)
#  listing_i   :integer(4)
#  office_a    :integer(4)
#  office_i    :integer(4)
#  office_p    :integer(4)
#  office_s    :integer(4)
#  team        :integer(4)
#  created_at  :datetime
#  updated_at  :datetime

class PropertySummary < ActiveRecord::Base

  def developer_t
    developer_a + developer_i + developer_s
  end

  def client_t
    client_a + client_i + client_s
  end

  def office_t
    office_a + office_i + office_p + office_s
  end

  def listing_t
    listing_a + listing_i
  end

  def self.generate_summary
    countries = Agent.all(:order => "agents.country asc", :group => "agents.country", :select => "agents.country").map(&:country)
    stats = {}
    total = {:developer_a => 0, :developer_i => 0, :developer_s => 0, :client_a => 0, :client_i => 0, :client_s => 0, :office_a => 0, :office_i => 0, :office_p => 0, :office_s => 0, :team => 0, :listing_a => 0, :listing_i => 0}
    countries.each do |country|
      s = {}
      developers = Developer.scoped_by_country(country)
      total[:developer_a] += (s[:developer_a] = developers.active.count)
      total[:developer_i] += (s[:developer_i] = developers.inactive.count)
      total[:developer_s] += (s[:developer_s] = developers.suspended.count)


      clients = Agent.scoped_by_country(country)
      total[:client_a] += (s[:client_a] = clients.active.count)
      total[:client_i] += (s[:client_i] = clients.inactive.count)
      total[:client_s] += (s[:client_s] = clients.suspended.count)

      the_offices = Office.scoped_by_country(country)
      total[:office_a] += (s[:office_a] = the_offices.active.count)
      total[:office_i] += (s[:office_i] = the_offices.inactive.count)
      total[:office_p] += (s[:office_p] = the_offices.pending.count)
      total[:office_s] += (s[:office_s] = the_offices.suspended.count)

      total[:team] += (s[:team] = AgentUser.count(:conditions => { :office_id => the_offices.map(&:id) }))

      al = Property.all(:conditions => { :status => Property::STATUS[:available], :country => country }).size
      total[:listing_a] += (s[:listing_a] = al)
      total[:listing_i] += (s[:listing_i] = Property.scoped_by_country(country).count - al)
      stats[country] = s
    end

    PropertySummary.transaction do
      PropertySummary.delete_all
      stats.each do |c, s|
        PropertySummary.create(s.merge(:country => c))
      end
      PropertySummary.create(total.merge(:country => "Total"))
    end
  end

  named_scope :countries, :conditions => ["country <> ?", "Total"], :order => "country asc"
  named_scope :total, :conditions => { :country => "Total" }
end
