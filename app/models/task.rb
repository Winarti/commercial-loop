class Task < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :agent_user
  belongs_to :task_type
  belongs_to :property
  belongs_to :office
  belongs_to :tenancy_record

  validates_presence_of :task_type_id

  def create_offer
    Offer.create(:property_id => self.property_id, :agent_contact_id => self.agent_contact_id, :date => self.task_date, :detail => self.description, :task_id => self.id, :duplicate => 0) if self.task_type_id == 7 && self.property_id != nil
  end

  def as_json
    {
      :id => self.id,
      :agent_contact => (self.agent_contact.full_name if self.agent_contact.present?),
      :agent_user => (self.agent_user.full_name if self.agent_user.present?),
      :task_type => self.task_type.name,
      :address => self.property.address_fix,
      :description => self.description,
      :task_date => self.task_date.strftime("%d-%m-%Y"),
      :email_reminder => self.email_reminder ? "Yes" : "No",
      :property_id => self.property_id,
      :status => self.completed ? "Completed" : "Incomplete",
      :completed => self.completed
    }
  end


  # Class Method
  def self.build_sorting(sort, dir)
    case sort
    when "id"
      return "tasks.id #{dir}"
    when "agent_contact"
      return "agent_contacts.first_name #{dir}, agent_contacts.last_name #{dir}"
    when "agent_user"
      return "users.first_name #{dir}, users.last_name #{dir}"
    when "task_type"
      return "task_types.name #{dir}"
    when "address"
      return "tasks.address #{dir}"
    when "description"
      return "tasks.description #{dir}"
    when "task_date"
      return "tasks.task_date #{dir}"
    when "email_reminder"
      return "tasks.email_reminder #{dir}"
    when "office"
      return "offices.name #{dir}"
    when "status"
      return "tasks.completed #{dir}"
    end
  end

end
