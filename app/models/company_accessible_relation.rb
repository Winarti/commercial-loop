class CompanyAccessibleRelation < ActiveRecord::Base
  belongs_to :agent_company
  belongs_to :accessible, :class_name => 'AgentUser', :foreign_key => "accessible_by"
end
