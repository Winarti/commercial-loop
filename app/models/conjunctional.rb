class Conjunctional < ActiveRecord::Base
  has_many :agent_users
  validates_uniqueness_of :conjunctional_rea_agent_id
end
