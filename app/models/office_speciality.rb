class OfficeSpeciality < ActiveRecord::Base
  has_many   :office_specialities_relations
  belongs_to :office
  validates_presence_of :name
end
