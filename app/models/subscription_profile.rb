# == Schema Info
# Schema version: 20090614051807
#
# Table name: subscription_profiles
#
#  id                           :integer(4)      not null, primary key
#  recurring_payment_profile_id :integer(4)      not null
#  subscription_id              :integer(4)      not null
#  created_at                   :datetime        not null

class SubscriptionProfile < ActiveRecord::Base
  belongs_to :subscription
  belongs_to :recurring_payment_profile
end
