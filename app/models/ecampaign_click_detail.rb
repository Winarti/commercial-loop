class EcampaignClickDetail < ActiveRecord::Base
  belongs_to :agent_contact

  def count_click
    ecampaign_detail = EcampaignClickDetail.find(:all, :conditions=> "`agent_contact_id`=#{self.agent_contact_id} And `ecampaign_click_id`=#{self.ecampaign_click_id}")
    return ecampaign_detail.size
  end
end
