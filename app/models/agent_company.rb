class AgentCompany < ActiveRecord::Base
  require "savon"
  extend Savon::Model
  belongs_to :agent
  belongs_to :agent_user
  belongs_to :category_company
  belongs_to :industry_sub_category
  belongs_to :industry_category
  belongs_to :assigned, :class_name => 'AgentUser', :foreign_key => "assigned_to"
  belongs_to :accessible, :class_name => 'AgentUser', :foreign_key => "accessible_by"
  belongs_to :office
  has_many :company_alerts, :dependent => :destroy
  has_many :company_notes, :dependent => :destroy
  has_many :property_company_enquiries
  has_many :company_category_relations, :dependent => :destroy
  has_many :company_property_type_relations, :dependent => :destroy
  has_many :company_assigned_relations, :dependent => :destroy
  has_many :company_accessible_relations, :dependent => :destroy
  has_many :properties
  has_many :agent_contacts
  has_many :company_offers
  has_many :group_company_relations, :dependent => :destroy
  has_many :company_contact_relations
  belongs_to :heard_from
  belongs_to :creator, :class_name => "AgentUser", :foreign_key => "creator_id"
  attr_accessor :skip_validation,:categories_id, :industry_sub_categories_id, :assigns_id, :check_error, :contract, :alert, :boom_status, :group_id, :ecampaign_contact, :property_types_id
  before_create :check_user
  validates_presence_of :company
  validate :validate_username, :validate_password, :if => proc{|x|x.skip_validation != true}

  def self.authenticate(login,password)
    return nil if login.blank? || password.blank?
    require "base64"
    ag = AgentCompany.find(:first, :conditions => { :username => login, :password => Base64.encode64(password)})
  end

  def self.build_sorting(sort,dir)
    case sort
    when 'id'
      return 'agent_companies.id '+dir
    when 'category'
      return 'category_companies.name '+dir
    when 'first_name'
      return 'agent_companies.company '+dir
    when 'last_name'
      return 'agent_companies.last_name '+dir
    when 'assigned_to'
      return 'users.first_name '+dir
    when 'accessible_by'
      return 'users.first_name '+dir
    when 'updated_at'
      return 'agent_companies.updated_at '+dir
    end
  end

  def validate_email
    if self.office.present? && ((self.office.empty_contact_email.blank? || !self.office.empty_contact_email))
      if email.blank?
        errors.add(:email, "can't be blank")
        false
      else
        unless email =~ Authentication.email_regex
          errors.add(:email, Authentication.bad_email_message)
          false
        end
      end
    end
  end

  def validate_password
    if login_required == true
      if id.blank?
        if password.blank?
          errors.add(:password, "can't be blank")
          false
        end
      end
    end
  end

  def validate_username
    if !username.blank?
      u = User.find(:first, :conditions => "login = '#{username}'")
      if !u.blank?
        errors.add(:username, "already exist")
        false
      else
        ac = AgentCompany.find(:first, :conditions => "username = '#{username}'")
        unless ac.blank?
          current_contact = AgentCompany.find(id)
          if current_contact.username != username
            errors.add(:username, "already exist")
            false
          end
        end
      end
    else
      if login_required == true
        errors.add(:username, "can't be blank")
        false
      end
    end
  end

  def check_user
    duplicate = 0
    duplicate = check_exist_model("email") unless email.blank?
    (duplicate = check_exist_model("contact_number") unless duplicate == 1) unless contact_number.blank?
    return unless duplicate == 1
    errors.add(:check_error, "duplicate")
    false
  end

  def check_exist_model(type)
    conditions_string = "`office_id` = :office_id and `company` = :company"
    conditions_values = {:office_id => office_id, :company => company}

    case type
      when 'email'
       conditions_string << "  and `email`= :email"
       conditions_values[:email] = email
      when 'contact_number'
       conditions_string << "  and `contact_number`= :contact_number"
       conditions_values[:contact_number] = contact_number
      when 'home_number'
       conditions_string << "  and `home_number`= :home_number"
       conditions_values[:home_number] = home_number
    end

    check = AgentCompany.find(:first, :conditions => [conditions_string, conditions_values])
    return check.nil? ? 0 : 1
  end

  def add_property_types(ids, status = nil)
    ids ||= []
    old_ids = company_property_type_relations.map(&:property_type_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    CompanyPropertyTypeRelation.transaction do
      company_property_type_relations.all(:conditions => { :property_type_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        company_property_type_relations.create(:property_type_id => id.to_i) unless id.to_i == 0
      end
    end
    self.property_type_id = company_property_type_relations.first.nil? ? nil : company_property_type_relations.first.property_type_id
    self.save
  end

  def add_categories(ids, status = nil)
    ids ||= []
    old_ids = company_category_relations.map(&:category_company_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    CompanyCategoryRelation.transaction do
      company_category_relations.all(:conditions => { :category_company_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        company_category_relations.create(:category_company_id => id.to_i) unless id.to_i == 0
      end
    end
    self.category_company_id = company_category_relations.first.nil? ? nil : company_category_relations.first.category_company_id
    self.save
  end

  def add_groups(ids, status = nil)
    ids ||= []
    old_ids = group_company_relations.map(&:group_company_id)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    GroupCompanyRelation.transaction do
      group_company_relations.all(:conditions => { :group_company_id => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        group_company_relations.create(:group_company_id => id.to_i) unless id.to_i == 0
      end
    end
  end

  def add_assigns(ids, status = nil)
    ids ||= []
    old_ids = company_assigned_relations.map(&:assigned_to)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    ContactAssignedRelation.transaction do
      company_assigned_relations.all(:conditions => { :assigned_to => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        company_assigned_relations.create(:assigned_to => id.to_i) unless id.to_i == 0
      end
    end
    self.assigned_to = company_assigned_relations.first.nil? ? nil : company_assigned_relations.first.assigned_to
    self.save
  end

  def add_access(ids, status = nil)
    ids ||= []
    old_ids = company_accessible_relations.map(&:accessible_by)
    new_ids = ids.map { |i| i.to_i }

    to_delete = old_ids - new_ids if status.nil?
    to_add = new_ids - old_ids

    CompanyAccessibleRelation.transaction do
      company_accessible_relations.all(:conditions => { :accessible_by => to_delete }).each(&:destroy) if status.nil?
      to_add.each do |id|
        company_accessible_relations.create(:accessible_by => id.to_i) unless id.to_i == 0
      end
    end
    self.accessible_by = company_accessible_relations.first.nil? ? nil : company_accessible_relations.first.accessible_by
    self.save
  end

  def self.check_exist(param, type)
    conditions_string = "`office_id` = :office_id and `company` = :company"
    conditions_values = { :office_id => param[:office_id], :company => param[:company]}

    case type
      when 'email'
       conditions_string << "  and `email`= :email"
       conditions_values[:email] = param[:email]
      when 'contact_number'
       conditions_string << "  and `contact_number`= :contact_number"
       conditions_values[:contact_number] = param[:contact_number]
    end

    check = AgentCompany.find(:first, :conditions => [conditions_string, conditions_values])
    return check
  end

  def create_note(id, prenote, note_type_id, current_id)
    property = Property.find_by_id(id)
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:agent_company_id => self.id,:agent_user_id => current_id, :note_type_id => note_type_id,
            :address => address,:description => prenote, :note_date => Time.now, :property_id => id}
    company_note = CompanyNote.new(note)
    return company_note.id if company_note.save
  end

  def create_alert(id, current_id, palert_type = nil)
    property = Property.find_by_id(id)
    alert_id = alertnote_id = ""
    bedroom = (property.detail.bedrooms.nil? ? "" : property.detail.bedrooms) if property.detail.respond_to?(:bedrooms)
    alert_type = palert_type.nil? ? "Newly Listed Properties" : palert_type
    note = {:agent_company_id => self.id, :alert_type => alert_type, :listing_type => property.type,
            :property_type => property.property_type, :min_bedroom => bedroom, :min_bathroom => "",
            :min_carspace => "", :min_price => "", :max_price => property.price, :suburb => ""}
    company_alert = CompanyAlert.new(note)
    if company_alert.save
      company_alert.add_suburbs([property.suburb]) unless property.suburb.nil?
      alert_id = company_alert.id
      if alert_type == 'Latest News'
         prenote = "Alert Type = #{alert_type}"
      else
         prenote = "Alert Type = #{alert_type}, Type = #{property.type}, Property Type = #{property.property_type}, Min bedrooms  = #{bedroom}, Max price  = #{property.price}, Suburb = #{property.suburb}"
      end
      #note type = submit alert
      alertnote_id = self.create_note(id, prenote, 4, current_id)
    end
    return {:alert_id => alert_id, :alert_note_id=> alertnote_id}
  end

  def create_note_for_property(id, prenote, note_type_id, heard_form_id, current_id)
    property = Property.find_by_id(id)
    description = (prenote == "")? "Attend Open Inspection" : prenote
    ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
    address = [ad, property.suburb ].join(", ")
    note = {:property_id => property.id, :agent_company_id => self.id, :agent_user_id => current_id,
            :note_type_id => note_type_id,:address => address, :description => description, :note_date => Time.now, :heard_from_id => heard_form_id, :duplicate => 0}
    property_note = PropertyNote.new(note)
    return property_note.id if property_note.save
  end

  def create_note_for_company(type, pnote, current_id, address = "")
    note = {:agent_company_id => self.id,:agent_user_id => current_id, :note_type_id => type,
            :address => address, :description => pnote, :note_date => Time.now}
    company_note = CompanyNote.new(note)
    return company_note.id if company_note.save
  end

  def create_alert_for_company(param, current_id, created_from = nil)
    alert_type = (param[:alert_type] == "")? "Latest News" : param[:alert_type]
    note = {:agent_company_id => self.id, :alert_type => alert_type,
              :listing_type => param[:listing_type], :property_type => param[:property_type], :min_bedroom => param[:min_bedroom],
              :min_bathroom => param[:min_bathroom], :min_carspace => param[:min_carspace], :min_price => param[:min_price], :max_price => param[:max_price],
              :suburb => ""}
    company_alert = CompanyAlert.new(note)
    if company_alert.save
      company_alert.add_suburbs(param[:suburbs].split(',')) unless param[:suburbs].blank?
      alert_id = company_alert.id
      prenote = "Alert Type = #{alert_type},"
      prenote += "Listing Type = #{param[:listing_type]}," unless param[:listing_type].blank?
      prenote +="Property Type = #{param[:property_type]}," unless param[:property_type].blank?
      prenote += "Min Bedroom = #{param[:min_bedroom]}," unless param[:min_bedroom].blank?
      prenote += "Min Bathroom = #{param[:min_bathroom]}," unless param[:min_bathroom].blank?
      prenote += "Min carspace = #{param[:min_carspace]}," unless param[:min_carspace].blank?
      prenote += "Min Price = #{param[:min_price]}," unless param[:min_price].blank?
      prenote += "Max Price = #{param[:max_price]}," unless param[:max_price].blank?
      prenote += "Suburbs = #{param[:suburb]}," unless param[:suburb].blank?

      #note type = alert sent
      alertnote_id = self.create_note_for_company((created_from.nil? ? 1 : 4), prenote, current_id)
    end
    return {:alert_id => alert_id, :alert_note_id=> alertnote_id}
  end

  def update_category_company(category_id)
    old_ids = company_category_relations.map(&:category_company_id)
    company_category_relations.create(:category_company_id => category_id.to_i) unless old_ids.include?(category_id)
  end

  def update_property_type(property_type_id)
    old_ids = company_property_type_relations.map(&:property_type_id)
    company_property_type_relations.create(:property_type_id => property_type_id.to_i) unless old_ids.include?(property_type_id)
  end

  def self.search_for_autocomplete(company, agent_id, office_id)
    AgentCompany.find(:all, :conditions => ["(`company LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%'+company+'%', agent_id, office_id])
  end

  def use_spawn_for_boom
    spawn(:kill => true) do
      self.send_boom_api
    end
  end

  def use_spawn_for_md
    spawn(:kill => true) do
      sleep 30
      self.send_to_mydesktop
    end
  end

  def use_spawn_for_irealty
    spawn(:kill => true) do
      sleep 6
      self.send_to_irealty
    end
  end

  def update_first_list
    self.category_company_id = company_category_relations.first.nil? ? nil : company_category_relations.first.category_company_id
    self.property_type_id = company_property_type_relations.first.nil? ? nil : company_property_type_relations.first.property_type_id
    self.assigned_to = company_assigned_relations.first.nil? ? nil : company_assigned_relations.first.assigned_to
    self.accessible_by = company_accessible_relations.first.nil? ? nil : company_accessible_relations.first.accessible_by
    self.save
  end

end
