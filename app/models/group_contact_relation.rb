class GroupContactRelation < ActiveRecord::Base
  belongs_to :agent_contact
  belongs_to :group_contact
end
