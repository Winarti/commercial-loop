class TaskType < ActiveRecord::Base
  has_many :tasks, :dependent => :destroy
  belongs_to :office

  validates_presence_of :name
end
