# Table name: developer_quicklink
#
#  id         :integer(4)      not null, primary key
#  data       :text
#  created_at :datetime
#  updated_at :datetime

class DeveloperQuicklink < ActiveRecord::Base
  serialize :data

  def self.first
    self.find_or_create_by_id(1)
  end

  # to retreive data[:residential_sale]
  def data
    read_attribute("data")
  end

  def build_data(quicklink_data_key, quicklink_data)
    self.data = {}
    quicklink_data_key.each_with_index do |k,i|
      self.data[k] = quicklink_data[i]
    end
  end

end
