require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
begin
xml.property_detail do

  xml.tag!(:headline, @property.headline)
  xml.tag!(:suburb, @property.suburb)
  xml.tag!(:description, @property.description)
  xml.tag!(:latitude, @property.latitude)
  xml.tag!(:longitude, @property.longitude)
  xml.tag!(:price, @property.price)
  xml.tag!(:to_price, @property.to_price)
  xml.tag!(:status, Property::STATUS_NAME[@property.status.to_i - 1])
  xml.tag!(:display_price, @property.display_price)
  xml.tag!(:display_price_text, @property.display_price_text)
  xml.tag!(:property_type, @property.property_type)
  xml.tag!(:address, "#{@property.unit_number.blank? ? '' : @property.unit_number + '/'}#{@property.street_number} #{@property.street}")
  xml.tag!(:type, @property.type)
  xml.tag!(:deal_type, @property.deal_type)
  xml.tag!(:sell_type, ["ResidentialLease", "HolidayLease"].include?(@property.type.to_s) ? "Lease" : (("Commercial" == @property.type.to_s) ? @property.deal_type : "Sale"))
  xml.tag!(:updated_at, @property.updated_at)
  xml.tag!(:id_property, @property.id)
  case @property.type.to_s.underscore
  when 'business_sale'
    xml.tag!(:bedrooms, nil)
    xml.tag!(:bathrooms, nil)
    xml.tag!(:carspaces, nil)
    xml.tag!(:date_available, nil)
  when 'commercial'
    xml.tag!(:bedrooms, nil)
    xml.tag!(:bathrooms, nil)
    xml.tag!(:carspaces, nil)
    xml.tag!(:date_available, @property.detail.date_available)
  when 'residential_lease'
    xml.tag!(:bedrooms, @property.detail.bedrooms)
    xml.tag!(:bathrooms, @property.detail.bathrooms)
    xml.tag!(:carspaces, @property.detail.carport_spaces)
    xml.tag!(:date_available, @property.detail.date_available)
  when 'new_development'
    xml.tag!(:bedrooms, nil)
    xml.tag!(:bathrooms, nil)
    xml.tag!(:carspaces, nil)
    xml.tag!(:date_available, nil)
  else
    xml.tag!(:bedrooms, @property.detail.bedrooms)
    xml.tag!(:bathrooms, @property.detail.bathrooms)
    xml.tag!(:carspaces, @property.detail.carport_spaces)
    xml.tag!(:date_available, nil)
  end


  #  unless @property.agent_user.blank?
  #    xml.tag!(:id_agent, @property.agent_user.id)
  #    xml.tag!(:email_agent, @property.agent_user.email)
  #    xml.tag!(:name_agent, @property.agent_user.full_name)
  #    xml.tag!(:phone_agent, @property.agent_user.phone)
  #    xml.tag!(:mobile_agent, @property.agent_user.mobile)
  #    xml.tag!(:fax_agent, @property.agent_user.fax)
  #    xml.tag!(:description_agent, @property.agent_user.description)
  #    xml.tag!(:photo_agent,@property.agent_user.portrait_image.blank? ? nil : @property.agent_user.portrait_image.first.public_filename(:thumb))
  #  end

  xml.contacts do

    unless @property.primary_contact.blank?
      xml.contact do
        xml.tag!(:id, @property.primary_contact.id)
        xml.tag!(:email, @property.primary_contact.email)
        xml.tag!(:name,@property.primary_contact.full_name)
        xml.tag!(:role,@property.primary_contact.role);
        xml.tag!(:phone,@property.primary_contact.phone)
        xml.tag!(:mobile,@property.primary_contact.mobile)
        xml.tag!(:fax,@property.primary_contact.fax)
        xml.tag!(:description, @property.primary_contact.description)
        xml.tag!(:property_available,@property.primary_contact.properties.count(:conditions => " properties.status = 1 OR properties.status = 5 "));
        xml.tag!(:property_sold,@property.primary_contact.properties.count(:conditions => " properties.status = 2 "));
        xml.tag!(:count_testimonial,@property.primary_contact.testimonials.length);
        xml.tag!(:facebook_username,@property.primary_contact.facebook_username);
        xml.tag!(:twitter_username,@property.primary_contact.twitter_username);
        xml.tag!(:linkedin_username,@property.primary_contact.linkedin_username);
        xml.tag!(:photo,@property.primary_contact.portrait_image.blank? ? nil : @property.primary_contact.portrait_image.first.public_filename(:thumb))
      end
    end


    unless @property.secondary_contact.blank?
      xml.contact do
        xml.tag!(:id, @property.secondary_contact.id)
        xml.tag!(:email, @property.secondary_contact.email)
        xml.tag!(:name,@property.secondary_contact.full_name)
        xml.tag!(:role,@property.secondary_contact.role);
        xml.tag!(:phone,@property.secondary_contact.phone)
        xml.tag!(:mobile,@property.secondary_contact.mobile)
        xml.tag!(:fax,@property.secondary_contact.fax)
        xml.tag!(:description, @property.secondary_contact.description)
        xml.tag!(:property_available,@property.secondary_contact.properties.count(:conditions => " properties.status = 1 OR properties.status = 5 "));
        xml.tag!(:property_sold,@property.secondary_contact.properties.count(:conditions => " properties.status = 2 "));
        xml.tag!(:count_testimonial,@property.secondary_contact.testimonials.length);
        xml.tag!(:facebook_username,@property.secondary_contact.facebook_username);
        xml.tag!(:twitter_username,@property.secondary_contact.twitter_username);
        xml.tag!(:linkedin_username,@property.secondary_contact.linkedin_username);
        xml.tag!(:photo,@property.secondary_contact.portrait_image.blank? ? nil : @property.secondary_contact.portrait_image.first.public_filename(:thumb))
      end
    end



    #    unless @property.third_contact.blank?
    #      xml.contact do
    #        xml.tag!(:id, @property.third_contact.id)
    #        xml.tag!(:email, @property.third_contact.email)
    #        xml.tag!(:name,@property.third_contact.full_name)
    #        xml.tag!(:phone,@property.third_contact.phone)
    #        xml.tag!(:mobile,@property.third_contact.mobile)
    #        xml.tag!(:fax,@property.third_contact.fax)
    #        xml.tag!(:description, @property.third_contact.description)
    #        xml.tag!(:property_available,@property.secondary_contact.properties.count(:conditions => " properties.status = 1 AND properties.status = 5 "));
    #        xml.tag!(:property_sold,@property.secondary_contact.properties.count(:conditions => " properties.status = 2 "));
    #        xml.tag!(:count_testimonial,@property.secondary_contact.testimonials.length);
    #        xml.tag!(:facebook_username,@property.secondary_contact.facebook_username);
    #        xml.tag!(:twitter_username,@property.secondary_contact.twitter_username);
    #        xml.tag!(:linkedin_username,@property.secondary_contact.linkedin_username);
    #        xml.tag!(:photo,@property.third_contact.portrait_image.blank? ? nil : @property.third_contact.portrait_image.first.public_filename(:thumb))
    #      end
    #    end

  end

  xml.extra do
    unless @property.extra.blank?
      xml.tag!(:featured_property, @property.extra.featured_property)
      xml.tag!(:new_project_rea, @property.new_project_rea)
      xml.tag!(:property_of_week,@property.extra.property_of_week);
      xml.tag!(:projects,@property.extra.projects);
      xml.tag!(:description,@property.extra.description);
      xml.tag!(:field_name1,@property.extra.field_name1);
      xml.tag!(:field_value1,@property.extra.field1);
      xml.tag!(:field_name2,@property.extra.field_name2);
      xml.tag!(:field_value2,@property.extra.field2);
      xml.tag!(:field_name3,@property.extra.field_name3);
      xml.tag!(:field_value3,@property.extra.field3);
      xml.tag!(:field_name4,@property.extra.field_name4);
      xml.tag!(:field_value4,@property.extra.field4);
      xml.tag!(:field_name5,@property.extra.field_name5);
      xml.tag!(:field_value5,@property.extra.field5);
      xml.tag!(:field_name6,@property.extra.field_name6);
      xml.tag!(:field_value6,@property.extra.field6);
      xml.tag!(:field_name7,@property.extra.field_name7);
      xml.tag!(:field_value7,@property.extra.field7);
      xml.tag!(:field_name8,@property.extra.field_name8);
      xml.tag!(:field_value8,@property.extra.field8);
      xml.tag!(:field_name9,@property.extra.field_name9);
      xml.tag!(:field_value9,@property.extra.field9);
      xml.tag!(:dropdown_name,@property.extra.dropdown_name);
      xml.tag!(:dropdown_value,@property.extra.dropdown_value);
    end
  end

  xml.office do
    unless @property.office.blank?
      xml.tag!(:email, @property.office.email)
      xml.tag!(:name,@property.office.name);
      xml.tag!(:phone,@property.office.phone_number);
      xml.tag!(:fax,@property.office.fax_number);
    end
  end

  xml.photos do
    @property.images.each do |image|
      xml.tag!(:photo, image.public_filename)
    end
  end

  xml.floorplans do
    @property.floorplans.each do |plan|
      xml.tag!(:floorplan, plan.public_filename)
    end
  end

  xml.opentimes do
    @property.opentimes.each do |open|
      xml.tag!(:opentime, "#{open.date.strftime("%d %b %y, %a")} #{open.start_time.strftime("%I:%M %p")} - #{open.end_time.strftime("%I:%M %p")}")
    end
  end

  case @property.type.underscore
  when 'business_sale'
    xml.tag!(:auction_place, @property.detail.auction_place)
    xml.tag!(:auction_date, "#{@property.detail.auction_date} : #{@property.detail.auction_time}")
  when 'residential_sale'
    xml.tag!(:auction_place, @property.detail.auction_place)
    xml.tag!(:auction_date, "#{@property.detail.auction_date} : #{@property.detail.auction_time}")
  when 'commercial'
    xml.tag!(:auction_place, @property.detail.auction_place)
    xml.tag!(:auction_date, "#{@property.detail.auction_date} : #{@property.detail.auction_time}")
  end

  xml.features do
    @property.features.each do |feature|
      xml.tag!(:feature, feature.name)
    end
  end

end
rescue
end

