require 'builder'
xml = Builder::XmlMarkup.new(:indent => 2)
xml.instruct!
begin
unless @properties.blank?
  xml.properties do
    @properties.each do |property|
      xml.property do
        xml.tag!(:id_property, property.id)
        xml.tag!(:suburb, property.suburb)
        xml.tag!(:headline, property.headline)
        xml.tag!(:description, property.description)
        xml.tag!(:latitude, property.latitude)
        xml.tag!(:longitude, property.longitude)
        xml.tag!(:price, property.price)
        xml.tag!(:status, Property::STATUS_NAME[property.status.to_i - 1])
        xml.tag!(:display_price, property.display_price)
        xml.tag!(:display_price_text, property.display_price_text)
        xml.tag!(:bedrooms, property.detail.bedrooms)
        xml.tag!(:bathrooms, property.detail.bathrooms)
        xml.tag!(:carspaces, property.detail.carport_spaces)
        xml.tag!(:property_type, property.property_type)
        xml.tag!(:address, property.display_street.blank? ? "" : property.display_street.to_s)
        xml.tag!(:type, property.type)
        xml.tag!(:deal_type, property.deal_type)
        xml.tag!(:sell_type, property.lease? ? "Lease" : "Sale")
        unless property.images.blank?
          unless property.images.blank?
            xml.tag!(:photo, property.images.first.public_filename(:thumb))
          end
        end
      end
    end
  end
end
rescue
end