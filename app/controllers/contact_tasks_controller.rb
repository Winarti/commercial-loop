class ContactTasksController < TasksController
  def populate_task_types
    @task_types = ContactTaskType.find(:all, :conditions => ["office_id is NULL OR office_id = ?", @office.id]).map{|c| [c.name, c.id]}
    render :json => @task_types
  end

  protected

  def prepare_creation
    @agent_contact = AgentContact.find(params[:agent_contact_id])
    @task_types = ContactTaskType.all.map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @team_members = @office.agent_users.map{|u| [u.full_name, u.id]}.unshift(['Please select', nil]).uniq
    @tasks = @agent_contact.tasks.paginate(:all, :order => "`created_at` DESC", :page =>params[:page], :per_page => 30)
    if current_user.type == "AgentUser"
      @active_user = User.find(:first, :conditions => ["developer_id = ? AND office_id = ?", current_user.agent.developer_id, @office.id])
    else
      @active_user = ''
    end
  end

end
