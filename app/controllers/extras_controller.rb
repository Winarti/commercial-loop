class ExtrasController < ApplicationController
  before_filter :login_required
  before_filter :get_agent_and_office, :get_property, :prepare_extra
  before_filter :check_permission_page
  before_filter :prepare_creation, :only => [:option_form]
  skip_before_filter :verify_authenticity_token, :except => [:index, :create, :destroy]
  layout "agent"

  def index
    @extra = @property.extra || Extra.new
    @extra_details = @property.extra_details
    @extra_detail_names = ExtraDetailName.find(:all, :conditions => ["office_id = ? AND property_id = ?", @office.id, @property.id])
    @extra.new_project_rea = @property.new_project_rea unless @property.blank?
    if request.subdomains[0] == "knightfrank"
      newdev = Property.find(:all, :conditions => "office_id = #{@office.id} AND type LIKE 'NewDevelopment'")
      if newdev.present?
        @new_devs = [['-','']]
        @new_devs.concat(newdev.map{|a| [a.detail.development_name,a.detail.development_name]})
      else
        @new_devs = ["-",""]
      end
    end
  end

  def option_form
    @extra_option = ExtraOption.new
    @value_hidden = params[:hidden_val]
    render :layout => false
  end

  def update_extra

    if @property.extra.blank?
      params[:extra] = params[:extra].merge(:property_id => @property.id, :office_id => @office.id)
      @extra = Extra.new(params[:extra])
    else
      @extra = @property.extra
    end
    respond_to do |format|
      status = false
      if @property.extra.blank?
        status = @extra.save ? true : false
      else
        status = @extra.update_attributes(params[:extra]) ? true : false
      end
      update_name_for_office
      if status == true
        unless params[:detail_name].blank?
          params[:detail_name][:field_name].each_with_index do |x,i|
            ExtraDetailName.update_all("field_name = '#{params[:detail_name][:field_name][i]}'", "`id` = #{params[:detail_name][:field_name_id][i]}")
            if params[:detail_name][:field_value_id][i].blank?
              ExtraDetail.create({:property_id => @property.id, :office_id => @office.id, :extra_detail_name_id => params[:detail_name][:field_name_id][i], :field_value => params[:detail_name][:field_value][i]})
            else
              ExtraDetail.update_all("field_value = '#{params[:detail_name][:field_value][i]}'", "`id` = #{params[:detail_name][:field_value_id][i]}")
            end
          end
        end
        unless params[:additional_field].blank?
          params[:additional_field].each do |par|
            en = ExtraDetailName.create({:property_id => @property.id, :office_id => @office.id, :field_name => par.last[:field_name].to_s, :input_type => (par.last[:option_value].blank? ? "Text" : "Dropdown")})
            unless par.last[:option_value].nil?
              ExtraDetail.create({:property_id => @property.id, :office_id => @office.id, :extra_detail_name_id => en.id, :field_value => par.last[:field_value].to_s})
              par.last[:option_value].each do |opt_val|
                title = opt_val.split("|")[0]
                order = opt_val.split("|")[1]
                ExtraOption.create({:property_id => @property.id, :option_title => title, :option_order => order, :office_id => @office.id, :extra_detail_name_id => en.id})
              end
            else
              ExtraDetail.create({:property_id => @property.id, :office_id => @office.id, :extra_detail_name_id => en.id, :field_value => par.last[:field_value].to_s})
            end
          end
        end

        @property.updated_at = Time.now
        @property.new_project_rea = params[:extra][:new_project_rea] unless params[:extra][:new_project_rea].blank?
        @property.save(false)
        flash[:notice] = 'Extra was successfully updated.'
        format.html { redirect_to :action => "index"}
      else
        format.html { render :action => "index"}
      end
      format.html { render :action => "index"}
    end
  end

  def prepare_creation
    @extra_options = ExtraOption.paginate(:all, :conditions => ["`office_id` = ?", params[:office_id]], :order => "`option_order` ASC", :page =>params[:page], :per_page => 10)
  end

  def prepare_extra
    @extra_options = ExtraOption.find(:all, :conditions => "office_id = #{params[:office_id]}", :order => "option_order ASC").map{|u| [u.option_title, u.id]}.unshift(['Please select', nil]).uniq
    @property = Property.find(params[:property_id])
    if @property.type == "ResidentialSale"
      e = Extra.find(:first, :conditions => "property_id = #{params[:property_id]}")
    else
      e = Extra.find(:first, :conditions => "office_id = #{params[:office_id]}")
    end

    if @property.type == "ResidentialSale"
      @default_name = {"field_name1" => e.try.field_name1, "field_name2" => e.try.field_name2, "field_name3" => e.try.field_name3, "field_name4" => e.try.field_name4, "field_name5" => e.try.field_name5, "field_name6" => e.try.field_name6, "field_name7" => e.try.field_name7, "field_name8" => e.try.field_name8, "field_name9" => e.try.field_name9, "field_name10" => e.try.field_name10, "field_name11" => e.try.field_name11, "field_name12" => e.try.field_name12, "field_name13" => e.try.field_name13, "field_name14" => e.try.field_name14, "dropdown_name" => e.try.dropdown_name }
    else
      @default_name = {"field_name1" => e.try.field_name1, "field_name2" => e.try.field_name2, "field_name3" => e.try.field_name3, "field_name4" => e.try.field_name4, "field_name5" => e.try.field_name5, "field_name6" => e.try.field_name6, "field_name7" => e.try.field_name7, "field_name8" => e.try.field_name8, "field_name9" => e.try.field_name9, "dropdown_name" => e.try.dropdown_name }
    end
  end

  def update_name_for_office
    #    if @property.type == "ResidentialSale"
    #      sql="UPDATE extras SET field_name1='#{@extra.field_name1}', field_name2='#{@extra.field_name2}', field_name3='#{@extra.field_name3}', field_name4='#{@extra.field_name4}', field_name5='#{@extra.field_name5}', field_name6='#{@extra.field_name6}', field_name7='#{@extra.field_name7}', field_name8='#{@extra.field_name8}', field_name9='#{@extra.field_name9}, field_name10='#{@extra.field_name10}, field_name11='#{@extra.field_name11}, field_name12='#{@extra.field_name12}, field_name13='#{@extra.field_name13}, field_name14='#{@extra.field_name14}', dropdown_name='#{@extra.dropdown_name}' WHERE office_id=#{params[:office_id]} AND property_id=#{params[:property_id]}"
    #    else
    sql="UPDATE extras SET field_name1='#{@extra.field_name1}', field_name2='#{@extra.field_name2}', field_name3='#{@extra.field_name3}', field_name4='#{@extra.field_name4}', field_name5='#{@extra.field_name5}', field_name6='#{@extra.field_name6}', field_name7='#{@extra.field_name7}', field_name8='#{@extra.field_name8}', field_name9='#{@extra.field_name9}', dropdown_name='#{@extra.dropdown_name}' WHERE office_id=#{params[:office_id]}"
    #    end
    ActiveRecord::Base.connection.execute(sql)
  end
end