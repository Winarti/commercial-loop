class Admin::CountriesController < ApplicationController
  require 'faster_csv'
  before_filter :init_country_property_type

  layout 'admin'
  require_role 'AdminUser'

  make_resourceful do
    actions :all
  end

  def show
    redirect_to :action => "edit"
  end

  def create
    @country = Country.new(params[:country])
    if @country.save
      params[:category].each do |c|
        @country_property_relationship = CountryPropertyTypeRelationship.new
        @country_property_relationship.country_id = @country.id
        @country_property_relationship.property_type_id = c
        @country_property_relationship.save
      end
      redirect_to :action => "index"
    else
      render :action =>"new"
    end
  end

  def update
    @country = Country.find(params[:id])
    if @country.update_attributes(params[:country])
      unless params[:category].blank?
        params[:category].each do |c|
          @country_property_relationship = CountryPropertyTypeRelationship.new
          @country_property_relationship.country_id = @country.id
          @country_property_relationship.property_type_id = c
          @country_property_relationship.save
        end
      end
      redirect_to :action => "index"
    else
      render :action =>"edit"
    end
  end

  def delete_flag
    @country = Country.find(params[:id])
    @country.flag = nil
    @country.flag.destroy
    @country.save
    redirect_to :action => "show", :id => params[:id]
  end

  def suburbs
    respond_to do |format|
      format.csv { render :csv => Suburb.find_all_by_country_id(params[:id], :include => [:country,:state]) }
    end
  end

  def bulk_upload
    @country = Country.find(params[:id])
    @country.states.delete_all
    @country.suburbs.delete_all
    n=0
    FasterCSV.parse(params[:csv][:file], :headers => true, :col_sep => ",") do |row|
      country = Country.find_by_code(row[0])
      state = country.states.find_or_create_by_name(row[1])
      Suburb.create!(:name => row[2], :postcode => row[3], :country_id => country.id, :state_id => state.id )
      n += 1
      GC.start if n%20 == 0
    end
    flash[:notice] = "CSV successfully imported. #{n} records added to database."
    redirect_to :action => 'index'
  end

  protected
  def init_country_property_type
    @property_types = PropertyType.all
    @property_type_categories = PropertyType.find(:all,:select =>"DISTINCT category")
  end

end
