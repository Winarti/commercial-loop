class Admin::UpdatesController < ApplicationController
  layout 'admin'
  require_role 'AdminUser'

  def show
    @help_data = News.first.data
  end

  def update
    @help = News.first
    @help.build_data(params[:help_data_key], params[:help_data])
    @help.save
    redirect_to :action => "show"
  end
end
