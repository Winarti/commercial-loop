class Admin::DeveloperQuicklinksController < ApplicationController
  layout 'admin'
  require_role 'AdminUser'

  def show
    @updates_data = DeveloperQuicklink.first.data
  end

  def update
    @updates = DeveloperQuicklink.first
    @updates.build_data(params[:updates_data_key], params[:updates_data])
    @updates.save
    redirect_to :action => "show"
  end
end
