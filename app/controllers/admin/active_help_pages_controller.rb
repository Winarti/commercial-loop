class Admin::ActiveHelpPagesController < ApplicationController

  layout 'admin'
  require_role 'AdminUser'
  before_filter :prepare_form, :only => [:new, :create, :edit, :update]
  before_filter :find_active_help, :except => [:new, :index, :create]

  def index
    @help_pages = ActiveHelpPage.find(:all, :conditions => ["user_type = 'DeveloperUser'"])
  end

  def new
    @help_page = ActiveHelpPage.new
  end

  def create
    @help_page = ActiveHelpPage.new(params[:help])
    @help_page.user_type = 'DeveloperUser'
    if @help_page.save
      flash[:notice] = "Your data has successfully created"
      redirect_to :action => "index" and return
    end

    flash.now[:error] = "Failed to created data"
    render :action => "edit"
  end

  def edit;
  end

  def update
    if @help_page.update_attributes(params[:help])
      flash[:notice] = "Your data has successfully update"
      redirect_to :action => "index" and return
    end

    flash.now[:error] = "Failed to update data"
    render :action => "edit"
  end

  def destroy
    flash[:notice] = @help_page.destroy ? "Your data has successfully deleted" : "Failed to delete data"
    redirect_to :action => "index"
  end

  private

  def find_active_help
    @help_page = ActiveHelpPage.find(params[:id])
    flash[:error] = "ID cant be found" and redirect_to :action => "index" and return unless @help_page
  end

  def prepare_form
    @pages = [['Please Select', nil],['My Dashboard', 'My Dashboard'],['Client Summary', 'Client Summary'],['Client Office', 'Client Office'],['Client Messages', 'Client Messages'],['Commercial Loop Messages', 'Commercial Loop Messages'],['Current Plans', 'Current Plans'],['Billing', 'Billing'], ['Business Details', 'Business Details'], ['Team Member', 'Team Member'], ['Add/Edit Client', 'Add/Edit Client'], ['Add/Edit Office', 'Add/Edit Office'], ['API', 'API'], ['Exports', 'Exports']]
  end

end
