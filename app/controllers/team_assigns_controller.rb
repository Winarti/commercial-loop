class TeamAssignsController < ApplicationController
  layout 'agent'
  before_filter :login_required
  before_filter :get_agent_and_office

  def index
    agent_users = @office.agent_users
    # do not change the text value on the map, it will be used to change the text of the drop list
    unless agent_users.blank?
      @primary_contact = agent_users.map{|m| ;["#{m.full_name} (#{m.id}) #{get_psize(m.id,1)}",m.id]}.uniq.sort {|x,y| x.to_s.strip <=> y.to_s.strip unless (x.blank? && y.blank?)}.compact.unshift(['Please Select', ''])
      @secondary_contact = agent_users.map{|m| ;["#{m.full_name} (#{m.id}) #{get_psize(m.id,2)}",m.id]}.uniq.sort {|x,y| x.to_s.strip <=> y.to_s.strip unless (x.blank? && y.blank?)}.compact.unshift(['Please Select', ''])
      @third_contact = agent_users.map{|m| ;["#{m.full_name} (#{m.id}) #{get_psize(m.id,3)}",m.id]}.uniq.sort {|x,y| x.to_s.strip <=> y.to_s.strip unless (x.blank? && y.blank?)}.compact.unshift(['Please Select', ''])
      @fourth_contact = agent_users.map{|m| ;["#{m.full_name} (#{m.id}) #{get_psize(m.id,4)}",m.id]}.uniq.sort {|x,y| x.to_s.strip <=> y.to_s.strip unless (x.blank? && y.blank?)}.compact.unshift(['Please Select', ''])
    end
  end

  def update
    case params[:id].to_i
    when 1
      @properties = Property.find(:all,:conditions =>"office_id = #{@office.id} AND (agent_user_id = #{params[:contact]} OR primary_contact_id = #{params[:contact]})")
      if !@properties.blank?
        if !params[:assign].blank?
          @properties.each do |property|
            if property.status == 2 || property.status == 3 || property.status == 4 || property.status == 6
              GLOBAL_ATTR[:status] = true
              GLOBAL_ATTR[:time] = property.updated_at
            else
              GLOBAL_ATTR[:status] = false
            end

            if property.status == 1
              property.primary_contact_id = params[:assign].to_i
              property.agent_user_id = params[:assign].to_i
              property.updated_at = Time.now
              property.save(false)
            else
              ActiveRecord::Base.connection.execute("UPDATE properties SET primary_contact_id=#{params[:assign]}, agent_user_id=#{params[:assign]}, updated_at = '#{Time.now.to_formatted_s(:db_format)}' WHERE id = #{property.id}")
            end
          end
        else
          return render :json => [params[:id],false]
        end
      else
        return render :json => [params[:id],false]
      end
    when 2
      if !params[:contact].blank?
        @properties = Property.find(:all,:conditions =>"office_id = #{@office.id} AND secondary_contact_id = #{params[:contact]}")
        if !@properties.blank?
          begin
            @properties.each do |property|
              if property.status == 2 || property.status == 3 || property.status == 4 || property.status == 6
                GLOBAL_ATTR[:status] = true
                GLOBAL_ATTR[:time] = property.updated_at
              else
                GLOBAL_ATTR[:status] = false
              end
              if property.status == 1
                property.update_attribute(:secondary_contact_id,params[:assign])
              else
                ActiveRecord::Base.connection.execute("UPDATE properties SET secondary_contact_id=#{params[:assign]}, updated_at = '#{Time.now.to_formatted_s(:db_format)}' WHERE id = #{property.id}")
              end
            end
          rescue Exception => ex
            Log.create(:message => "Secondary contact assign error : #{ex.inspect}")
            return render :json => [params[:id],false]
          end
        else
          return render :json => [params[:id],false]
        end
      else
        return render :json => [params[:id],false]
      end
    when 3
      if !params[:contact].blank?
        @properties = Property.find(:all,:conditions =>"office_id = #{@office.id} AND third_contact_id = #{params[:contact]}")
        if !@properties.blank?
          begin
            @properties.each do |property|
              if property.status == 2 || property.status == 3 || property.status == 4 || property.status == 6
                GLOBAL_ATTR[:status] = true
                GLOBAL_ATTR[:time] = property.updated_at
              else
                GLOBAL_ATTR[:status] = false
              end
              if property.status == 1
                property.update_attribute(:third_contact_id,params[:assign])
              else
                ActiveRecord::Base.connection.execute("UPDATE properties SET third_contact_id=#{params[:assign]}, updated_at = '#{Time.now.to_formatted_s(:db_format)}' WHERE id = #{property.id}")
              end
            end
          rescue Exception => ex
            Log.create(:message => "Third contact assign error : #{ex.inspect}")
            return render :json => [params[:id],false]
          end
        else
          return render :json => [params[:id],false]
        end
      else
        return render :json => [params[:id],false]
      end
    when 4
      if !params[:contact].blank?
        @properties = Property.find(:all,:conditions =>"office_id = #{@office.id} AND (fourth_contact_id = #{params[:contact]} And fourth_contact_type ='Normal')")
        if !@properties.blank?
          begin
            @properties.each do |property|
              if property.status == 2 || property.status == 3 || property.status == 4 || property.status == 6
                GLOBAL_ATTR[:status] = true
                GLOBAL_ATTR[:time] = property.updated_at
              else
                GLOBAL_ATTR[:status] = false
              end
              if property.status == 1
                property.update_attribute(:fourth_contact_id,params[:assign])
              else
                ActiveRecord::Base.connection.execute("UPDATE properties SET fourth_contact_id=#{params[:assign]}, updated_at = '#{Time.now.to_formatted_s(:db_format)}' WHERE id = #{property.id}")
              end
            end
          rescue Exception => ex
            Log.create(:message => "Fourth contact assign error : #{ex.inspect}")
            return render :json => [params[:id],false]
          end
        else
          return render :json => [params[:id],false]
        end
      else
        return render :json => [params[:id],false]
      end

    else
      return render :json => [params[:id],false]
    end
    return render :json => [params[:id],true,params[:contact],params[:assign]]
  end
  protected
  def get_psize(id,type)
      case type.to_i
      when 1
        p_count = ActiveRecord::Base.connection.execute("SELECT count(id) as ids FROM `properties` where office_id = #{@office.id} AND (agent_user_id = #{id} OR primary_contact_id = #{id}) AND deleted_at IS NULL")
        psize = p_count.all_hashes[0]["ids"]
      when 2
        p_count = ActiveRecord::Base.connection.execute("SELECT count(id) as ids FROM `properties` where office_id = #{@office.id} AND secondary_contact_id = #{id} AND deleted_at IS NULL")
        psize = p_count.all_hashes[0]["ids"]
      when 3
        p_count = ActiveRecord::Base.connection.execute("SELECT count(id) as ids FROM `properties` where office_id = #{@office.id} AND third_contact_id = #{id} AND deleted_at IS NULL")
        psize = p_count.all_hashes[0]["ids"]
      when 4
        p_count = ActiveRecord::Base.connection.execute("SELECT count(id) as ids FROM `properties` where office_id = #{@office.id} AND (fourth_contact_id = #{id} And fourth_contact_type ='Normal') AND deleted_at IS NULL")
        psize = p_count.all_hashes[0]["ids"]
      else
        psize = 0
      end
    return (( psize.to_i == 1 || psize.to_i == 0 ) ? "#{psize} property" : "#{psize} properties")
  end
end