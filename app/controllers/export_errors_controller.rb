class ExportErrorsController < ApplicationController
  layout 'agent'
  before_filter :login_required
  before_filter :get_agent_and_office

  def index
    @export_errors = ExportError.find(:all, :conditions => "`office_id`=#{params[:office_id]}", :order => "`id` DESC")
  end

  def show; end

  def new; end

  def edit; end

  def create; end

  def update; end

  def destroy; end

end