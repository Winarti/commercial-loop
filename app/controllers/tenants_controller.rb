class TenantsController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office

  def index
  	get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def populate_combobox
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    condition = [" tenancy_records.office_id = ? "]
    condition << current_office.ID
    properties = TenancyRecord.find(:all, :include =>[:property, :agent_contact], :conditions => ["tenancy_records.agent_id = ? AND tenancy_records.office_id = ?", @agent.id, @office.id])
    properties.map{|x|
      suburb << {
        :suburb => x.property.suburb.to_s,
        :suburbText =>x.property.suburb.to_s
      } unless x.property.suburb.blank?;
      type << {
        :type => x.property.property_type.to_s,
        :typeText =>x.property.property_type.to_s
      } unless x.property.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?) ,
        :assigned_toText => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?)
      } unless x.property.primary_contact.blank?
      ((floor << {
            :floor => x.property.detail.number_of_floors.to_s,
            :floorText => x.property.detail.number_of_floors.to_s
          } unless x.property.detail.number_of_floors.blank?) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale")
    }
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listing
        condition = [" properties.office_id = ? "]
        condition << current_office.id

        if @property.type.to_s == "Commercial"
          condition[0] += " AND tenancy_records.property_id = ?"
          condition << "#{@property.id}"
        elsif @property.type.to_s == "CommercialBuilding"
          condition[0] += " AND properties.parent_listing_id = ?"
          condition << "#{@property.id}"
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if @property.type.to_s == "Commercial"
        condition[0] += " AND tenancy_records.property_id = ?"
        condition << "#{@property.id}"
      elsif @property.type.to_s == "CommercialBuilding"
        condition[0] += " AND properties.parent_listing_id = ?"
        condition << "#{@property.id}"
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = TenancyRecord.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm'
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
    else
      @properties = TenancyRecord.paginate(:all, :include => [:property,:agent_contact], :conditions => condition, :order => order, :page => params[:page], :per_page => params[:limit].to_i)
    end

    if @properties.present?
      @properties.each do |x|
        check_and_update_icon(x.property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      results = TenancyRecord.find(:all,:select => 'count(tenancy_records.property_id)',:include =>[:property, :agent_contact],:conditions => condition,:limit => params[:limit].to_i)
    else
      results = TenancyRecord.count(:all, :include => [:property,:agent_contact], :conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'property_id' => x.property.id,
              'listing'=>Property.abbrv_properties(x.property.type),
              'address' =>((x.property.street_number.blank? ? "" : x.property.street_number)+" "+(x.property.street.blank? ? "" : x.property.street.to_s)),
              'suite'=>(x.property.unit_number.blank? ? "" : x.property.unit_number),
              'sqm'=>x.property.detail.floor_area.blank? ? "" : x.property.detail.floor_area,
              'level'=>((x.property.detail.number_of_floors.blank? ? "" : x.property.detail.number_of_floors) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale"),
              'I/E'=> x.property.ownership.blank? ? "Internal" : x.property.ownership,
              'tenancy'=>(x.agent_contact.blank? ? "" : (x.agent_contact.full_name.blank? ? x.agent_contact.first_name : x.agent_contact.full_name)),
              'rent_pa'=>(x.total_rental_pa.blank? ? "" : x.total_rental_pa),
              'start_date'=>(x.start_date.blank? ? "" : x.start_date),
              'end_date'=>(x.end_date.blank? ? "" : x.end_date),
              'suburb' => x.property.suburb.blank? ? "" : x.property.suburb,
              'type' => x.property.property_type.blank? ? "" : x.property.property_type,
              'price' => Property.check_price(x.property),
              'assigned_to'=>x.property.primary_contact.blank? ? '' : x.property.primary_contact.full_name.to_s.strip,
              'edit'=>[x.property.id, x.id],
              'status' => Property.status_name(x.property),
              'fb'=> [x.property.id,@office.url.blank? ? (x.property.detail.respond_to?(:deal_type) ? x.property.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}", tweet(x.property)]} unless x.property.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def get_session_listing
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:floor] = params[:floor].blank? ? "all" : params[:floor]
    session[:sold_price] = params[:sold_price].blank? ? "all" : params[:sold_price]
    session[:sold_date] = params[:sold_date].blank? ? "all" : params[:sold_date]
    session[:purchaser] = params[:purchaser].blank? ? "all" : params[:purchaser]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listing
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:floor] = nil
    session[:sold_price] = nil
    session[:purchaser] = nil
    session[:sold_date] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def check_and_update_icon(children_listing)
    img_count = children_listing.images.count rescue 0
    floorp_count = children_listing.floorplans.count rescue 0
    if !children_listing.image_exist && img_count > 0
      children_listing.image_exist = true
      children_listing.save(false)
    end
    if children_listing.image_exist && img_count == 0
      children_listing.image_exist = false
      children_listing.save(false)
    end
    if !children_listing.floorplan_exist && floorp_count > 0
      children_listing.floorplan_exist = true
      children_listing.save(false)
    end
    if children_listing.floorplan_exist && floorp_count == 0
      children_listing.floorplan_exist = false
      children_listing.save(false)
    end
  end

  def tweet(children_listing)
    unless children_listing.blank?
      case children_listing.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

end
