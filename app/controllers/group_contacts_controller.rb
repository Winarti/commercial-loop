class GroupContactsController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
    @group_contact = GroupContact.new
  end

  def show
  end

  def new
  end

  def edit
    @group_contact = GroupContact.find(params[:id])
  end

  def create
   @group_contact = GroupContact.new(params[:group_contact].merge(:creator_id => current_user.id, :office_id => @office.id))

    respond_to do |format|
      if @group_contact.save
        flash[:notice] = 'Mailing Group contact was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    @group_contact = GroupContact.find(params[:id])

    respond_to do |format|
      if @group_contact.update_attributes(params[:group_contact])
        flash[:notice] = 'Mailing Group Contact was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    GroupContactRelation.find(:all, :conditions => { :group_contact_id => params[:id] }).each(&:destroy)
    @group_contact = GroupContact.find(params[:id])
    @group_contact.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @groups = GroupContact.paginate(:all, :conditions => ["`office_id` = ?", @office.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end
end
