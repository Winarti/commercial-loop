class SearchesController < ApplicationController
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office

  def new
    @search = Search.new
  end

  def create
    @search = Search.new(params[:search])
    respond_to do |format|
      if @search.save
        flash[:notice] = "Search was successfuly saved."
        format.html { redirect_to agent_office_search_results(@agent,@office) }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def destroy
    @search = Search.find(params[:id])
    @search.destroy
    respond_to do |format|
      flash[:notice] = "Search keyword was successfully deleted."
      format.html { redirect_to history_agent_office_search_results_path(@agent.id,@office.id) }
    end
  end

end