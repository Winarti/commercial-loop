class Mp3sController < ApplicationController
  before_filter :login_required
  before_filter :get_agent_and_office, :get_property
  # GET /mp3s
  # GET /mp3s.xml
  def index
    @mp3 = @property.mp3 || Mp3.new
    #unless @mp3.new_record?
    # @mp3_file = @mp3.public_filename if File.exist?(@mp3_file.public_filename)
    #end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @mp3s }
      format.js { render :layout => false }
    end
  end

  # GET /mp3s/1
  # GET /mp3s/1.xml
  def show
    @mp3 = Mp3.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @mp3 }
    end
  end

  # GET /mp3s/new
  # GET /mp3s/new.xml
  def new
    @mp3 = Mp3.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @mp3 }
    end
  end

  # GET /mp3s/1/edit
  def edit
    @mp3 = Mp3.find(params[:id])
  end

  # POST /mp3s
  # POST /mp3s.xml
  def create
    @mp3 = Mp3.new(params[:mp3])
    original = @property.mp3
    @mp3.attachable = @property

    if @mp3.save
      original.try(:destroy)
      flash[:notice] = 'Mp3 was successfully created.'
      redirect_to agent_office_property_media_path(@agent,@office,@property)
    else
      redirect_to :action => "edit"
    end

  end

  # PUT /mp3s/1
  # PUT /mp3s/1.xml
  def update
    @mp3 = Mp3.find(params[:id])

    respond_to do |format|
      if @mp3.update_attributes(params[:mp3])
        flash[:notice] = 'Mp3 was successfully updated.'
        format.html { redirect_to(@mp3) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @mp3.errors, :status => :unprocessable_entity }
      end
    end
  end

  def download
    mp3_file = Mp3.find(params[:id])
    flash[:error] = 'Mp3 file was missing' and
    redirect_to :controller => 'properties', :action => 'index', :agent_id => params[:agent_id], :office_id => params[:office_id] and return unless mp3_file
    #send_file("#{RAILS_ROOT}/public#{mp3_file.public_filename}")
    redirect_to mp3_file.public_filename
  end

  # DELETE /mp3s/1
  # DELETE /mp3s/1.xml
  def destroy
    @mp3 = Mp3.find_by_id(params[:id])
    @mp3.destroy unless @mp3.blank?

    respond_to do |format|
      format.html { redirect_to(agent_office_properties_path(@agent, @office)) }
      format.xml  { head :ok }
    end
  end
end
