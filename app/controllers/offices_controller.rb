#--------------------------------------------------
# OfficesController is the template of two concrete controllers:
#
#   * OfficesAController: for agent version office pages
#   * OfficesDcontroller: for developer version office pages
#
# Find more clue in config/routes.rb
#--------------------------------------------------

class OfficesController < ApplicationController
  before_filter :login_required, :except =>[:update_logo]
  before_filter :set_hostname

  def set_hostname
    agent = Agent.find_by_id(params['agent_id'])
    return if agent.blank?
    GLOBAL_ATTR["OFF-#{agent.id}-#{agent.developer.id}"] = request.host
  end

  def office_notification
    @office = Office.find(params[:id])
    unless params[:processed].blank?
      GLOBAL_ATTR.delete_if{|k,v| v == params[:token]}
      sql = ActiveRecord::Base.connection()
      sql.update "UPDATE offices SET processed = #{params[:processed].to_i} WHERE id = #{@office.id}"
      sql.commit_db_transaction
      return render(:text => "Office with id : #{@office.id} is successfully updated")
    else
      return render(:text => "false")
    end
  end

  # GET /offices
  # GET /offices.xml
  def index
    @offices = @agent.offices.find(:all, :conditions => "deleted_at IS NULL")
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Offices' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    respond_to do |format|
      format.html
    end
  end

  # GET /offices/1
  # GET /offices/1.xml
  def show
    @office = @agent.offices.find(params[:id])
    session[:type] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
    session[:sort_by] = "properties.updated_at DESC"
    user = User.find(current_user)
    if user.type == "DeveloperUser"
      session[:office_id] = params[:id]
    end
    @stats = %w(ResidentialSale ResidentialLease Commercial CommercialBuilding HolidayLease ProjectSale BusinessSale NewDevelopment LandRelease GeneralSale LivestockSale ClearingSalesEvent ClearingSale).map{|t| [t, @office.stats(t,current_office.id)] }
    @export_errors = ExportError.find(:all, :conditions => "`office_id`=#{@office.id}", :order => "`id` DESC",:limit => 7)
    @properties = @office.properties.find :all, :order => 'updated_at desc', :limit => 20
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Dashboard' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?

    @tasks = @office.tasks.find(:all, :conditions => ["tasks.office_id = ?", current_office.id], :limit => 9)

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /offices/new
  # GET /offices/new.xml
  def new
    @office = @agent.offices.new
    @office.apply_agent_settings

    respond_to do |format|
      format.html # show.html.erb
    end
  end

  # GET /offices/1/edit
  def edit
    @office = @agent.offices.find(params[:id])

    respond_to do |format|
      format.html # edit.html.erb
    end
  end

  # POST /offices
  # POST /offices.xml
  def create
    @office = @agent.offices.new(params[:office])
    @office.is_developer = false
    respond_to do |format|
      if @office.save
        flash.now[:notice] = 'Office was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => 'new' }
      end
    end
  end

  # PUT /offices/1
  # PUT /offices/1.xml
  def update
    respond_to do |format|
      @office = @agent.offices.find(params[:id])
      @office.is_developer = false
      if @office.update_attributes(params[:office])
        flash[:notice] = 'Office was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  # handle logo create/update/destroy
  # Attachemt's order column is used to keep type info here
  # type 1: 160x30
  # type 2: 150x75
  # type 3: 250x125
  def logo
    @office = @agent.offices.find(params[:id])

    if request.get? && params[:view_type]
      @logo = @office.logos.find_by_order params[:view_type].to_i
      if @logo
        send_file @logo.full_filename, :type => @logo.content_type, :disposition => 'inline'
        return
      else
        flash[:notice] = 'No logo for this office'
        redirect_to :action => 'edit'
        return
      end
    end

    if request.post?
      unless params[:logo][:uploaded_data].blank?
        @logo = Image.new params[:logo]
        @logo.order = params[:logo][:order].to_i # can't mass-assign
         unless [1,2,3].include?(@logo.order)
            flash[:notice] = 'Invalid logo type'
         else
           unless @logo.validate_logo
            old_logo = @office.logos.find_by_order @logo.order
            @logo.dimension = Image.const_get("LOGO#{@logo.order}")
            @logo.attachable = @office
            @logo.category = "Office Logo"
            if @logo.save
              old_logo && old_logo.destroy
              flash.now[:notice] = 'Logo was successfully save.'
              @office.logos.reload
              redirect_to  update_logo_developer_agent_office_path(@developer,@agent,@office)+"?logo_order=#{@logo.order}"
              return
            else
              flash.now[:notice] = 'Failed to save image.'
            end
           else
            flash[:notice] = 'This logo is a CMYK image which is not compatible with Internet Explorer. Please load an image which is RGB'
           end
         end
      else
        flash.now[:notice] = 'Please upload an image.'
      end
    end

    if request.delete? && params[:type]
      @logo = @office.logos.find_by_order params[:type].to_i
      @logo.destroy if @logo
      flash.now[:notice] = "Logo was successfully deleted."
      redirect_to  :action => "edit"
      return
    end
    render :layout => false
  end

  def update_logo
    @office = Office.find(params[:id])
    @logo_order = params[:logo_order]
    @developer_id = params[:developer_id]
    @agent_id = params[:agent_id]
    render :layout => false
  end

  def change_logo
    @office = Office.find(params[:id])
    @logo_order = params[:logo_order]
    render :layout => false
  end

  def delete_logo
    @office = Office.find_by_id(params[:id])
    @logo_order = params[:logo_order]
    @logo = @office.logos.find_by_order params[:logo_order].to_i
    @logo.destroy if @logo
    render :layout => false
  end

  # DELETE /offices/1
  # DELETE /offices/1.xml
  def destroy
    @office = @agent.offices.find_by_id(params[:id])
    @office.destroy unless @office.blank?

    respond_to do |format|
      format.html { redirect_to agent_offices_url(@agent) }
    end
  end

  private

  def translate_url(text)
    return 'http://' + text.gsub('http://', '')
  end

end
