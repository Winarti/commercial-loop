class VideosController < ApplicationController
  before_filter :login_required
  before_filter :get_agent_and_office, :get_property, :except => [:show_video]

  def index
    @videos = Video.find(:all)

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @videos }
    end
  end

  def show
    @video = Video.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @video, :layout => false }
    end
  end

  def show_video
    @video = Video.find(params[:id])
  end

  def new
    @videos = Video.find(:all, :conditions => ["attachable_id = ?", @property.id])
    @video = Video.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @video }
      format.js { render :layout => false }
    end
  end

  def edit
    @video = Video.find(params[:id])
  end

  def create
    @video = Video.new(params[:video])
    #respond_to do |format|
    #  if @video.save
    #  format.html # new.html.erb
    #  format.xml  { render :xml => @video }
    #  end
    #  format.js { render :layout => false }
    #end
    @video.attachable_id = @property.id
    #render :update do |page|
    if @video.save
      #page.replace_html :message, 'Video was successfully created.'
      #@videos = Video.all
      flash[:notice] = 'Video was successfully saved.'
      session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_video-1"
      redirect_to new_agent_office_property_video_path(@agent, @office, @property)
      return
    else
      render :update do |page|
        page.replace_html :message, 'Video failed to save.'
      end
    end
    #end
  end

  def update
    @video = Video.find(params[:id])

    respond_to do |format|
      if @video.update_attributes(params[:video])
        flash[:notice] = 'Video was successfully updated.'
        format.html { redirect_to(@video) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @video.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.xml
  def destroy
    @video = Video.find(params[:id])
    @video.destroy
    if @property.videos.blank?
      session[:update_listing] = "#{(@property.type.to_s.underscore unless @property.type.blank?)}-#{@property.id}-icon_video-0"
    else
      session[:update_listing] = "#{(@property.type.to_s.underscore  unless @property.type.blank?)}-#{@property.id}-icon_video-1"
    end
    respond_to do |format|
      format.html { redirect_to new_agent_office_property_video_path(@agent, @office, @property) }
      format.xml  { head :ok }
    end
  end
end
