class OpentimesController < ApplicationController
  layout "agent"
  require_role ['AgentUser','DeveloperUser']
  prepend_before_filter :login_required
  before_filter :get_property, :get_agent_and_office, :provide_data
  before_filter :check_permission_page
  skip_before_filter :verify_authenticity_token, :only => %w( destroy end_times )

  # GET /opentimes
  # GET /opentimes.xml
  def index
    get_session_listing
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?

    add_all_opentime_to_pastopentime
    @opentime = Opentime.new
    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @opentimes }
      format.js { render :layout => false }
    end
  end

  def show
  end

  def new
  end

  def edit
    @opentime = Opentime.find(params[:id])
  end

  # POST /opentimes
  # POST /opentimes.xml
  def create
    params[:opentime][:start_time] = Time.parse(params[:opentime][:start_hour]+":"+params[:opentime][:start_minute]+":00") if !params[:opentime][:start_hour].blank? && !params[:opentime][:start_minute].blank?
    params[:opentime][:end_time] = Time.parse(params[:opentime][:end_hour]+":"+params[:opentime][:end_minute]+":00") if !params[:opentime][:end_hour].blank? && !params[:opentime][:end_minute].blank?

    @opentime = @property.opentimes.new(params[:opentime])
    respond_to do |format|
      if @opentime.save
        if params[:opentime][:number_of_weeks].to_i > 1
          x = params[:opentime][:number_of_weeks].to_i - 2
          for i in 0..x
           params[:opentime][:date] = params[:opentime][:date].to_date + 7.days
           @opentime = @property.opentimes.new(params[:opentime])
           @opentime.save!
          end
        end
        @property.update_attribute(:updated_at,Time.now)
        ChangesLog.create_log(@property, @opentime, @opentime.attributes, current_user, @office, 1)
        flash[:notice] = 'Opentime was successfully created.'
        format.html { redirect_to :action => "index"}
      else
        format.html { render :action => "index"}
        format.xml  { render :xml => @opentime.errors, :status => :unprocessable_entity }
        format.xml  { render :xml => @opentimes }
      end
    end

  end

  # PUT /opentimes/1
  # PUT /opentimes/1.xml
  def update
    @opentime = Opentime.find(params[:id])

    respond_to do |format|
      @opentime.attributes = params[:opentime]
      changes = @opentime.changes

      if @opentime.save
        @property.update_attribute(:updated_at,Time.now)
        session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_date-1"
        ChangesLog.create_log(@property, @opentime, changes, current_user, @office, 2)
        flash[:notice] = 'Opentime was successfully updated.'
        format.html { redirect_to(@opentime) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @opentime.errors, :status => :unprocessable_entity }
      end
    end
  end

  def destroy
    @opentime = @property.opentimes.find_by_id(params[:id]) unless @property.opentimes.blank?
    unless @opentime.blank?
      pastopentime = PastOpentime.find_by_opentime_id(@opentime.id)
      contact_pastopentime_rels = ContactPastopentimeRelation.find(:all, :conditions => "past_opentime_id = #{pastopentime.id}")
      ChangesLog.create_opentime_destroy_log(@office, @property, @opentime, pastopentime, contact_pastopentime_rels, current_user, 3)
      ContactPastopentimeRelation.destroy_all({:past_opentime_id => pastopentime.id})
      pastopentime.destroy unless pastopentime.blank?
      if @opentime.destroy
        p = Property.find(@property.id)
        p.update_attribute(:updated_at,Time.now)
      end
    end
    session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_date-0"
    respond_to do |format|
      format.html { redirect_to(:action=>"index") }
      format.xml  { head :ok }
    end
  end

  def provide_data
    @range = (1..10).map{|x|[x,x]}
    @hours = (0..23).map{|x| x.to_s.size < 2 ? ["0"+x.to_s, "0"+x.to_s] : [x.to_s,x.to_s] }.unshift(["Please select", ''])
    @minutes = (0..55).step(5).map{|x| x.to_s.size < 2 ? ["0"+x.to_s, "0"+x.to_s] : [x.to_s,x.to_s] }.unshift(["Please select", ''])
    @opentimes = @property.opentimes.find(:all, :order => "date ASC")
    @past_opentimes = @property.past_opentimes.find(:all, :order => "date ASC")
  end

  #
  def end_times
    @end_times = []
    (Time.parse(params[:start_time])..Time.parse("22:00:00")).step(15.minutes){ |x| @end_times << x }
    @end_times.delete_at(0)
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def destroy_past_opentime
    @past_opentime = @property.past_opentimes.find_by_id(params[:id]) unless @property.past_opentimes.blank?
    unless @past_opentime.blank?
      ContactPastopentimeRelation.destroy_all({:past_opentime_id => @past_opentime.id})
      @past_opentime.destroy
      @property.update_attribute(:updated_at,Time.now)
    end
    respond_to do |format|
      format.html { redirect_to(:action=>"index") }
      format.xml  { head :ok }
    end
  end

  def add_all_opentime_to_pastopentime
    @opentimes.each do |opentime|
      conditions = ["`date`= ? And `start_time` = ? And `end_time` = ? ", opentime.date, opentime.start_time, opentime.end_time]
      check = @property.past_opentimes.find(:first, :conditions => conditions)
      opentime.create_past_opentime if check.nil?
    end
    @past_opentimes = @property.past_opentimes.find(:all, :order => "date ASC")
  end

  # ------------------------------------------listing------------------------------------------------
  def populate_combobox
    dates, heards = [],[]
    condition = [" past_opentimes.property_id = ? ", @property.id]
    past_opentimes = ContactPastopentimeRelation.find(:all, :include => [:past_opentime, :agent_contact], :conditions => condition)
    past_opentimes.map{|x|
      unless x.blank?
        dates << {
          :date => ( x.past_opentime.date.to_s + "~" + x.past_opentime.start_time.to_s + "~" + x.past_opentime.end_time.to_s unless x.past_opentime.date.blank?) ,
          :dateText => (format_date(x.past_opentime) unless x.past_opentime.date.blank?)
        } unless x.past_opentime.date.blank?
        heards << {
          :heard => (x.heard_from_id unless x.heard_from_id.blank?) ,
          :heardText => (x.heard_from.name unless x.heard_from_id.blank?)
        } unless x.heard_from_id.nil?
      end
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_dates =[]
    uniq_contracts =[]
    uniq_heards =[]
    uniq_alerts =[]
    dates.each{|x|uniq_dates << x if !uniq_dates.include?(x) and !x.blank?}
    heards.each{|x|uniq_heards << x if !uniq_heards.include?(x) and !x.blank?}
    uniq_contracts << [{:contract=>'1',:contractText=>"Yes"}, {:contract=>'0',:contractText=>"No"}]
    uniq_alerts << [{:alert=>'1',:alertText=>"Yes"}, {:alert=>'0',:alertText=>"No"}]

    uniq_dates.to_a.sort!{|x,y| x[:date].to_s <=> y[:dateText].to_s}
    uniq_heards.to_a.sort!{|x,y| x[:heard].to_s <=> y[:heardText].to_s}
    return(render(:json =>{:results => past_opentimes.length,
          :date=>[uniq_dates.unshift({:date=>'all',:dateText=>"All Dates"})].flatten,
          :contract=>[uniq_contracts.unshift({:contract=>'all',:contractText=>"All Contract"})].flatten,
          :heard=>[uniq_heards.unshift({:heard=>'all',:heardText=>"All Heard about Us"})].flatten,
          :alert=>[uniq_alerts.unshift({:alert=>'all',:alertText=>"All Alerts"})].flatten
        }))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      unless session[:ID].blank?
        condition = ["past_opentimes.property_id = #{session[:ID]} "]
      else
        get_session_listing
        condition = [" past_opentimes.property_id = ? ", @property.id]

        unless session[:date] == "all"
          if(!session[:date].blank?)
            condition[0] += " AND past_opentimes.date = ?"
            condition <<  session[:date]
            condition[0] += " AND past_opentimes.start_time = ?"
            condition <<  session[:start_time]
            condition[0] += " AND past_opentimes.end_time = ?"
            condition <<  session[:end_time]
          end
        end

        unless session[:contract] == "all"
          if(!session[:contract].blank?)
            condition[0] += " AND contact_pastopentime_relations.contract = ?"
            condition <<  session[:contract]
          end
        end

        unless session[:alert] == "all"
          if(!session[:alert].blank?)
            condition[0] += " AND contact_pastopentime_relations.alert = ?"
            condition <<  session[:alert]
          end
        end

        unless session[:heard] == "all"
          if(!session[:heard].blank?)
            condition[0] += " AND contact_pastopentime_relations.heard_from_id = ?"
            condition <<  session[:heard]
          end
        end
      end
    end

    params[:page] = params[:start].to_i / (params[:limit].to_i + 1)
    order = ContactPastopentimeRelation.build_sorting(params[:sort],params[:dir])
    @past_opentimes = ContactPastopentimeRelation.paginate(:all, :include => [:past_opentime, :agent_contact, :heard_from], :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    results = 0
    if params[:limit].to_i == 20
      results = ContactPastopentimeRelation.find(:all, :include => [:past_opentime, :agent_contact],:select => 'count(contact_pastopentime_relations.id)',:conditions => condition,:limit => params[:limit].to_i)
    else
      results = ContactPastopentimeRelation.count(:include => [:past_opentime, :agent_contact], :conditions => condition)
    end

    return(render(:json =>{:results => results, :rows=>
             @past_opentimes.map{|x|{
               'id' => x.id.to_s + "~" + x.agent_contact.id.to_s,
              'date' => (x.past_opentime.date.blank? ? "" : format_date(x.past_opentime)),
              'first_name' => (x.agent_contact.first_name.blank? ? "" : x.agent_contact.first_name.to_s),
              'last_name' => (x.agent_contact.last_name.blank? ? "" : x.agent_contact.last_name.to_s),
              'contract' => (x.contract == true ? "Yes" : "No"),
              'heard' => (x.heard_from_id.blank? ? "" : x.heard_from.name),
              'alert' => (x.alert == true ? "Yes" : "No"),
              'actions'=> ''
            }
    }}))
  rescue
    return(render(:json =>{:results => nil, :rows=> nil}))
  end

  def format_date(x)
    f_date = x.date.strftime("%d %b %y, %a") + " From " + x.start_time.strftime("%I:%M %p") + " To " + x.end_time.strftime("%I:%M %p")
  end

  def get_session_listing
    unless params[:date].nil?
      unless params[:date]=='all'
        all = params[:date].split('~')
        session[:date] = all[0].to_date unless all[0].nil?
        session[:start_time] = all[1].to_time unless all[1].nil?
        session[:end_time] = all[2].to_time unless all[2].nil?
      else
        session[:date] = "all"
      end
    else
      session[:date] = "all"
    end

    session[:contract] = params[:contract].nil? ? "all" : params[:contract]
    session[:heard] = params[:heard].nil? ? "all" : params[:heard]
    session[:alert] = params[:alert].nil? ? "all" : params[:alert]
    session[:ID] = params[:ID].nil? ? nil : params[:ID]
    session[:sort_by] = "past_opentimes.date ASC"
  end

  def nil_session_listing
    session[:date] = nil
    session[:start_time] = nil
    session[:end_time] = nil
    session[:contract] = nil
    session[:heard] = nil
    session[:alert] = nil
  end
end
