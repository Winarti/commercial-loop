class MediaController < ApplicationController
  layout "agent"
  before_filter :login_required, :except => [:update_description,:create_video, :video_ipn, :update_images,:update_plans,:destroy_image,:destroy_plan,:update_brochure,:delete_brochure,:update_mp3,:delete_mp3,:update_logo,:delete_logo,:update_uploaded_video,:destroy_uploaded_video]
  #require_role ['AgentUser','DeveloperUser'],:except => [:update_description,:create_video, :video_ipn, :update_images,:update_plans,:destroy_image,:destroy_plan,:update_brochure,:delete_brochure,:update_mp3,:delete_mp3]

  before_filter :get_agent_and_office, :get_property
  before_filter :check_permission,:except => [:update_description,:load_embed_video, :create_video, :video_ipn, :update_images,:update_plans,:destroy_image,:destroy_plan,:update_brochure,:delete_brochure,:update_mp3,:delete_mp3,:update_logo,:delete_logo,:update_uploaded_video,:destroy_uploaded_video]
  before_filter :check_permission_page, :only => [:index]
  before_filter :dont_send_alert, :only => [:update_simple_brochures, :update_mp3, :update_simple_images]
  protect_from_forgery :except => [:update_description,:load_embed_video, :create_video, :video_ipn, :update_images,:update_plans,:destroy_image,:destroy_plan,:update_brochure,:delete_brochure,:update_mp3,:destroy_mp3,:update_logo,:destroy_logo, :sort_images, :sort_plans, :sort_brochure,:update_uploaded_video,:destroy_uploaded_video]
  skip_before_filter :verify_authenticity_token , :only => [:destroy_brochure]

  def index
    uploader  = @office.uploader
    unless params[:isBrowser].blank?
     uploader = (params[:isBrowser] == 'true') ? true :false
     ActiveRecord::Base.connection.execute("UPDATE offices SET uploader= #{(params[:isBrowser] == 'true') ? 1 : 0} WHERE ID=#{@office.id}") unless @office.uploader.to_s == params[:isBrowser]
    end

    if uploader == true
      unless flash[:image_errors].blank?
        flash[:notice] = nil
        @photo_errors = flash[:image_errors]
      end

      unless flash[:plan_errors].blank?
        flash[:notice] = nil
        @plan_errors = flash[:plan_errors]
      end

      unless flash[:brochure_errors].blank?
        flash[:notice] = nil
        @brochure_position = flash[:brochure_position]
        @brochure_errors = flash[:brochure_errors]
      end
      @mp3 = @property.mp3 || @property.build_mp3
      @uploaded_video = @property.uploaded_video || @property.build_uploaded_video
      @logo = @property.property_logo || @property.build_property_logo
    end

    redirect_url = agent_office_property_media_url(@agent, @office, @property)
    if !params[:status].blank? and !params[:id].blank?
      if params[:status] == '200'
        PropertyVideo.create({:property_id => @property.id, :youtube_id => params[:id], :title => params[:id]}) unless params[:id].blank?
        flash[:notice] = "Successfully upload Video to youtube"
        redirect_to redirect_url and return
      end
    end

    if params[:token].blank?
      @youtube_login = "http://www.youtube.com/auth_sub_request?scope=http://gdata.youtube.com&session=0&next=#{redirect_url}&secure=0"
    else
      token = params[:token]
      dev_key = 'AI39si4IogmMCkGt-MYz9yRP8CHD9x-YySmIUJGXzxI2jm5B8PuKERk5GFBEGxen80yUwNO9hP4YJ3RI-S6ButpSFBvgIX8dhQ'
      t = Time.now
      title = description = key = 'agentpoint' + t.strftime("%m/%d/%Y-%H:%I:%S").to_s
      xml = "<entry xmlns='http://www.w3.org/2005/Atom' xmlns:media='http://search.yahoo.com/mrss/' xmlns:yt='http://gdata.youtube.com/schemas/2007'><media:group><media:title type='plain'>#{title}</media:title><media:description type='plain'>#{description}</media:description><media:category scheme='http://gdata.youtube.com/schemas/2007/categories.cat'>People</media:category><media:keywords>#{key}</media:keywords></media:group></entry>"
      xml_data = `echo \"#{xml}\" | curl -X POST -d @- -H 'Host: gdata.youtube.com' -H 'Authorization: AuthSub token=#{token}' -H 'GData-Version: 2' -H 'X-GData-Key: key=#{dev_key}' -H 'Content-Type:application/atom+xml' http://gdata.youtube.com/action/GetUploadToken`

      require 'net/http'
      require 'rexml/document'
      doc = REXML::Document.new(xml_data)
      unless doc.root.blank?
        @upload_url = doc.root.elements["url"].text unless doc.root.elements["url"].blank?
        @token_url = doc.root.elements["token"].text unless doc.root.elements["token"].blank?
        @upload_url = @upload_url.to_s + "?nexturl=#{redirect_url}"
      end
    end
    render 'media/simple_media' if uploader == true
  end

  def simple_media
    redirect_to(:action => :index)
  end

  def update_simple_images
    begin
      redirect_to(:action => 'index') and return unless request.post?
      image_errors= []
      flash[:image_errors] = {}
      unless params[:images].blank?
        params[:images].each do |img|
          original = @property.images.find_by_id(img[:id])
          #@property.send_image_api = true
          if img[:uploaded_data]
            if original
              original.uploaded_data = img[:uploaded_data]
              original.description = img[:description]
              original.dimension = Image::PHOTO
              original.updated_at = Time.now
              media_changes = {"image" => original.changes}
              ChangesLog.create_log(@property, original, media_changes, current_user, @office, 2) if original.changes.present?
              original.save
              ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{original.position} WHERE parent_id = #{original.id}")
              check_and_upload_wmark(@property.office.watermark)
#              (original.add_watermark(@property.office.watermark.public_filename) unless @property.office.watermark.blank?) if @property.office.include_watermark
            else
              if @property.images.size < Property::MAX_PHOTO
                image = Image.new(img)
                if !image.validate_photos
                  image.errors.each_with_index do |k,index|
                    image_errors.push(k[1].humanize)
                  end
                else
                  @property.images << image
                  image.update_attribute(:updated_at,Time.now)
                  (image.add_watermark(@property.office.watermark.public_filename) unless @property.office.watermark.blank?) if @property.office.include_watermark
                end
              end
              if image.present? && image.id.present?
                ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{image.position} WHERE parent_id = #{image.id}")
                ChangesLog.create_media_creation_log(@office, image, @property, current_user, 1)
              end
            end
          elsif img[:description]
            unless original.nil?
              original.update_attribute(:description, img[:description])
              original.update_attribute(:updated_at ,Time.now)
            end
          end
        end
      end

      unless @property.images.blank?
        @property.updated_at = Time.now
        @property.image_exist =true
        @property.status = 6  if current_user.allow_property_owner_access?
        @property.images.each do |image|
          ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{image.position} WHERE parent_id = #{image.id}")
        end
        @property.save(false)
        session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_photo-1"
      end

      unless image_errors.blank?
        flash[:image_errors][:message] =  "Error for uploading #{image_errors.uniq.join(", ").to_s}, you can only upload photos with JPEG format".humanize
      else
        flash[:notice] = "Photos updated."
      end
      users = @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles")
      users.each do |user|
        UserMailer.deliver_media_notification_for_level1(user, current_user, @office, @property, "Image") if current_user.allow_property_owner_access? and @office.property_owner_access? and @dont_send.blank?
      end
      unless params[:come_from].blank?
        return render(:layout => false)
      end
      redirect_to agent_office_property_media_path(@agent,@office,@property)

    rescue Exception => ex
      Log.create(:message => "Update image ---> errors : #{ex.inspect} ")
      @property.save(false) if @property.errors.blank?
      redirect_to agent_office_property_media_path(@agent,@office,@property)
    end

  end

  def update_images
   begin
    return render(:text =>"Limit Error") if (@property.images.length + 1) > Property::MAX_PHOTO
    image = Image.new(:uploaded_data => params[:uploaded_data])
    image.description = params[:description]
    image.attachable = @property
    image.updated_at = Time.now
    if image.validate_photos && image.save
      @property.updated_at = Time.now
      @property.image_exist =true
      @property.save(false)
      session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_photo-1"
      curr_user = User.find_by_id(params[:user_id].to_i)
      @property.status = 6  if curr_user.allow_property_owner_access?
      ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{image.position} WHERE parent_id = #{image.id}")
      check_and_upload_wmark(@property.office.watermark)
#      (image.add_watermark(@property.office.watermark.public_filename) unless @property.office.watermark.blank?) if @property.office.include_watermark
      flash_uploader_send_api(@property, @office, params[:user_id].to_i, "Image")
      ChangesLog.create_media_creation_log(@office, image, @property, curr_user, 1)

      unless @property.images.blank?
        @property.images.each do |img|
          ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{img.position} WHERE parent_id = #{img.id}")
        end
      end
      return render(:text => "#{params[:unit_id]},photo_images_#{params[:unit_id]},#{image.id},#{image.description},#{image.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/destroy_image?image_id=#{image.id}"},#{image.public_filename},image,#{image.public_filename(:thumb)},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/update_description"}")
    end
    return render(:nothing => true)
   rescue Exception => ex
     Log.create(:message => "error : #{ex.inspect}")
    return render(:nothing => true)
   end
  end

  def check_and_upload_wmark(wmark)
    if wmark.present?
      if !wmark.moved_to_s3
        wmark.upload_to_s3
      end
    end
  end

  def flash_uploader_send_api(property, office, current_user_id, type)
    id_exist = nil
    jobs = Delayed::Job.all
    unless jobs.blank?
      jobs.each do |job|
        handler = job.handler.split
        if handler[1].include?("FlashUploaderSenderApi") && handler[2].include?("property_id") && property.id.to_i == handler[3].to_i
          id_exist = true
          break
        end
      end
    end
    Delayed::Job.enqueue(FlashUploaderSenderApi.new(property.id, office.id, current_user_id, type), 3, 60.seconds.from_now.getutc) if id_exist.blank?
  end

  def sort_images
    begin
      @property.images.each do |img|
        unless params["image"].index(img.id.to_s).blank?
          img.position = params["image"].index(img.id.to_s) + 1
          img.save(false)
          ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{img.position} WHERE parent_id = #{img.id}")
        end
      end
      sort_send_api(@property, "Image")
      session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_photo-1"
      render :nothing => true
    rescue
    end
  end

  def sort_send_api(property, type)
      id_exist = nil
      jobs = Delayed::Job.all
      unless jobs.blank?
        jobs.each do |job|
          handler = job.handler.split
          if handler[1].include?("DelayJobSenderApi") && handler[2].include?("property_id") && property.id.to_i == handler[3].to_i
            id_exist = true
            break
          end
        end
      end
      Delayed::Job.enqueue(DelayJobSenderApi.new(property.id, type), 3, 45.seconds.from_now.getutc) if id_exist.blank?
  end

  def destroy_image
    del_st = false
    begin
      attachment = Attachment.find(params[:image_id])
     ActiveRecord::Base.connection.execute("DELETE FROM attachments WHERE id = #{params[:image_id]} OR parent_id = #{params[:image_id]}")
     del_st = true
    rescue
    end

    if del_st == true
     spawn(:kill => true) do
        @property.updated_at = Time.now
        @property.image_exist = false if @property.images.blank?
        @property.save(false)
        ChangesLog.create_media_destroy_log(@office, attachment, @property, current_user, 3)
      end
    end
    session[:update_listing] = @property.images.blank? ? "#{@property.type.to_s.underscore}-#{@property.id}-icon_photo-0" : "#{@property.type.to_s.underscore}-#{@property.id}-icon_photo-1"
    respond_to do |format|
      format.html {
        flash[:notice] = "Photo deleted."
        redirect_to :action => 'index'
      }
      format.js {
        render :update do |page|
          if del_st == true
            page["image_#{params[:image_id]}"].remove #fadeOut
            page << "update_orders('photo_image')"
            page << "sort_images()"
            page << "update_photo_button(#{Property::MAX_PHOTO})"
          else
            page["del_img_#{params[:image_id]}"].attr("src", "/images/delete.gif")
          end
        end
      }
    end
   end

  def update_simple_plans
    plan_errors = []
    flash[:plan_errors] = {}
    unless params[:images].blank?
      params[:images].each do |img|
        original = @property.floorplans.find_by_id(img[:id])
        if img[:uploaded_data]
          if original
            original.uploaded_data = img[:uploaded_data]
            original.description = img[:description]
            original.dimension = Floorplan::FLOORPLAN
            original.updated_at = Time.now
            media_changes = {"floor_plan" => original.changes}
            ChangesLog.create_log(@property, original, media_changes, current_user, @office, 2) if original.changes.present?
            original.save
            ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{original.position} WHERE parent_id = #{original.id}")
            @property.updated_at = Time.now
            @property.floorplan_exist =true
            @property.save(false)
            @property.update_attribute(:status , 6) if current_user.allow_property_owner_access?
          else
            if @property.floorplans.size < Property::MAX_PLAN
              image = Floorplan.new(img)

              unless image.validate_floorplan
                image.errors.each_with_index do |k,index|
                  plan_errors.push(k[1].humanize)
                end
              else
                @property.floorplans << image
                image.update_attribute(:updated_at,Time.now)
              end
              @property.updated_at = Time.now
              @property.floorplan_exist = true
              @property.save(false)
              @property.update_attribute(:status , 6) if current_user.allow_property_owner_access?
            end
            unless @property.floorplans.blank?
              @property.updated_at = Time.now
              @property.floorplan_exist = true
              @property.save(false)
            end
            if image.id.present?
              ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{image.position} WHERE parent_id = #{image.id}")
              ChangesLog.create_media_creation_log(@office, image, @property, current_user, 1)
            end
          end
        elsif img[:description]
          unless original.nil?
            original.update_attribute(:description, img[:description])
            original.update_attribute(:updated_at,Time.now)
            @property.updated_at = Time.now
            @property.floorplan_exist = true
            @property.save(false)
            @property.update_attribute(:status , 6) if current_user.allow_property_owner_access?
          end
          unless @property.floorplans.blank?
            @property.updated_at = Time.now
            @property.floorplan_exist = true
            @property.save(false)
          end
        end
      end
    end
    session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_plan-1"
    unless plan_errors.blank?
    	flash[:plan_errors][:message] =  "Error for uploading #{plan_errors.uniq.join(", ").to_s}, you can only upload Floorplan images with JPEG/GIF format".humanize
    else
    	flash[:notice] = "Floorplans updated."
    end
    unless params[:come_from].blank?
      return render(:layout => false)
    end

    redirect_to agent_office_property_media_path(@agent,@office,@property)
  end

  def update_plans
    return render(:text =>"Limit Error") if (@property.floorplans.length + 1) > Property::MAX_PLAN
    plan = Floorplan.new(:uploaded_data => params[:uploaded_data])
    plan.description = params[:description]
    plan.updated_at = Time.now
    plan.attachable = @property
    if plan.validate_floorplan && plan.save
      curr_user = User.find_by_id(params[:user_id].to_i)
      @property.updated_at = Time.now
      @property.floorplan_exist = true
      @property.save(false)
      @property.update_attribute(:status , 6) if curr_user.present? && curr_user.allow_property_owner_access?
      session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_plan-1"
      ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{plan.position} WHERE parent_id = #{plan.id}")
      flash_uploader_send_api(@property, @office, params[:user_id].to_i, "Plan")
      ChangesLog.create_media_creation_log(@office, plan, @property, current_user, 1)

      return render(:text => "#{params[:unit_id]},plan_images_#{params[:unit_id]},#{plan.id},#{plan.description},#{plan.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/destroy_plan?image_id=#{plan.id}"},#{plan.public_filename},plan,#{plan.public_filename(:thumb)},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/update_description"}")
    end
    return render(:nothing => true)
  end

  def sort_plans
    position = []
    @property.floorplans.each do |img|
      unless params["plan"].index(img.id.to_s).blank?
        img.position = params["plan"].index(img.id.to_s) + 1

        if img.changes.present?
          position << {"image_id" => img.id, "image_position" => img.changes}
        end

        img.save(false)
        ActiveRecord::Base.connection.execute("UPDATE attachments SET position = #{img.position} WHERE parent_id = #{img.id}")
      end
    end
    sort_send_api(@property, "Plan")

    update_position = {"floor_plan" => position}
    ChangesLog.create_log(@property, nil, update_position, current_user, @office, 2)

    session[:update_listing] = "#{@property.type.to_s.underscore}-#{@property.id}-icon_plan-1"
    render :nothing => true
  end

  def destroy_plan
   del_st = false
   begin
     attachment = Attachment.find(params[:image_id])
#     img = @property.floorplans.find(params[:image_id])
#     dir = File.dirname(img.full_filename)
     ActiveRecord::Base.connection.execute("DELETE FROM attachments WHERE id = #{params[:image_id]} OR parent_id = #{params[:image_id]}")
     del_st = true
   rescue
   end

   if del_st == true
     spawn(:kill => true) do
        #@property.floorplans.find(params[:image_id]).destroy
#        unless dir.blank?
#          Dir.chdir(dir)
#          Dir.foreach(dir) do |files|
#            File.delete(files) if File.file?(files)
#          end
#          Dir.delete(dir)
#        end
        @property.updated_at = Time.now
        @property.floorplan_exist = false if @property.floorplans.blank?
        @property.status = 6 if current_user.allow_property_owner_access?
        @property.save(false)
        ChangesLog.create_media_destroy_log(@office, attachment, @property, current_user, 3)
      end
    end
    session[:update_listing] = @property.floorplans.blank? ? "#{@property.type.to_s.underscore}-#{@property.id}-icon_plan-0" : "#{@property.type.to_s.underscore}-#{@property.id}-icon_plan-1"
    respond_to do |format|
      format.html {
        flash[:notice] = "Plan deleted."
        redirect_to :action => 'index'
      }
      format.js {
        render :update do |page|
          if del_st == true
            page["plan_#{params[:image_id]}"].remove
            page << "update_orders('plan_image')"
            page << "update_plan_button(#{Property::MAX_PLAN})"
          else
            page["del_img_#{params[:image_id]}"].attr("src", "/images/delete.gif")
          end
        end
      }
    end
  end

  def update_brochure
    return render(:text =>"Limit Error") if (@property.brochure.length + 1) > Property::MAX_BROCHURE
    brochure = Brochure.new(:uploaded_data => params[:uploaded_data])
    brochure.title = params[:description]
    brochure.attachable = @property
    brochure.updated_at = Time.now
    if brochure.validate_brochures && brochure.save
      flash_uploader_send_api(@property, @office, params[:user_id].to_i, "Brochure")
      session[:update_listing] = "#{@property.type.to_s.underscore}_#{@property.id}-icon_brochure-1"
      ChangesLog.create_media_creation_log(@office, brochure, @property, current_user, 1)
#      brochure.upload_to_s3
      return render(:text => "#{params[:unit_id]},brochure_images_#{params[:unit_id]},#{brochure.id},#{brochure.title},#{brochure.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/destroy_brochure?image_id=#{brochure.id}"},#{brochure.public_filename},brochure,#{brochure.public_filename},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/update_description"}")
    end
    return render(:nothing => true)
  end

  def update_simple_brochures
    unless params[:brochures].blank?
     params[:brochures].each do |b|
      unless (@property.brochure.length + 1) > Property::MAX_BROCHURE
       if b[:uploaded_data]
          original = @property.brochure.find_by_id(b[:id])
          if original
            original.uploaded_data = b[:uploaded_data]
            original.description = b[:description]
            original.updated_at = Time.now

            media_changes = {"brochure" => original.changes}
            ChangesLog.create_log(@property, original, media_changes, current_user, @office, 2) if original.changes.present?

            original.save
            @property.updated_at = Time.now
#            @property.floorplan_exist =true
            @property.save(false)
            @property.update_attribute(:status , 6) if current_user.allow_property_owner_access?
          else
            brochure = Brochure.new(:uploaded_data => b[:uploaded_data])
            brochure.title = b[:description]
            brochure.attachable = @property
            brochure.updated_at = Time.now
            if brochure.validate_brochures && brochure.save
#              brochure.upload_to_s3
              @property.updated_at = Time.now
              @property.status = 6 if current_user.allow_property_owner_access?
              @property.save(false)
              ChangesLog.create_media_creation_log(@office, brochure, @property, current_user, 1)
              session[:update_listing] = "#{@property.type.underscore}_#{@property.id}-icon_brochure-1"
              @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles").each do |user|
                UserMailer.deliver_media_notification_for_level1(user, current_user, @office, @property, "Brochure") if current_user.allow_property_owner_access? and @office.property_owner_access? and @dont_send.blank?
              end
             end
          end
        end
      end
     end
    end
    redirect_to agent_office_property_media_path(@agent,@office,@property)
  end

  def sort_brochure
    position = []
    @property.brochure.each do |b|
      unless params["brochure"].index(b.id.to_s).blank?
        b.position = params["brochure"].index(b.id.to_s) + 1

        if b.changes.present?
          position << {"brochure_id" => b.id, "brochure_position" => b.changes}
        end

        b.save(false)
      end
    end
    sort_send_api(@property, "Brochure")

    update_position = {"brochure" => position}
    ChangesLog.create_log(@property, nil, update_position, current_user, @office, 2)
    render :nothing => true
  end

  def destroy_brochure
    @property.brochure.find(params[:image_id]).destroy
    @property.updated_at = Time.now
    @property.status = 6 if current_user.allow_property_owner_access?
    @property.save(false)
    session[:update_listing] = @property.images.blank? ? "#{@property.type.underscore}_#{@property.id}-icon_brochure-0" : "#{@property.type.underscore}_#{@property.id}-icon_brochure-1"
    ChangesLog.create_media_destroy_log(@office, brochure, @property, current_user, 3)

    respond_to do |format|
      format.html {
        flash[:notice] = "Brochure deleted."
        redirect_to :action => 'index'
      }
      format.js {
        render :update do |page|
          page["brochure_#{params[:image_id]}"].remove #fadeOut
          page << "update_orders('brochure_image')"
        end
      }
    end
  end

  def update_mp3
    current_user = User.find_by_id(params[:user_id].to_i)
    return render(:text =>"Limit Error") unless @property.mp3.blank?
    audio = Mp3.new(:uploaded_data => params[:uploaded_data])
    audio.description = params[:description]
    audio.attachable = @property
    audio.updated_at = Time.now
    if audio.save
      @property.updated_at = Time.now
      @property.status = 6 if current_user.allow_property_owner_access?
      @property.save(false)
      session[:update_listing] = "#{@property.type.underscore}_#{@property.id}-icon_mp3-1"
      ChangesLog.create_media_creation_log(@office, audio, @property, current_user, 1)
      @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles").each do |user|
        UserMailer.deliver_media_notification_for_level1(user, current_user, @office, @property, "Mp3") if current_user.allow_property_owner_access? and @office.property_owner_access? and @dont_send.blank?
      end
      return render(:text => "#{params[:unit_id]},mp3_images_#{params[:unit_id]},#{audio.id},#{audio.description},#{audio.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/destroy_mp3?image_id=#{audio.id}"},#{audio.public_filename},mp3,#{audio.public_filename},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/update_description"}")
    end
    return render(:nothing => true)
  end

  def destroy_mp3
    mp3 = @property.mp3
    @property.mp3.destroy
    @property.updated_at = Time.now
    @property.status = 6 if current_user.allow_property_owner_access?
    @property.save(false)
    session[:update_listing] = @property.mp3.blank? ? "#{@property.type.underscore}_#{@property.id}-icon_mp3-0" : "#{@property.type.underscore}_#{@property.id}-icon_mp3-1"
    ChangesLog.create_media_destroy_log(@office, mp3, @property, current_user, 3)

    respond_to do |format|
      format.html {
        flash[:notice] = "MP3 deleted."
        redirect_to :action => 'index'
      }
      format.js {
        render :update do |page|
          page["mp3_#{params[:image_id]}"].remove #fadeOut
          page << "update_orders('mp3_image')"
        end
      }
    end
  end

  def update_logo
    if params[:upload_type] == "browsers"
      @data = params[:logo]
    else
      @data = params
    end
    old_logo = @property.property_logo
    logo = PropertyLogo.new(:uploaded_data => @data[:uploaded_data])
    logo.description = @data[:description]
    logo.attachable = @property
    logo.updated_at = Time.now
    if logo.save
      old_logo.destroy unless old_logo.blank?
      @property.updated_at = Time.now
      @property.save(false)
      if params[:upload_type] == "browsers"
        redirect_to agent_office_property_media_path(@agent,@office,@property) and return
      else
        return render(:text => "#{params[:unit_id]},logo_images_#{params[:unit_id]},#{logo.id},#{logo.description},#{logo.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/destroy_logo?image_id=#{logo.id}"},#{logo.public_filename},logo,#{logo.public_filename},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/update_description"}")
      end
    end

    if params[:upload_type] == "browsers"
      redirect_to agent_office_property_media_path(@agent,@office,@property) and return
    else
      return render(:nothing => true)
    end
  end

  def destroy_logo
    @property.property_logo.destroy
    @property.updated_at = Time.now
    @property.save(false)
    flash[:notice] = "Logo deleted."
    redirect_to :action => 'index' and return
  end

  def destroy_youtube_video
    property_video = PropertyVideo.find(params[:id])
    property_video.destroy
    @property.update_attribute(:updated_at,Time.now)
    redirect_to :action => 'index'
  end

  def load_embed_video
    property_video = PropertyVideo.find(params[:id])
    return(render(:json => property_video.embed_code))
  end

  def update_uploaded_video
#    return render(:text =>"Limit Error") if (@property.uploaded_video.length + 1) > Property::MAX_VIDEO
    unless @property.uploaded_video.blank?
      return render(:text =>"Limit Error")
    end
    uploaded_video = UploadedVideo.new(:uploaded_data => params[:uploaded_data])
    uploaded_video.title = params[:description]
    uploaded_video.attachable = @property
    uploaded_video.updated_at = Time.now

    if uploaded_video.save
      flash_uploader_send_api(@property, @office, params[:user_id].to_i, "UploadedVideo")
      session[:update_listing] = "#{@property.type.to_s.underscore}_#{@property.id}-icon_uploaded_video-1"
      ChangesLog.create_media_creation_log(@office, uploaded_video, @property, current_user, 1)

      return render(:text => "#{params[:unit_id]},uploaded_video_#{params[:unit_id]},#{uploaded_video.id},#{uploaded_video.title},#{uploaded_video.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/destroy_uploaded_video?image_id=#{uploaded_video.id}"},#{uploaded_video.public_filename},uploaded_video,#{uploaded_video.public_filename},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/media/update_description"}")
    end
    return render(:nothing => true)
  end

  def update_simple_uploaded_video
    unless params[:uploaded_video].blank?
       if params[:uploaded_video][:uploaded_data]
          original = @property.uploaded_video
          if original
            original.uploaded_data = params[:uploaded_video][:uploaded_data]
            original.description = params[:description]
            original.updated_at = Time.now
            media_changes = {"image" => original.changes}
            ChangesLog.create_log(@property, original, media_changes, current_user, @office, 2) if original.changes.present?
            original.save
            @property.updated_at = Time.now
#            @property.floorplan_exist =true
            @property.save(false)
            @property.update_attribute(:status , 6) if current_user.allow_property_owner_access?
          else
            uploaded_video = UploadedVideo.new(:uploaded_data => params[:uploaded_video][:uploaded_data])
            uploaded_video.title = params[:description]
            uploaded_video.attachable = @property
            uploaded_video.updated_at = Time.now
            if uploaded_video.save
              @property.updated_at = Time.now
              @property.status = 6 if current_user.allow_property_owner_access?
              @property.save(false)
              session[:update_listing] = "#{@property.type.underscore}_#{@property.id}-icon_uploaded_video-1"
              ChangesLog.create_media_creation_log(@office, uploaded_video, @property, current_user, 1)
              @office.agent_users.find(:all, :conditions => "roles.id = 3 AND type = 'AgentUser'", :include => "roles").each do |user|
                UserMailer.deliver_media_notification_for_level1(user, current_user, @office, @property, "UploadedVideo") if current_user.allow_property_owner_access? and @office.property_owner_access? and @dont_send.blank?
              end
             end
          end
        end
    end
    redirect_to agent_office_property_media_path(@agent,@office,@property)
  end

  def destroy_uploaded_video
    uploaded_video = @property.uploaded_video
    @property.uploaded_video.destroy
    @property.updated_at = Time.now
    @property.status = 6 if current_user.allow_property_owner_access?
    @property.save(false)
    session[:update_listing] = @property.images.blank? ? "#{@property.type.underscore}_#{@property.id}-icon_uploaded_video-0" : "#{@property.type.underscore}_#{@property.id}-icon_uploaded_video-1"
    ChangesLog.create_media_destroy_log(@office, uploaded_video, @property, current_user, 3)

    respond_to do |format|
      format.html {
        flash[:notice] = "Uploaded video deleted."
        redirect_to :action => 'index'
      }
      format.js {
        render :update do |page|
          page["uploaded_video_#{params[:image_id]}"].remove #fadeOut
        end
      }
    end
  end

  def request_token
  end

  def video_ipn
  end

  def create_video
    if !params['video_url'].blank?
      begin
        require 'uri'
        require 'cgi'
        url_src = params['video_url'].strip
        uri = URI.parse(url_src)
        uri.scheme
        status = nil
        if uri.scheme == 'http' && uri.host == 'www.youtube.com'
          unless url_src.blank?
            url = url_src.split('?')
            unless url.blank?
              par = CGI::parse(url[1]) unless url[1].blank?
              vid = (par["v"].to_s.strip unless par["v"].blank?) unless par.blank?
            end
          end
        else
          unless url_src.blank?
            vid = url_src.to_s.strip
            status = 0
          end
        end
      rescue
      end
      if !vid.blank?
        PropertyVideo.create({:property_id => @property.id, :youtube_id => vid, :status => status, :title => params['title']})
      else
        flash[:notice] = "Please enter correct format!"
      end
    else
      PropertyVideo.create({:property_id => @property.id, :embed_code => params['embed_code'], :title => params['title_embed'], :status => true}) unless params['embed_code'].blank?
    end
    @property.update_attribute(:updated_at,Time.now)
    redirect_to :action => 'index'
  end

  def update_description
    params[:media_type] = "floorplan" if(params[:media_type] == "plan")
    media = Kernel.const_get(params[:media_type].to_s.capitalize).find_by_id(params[:media_id]) unless params[:media_type].blank?
    begin
      media.update_attribute("description", params["description"]) unless media.blank?
      ChangesLog.create_media_desc_log(@office, media, @property, current_user, 2)
    rescue
    end
    return render(:json =>(media.description unless media.blank?))
  end


  private
  def check_permission
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Media' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Media' AND user_type = 'AgentUser'")
  end

  def dont_send_alert
    if @office.edit_alert && current_user.allow_property_owner_access?
      @dont_send = true
    end
  end

end
