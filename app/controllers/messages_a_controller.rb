#--------------------------------------------------
# for agent version messages centre
#--------------------------------------------------
class MessagesAController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office
  before_filter :get_sort_by, :only => %w(index unread archives)
  before_filter :get_message, :only => %w(show edit update)
  before_filter :prepare_data
  before_filter :check_edit_permission, :only => %w(edit update)


  def index
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'All Messages' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    @title = "All Messages"
    @messages = current_user.messages.inbox.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page]) unless current_user.blank?
  end

  def new
    @message = DeveloperMessage.new(:recipient => (params[:recipient_id] || 0).to_i)
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Create a New Message' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) if !@active_help_agent.blank? && !current_user.blank?
    if params[:layout] == "false"
      render :action => "new_ajax", :layout => false
    else
      # render
    end
  end

  def edit
  end

  def update
    @message.attributes = params[:message]
    if @message.save_with_file

      redirect_to agent_message_path(@agent, @message)
      flash[:notice] = "Message has been updated."
    else
      render :action => "edit"
    end
  end

  def show
    redirect_to :action => "index" and return unless current_user.is_a? AgentUser
    @message.mark_read_for!(current_user)
  end

  def create
    @message = DeveloperMessage.new(params[:message].merge(:sender_id => current_user.id))
    @message.message_recipients.build(:user_id => @message.recipient)
    @agent.developer.developer_users.find(:all, :conditions => "`assign_to_all_ticket`=1").each do |user|
      @message.message_recipients.build(:user_id => user.id)
    end
    if @message.save_with_file
      @message.notify_recipients

      flash[:notice] = "Message has been sent."
      redirect_to agent_messages_path(@agent)
    else
      render :action => "new"
    end
  end

  def unread
    redirect_to :action => "index" and return unless current_user.is_a? AgentUser
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'View Unread' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    @title = "Unread Messages"
    @messages = current_user.unread_messages.filter_by_category(params[:category]).order_by(params[:sortby], current_user).paginate(:page => params[:page]) unless current_user.blank?
    render :action => "index"
  end

  def archives
    redirect_to :action => "index" and return unless current_user.is_a? AgentUser
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'View Archives' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?
    @title = "Archived Messages"
    @messages = current_user.messages.filter_by_category(params[:category]).archives.order_by(params[:sortby], current_user).paginate(:page => params[:page]) unless current_user.blank?
    render :action => "index"
  end

  private

  def prepare_data
    @priorities = [['Low', 'Low'],['Medium', 'Medium'],['High', 'High'],['Critical', 'Critical']]
  end

  def get_agent_and_office
    @agent= Agent.find params[:agent_id]
    @office = current_agent.offices.first
    @body_id = "messages"
  end

  def get_sort_by
    params[:sortby] ||= 'date'
  end

  def get_message
    @message = current_user.messages.find(params[:id]) unless current_user.blank?
  end

  def check_edit_permission
    redirect_to :action => 'index' and return unless @message.editable_by?(current_user)
  end
end
