class ProjectDevelopmentsController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new, :refresh]
  before_filter :get_agent_and_office

  def index
    @project_developments = ProjectDevelopment.find(:all, :conditions => ["`office_id` = ?", @office.id])
  end

  def show
  end

  def new
  end

  def edit
    @project_development = ProjectDevelopment.find(params[:id])
  end

  def create
   @project_development = ProjectDevelopment.new(params[:project_development].merge(:office_id => @office.id))
    respond_to do |format|
      if @project_development.save
        flash[:notice] = 'ProjectDevelopment was successfully created.'
        format.html { redirect_to :action => 'new' }
      else
        format.html { render :action => "new" }
      end
    end
  end

  def update
    @project_development = ProjectDevelopment.find(params[:id])
    respond_to do |format|
      if @project_development.update_attributes(params[:project_development])
        flash[:notice] = 'ProjectDevelopment was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @project_development = ProjectDevelopment.find_by_id(params[:id])
    @project_development.destroy unless @project_development.blank?
    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def refresh
    #@project_developments = ProjectDevelopment.find(:all, :conditions => ["`office_id` = ?", params[:office_id]]).map{|c| [c.name, c.id]}
    @project_developments = Property.find(:all, :conditions => ["`type` = 'NewDevelopment' And `office_id` = ?", params[:office_id]]).map{|c| [c.detail.development_name, c.id]}
    return(render(:json => @project_developments))
  end
end
