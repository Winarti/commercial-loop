# This controller handles the login/logout function of the site.
class SessionsController < ApplicationController

  skip_before_filter :verify_authenticity_token, :only => [:get_token,:do_login, :continue, :success]
  # render new.rhtml
  def new
    @popup = params[:popup]
    if logged_in?
      redirect_to find_user_home_path and return
    end

    @subdomain = get_sub_domain
    if cookies[:remember_login]
      require "base64"
      @login = cookies[:remember_login]
      @password = Base64.decode64(cookies[:remember_pass])
      @remember_me = 1
    end

    if check_sub_domain.length > 2 && check_sub_domain.length == 3
      redirect_to_full_url("http://#{check_sub_domain.second}.#{request.domain}", 307) and return
    end

    if @subdomain.blank? || %w(admin www).include?(@subdomain)
      render :layout => "login" and return
    end
    #    @developer = Developer.find_by_entry(@subdomain)
    #    unless @developer
    #      raise NotAuthorized
    #    end
    render :layout => "login"
  end

  def get_token
    require 'digest/sha1'
    token_xml = []
    if params[:login].present? && params[:password].present?
      begin
        subdomain = get_sub_domain
        if subdomain.blank? || %w(admin www).include?(subdomain)
          user = AdminUser.authenticate(params[:login], params[:password])
        else
          user = Developer.authenticate_user(subdomain, params[:login], params[:password])
        end
        if user
          token = Digest::SHA1.hexdigest("#{user.salt}-#{Time.now}")
          token_xml << {:success => Digest::SHA1.hexdigest("#{user.salt}-#{Time.now}")}
          ActiveRecord::Base.connection.execute("UPDATE users SET url_login_token = '#{token}' WHERE id = #{user.id}")
          if params[:redirect].present?
            ActiveRecord::Base.connection.execute("UPDATE users SET url_redirect = '#{params[:redirect]}' WHERE id = #{user.id}")
          end
        else
          token_xml << {:fail => "Fail to authenticate"}
        end
      rescue Exception => ex
        token_xml << {:fail => "#{ex.inspect}"}
      end
    else
      token_xml << {:fail => "Parameters incomplete"}
    end
    respond_to do |format|
      format.xml { render :xml => token_xml}
    end
  end

  def do_login
    if params[:token].present?
      user = User.find(:first, :conditions => "url_login_token = '#{params[:token]}' AND url_login_token IS NOT NULL")
      if user
        ActiveRecord::Base.connection.execute("UPDATE users SET url_login_token = NULL WHERE id = #{user.id}")
        if user.url_redirect.present?
          session[:return_to] = user.url_redirect
          ActiveRecord::Base.connection.execute("UPDATE users SET url_redirect = NULL WHERE id = #{user.id}")
        end
        self.current_user = user
        if params[:remember_me] == "1"
          require "base64"
          cookies[:remember_login] = { :value => params[:login], :expires => 3.days.from_now }
          cookies[:remember_pass] = { :value => Base64.encode64(params[:password]), :expires => 3.days.from_now }
        else
          cookies.delete :remember_login if cookies[:remember_login]
          cookies.delete :remember_pass if cookies[:remember_pass]
        end

        if current_user.limited_client_access? || current_user.allow_property_owner_access?
          redirect_to agent_office_path(user.agent,user.office)+"/properties"
        else
          redirect_back_or_default(user)
        end
        flash[:notice] = "Logged in successfully"
      else
        note_failed_signin
        @login       = params[:login]
        @remember_me = params[:remember_me]
        render :action => 'new', :layout => "login"
      end
    end
  end

  def create
    tmp_redirect = session[:return_to] unless session[:return_to].blank?
    logout_killing_session!
    session[:return_to] = tmp_redirect unless tmp_redirect.blank?

    subdomain = get_sub_domain
    if check_contact_login
      @agent_contact = AgentContact.find(session[:contact_login])
      if @agent_contact.access_type == "Vault"
        redirect_to contact_vault_path
      else
        redirect_to residential_projects_path
      end
    else
      if subdomain.blank? || %w(admin www).include?(subdomain)

        user = AdminUser.authenticate(params[:login], params[:password])
      else
        puts "#{subdomain}.dasdas"
        user = Developer.authenticate_user(subdomain, params[:login], params[:password])
      end

      #      if params[:login].include?("mandevlogin-")
      #        arr = params[:login].split("mandevlogin-")
      #        user = User.first(:conditions => ["id=?", arr[1]])
      #      end

      #      if params[:login] == "a"
      #        user = User.first(:conditions => ["type=?", "DeveloperUser"])
      #      elsif params[:login] == "b"
      #        user = User.first(:conditions => ["type=?", "AdminUser"])
      #      else
      #        user = User.first(:conditions => ["login=?", "asdfasdf"])
      #      end
      if user.present?
        if user.office_id.present?
          office = Office.find(user.office_id)
        end
      end
      if user.present? && office.present? && user.type == "AgentUser" && office.suspend_team
        flash[:error] = "Dear '#{user.login}',<br/>
Your access to Commercial Loop has been suspended due to overdue invoices. Please contact your accounts department and have them settle all outstanding invoices.<br/>
Access to your account will be reactivated as soon as your account is settled.<br/> Thanks."
        render :action => 'new', :layout => "login"
      elsif user
        # Protects against session fixation attacks, causes request forgery
        # protection if user resubmits an earlier form using back
        # button. Uncomment if you understand the tradeoffs.
        # reset_session
        self.current_user = user
        #new_cookie_flag = (params[:remember_me] == "1")
        #handle_remember_cookie! new_cookie_flag
        if params[:remember_me] == "1"
          require "base64"
          cookies[:remember_login] = { :value => params[:login], :expires => 3.days.from_now }
          cookies[:remember_pass] = { :value => Base64.encode64(params[:password]), :expires => 3.days.from_now }
        else
          cookies.delete :remember_login if cookies[:remember_login]
          cookies.delete :remember_pass if cookies[:remember_pass]
        end

        if current_user.limited_client_access? || current_user.allow_property_owner_access?
          if params[:popup].present?
            redirect_to success_path
          else
            redirect_to agent_office_path(user.agent,user.office)+"/properties"
          end
        else
          redirect_back_or_default(user)
        end
        flash[:notice] = "Logged in successfully"
      else
        note_failed_signin
        @login       = params[:login]
        @remember_me = params[:remember_me]
        render :action => 'new', :layout => "login"
      end
    end
  end

  def destroy
    logout_killing_session!
    flash[:notice] = "You have been logged out."
    redirect_back_or_default('/')
  end

  def api_login
    # commercialloop.com.au/api_login
    success = false
    errors = error_field = access_key = private_key = agent_id = office_id = ""

    if !params[:username].blank? && !params[:password].blank?
      subdomain = get_sub_domain
      if subdomain.blank? || %w(admin www).include?(subdomain)
        user = AgentUser.authenticate(params[:username], params[:password])
      else
        user = Developer.authenticate_user(subdomain, params[:username], params[:password])
      end

      if user
        success = true
        source_keys = (user.type == "DeveloperUser") ? user.developer : user.office.agent
        access_key = source_keys.access_key
        private_key = source_keys.private_key
        if user.type == "AgentUser"
          agent_id = user.office.agent.id
          office_id = user.office.id
        end
      else
        errors = "Username and password incorrect"
      end
    else
      errors = "Username and password can't be blank"
      error_field = "username, password"
    end
    data = params.delete_if{|x,y| %w(action submit controller).include?(x)}

    login_data = []
    login_data << {:success => success, :error_message => errors, :error_fields => error_field, :data => data, :keys => {:access_key => access_key, :private_key => private_key, :agent_id => agent_id, :office_id => office_id}}
    respond_to do |format|
      format.xml { render :xml => login_data}
    end
  end

  def continue
    render :layout => "login"
  end

  def success;end

  protected
  # Track failed login attempts
  def note_failed_signin
    flash[:error] = "Failed to login as '#{params[:login]}'"
    logger.warn "Failed login for '#{params[:login]}' from #{request.remote_ip} at #{Time.now.utc}"
  end

  def check_contact_login
    contact = AgentContact.authenticate(params[:login], params[:password])
    if !contact.blank?
      session[:contact_login] = contact.id
      return true
    else
      return false
    end
  end
end
