class ServicesController < Api::ApiBaseController
  before_filter :set_page

  def properties
    order = "properties.updated_at desc"
    order = "properties.id asc" unless params[:sort_by_id_asc].blank?
    order = "properties.id desc" unless params[:sort_by_price_desc].blank?
    order = "properties.price asc" unless params[:sort_by_price_asc].blank?
    order = "properties.price desc" unless params[:sort_by_price_desc].blank?

    @per_page = params[:per_page].blank? ? Property::PER_PAGE : params[:per_page].to_i

    sql = [" SELECT DISTINCT `properties`.`id`,`properties`.`suburb`,`properties`.`display_price`,`properties`.`display_price_text`,`properties`.`updated_at`,`properties`.`deal_type`,`properties`.`type`,`properties`.`status`,`properties`.`latitude`,`properties`.`longitude`,`properties`.`headline`,`properties`.`description`,`properties`.`property_type`,`properties`.`price`,`properties`.`unit_number`,`properties`.`street_number`,`properties`.`street`,`residential_sale_details`.`auction_place` AS `residential_sale_auction_place`,`commercial_details`.`auction_place` AS `commercial_auction_place`,`business_sale_details`.`auction_place` AS `business_sale_auction_place`,`residential_sale_details`.`auction_date` AS `residential_sale_auction_date`,`commercial_details`.`auction_date` AS `commercial_auction_date`,`commercial_details`.`date_available` AS `commercial_date_available`,`business_sale_details`.`auction_date` AS `business_sale_auction_date`,`residential_sale_details`.`auction_time` AS `residential_sale_auction_time`,`commercial_details`.`auction_time` AS `commercial_auction_time`,`business_sale_details`.`auction_time` AS `business_sale_auction_time`, `residential_sale_details`.`bedrooms` as residential_sale_bedrooms, `residential_lease_details`.`bedrooms` as residential_lease_bedrooms,`project_sale_details`.`bedrooms` as project_sale_bedrooms ,`residential_sale_details`.`bathrooms` as residential_sale_bathrooms, `residential_lease_details`.`bathrooms` as residential_lease_bathrooms, `residential_lease_details`.`date_available` as residential_lease_date_available,`project_sale_details`.`bathrooms`  as project_sale_bathrooms,`residential_sale_details`.`carport_spaces` as residential_sale_carport_spaces, `residential_lease_details`.`carport_spaces` as residential_lease_carport_spaces,`project_sale_details`.`carport_spaces` as project_sale_carport_spaces,`holiday_lease_details`.`bedrooms` as holiday_lease_bedrooms, `holiday_lease_details`.`bathrooms` as holiday_lease_bathrooms,`holiday_lease_details`.`carport_spaces` as holiday_lease_carport_spaces from `properties` "]
    count = [" SELECT DISTINCT COUNT(*) as data_length FROM `properties` "]
    sql[0] += " LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    count[0] += " LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    conditions = [" properties.office_id = #{params[:office_id]} "]
    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) AND properties.status NOT IN (2,3,4) "]
    sql_part3 = [" ORDER BY #{order} LIMIT #{(params[:page].to_i - 1) * @per_page},#{@per_page} "]

    sql[0] += sql_part1.to_s
    count[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    count[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    counting = ActiveRecord::Base.connection.execute(count.to_s)
    counting.all_hashes.each{|x| @total_entries = x['data_length']}
    @properties = @mysql_result.all_hashes
    @total_pages = ((@total_entries.to_i - 1) / @per_page.to_i ).to_f.floor + 1
    @current_page = params[:page]
    @mysql_result.free
    counting.free
    begin
      o = Office.find(params[:office_id])
      ag = o.agent
      dev_id = ag.developer_id
      if dev_id == 186
        Log.create(:message => "KF API params : #{params.inspect}")
      end
    rescue
    end
  rescue
  end

  def opentimes
    order = "properties.updated_at desc"
    order = "properties.id asc" unless params[:sort_by_id_asc].blank?
    order = "properties.id desc" unless params[:sort_by_id_desc].blank?
    order = "properties.price asc" unless params[:sort_by_price_asc].blank?
    order = "properties.price desc" unless params[:sort_by_price_desc].blank?

    @per_page = params[:per_page].blank? ? Property::PER_PAGE : params[:per_page].to_i

    sql = [" SELECT DISTINCT `properties`.`id`,`properties`.`suburb`,`properties`.`display_price`,`properties`.`display_price_text`,`properties`.`updated_at`,`properties`.`deal_type`,`properties`.`type`,`properties`.`status`,`properties`.`latitude`,`properties`.`longitude`,`properties`.`headline`,`properties`.`description`,`properties`.`property_type`,`properties`.`price`,`properties`.`unit_number`,`properties`.`street_number`,`properties`.`street`,`residential_sale_details`.`auction_place` AS `residential_sale_auction_place`,`commercial_details`.`auction_place` AS `commercial_auction_place`,`business_sale_details`.`auction_place` AS `business_sale_auction_place`,`residential_sale_details`.`auction_date` AS `residential_sale_auction_date`,`commercial_details`.`auction_date` AS `commercial_auction_date`,`business_sale_details`.`auction_date` AS `business_sale_auction_date`,`residential_sale_details`.`auction_time` AS `residential_sale_auction_time`,`commercial_details`.`auction_time` AS `commercial_auction_time`,`business_sale_details`.`auction_time` AS `business_sale_auction_time`, `residential_sale_details`.`bedrooms` as residential_sale_bedrooms, `residential_lease_details`.`bedrooms` as residential_lease_bedrooms,`project_sale_details`.`bedrooms` as project_sale_bedrooms ,`residential_sale_details`.`bathrooms` as residential_sale_bathrooms, `residential_lease_details`.`bathrooms` as residential_lease_bathrooms,`project_sale_details`.`bathrooms`  as project_sale_bathrooms,`residential_sale_details`.`carport_spaces` as residential_sale_carport_spaces, `residential_lease_details`.`carport_spaces` as residential_lease_carport_spaces,`project_sale_details`.`carport_spaces` as project_sale_carport_spaces,`holiday_lease_details`.`bedrooms` as holiday_lease_bedrooms, `holiday_lease_details`.`bathrooms` as holiday_lease_bathrooms,`holiday_lease_details`.`carport_spaces` as holiday_lease_carport_spaces from `properties` "]
    count = [" SELECT DISTINCT `properties`.`id` FROM `properties` "]
    sql[0] += " LEFT JOIN `opentimes` ON (`opentimes`.`property_id` = `properties`.`id` ) LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    count[0] += " LEFT JOIN `opentimes` ON (`opentimes`.`property_id` = `properties`.`id` ) LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    conditions = [" `opentimes`.`property_id` = `properties`.`id` AND properties.office_id = #{params[:office_id]}  AND properties.status NOT IN (2,3,4) "]
    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) "]
    sql_part3 = [" ORDER BY #{order} LIMIT #{(params[:page].to_i - 1) * @per_page},#{@per_page} "]

    sql[0] += sql_part1.to_s
    count[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    count[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    counting = ActiveRecord::Base.connection.execute(count.to_s)
    @total_entries = counting.all_hashes.length
    @properties = @mysql_result.all_hashes
    @total_pages = ((@total_entries.to_i - 1) / @per_page.to_i ).to_f.floor + 1
    @current_page = params[:page]
    @mysql_result.free
    counting.free
  rescue
  end

  def auction
    order = "properties.updated_at desc"
    order = "properties.id asc" unless params[:sort_by_id_asc].blank?
    order = "properties.id desc" unless params[:sort_by_id_desc].blank?
    order = "properties.price asc" unless params[:sort_by_price_asc].blank?
    order = "properties.price desc" unless params[:sort_by_price_desc].blank?

    @per_page = params[:per_page].blank? ? Property::PER_PAGE : params[:per_page].to_i

    sql = [" SELECT DISTINCT `properties`.`id`,`properties`.`suburb`,`properties`.`display_price`,`properties`.`display_price_text`,`properties`.`updated_at`,`properties`.`deal_type`,`properties`.`type`,`properties`.`status`,`properties`.`latitude`,`properties`.`longitude`,`properties`.`headline`,`properties`.`description`,`properties`.`property_type`,`properties`.`price`,`properties`.`to_price`,`properties`.`unit_number`,`properties`.`street_number`,`properties`.`street`,`residential_sale_details`.`auction_place` AS `residential_sale_auction_place`,`commercial_details`.`auction_place` AS `commercial_auction_place`,`business_sale_details`.`auction_place` AS `business_sale_auction_place`,`residential_sale_details`.`auction_date` AS `residential_sale_auction_date`,`commercial_details`.`auction_date` AS `commercial_auction_date`,`business_sale_details`.`auction_date` AS `business_sale_auction_date`,`residential_sale_details`.`auction_time` AS `residential_sale_auction_time`,`commercial_details`.`auction_time` AS `commercial_auction_time`,`business_sale_details`.`auction_time` AS `business_sale_auction_time`, `residential_sale_details`.`bedrooms` as residential_sale_bedrooms, `residential_lease_details`.`bedrooms` as residential_lease_bedrooms,`project_sale_details`.`bedrooms` as project_sale_bedrooms ,`residential_sale_details`.`bathrooms` as residential_sale_bathrooms, `residential_lease_details`.`bathrooms` as residential_lease_bathrooms,`project_sale_details`.`bathrooms`  as project_sale_bathrooms,`residential_sale_details`.`carport_spaces` as residential_sale_carport_spaces, `residential_lease_details`.`carport_spaces` as residential_lease_carport_spaces,`project_sale_details`.`carport_spaces` as project_sale_carport_spaces,`holiday_lease_details`.`bedrooms` as holiday_lease_bedrooms, `holiday_lease_details`.`bathrooms` as holiday_lease_bathrooms,`holiday_lease_details`.`carport_spaces` as holiday_lease_carport_spaces from `properties` "]
    count = [" SELECT DISTINCT COUNT(*) as data_length FROM `properties` "]
    sql[0] += " LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    count[0] += " LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "

    conditions = [" properties.office_id = #{params[:office_id]} "]
    conditions[0] += " AND( ( `residential_sale_details`.`forthcoming_auction` = 1 )  OR ( `commercial_details`.`forthcoming_auction` = 1 ) OR ( `business_sale_details`.`forthcoming_auction` = 1 ))  AND properties.status NOT IN (2,3,4) "

    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) "]
    sql_part3 = [" ORDER BY #{order} LIMIT #{(params[:page].to_i - 1) * @per_page},#{@per_page} "]

    sql[0] += sql_part1.to_s
    count[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    count[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    counting = ActiveRecord::Base.connection.execute(count.to_s)
    counting.all_hashes.each{|x| @total_entries = x['data_length']}
    @properties = @mysql_result.all_hashes
    @total_pages = ((@total_entries.to_i - 1) / @per_page.to_i ).to_f.floor + 1
    @current_page = params[:page]
    @mysql_result.free
    counting.free
    begin
      o = Office.find(params[:office_id])
      ag = o.agent
      dev_id = ag.developer_id
      if dev_id == 186
        Log.create(:message => "KF API params : #{params.inspect}")
      end
    rescue
    end
  rescue
  end

  def search
    order = "properties.updated_at desc"
    order = "properties.id asc" unless params[:sort_by_id_asc].blank?
    order = "properties.id desc" unless params[:sort_by_id_desc].blank?
    order = "properties.price asc" unless params[:sort_by_price_asc].blank?
    order = "properties.price desc" unless params[:sort_by_price_desc].blank?

    @per_page = params[:per_page].blank? ? Property::PER_PAGE : params[:per_page].to_i

    sql = [" SELECT DISTINCT `properties`.* ,`residential_sale_details`.`bedrooms` as residential_sale_bedrooms, `residential_lease_details`.`bathrooms` as residential_lease_bathrooms, `residential_lease_details`.`bedrooms` as residential_lease_bedrooms,`project_sale_details`.`bedrooms` as project_sale_bedrooms ,`residential_sale_details`.`bathrooms` as residential_sale_bathrooms, `residential_lease_details`.`date_available` as residential_lease_date_available,`project_sale_details`.`bathrooms`  as project_sale_bathrooms,`residential_sale_details`.`carport_spaces` as residential_sale_carport_spaces, `residential_lease_details`.`carport_spaces` as residential_lease_carport_spaces,`project_sale_details`.`carport_spaces` as project_sale_carport_spaces,`holiday_lease_details`.`bedrooms` as holiday_lease_bedrooms, `holiday_lease_details`.`bathrooms` as holiday_lease_bathrooms,`holiday_lease_details`.`carport_spaces` as holiday_lease_carport_spaces,`commercial_details`.`date_available` as commercial_date_available FROM `properties` "]
    count = [" SELECT DISTINCT COUNT(*) as data_length FROM `properties` "]
    sql[0] += " LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "
    count[0] += " LEFT JOIN `residential_sale_details` ON (`residential_sale_details`.`residential_sale_id` = `properties`.`id` ) LEFT JOIN `residential_lease_details` ON ( `properties`.`id` = `residential_lease_details`.`residential_lease_id`)  LEFT JOIN `project_sale_details` ON (`project_sale_details`.`project_sale_id` = `properties`.`id` ) LEFT JOIN `commercial_details` ON (`commercial_details`.`commercial_id` = `properties`.`id` )  LEFT JOIN `business_sale_details` ON (`business_sale_details`.`business_sale_id` = `properties`.`id` )  LEFT JOIN `holiday_lease_details` ON (`holiday_lease_details`.`holiday_lease_id` = `properties`.`id`  )   "

    conditions = [" properties.office_id = #{params[:office_id]} "]

    #STATUS = { :available => 1, :sold => 2, :leased => 3, :withdrawn => 4, :underoffer => 5 , :draft => 6}

    unless params["status"].blank?
      if params["status"] != "any"
        conditions[0] += " AND ( properties.status IN (#{params["status"]}) ) " unless params["status"].blank?
      end
    end

    unless params["property_type"].blank?
    if params["property_type"] != "any"
      property_types = [" "]
      types = params["property_type"].to_s.split(",")
      unless types.blank?
        if types.length > 1
          types.each_with_index do |type,index|
            type = type.to_s.split("_")
            type = type.join(" ") if type.length > 1
            property_types[0] += " properties.property_type = '#{type.strip}' " if index == 0
            property_types[0] += " OR properties.property_type = '#{type.strip}' "
          end
        else
          types = types.to_s.split("_")
          types = types.join(" ") if types.length > 1
          property_types[0] += " properties.property_type = '#{types.to_s.strip}' "
        end
        conditions[0] += " AND ( #{property_types.to_s} ) "
      end
    end
    end

    unless params["listing_type"].blank?
    if params["listing_type"] != "any"
      listing_types = [" "]
      ltypes = params["listing_type"].to_s.split(",")
      unless ltypes.blank?
        if ltypes.length > 1
          ltypes.each_with_index do |type,index|
            listing_types[0] += " properties.type = '#{type.strip}' " if index == 0
            listing_types[0] += " OR properties.type = '#{type.strip}' "
          end
        else
          listing_types[0] += " properties.type = '#{ltypes.to_s.strip}' "
        end
        conditions[0] += " AND ( #{listing_types.to_s} ) "
      end
    end
    end

    if params["suburb"] != "any"
      holder = []
      unless params["suburb"].blank?
        suburbs = params["suburb"].split(",")

        if suburbs.length > 1
          suburbs.each_with_index do |suburb,index|
            params["suburb"] = suburb.to_s.split("_")
            params["suburb"] = params["suburb"].join(" ") if params["suburb"].length > 1
            holder[index] = params["suburb"]
          end
        else
          params["suburb"] = params["suburb"].to_s.split("_")
          params["suburb"] = params["suburb"].join(" ") if params["suburb"].length > 1
        end
        conditions[0] += " AND properties.suburb = '#{params["suburb"]}' " if suburbs.length == 1
        if suburbs.length > 1
          suburbs = holder.flatten.map{|suburb| "'#{suburb}'"}.join(",").to_s
          conditions[0] += " AND properties.suburb IN(#{suburbs}) "
        end
      end
    end

    if !params["minprice"].blank? and !params["maxprice"].blank?
      conditions[0] += " AND properties.price > #{params["minprice"]}" if params["minprice"] != 0 and params["maxprice"] == "any"
      conditions[0] += " AND properties.price < #{params["maxprice"]} " if params["minprice"] == 0 and params["maxprice"] != "any"
      conditions[0] += " AND properties.price BETWEEN #{params["minprice"]} AND #{params["maxprice"]} " if params["minprice"] != 0 and params["maxprice"] != "any"
    end

    if params["listing_type"] == "any" || params["listing_type"].blank?
      if params["type"].to_s.downcase == "sale"
        conditions[0] += " AND properties.type IN('ResidentialSale','ProjectSale','BusinessSale','Commercial') AND (properties.deal_type IN('Sale','Both') OR properties.deal_type IS NULL)"
      end

      if params["type"].to_s.downcase == "lease"
        conditions[0] += " AND properties.type IN('ResidentialLease','HolidayLease','Commercial') AND (properties.deal_type IN('Lease','Both') OR properties.deal_type IS NULL)"
      end

      if params["type"].to_s.downcase == "sold"
        conditions[0] += " AND properties.status = #{Property::STATUS[:sold]} "
      end
    end

    if params["deal_type"] != "any"
      unless params["deal_type"].blank?
        if params["deal_type"].to_s.downcase == "sale"
          conditions[0] += " AND (properties.deal_type IN('Sale','Both') OR properties.deal_type IS NULL)"
        end
        if params["deal_type"].to_s.downcase == "lease"
          conditions[0] += " AND (properties.deal_type IN('Lease','Both') OR properties.deal_type IS NULL)"
        end
      end
    end

    residential_sale_cond = [" 1=1 "]
    project_sale_cond = [" 1=1 "]
    residential_lease_cond = [" 1=1 "]
    holiday_lease_cond = [" 1=1 "]

    unless params["bedrooms"].blank?
      if params["bedrooms"] != "any"
        residential_sale_cond[0] += " AND ( `residential_sale_details`.`bedrooms` >= #{params["bedrooms"]} ) "
        project_sale_cond[0] += " AND ( `project_sale_details`.`bedrooms` >= #{params["bedrooms"]} ) "
        residential_lease_cond[0] += " AND ( `residential_lease_details`.`bedrooms` >= #{params["bedrooms"]} ) "
        holiday_lease_cond[0] += " AND ( `holiday_lease_details`.`bedrooms` >= #{params["bedrooms"]} ) "
      end
    end

    unless params["bathrooms"].blank?
      if params["bathrooms"] != "any"
        residential_sale_cond[0] += " AND ( `residential_sale_details`.`bathrooms` >= #{params["bathrooms"]} ) "
        project_sale_cond[0] += " AND ( `project_sale_details`.`bathrooms` >= #{params["bathrooms"]} ) "
        residential_lease_cond[0] += " AND ( `residential_lease_details`.`bedrooms` >= #{params["bathrooms"]} ) "
        holiday_lease_cond[0] += " AND ( `holiday_lease_details`.`bathrooms` >= #{params["bathrooms"]} ) "
      end
    end

    unless params["carspaces"].blank?
      if params["carspaces"] != "any"
        residential_sale_cond[0] += " AND ( `residential_sale_details`.`carport_spaces` >= #{params["carspaces"]} ) "
        project_sale_cond[0] += " AND ( `project_sale_details`.`carport_spaces` >= #{params["carspaces"]} ) "
        residential_lease_cond[0] += " AND ( `residential_lease_details`.`carport_spaces` >= #{params["carspaces"]} ) "
        holiday_lease_cond[0] += " AND ( `holiday_lease_details`.`carport_spaces` >= #{params["carspaces"]} ) "
      end
    end

    conditions[0] += " AND ( (#{residential_sale_cond.to_s}) OR (#{project_sale_cond.to_s}) OR (#{residential_lease_cond.to_s}) OR (#{holiday_lease_cond.to_s}) ) "

    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" AND ( properties.deleted_at IS NULL ) "]
    sql_part3 = [" ORDER BY #{order} LIMIT #{(params[:page].to_i - 1) * @per_page},#{@per_page} "]

    sql[0] += sql_part1.to_s
    count[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s
    count[0] += sql_part2.to_s
    sql[0] += sql_part3.to_s
    @mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    counting = ActiveRecord::Base.connection.execute(count.to_s)
    counting.all_hashes.each{|x| @total_entries = x['data_length']}
    @properties = @mysql_result.all_hashes
    @total_pages = ((@total_entries.to_i - 1) / @per_page.to_i ).to_f.floor + 1
    @current_page = params[:page]
    @mysql_result.free
    counting.free
    begin
      o = Office.find(params[:office_id])
      ag = o.agent
      dev_id = ag.developer_id
      if dev_id == 186
        Log.create(:message => "KF API params : #{params.inspect}")
      end
    rescue
    end
  end

  def detail
    @property = Property.find(:first, :conditions => "id = '#{params[:id]}' And office_id='#{params[:office_id]}'")
    begin
      o = Office.find(params[:office_id])
      ag = o.agent
      dev_id = ag.developer_id
      if dev_id == 186
        Log.create(:message => "KF API params : #{params.inspect}")
      end
    rescue
    end
    return render(:xml => {:error => "Property not found"}) if @property.blank?
  end

  def suburb
    (return render(:nothing => true)) if params[:office_id].blank?
    begin
      o = Office.find(params[:office_id])
      ag = o.agent
      dev_id = ag.developer_id
      if dev_id == 186
        Log.create(:message => "KF API params : #{params.inspect}")
      end
    rescue
    end
    @per_page = params[:per_page].blank? ? Property::PER_PAGE : params[:per_page].to_i
    @suburbs = Property.find(:all, :conditions =>[" office_id = ? ", params[:office_id]], :select => "DISTINCT suburb as name", :limit =>"#{(params[:page].to_i - 1) * @per_page},#{@per_page} ")
    @total_pages = (@suburbs.length.to_i - 1 / @per_page.to_i ).to_f.floor + 1
    @current_page = params[:page]
  end

  def agents
    @office = Office.find_by_id(params[:office_id])
    @agents = @office.agent_users
  end

  def agent_property
    require 'to_xml_extensions'
    WillPaginateHelpers.param_request_uri = request.url()
    WillPaginateHelpers.param_mobile_service = "agent_properties"
    conditions = ["properties.agent_user_id = #{params[:agent_id]} OR properties.primary_contact_id = #{params[:agent_id]}"]
    unless params["status"].blank?
      if params["status"].downcase == "sold"
        conditions[0] += " AND properties.status = #{Property::STATUS[:sold]} "
      else
        conditions[0] += " AND properties.status = #{Property::STATUS[:leased]} "
      end
    end

    @properties = Property.paginate(:all,:conditions=>conditions,:limit =>10, :page =>params[:page], :per_page => 10)
    return render(:nothing => true) if @properties.blank?
    return render(:xml => @properties)
  rescue
    return render(:nothing => true)
  end

  def testimonial
    sql = [" SELECT `testimonials`.* FROM `testimonials` "]
    count = [" SELECT DISTINCT COUNT(*) as data_length FROM `testimonials` "]
    sql[0] += " LEFT JOIN `users` ON (`users`.`id` = `testimonials`.`agent_user_id`  ) "
    count[0] += " LEFT JOIN `users` ON (`users`.`id` = `testimonials`.`agent_user_id`  ) "
    conditions = [" `testimonials`.`agent_user_id` = #{params[:agent_id]} "]
    sql_part1 = [" WHERE ( #{conditions.to_s} ) "]
    sql_part2 = [" LIMIT #{(params[:page].to_i - 1) * 20},20 "]

    sql[0] += sql_part1.to_s
    count[0] += sql_part1.to_s
    sql[0] += sql_part2.to_s

    mysql_result = ActiveRecord::Base.connection.execute(sql.to_s)
    counting = ActiveRecord::Base.connection.execute(count.to_s)
    counting.all_hashes.each{|x| @total_entries = x['data_length']}
    @testimonials = mysql_result.all_hashes
    @total_pages = (@total_entries.to_i / 10 ).to_f.floor + 1
    @current_page = params[:page]
    mysql_result.free
    counting.free
  rescue
  end

  def send_property
    success = false
    error = ""
    begin
      unless params[:property_id].blank?
        p = Property.find_with_deleted(params[:property_id])
        unless p.blank?
          p.use_spawn
          success = true
        else
          error = "Property Not Found"
        end
      else
        error = "Property Id can't be blank"
      end
    rescue
    end
    respond_to do |format|
      format.xml { render :xml => [{:success => success, :error => error}]}
    end
  end

  def send_team
    success = false
    error = ""
    begin
      unless params[:user_id].blank?
        a = AgentUser.find_with_deleted(params[:user_id])
        unless a.blank?
          a.use_spawn
          success = true
        else
          error = "User Not Found"
        end
      else
        error = "User Id can't be blank"
      end
    rescue Exception => ex
      Log.create(:messages => "SEND TEAM err : #{ex.inspect}")
    end
    respond_to do |format|
      format.xml { render :xml => [{:success => success, :error => error}]}
    end
  end

  def send_office
    success = false
    error = ""
    begin
      unless params[:office_id].blank?
        o = Office.find_by_id(params[:office_id])
        unless o.blank?
          o.use_spawn
          success = true
        else
          error = "Office Not Found"
        end
      else
        error = "Office Id can't be blank"
      end
    rescue
    end
    respond_to do |format|
      format.xml { render :xml => [{:success => success, :error => error}]}
    end
  end

  def property_types
    if params[:type].blank?
      @ptypes = PropertyType.residential.map{|t| t.name }
    else
       @ptypes = PropertyType.residential.map{|t| t.name } if params[:type] == "residential"
       @ptypes = PropertyType.commercial.map{|t| t.name } if params[:type] == "commercial"
       @ptypes = PropertyType.business.map{|t| t.name } if params[:type] == "business"
       @ptypes = PropertyType.holiday.map{|t| t.name } if params[:type] == "holiday"
    end
  end

  private

  def set_page
    params[:page] = 1 if params[:page].blank?
  end

end