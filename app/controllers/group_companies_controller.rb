class GroupCompaniesController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation

  def index
    @group_company = GroupCompany.new
  end

  def show
  end

  def new
  end

  def edit
    @group_company = GroupCompany.find(params[:id])
  end

  def create
   @group_company = GroupCompany.new(params[:group_company].merge(:creator_id => current_user.id, :office_id => @office.id))

    respond_to do |format|
      if @group_company.save
        flash[:notice] = 'Mailing Group company was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    @group_company = GroupCompany.find(params[:id])

    respond_to do |format|
      if @group_company.update_attributes(params[:group_company])
        flash[:notice] = 'Mailing Group Contact was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    GroupCompanyRelation.find(:all, :conditions => { :group_company_id => params[:id] }).each(&:destroy)
    @group_company = GroupCompany.find(params[:id])
    @group_company.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def prepare_creation
    @groups = GroupCompany.paginate(:all, :conditions => ["`office_id` = ?", @office.id], :order => "`created_at` DESC", :page =>params[:page], :per_page => 10)
  end
end
