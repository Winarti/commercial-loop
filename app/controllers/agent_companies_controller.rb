class AgentCompaniesController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation
  before_filter :prepare_agent_office, :only => [:create, :update]
  skip_before_filter :verify_authenticity_token, :only => [:search]

  def index
    get_session_listing
    nil_session_value
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    @properties_filter = Property.find(:all,:include => [:primary_contact],:conditions=>["office_id=?",current_office.id], :select => "suburb,id, property_type,status,primary_contact_id");
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Listings' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?

    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def show

  end

  def new
    @agent_company = AgentCompany.new
  end

  def edit
    @agent_company = AgentCompany.find(params[:id])
    @selected_property_types = @agent_company.company_property_type_relations
    @property_type_ids = @selected_property_types.collect(&:property_type_id)
    require "base64"
    @agent_company.password = Base64.decode64(@agent_company.password) unless @agent_company.password.blank?
    @groups = @list_groups = @assigned_main_id = @access_main_id = @categories = @property_types = @list_property_types = @list_categories = @assigns_id = @list_assigns = @access = @list_access =''

    @agent_company.company_category_relations.map{|cat|
      @categories = @categories + '#' + cat.category_company_id.to_s
      @list_categories = @list_categories + '<div id="'+cat.category_company_id.to_s+'" style="margin-right:20px;">'+cat.category_company.name+'<a href="javascript:remove_category('+cat.category_company_id.to_s+')">x</a></div>'
    }

    unless @agent_company.company_property_type_relations.blank?
      @agent_company.company_property_type_relations.map{|ptype|
        @property_types = @property_types + '#' + ptype.property_type_id.to_s
        @list_property_types = @list_property_types + '<div id="'+ptype.property_type_id.to_s+'" class="property_type_selected" style="margin-right:20px;">'+ptype.property_type.category.to_s+' - '+ptype.property_type.name+'<a href="javascript:remove_property_type('+ptype.property_type_id.to_s+')">x</a></div>'
      }
    end

    @agent_company.company_assigned_relations.map{|cat|
      @assigns_id = @assigns_id + '#' + cat.assigned_to.to_s
      @list_assigns = @list_assigns + '<div id="'+cat.assigned_to.to_s+'" style="margin-right:20px;">'+assigned_name(cat.assigned_to.to_i)+'<a href="javascript:remove_assign('+cat.assigned_to.to_s+')">x</a></div>'
    }
    @assigned_main_id = @agent_company.company_assigned_relations.first.nil? ? nil : @agent_company.company_assigned_relations.first.assigned_to

    @agent_company.company_accessible_relations.map{|cat|
      @access = @access + '#' + cat.accessible_by.to_s
      @list_access = @list_access + '<div id="'+cat.accessible_by.to_s+'" style="margin-right:20px;">'+assigned_name(cat.accessible_by.to_i)+'<a href="javascript:remove_access('+cat.accessible_by.to_s+')">x</a></div>'
    }
    @agent_company.group_company_relations.map{|cat|
      @groups = @groups + '#' + cat.group_company_id.to_s
      @list_groups = @list_groups + '<div id="'+cat.group_company_id.to_s+'" style="margin-right:20px;">'+cat.group_company.name+'<a href="javascript:remove_group('+cat.group_company_id.to_s+')">x</a></div>'
    }
    @access_main_id = @agent_company.company_accessible_relations.first.nil? ? nil : @agent_company.company_accessible_relations.first.accessible_by
  end

  def create
    @agent_company = AgentCompany.new(params[:agent_company].merge(:creator_id => current_user.id))
    @agent_company.company_type = "Company"
    respond_to do |format|
      unless params[:categories_id].blank? || params[:agent_company][:industry_category_id].blank? || params[:property_types_id].blank?
        require "base64"
        @agent_company.password=Base64.encode64(params[:agent_company][:password].gsub(/\s+/, "")) unless params[:agent_company][:password].blank?

        if @agent_company.save
          add_multiple_value
          @agent_company.update_first_list
          @agent_company.use_spawn_for_boom
          @agent_company.use_spawn_for_md
          @agent_company.use_spawn_for_irealty
          flash[:notice] = 'Company was successfully created.'
          format.html { redirect_to agent_office_agent_contacts_path(@agent,@office) }
        else
          get_session_value
          @agent_company.password = ""
          feed_multiple_value
          format.html { render :action => "new" }
          format.xml  { render :xml => @agent_company.errors}
        end
      else
        @agent_company.errors.add(:category_company_id, " can't be blank")
        @agent_company.errors.add(:industry_category_id, " can't be blank") if params[:agent_company][:industry_category_id].blank?
        @agent_company.errors.add("Industry sub-category can't be blank") if params[:property_types_id].blank?
        feed_multiple_value
        get_session_value
        format.html { render :action => "new" }
        format.xml  { render :xml => @agent_company.errors}
      end
    end
  end

  def update
    @agent_company = AgentCompany.find(params[:id])
    respond_to do |format|
      unless params[:categories_id].blank? || params[:agent_company][:industry_category_id].blank? || params[:property_types_id].blank?
        require "base64"
        params[:agent_company][:password]=Base64.encode64(params[:agent_company][:password].gsub(/\s+/, "")) unless params[:agent_company][:password].blank?
        if @agent_company.update_attributes(params[:agent_company])
          add_multiple_value
          @agent_company.update_first_list
          @agent_company.use_spawn_for_boom
          @agent_company.use_spawn_for_md
          @agent_company.use_spawn_for_irealty
          flash[:notice] = 'Company was successfully updated.'
          format.html { redirect_to :action => 'edit' }
        else
          @agent_company.password = ""
          feed_multiple_value
          get_session_value
          format.html { render :action => "edit" }
          format.xml  { render :xml => @agent_company.errors}
        end
      else
        @agent_company.errors.add(:category_company_id, " can't be blank") if params[:categories_id].blank?
        @agent_company.errors.add(:industry_category_id, " can't be blank") if params[:agent_company][:industry_category_id].blank?
        @agent_company.errors.add("Industry sub-category can't be blank") if params[:property_types_id].blank?
        get_session_value
        feed_multiple_value
        format.html { render :action => "new" }
        format.xml  { render :xml => @agent_company.errors}
      end
    end
  end

  def get_session_value
    session[:industry_category_id] = params[:agent_company][:industry_category_id]
  end

  def nil_session_value
    session[:industry_category_id] = nil
  end

  def destroy
    @agent_company = AgentCompany.find(params[:id])
    @agent_company.destroy

    respond_to do |format|
      format.html { redirect_to agent_office_agent_contacts_path(@agent,@office)}
    end
  end

  def prepare_creation
    @countries = Country.find(:all,:conditions=>["display_country=?",true]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @category_companies = CategoryCompany.find(:all).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @property_types = PropertyType.find(:all).map{|x| [x.name, x.id]}.unshift(['Please select', nil])
    @industry_categories = []
    if @office.residential_sale == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Residential Sale"]).map{|c| [c.name, c.id]}
    end
    if @office.residential_lease == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Residential Lease"]).map{|c| [c.name, c.id]}
    end
    if @office.holiday_lease == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Holiday Lease"]).map{|c| [c.name, c.id]}
    end
    if @office.commercial == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Commercial"]).map{|c| [c.name, c.id]}
    end
    if @office.business_sale == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Business Sale"]).map{|c| [c.name, c.id]}
    end
    if @office.project_sale == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Project Sale"]).map{|c| [c.name, c.id]}
    end
    @team_members = @office.agent_users.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5", :include => "roles", :order => "`users`.first_name ASC").map{|u| [u.full_name, u.id]}.unshift(['All Team Members', nil]).uniq
    @heard_froms = HeardFrom.find(:all, :conditions => ["`status` = 'heard_office'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @group_companies = GroupCompany.find(:all).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @email_formats = [["Plain text", "Plain text"], ["Magazine Colour Format", "Magazine Colour Format"]]

    @residential_sale_options = PropertyType.residential
    @residential_lease_options = PropertyType.residential
    @commercial_options = PropertyType.commercial
    @houseland_options = PropertyType.residential
    @business_options = PropertyType.business
    @holiday_options = PropertyType.holiday
  end

  def prepare_agent_office
    params[:agent_company] = params[:agent_company].merge(:agent_id => params[:agent_id],:office_id => params[:office_id])
  end

  def feed_multiple_value
    @groups = @list_groups = @categories = @property_types = @list_property_types = @list_categories = @assigns_id = @list_assigns = @access = @list_access = @assigned_main_id = @access_main_id =''
    unless params[:groups_id].blank?
      @ids = params[:groups_id].split('#')
      @groups_data = GroupCompany.find(:all, :conditions => ['`id` IN (?)', @ids])
      @groups_data.map{|cat|
        @groups = @groups + '#' + cat.id.to_s
        @list_groups = @list_groups + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.name+'<a href="javascript:remove_group('+cat.id.to_s+')">x</a></span>'
      }
    end
    unless params[:categories_id].blank?
      @ids = params[:categories_id].split('#')
      @categories_data = CategoryCompany.find(:all, :conditions => ['`id` IN (?)', @ids])
      @categories_data.map{|cat|
        @categories = @categories + '#' + cat.id.to_s
        @list_categories = @list_categories + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.name+'<a href="javascript:remove_category('+cat.id.to_s+')">x</a></span>'
      }
    end
    unless params[:property_types_id].blank?
      @ptype_ids = params[:property_types_id].split('#')
      @property_types_data = PropertyType.find(:all, :conditions => ['`id` IN (?)', @ptype_ids])
      @property_types_data.map{|ptype|
        @property_types = @property_types + '#' + ptype.id.to_s
        @list_property_types = @list_property_types + '<span id="'+ptype.id.to_s+'" style="margin-right:20px;">'+ptype.name+'<a href="javascript:remove_category('+ptype.id.to_s+')">x</a></span>'
      }
      #      @agent_company.add_property_types(@ptype_ids)
    end
    unless params[:assigns_id].blank?
      @a_ids = params[:assigns_id].split('#')
      @assigns_data = User.find(:all, :conditions => ['`id` IN (?)', @a_ids])
      ck = 0
      @assigns_data.map{|cat|
        @assigns_id = @assigns_id + '#' + cat.id.to_s
        @list_assigns = @list_assigns + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.full_name+'<a href="javascript:remove_assign('+cat.id.to_s+')">x</a></span>'
        @assigned_main_id = cat.id if ck == 0
        ck = ck + 1
      }
    end
    unless params[:access_id].blank?
      @ac_ids = params[:access_id].split('#')
      @access_data = User.find(:all, :conditions => ['`id` IN (?)', @ac_ids])
      ct = 0
      @access_data.map{|cat|
        @access = @access + '#' + cat.id.to_s
        @list_access = @list_access + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.full_name+'<a href="javascript:remove_access('+cat.id.to_s+')">x</a></span>'
        @access_main_id = cat.id if ct == 0
        ct = ct + 1
      }
    end
  end

  def add_multiple_value
    unless params[:groups_id].blank?
      @ids = params[:groups_id].split('#')
      @agent_company.add_groups(@ids)
    end

    unless params[:categories_id].blank?
      @ids = params[:categories_id].split('#')
      @agent_company.add_categories(@ids)
    end

    unless params[:property_types_id].blank?
      @ptype_ids = params[:property_types_id].split('#')
      @agent_company.add_property_types(@ptype_ids)
    end

    if !params[:assigns_id].blank?
      @a_ids = params[:assigns_id].split('#')
      @agent_company.add_assigns(@a_ids)
    else
      @agent_company.company_assigned_relations.destroy_all
    end

    if !params[:access_id].blank?
      @ac_ids = params[:access_id].split('#')
      @agent_company.add_access(@ac_ids)
    else
      @agent_company.company_accessible_relations.destroy_all
    end
  end

  def update_multiple_value
    unless params[:groups_id].blank?
      @ids = params[:groups_id].split('#')
      @agent_company.add_groups(@ids, "update")
    end

    unless params[:categories_id].blank?
      @ids = params[:categories_id].split('#')
      @agent_company.add_categories(@ids, "update")
    end

    unless params[:property_types_id].blank?
      @ptype_ids = params[:property_types_id].split('#')
      @agent_company.add_property_types(@ptype_ids, "update")
    end

    unless params[:assigns_id].blank?
      @a_ids = params[:assigns_id].split('#')
      @agent_company.add_assigns(@a_ids, "update")
    end

    unless params[:access_id].blank?
      @ac_ids = params[:access_id].split('#')
      @agent_company.add_access(@ac_ids, "update")
    end
  end

  def assigned_name(id)
    assign = User.find_by_id(id)
    return assign.nil? ? "" : assign.full_name
  end

  def populate_groups
    @group_companies = GroupCompany.find(:all, :conditions => ["`office_id` = ?", @office.id]).map{|c| [c.name, c.id]}
    return(render(:json => @group_companies))
  end

  def populate_categories
    @category_companies = CategoryCompany.find(:all, :conditions => ["creator_id is NULL OR creator_id = ?", current_user.id]).map{|c| [c.name, c.id]}
    return(render(:json => @category_companies))
  end

  def populate_property_types
    @property_types = PropertyType.find(:all, :conditions => ["category = ?", "Residential"])
    return(render(:json => @property_types))
  end

  def populate_heards
    agent_users = condition = ""
    unless current_user.blank?
      if current_user.type == "AgentUser"
        current_user.office.agent_users.each_with_index{|au, index| agent_users += (index == 0 ? "#{au.id}" : ", #{au.id}") }
        condition = "OR `creator_id` IN (#{agent_users})" unless agent_users.blank?
      end
    end
    @heard_froms = HeardFrom.find(:all, :conditions => ["(`creator_id` is NULL #{condition}) and `status` = 'heard_office'", agent_users]).map{|c| [c.name, c.id]}
    return(render(:json => @heard_froms))
  end

  def populate_notes
    @notes_type = NoteType.find(:all, :conditions => ["(`creator_id` is NULL OR `creator_id` = ?) and`status` = 'company_note'", current_user.id]).map{|c| [c.name, c.id]}
    return(render(:json => @notes_type))
  end

  def update_duplicate
    duplicate = 0
    detected = nil
    params[:agent_company] = params[:agent_company].merge(:office_id => params[:office_id])

    %w(email company_number work_number).each{|w|
      unless params[:agent_company][w].blank?
        check = AgentCompany.check_exist(params[:agent_company], w)
        unless check.nil?
          detected = check
          duplicate = 1
        end
      end
    }

    if duplicate == 1
      unless detected.nil?
        @agent_company = AgentCompany.find(detected.id)
        unless params[:categories_id].blank?
          if @agent_company.update_attributes(params[:agent_company].delete_if{|x,y| y.blank?})
            update_multiple_value
            flash[:notice] = 'Company was successfully updated.'
          end
        end
      end
    end
    redirect_to agent_office_agent_contacts_path(@agent,@office)
  end

  # ------------------------------------------listing------------------------------------------------
  def populate_combobox
    category, company, assigned_to, accessible_by, condition = [],[],[], []
    condition = [" agent_companies.office_id = ? ", current_office.id]
    companies = AgentCompany.find(:all, :conditions => condition, :select => "DISTINCT(agent_companies.category_company_id), agent_companies.company, agent_companies.assigned_to, agent_companies.accessible_by")
    companies.map{|x|
      company << {
        :company => (x.company.to_s.slice(0,1).capitalize unless x.company.blank?) ,
        :companyText => (x.company.to_s.slice(0,1).capitalize unless x.company.blank?)
      } unless x.company.blank?
    }

    categories = CompanyCategoryRelation.find(:all, :select => "DISTINCT(company_category_relations.category_company_id)")
    cid = categories.map{|c| c.category_company_id}
    cat_cons = CategoryContact.find(:all, :conditions => "id IN (#{cid.join(',')})") if cid.present?

    cat_cons.map{|x|
      category << {
        :category => (x.id) ,
        :categoryText => (x.name.to_s)
      }
    } unless cat_cons.blank?

    #    categories.map{|x|
    #      category << {
    #        :category => (x.category_company_id unless x.category_company_id.blank?) ,
    #        :categoryText => (x.category_company.name.to_s unless x.category_company_id.blank?)
    #      } unless x.category_company_id.nil?
    #    }

    assigned_to_db = CompanyAssignedRelation.find(:all, :select => "DISTINCT(company_assigned_relations.assigned_to)")
    a = assigned_to_db.map{|p| p.assigned_to.to_i unless p.assigned_to.blank?}
    usrs = User.find(:all, :conditions =>"id IN (#{a.join(',')}) and office_id = #{@office.id}") if a.present?

    usrs.map{|x|
      assigned_to << {
        :assigned_to => (x.id) ,
        :assigned_toText => (x.full_name)
      }
    } unless usrs.blank?
    #
    #    assigned_to_db.map{|x|
    #      assigned_to << {
    #        :assigned_to => (x.assigned_to unless x.assigned_to.blank?) ,
    #        :assigned_toText => (assigned_name(x.assigned_to.to_i) unless x.assigned_to.blank?)
    #      } unless x.assigned_to.nil?
    #    }

    accessible_by_db = CompanyAccessibleRelation.find(:all, :select => "DISTINCT(company_accessible_relations.accessible_by)")
    b = accessible_by_db.map{|p| p.accessible_by.to_i unless p.accessible_by.blank?}
    usrs2 = User.find(:all, :conditions =>"id IN (#{b.join(',')})and office_id = #{@office.id}") if b.present?
    usrs2.map{|x|
      accessible_by << {
        :accessible_by => (x.id) ,
        :accessible_byText => (x.full_name)
      }
    }

    #    accessible_by_db.map{|x|
    #      accessible_by << {
    #        :accessible_by => (x.accessible_by unless x.accessible_by.blank?) ,
    #        :accessible_byText => (assigned_name(x.accessible_by.to_i) unless x.accessible_by.blank?)
    #      } unless x.accessible_by.nil?
    #    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_assigned_to =[]
    uniq_accessible_by =[]
    uniq_company =[]
    uniq_category =[]

    uniq_assigned_to << {:assigned_to=>'all_team',:assigned_toText=>"All Team Members"}
    uniq_accessible_by << {:accessible_by=>'all_team',:accessible_byText=>"All Team Members"}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    accessible_by.each{|x|uniq_accessible_by << x if !uniq_accessible_by.include?(x) and !x.blank?}
    category.each{|x|uniq_category << x if !uniq_category.include?(x) and !x.blank?}
    company.each{|x|uniq_company << x if !uniq_company.include?(x) and !x.blank?}

    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_accessible_by.to_a.sort!{|x,y| x[:accessible_byText].to_s <=> y[:accessible_byText].to_s}
    uniq_category.to_a.sort!{|x,y| x[:categoryText].to_s <=> y[:categoryText].to_s}
    uniq_company.to_a.sort!{|x,y| x[:companyText].to_s <=> y[:companyText].to_s}

    return(render(:json =>{:results => companies.length, :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Assigned To"})].flatten,
          :access=>[uniq_accessible_by.unshift({:accessible_by=>'all',:accessible_byText=>"All Accessible By"})].flatten,
          :company=>[uniq_company.unshift({:company=>'all',:companyText=>"All Company"})].flatten,
          :category=>[uniq_category.unshift({:category=>'all',:categoryText=>"All Categories"})].flatten
        }))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      unless session[:ID].blank?
        condition = ["agent_companies.id = #{session[:ID]} "]
      else
        get_session_listing
        condition = [" agent_companies.office_id = ? ", current_office.id]

        unless session[:category] == "all"
          if(!session[:category].blank?)
            company_categories = CompanyCategoryRelation.find(:all, :conditions => ["`category_company_id` = ?", session[:category]])
            condition[0] += " AND agent_companies.id IN (?)"
            condition <<  company_categories.collect(&:agent_company_id)
          end
        end

        unless session[:company] == "all"
          if(!session[:company].blank?)
            condition[0] += " AND agent_companies.company LIKE ?"
            condition <<  session[:company]+"%"
          end
        end

        unless session[:assigned_to] == "all"
          if(!session[:assigned_to].blank?)
            unless session[:assigned_to] == "all_team"
              company_assigned_to = CompanyAssignedRelation.find(:all, :conditions => ["`assigned_to` = ?", session[:assigned_to]])
              condition[0] += " AND agent_companies.id IN (?)"
            else
              company_assigned_to = CompanyAssignedRelation.find(:all)
              condition[0] += " AND agent_companies.id NOT IN (?)"
            end
            condition <<  company_assigned_to.collect(&:agent_company_id)
          end
        end

        unless session[:accessible_by] == "all"
          if(!session[:accessible_by].blank?)
            unless session[:accessible_by] == "all_team"
              company_accessible_by = CompanyAccessibleRelation.find(:all, :conditions => ["`accessible_by` = ?", session[:accessible_by]])
              condition[0] += " AND agent_companies.id IN (?)"
            else
              company_accessible_by = CompanyAccessibleRelation.find(:all)
              condition[0] += " AND agent_companies.id NOT IN (?)"
            end
            condition <<  company_accessible_by.collect(&:agent_company_id)
          end
        end
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = AgentCompany.build_sorting(params[:sort],params[:dir])

    if current_user.limited_client_access?
      company_accessible_by2 = CompanyAccessibleRelation.find(:all, :conditions => ["`accessible_by` = ?", current_user.id])
      condition[0] += " AND agent_companies.id IN (?)"
      condition <<  company_accessible_by2.collect(&:agent_company_id)
      include = params[:sort] == 'assigned_to'? [:accessible, :assigned] : [:accessible, :category_company]
    else
      include = params[:sort] == 'assigned_to'? [:assigned] : [:accessible, :category_company]
    end


    @agent_companies = AgentCompany.paginate(:all, :include => include, :conditions => condition, :order => order, :page =>params[:page], :per_page => params[:limit].to_i)
    #    @agent_companies = AgentCompany.paginate(:all, :page =>1, :per_page => 10)
    results = params[:limit].to_i == 20 ? AgentCompany.find(:all,:select => 'count(agent_companies.id)',:conditions => condition,:limit => params[:limit].to_i) : AgentCompany.count(:conditions => condition)

    return(render(:json =>{:results => results,:rows=>@agent_companies.map{|x|{
              'id' => x.id.to_s + '#' + current_user.include_accessible_company?(x.id).to_s,
              'category'=> get_categories(x),
              'sub-category' => get_sub_categories(x),
              'company'=>(x.company.blank? ? "" : x.company.to_s),
              'assigned_to'=> get_assigns(x),
              'accessible_by'=> get_access(x),
              'actions'=> ""}}}))
    #  rescue
    #    return(render(:json =>{:results => nil, :rows=> nil}))
  end

  def search
    agent_companies = AgentCompany.find(:all, :conditions => "office_id = #{@office.id} and (first_name LIKE '%#{params[:search_value]}%' OR last_name LIKE '%#{params[:search_value]}%' OR email LIKE '%#{params[:search_value]}%')")
    jsons = []
    unless agent_companies.blank?
      agent_companies.each{|ac|
        unless ac.blank?
          jsons << {'id' => ac.id, 'company' => "#{ac.company}", 'email' =>"#{ac.email}", 'href' => agent_office_agent_companies_path(@agent, @office)+"?ID=#{ac.id}"}
        end
      } unless agent_companies.nil?
      text = {"results" => jsons, "total" => (agent_companies ? agent_companies.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  def get_categories(agent_company)
    @list_categories = ''
    agent_company.company_category_relations.map{|cat| @list_categories += cat.category_company.name + ', ' }
    return @list_categories
  end

  def get_property_types(agent_company)
    @list_property_types = ''
    agent_company.company_property_type_relations.map{|cat| @list_property_types += cat.property_type.name + ', ' }
    return @list_property_types
  end

  def get_assigns(agent_company)
    @list_assigns = ''
    agent_company.company_assigned_relations.map{|cat| @list_assigns += assigned_name(cat.assigned_to.to_i) + ', ' }
    return @list_assigns.blank? ? "All Team Members" : @list_assigns
  end

  def get_access(agent_company)
    @list_access = ''
    agent_company.company_accessible_relations.map{|cat| @list_access += assigned_name(cat.accessible_by.to_i) + ', ' }
    return @list_access.blank? ? "All Team Members" : @list_access
  end

  def get_session_listing
    session[:category] = params[:category].nil? ? "all" : params[:category]
    session[:company] = params[:company].nil? ? "all" : params[:company]
    session[:assigned_to] = params[:assinged_to].nil? ? "all" : params[:assinged_to]
    session[:accessible_by] = params[:accessible_by].nil? ? "all" : params[:accessible_by]
    session[:ID] = params[:ID].nil? ? nil : params[:ID]
    session[:sort_by] = "agent_companies.updated_at DESC"
  end

  def nil_session_listing
    session[:category] = nil
    session[:company] = nil
    session[:assigned_to] = nil
    session[:accessible_by] = nil
  end
end
