class AgentContactsController < ApplicationController
  layout 'agent'

  before_filter :login_required
  before_filter :get_agent_and_office
  append_before_filter :prepare_creation
  before_filter :prepare_agent_office, :only => [:create, :update]
  skip_before_filter :verify_authenticity_token, :only => [:search, :auto_complete]

  def index
    get_session_listing
    nil_session_value
    nil_session_listing if params[:sort_by].blank? && params[:filter].blank?
    @properties_filter = Property.find(:all,:include => [:primary_contact],:conditions=>["office_id=?",current_office.id], :select => "suburb,id, property_type,status,primary_contact_id");
    @active_help_agent = ActiveHelpPage.find(:first, :conditions => "`key` LIKE 'Listings' AND user_type = 'AgentUser'")
    @page_status = current_user.check_user_page_status(@active_help_agent.id) unless @active_help_agent.blank?

    respond_to do |format|
      format.html {render :action => 'index'}
      format.js
    end
  end

  def show

  end

  def new
    @agent_contact = AgentContact.new
    nil_session_value
  end

  def edit
    @agent_contact = AgentContact.find(params[:id])
    @selected_property_types = @agent_contact.contact_property_type_relations
    @property_type_ids = @selected_property_types.collect(&:property_type_id) unless @selected_property_types.blank?
    require "base64"
    @agent_contact.password = Base64.decode64(@agent_contact.password) unless @agent_contact.password.blank?
    @groups = @list_groups = @assigned_main_id = @access_main_id = @categories = @property_types = @list_property_types = @list_categories = @assigns_id = @list_assigns = @access = @list_access =''
    unless @agent_contact.contact_type.blank?
      unless @agent_contact.contact_category_relations.blank?
        @agent_contact.contact_category_relations.map{|cat|
          @categories = @categories + '#' + cat.category_contact_id.to_s
          @list_categories = @list_categories + '<div id="'+cat.category_contact_id.to_s+'" style="margin-right:20px;">'+cat.category_contact.name+'<a href="javascript:remove_category('+cat.category_contact_id.to_s+')">x</a></div>'
        }
      end
    end

    unless @agent_contact.contact_property_type_relations.blank?
      @agent_contact.contact_property_type_relations.map{|ptype|
        @property_types = @property_types + '#' + ptype.property_type_id.to_s
      unless ptype.property_type.blank?
        @list_property_types = @list_property_types + '<div id="'+ptype.property_type_id.to_s+'" class="property_type_selected" style="margin-right:20px;">'+ptype.property_type.category.to_s+' - '+ptype.property_type.name+'<a href="javascript:remove_property_type('+ptype.property_type_id.to_s+')">x</a></div>'
      end
      }
    end

    @agent_contact.contact_assigned_relations.map{|cat|
      @assigns_id = @assigns_id + '#' + cat.assigned_to.to_s
      @list_assigns = @list_assigns + '<div id="'+cat.assigned_to.to_s+'" style="margin-right:20px;">'+assigned_name(cat.assigned_to.to_i)+'<a href="javascript:remove_assign('+cat.assigned_to.to_s+')">x</a></div>'
    }
    @assigned_main_id = @agent_contact.contact_assigned_relations.first.nil? ? nil : @agent_contact.contact_assigned_relations.first.assigned_to

    @agent_contact.contact_accessible_relations.map{|cat|
      @access = @access + '#' + cat.accessible_by.to_s
      @list_access = @list_access + '<div id="'+cat.accessible_by.to_s+'" style="margin-right:20px;">'+assigned_name(cat.accessible_by.to_i)+'<a href="javascript:remove_access('+cat.accessible_by.to_s+')">x</a></div>'
    }
    @agent_contact.group_contact_relations.map{|cat|
      @groups = @groups + '#' + cat.group_contact_id.to_s
      @list_groups = @list_groups + '<div id="'+cat.group_contact_id.to_s+'" style="margin-right:20px;">'+cat.group_contact.name+'<a href="javascript:remove_group('+cat.group_contact_id.to_s+')">x</a></div>'
    }
    @access_main_id = @agent_contact.contact_accessible_relations.first.nil? ? nil : @agent_contact.contact_accessible_relations.first.accessible_by
  end

  def get_session_agent_contact
    return(session[:agent_contact_id])
  end

  def create
    @agent_contact = AgentContact.new(params[:agent_contact])
    @agent_contact.contact_type = "Individual"
    respond_to do |format|
      if params[:agent_contact][:login_required] != "0" && params[:agent_contact][:access_type] == "ResidentialProject"
        if params[:agent_contact][:assigned_to].blank?
          @agent_contact.errors.add(:assigned_to, " can't be blank") if params[:agent_contact][:assigned_to].blank?
          feed_multiple_value
          get_session_value
          format.html { render :action => "new" }
          format.xml  { render :xml => @agent_contact.errors}
        else
          unless params[:categories_id].blank? || params[:agent_contact][:industry_category_id].blank? || params[:property_types_id].blank?
            require "base64"
            @agent_contact.password=Base64.encode64(params[:agent_contact][:password].gsub(/\s+/, "")) unless params[:agent_contact][:password].blank?
            if params[:agent_contact][:access_type].blank?
              @agent_contact.access_type = "Vault"
            end
            if @agent_contact.save
              add_multiple_value
              @agent_contact.update_first_list
              @agent_contact.use_spawn_for_boom
              @agent_contact.use_spawn_for_md
              @agent_contact.use_spawn_for_irealty
              flash[:notice] = 'Contact was successfully created.'
              format.html { redirect_to :action => 'index' }
            else
              get_session_value
              @agent_contact.password = ""
              feed_multiple_value
              format.html { render :action => "new" }
              format.xml  { render :xml => @agent_contact.errors}
            end
          else
            @agent_contact.errors.add(:category_contact_id, " can't be blank") if params[:categories_id].blank?
            @agent_contact.errors.add(:industry_category_id, " can't be blank") if params[:agent_contact][:industry_category_id].blank?
            @agent_contact.errors.add("Industry sub-category can't be blank") if params[:property_types_id].blank?
            feed_multiple_value
            get_session_value
            format.html { render :action => "new" }
            format.xml  { render :xml => @agent_contact.errors}
          end
        end
      else
        unless params[:categories_id].blank? || params[:agent_contact][:industry_category_id].blank? || params[:property_types_id].blank?
          require "base64"
          @agent_contact.password=Base64.encode64(params[:agent_contact][:password].gsub(/\s+/, "")) unless params[:agent_contact][:password].blank?
          if params[:agent_contact][:access_type].blank?
            @agent_contact.access_type = "Vault"
          end
          if @agent_contact.save
            add_multiple_value
            @agent_contact.update_first_list
            @agent_contact.use_spawn_for_boom
            @agent_contact.use_spawn_for_md
            @agent_contact.use_spawn_for_irealty
            flash[:notice] = 'Contact was successfully created.'
            format.html { redirect_to :action => 'index' }
          else
            get_session_value
            @agent_contact.password = ""
            feed_multiple_value
            format.html { render :action => "new" }
            format.xml  { render :xml => @agent_contact.errors}
          end
        else
          @agent_contact.errors.add(:category_contact_id, " can't be blank") if params[:categories_id].blank?
          @agent_contact.errors.add(:industry_category_id, " can't be blank") if params[:agent_contact][:industry_category_id].blank?
          @agent_contact.errors.add("Industry sub-category can't be blank") if params[:property_types_id].blank?
          feed_multiple_value
          get_session_value
          format.html { render :action => "new" }
          format.xml  { render :xml => @agent_contact.errors}
        end
      end
    end
  end

  def create_company
    unless params[:agent_contact][:agent_contact_id].blank?
      agent_contact = AgentCompany.find_by_id(params[:agent_contact][:agent_contact_id])
      return params[:agent_contact][:agent_contact_id]
    else
      company = {:email => params[:agent_contact][:email], :contact_number => params[:agent_contact][:contact_number],
        :fax_number => params[:agent_contact][:fax_number], :inactive => params[:agent_contact][:inactive],
        :country_id => params[:agent_contact][:country_id], :unit_number => params[:agent_contact][:unit_number],
        :street_number => params[:agent_contact][:street_number], :street_name => params[:agent_contact][:street_number],
        :suburb => params[:agent_contact][:suburb], :website => params[:agent_contact][:website], :accessible_by => params[:agent_contact][:accessible_by],
        :heard_from_id => params[:agent_contact][:heard_from_id], :state => params[:agent_contact][:state], :username => params[:agent_contact][:username],
        :post_code => params[:agent_contact][:post_code], :agent_id => params[:agent_contact][:agent_id], :office_id => params[:agent_contact][:office_id],
        :assigned_to => params[:agent_contact][:assigned_to], :login_required => params[:agent_contact][:login_required]
      }

      @agent_contact = AgentCompany.new(company)
      if @agent_contact.save
        return @agent_contact.id
      else
        return nil
      end
    end
  end

  def update
    @agent_contact = AgentContact.find(params[:id])
    respond_to do |format|
      if params[:agent_contact][:login_required] != "0" && params[:agent_contact][:access_type] == "ResidentialProject"
        if params[:agent_contact][:assigned_to].blank?
          @agent_contact.errors.add(:assigned_to, " can't be blank") if params[:agent_contact][:assigned_to].blank?
          feed_multiple_value
          get_session_value
          format.html { render :action => "edit" }
          format.xml  { render :xml => @agent_contact.errors}
        else
          unless params[:categories_id].blank? || params[:agent_contact][:industry_category_id].blank? || params[:property_types_id].blank? || params[:agent_contact][:first_name].blank?
            require "base64"
            params[:agent_contact][:password]=Base64.encode64(params[:agent_contact][:password].gsub(/\s+/, "")) unless params[:agent_contact][:password].blank?
            if @agent_contact.update_attributes(params[:agent_contact])
              add_multiple_value
              @agent_contact.update_first_list
              @agent_contact.use_spawn_for_boom
              @agent_contact.use_spawn_for_md
              @agent_contact.use_spawn_for_irealty
              flash[:notice] = 'Contact was successfully updated.'
              format.html { redirect_to :action => 'edit' }
            else
              @agent_contact.password = ""
              feed_multiple_value
              get_session_value
              format.html { render :action => "edit" }
              format.xml  { render :xml => @agent_contact.errors}
            end
          else
            @agent_contact.errors.add(:category_contact_id, " can't be blank") if params[:categories_id].blank?
            @agent_contact.errors.add(:industry_category_id, " can't be blank") if params[:agent_contact][:industry_category_id].blank?
            @agent_contact.errors.add("Industry sub-category can't be blank") if params[:property_types_id].blank?
            get_session_value
            feed_multiple_value
            format.html { render :action => "edit" }
            format.xml  { render :xml => @agent_contact.errors}
          end
        end
      else
        unless params[:categories_id].blank? || params[:agent_contact][:industry_category_id].blank? || params[:property_types_id].blank? || params[:agent_contact][:first_name].blank?
          require "base64"
          params[:agent_contact][:password]=Base64.encode64(params[:agent_contact][:password].gsub(/\s+/, "")) unless params[:agent_contact][:password].blank?
          if @agent_contact.update_attributes(params[:agent_contact])
            add_multiple_value
            @agent_contact.update_first_list
            @agent_contact.use_spawn_for_boom
            @agent_contact.use_spawn_for_md
            @agent_contact.use_spawn_for_irealty
            flash[:notice] = 'Contact was successfully updated.'
            format.html { redirect_to :action => 'edit' }
          else
            @agent_contact.password = ""
            feed_multiple_value
            get_session_value
            format.html { render :action => "edit" }
            format.xml  { render :xml => @agent_contact.errors}
          end
        else
          @agent_contact.errors.add(:category_contact_id, " can't be blank") if params[:categories_id].blank?
          @agent_contact.errors.add(:industry_category_id, " can't be blank") if params[:agent_contact][:industry_category_id].blank?
          @agent_contact.errors.add("Industry sub-category can't be blank") if params[:property_types_id].blank?
          @agent_contact.errors.add(:first_name, " can't be blank") if params[:agent_contact][:first_name].blank?
          get_session_value
          feed_multiple_value
          format.html { render :action => "edit" }
          format.xml  { render :xml => @agent_contact.errors}
        end
      end
    end
  end


  def get_session_value
    session[:industry_category_id] = params[:agent_contact][:industry_category_id]
  end

  def nil_session_value
    session[:industry_category_id] = nil
  end

  def destroy
    @agent_contact = AgentContact.find(params[:id])
    @agent_contact.destroy

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def auto_complete
    result = []
    companies = AgentCompany.find(:all, :conditions => ["(`company` LIKE ?) and `agent_id` = ? and `office_id` = ? ", '%' + params[:q].to_s + '%', @agent.id, @office.id])
    companies.each{|x|
      company = x.company.to_s
      result << {'id' => "#{x.id.to_s}", 'name' => "#{company}" } unless x.company.blank? }
    render :json => {"results" => result, "total" => (companies ? companies.length : 0) }
  end

  def prepare_creation
    @countries = Country.find(:all,:conditions=>["display_country=?",true]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @category_contacts = CategoryContact.find(:all).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @property_types = PropertyType.find(:all).map{|x| [x.name, x.id]}.unshift(['Please select', nil])
    @industry_categories = []
    if @office.residential_sale == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Residential Sale"]).map{|c| [c.name, c.id]}
    end
    if @office.residential_lease == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Residential Lease"]).map{|c| [c.name, c.id]}
    end
    if @office.holiday_lease == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Holiday Lease"]).map{|c| [c.name, c.id]}
    end
    if @office.commercial == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Commercial"]).map{|c| [c.name, c.id]}
    end
    if @office.business_sale == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Business Sale"]).map{|c| [c.name, c.id]}
    end
    if @office.project_sale == true
      @industry_categories += IndustryCategory.find(:all, :conditions => ["name = ?", "Project Sale"]).map{|c| [c.name, c.id]}
    end
    @team_members = @office.agent_users.find(:all, :conditions => "roles.id = 3 OR roles.id = 4 OR roles.id = 5", :include => "roles", :order => "`users`.first_name ASC").map{|u| [u.full_name, u.id]}.unshift(['All Team Members', nil]).uniq
    @heard_froms = HeardFrom.find(:all, :conditions => ["`status` = 'heard_office'"]).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @group_contacts = GroupContact.find(:all).map{|c| [c.name, c.id]}.unshift(['Please select', nil])
    @email_formats = [["Plain text", "Plain text"], ["Magazine Colour Format", "Magazine Colour Format"]]

    @residential_sale_options = PropertyType.residential
    @residential_lease_options = PropertyType.residential
    @commercial_options = PropertyType.commercial
    @houseland_options = PropertyType.residential
    @business_options = PropertyType.business
    @holiday_options = PropertyType.holiday
  end

  def prepare_agent_office
    params[:agent_contact] = params[:agent_contact].merge(:agent_id => params[:agent_id],:office_id => params[:office_id])
  end

  def feed_multiple_value
    @groups = @list_groups = @categories = @property_types = @list_property_types = @list_categories = @assigns_id = @list_assigns = @access = @list_access = @assigned_main_id = @access_main_id =''
    unless params[:groups_id].blank?
      @ids = params[:groups_id].split('#')
      @groups_data = GroupContact.find(:all, :conditions => ['`id` IN (?)', @ids])
      @groups_data.map{|cat|
        @groups = @groups + '#' + cat.id.to_s
        @list_groups = @list_groups + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.name+'<a href="javascript:remove_group('+cat.id.to_s+')">x</a></span>'
      }
    end
    unless params[:categories_id].blank?
      @ids = params[:categories_id].split('#')
      @categories_data = CategoryContact.find(:all, :conditions => ['`id` IN (?)', @ids])
      @categories_data.map{|cat|
        @categories = @categories + '#' + cat.id.to_s
        @list_categories = @list_categories + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.name+'<a href="javascript:remove_category('+cat.id.to_s+')">x</a></span>'
      }
    end
    unless params[:property_types_id].blank?
      @ptype_ids = params[:property_types_id].split('#')
      @property_types_data = PropertyType.find(:all, :conditions => ['`id` IN (?)', @ptype_ids])
      @property_types_data.map{|ptype|
        @property_types = @property_types + '#' + ptype.id.to_s
        @list_property_types = @list_property_types + '<span id="'+ptype.id.to_s+'" style="margin-right:20px;">'+ptype.name+'<a href="javascript:remove_category('+ptype.id.to_s+')">x</a></span>'
      }
      #      @agent_contact.add_property_types(@ptype_ids)
    end
    unless params[:assigns_id].blank?
      @a_ids = params[:assigns_id].split('#')
      @assigns_data = User.find(:all, :conditions => ['`id` IN (?)', @a_ids])
      ck = 0
      @assigns_data.map{|cat|
        @assigns_id = @assigns_id + '#' + cat.id.to_s
        @list_assigns = @list_assigns + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.full_name+'<a href="javascript:remove_assign('+cat.id.to_s+')">x</a></span>'
        @assigned_main_id = cat.id if ck == 0
        ck = ck + 1
      }
    end
    unless params[:access_id].blank?
      @ac_ids = params[:access_id].split('#')
      @access_data = User.find(:all, :conditions => ['`id` IN (?)', @ac_ids])
      ct = 0
      @access_data.map{|cat|
        @access = @access + '#' + cat.id.to_s
        @list_access = @list_access + '<span id="'+cat.id.to_s+'" style="margin-right:20px;">'+cat.full_name+'<a href="javascript:remove_access('+cat.id.to_s+')">x</a></span>'
        @access_main_id = cat.id if ct == 0
        ct = ct + 1
      }
    end
  end

  def add_multiple_value
    unless params[:groups_id].blank?
      @ids = params[:groups_id].split('#')
      @agent_contact.add_groups(@ids)
    end

    unless params[:categories_id].blank?
      @ids = params[:categories_id].split('#')
      @agent_contact.add_categories(@ids)
    end

    unless params[:property_types_id].blank?
      @ptype_ids = params[:property_types_id].split('#')
      @agent_contact.add_property_types(@ptype_ids)
    end

    if !params[:assigns_id].blank?
      @a_ids = params[:assigns_id].split('#')
      @agent_contact.add_assigns(@a_ids)
    else
      @agent_contact.contact_assigned_relations.destroy_all
    end

    if !params[:access_id].blank?
      @ac_ids = params[:access_id].split('#')
      @agent_contact.add_access(@ac_ids)
    else
      @agent_contact.contact_accessible_relations.destroy_all
    end
  end

  def update_multiple_value
    unless params[:groups_id].blank?
      @ids = params[:groups_id].split('#')
      @agent_contact.add_groups(@ids, "update")
    end

    unless params[:categories_id].blank?
      @ids = params[:categories_id].split('#')
      @agent_contact.add_categories(@ids, "update")
    end

    unless params[:property_types_id].blank?
      @ptype_ids = params[:property_types_id].split('#')
      @agent_contact.add_property_types(@ptype_ids, "update")
    end

    unless params[:assigns_id].blank?
      @a_ids = params[:assigns_id].split('#')
      @agent_contact.add_assigns(@a_ids, "update")
    end

    unless params[:access_id].blank?
      @ac_ids = params[:access_id].split('#')
      @agent_contact.add_access(@ac_ids, "update")
    end
  end

  def assigned_name(id)
    assign = User.find_by_id(id)
    return assign.nil? ? "" : assign.full_name
  end

  def populate_groups
    @group_contacts = GroupContact.find(:all, :conditions => ["`office_id` = ?", @office.id]).map{|c| [c.name, c.id]}
    return(render(:json => @group_contacts))
  end

  def populate_categories
    @category_contacts = CategoryContact.find(:all, :conditions => ["creator_id is NULL OR creator_id = ?", current_user.id]).map{|c| [c.name, c.id]}
    return(render(:json => @category_contacts))
  end

  def populate_property_types
    @property_types = PropertyType.find(:all, :conditions => ["category = ?", "Residential"])
    return(render(:json => @property_types))
  end

  def populate_heards
    agent_users = condition = ""
    unless current_user.blank?
      if current_user.type == "AgentUser"
        current_user.office.agent_users.each_with_index{|au, index| agent_users += (index == 0 ? "#{au.id}" : ", #{au.id}") }
        condition = "OR `creator_id` IN (#{agent_users})" unless agent_users.blank?
      end
    end
    @heard_froms = HeardFrom.find(:all, :conditions => ["(`creator_id` is NULL #{condition}) and `status` = 'heard_office'", agent_users]).map{|c| [c.name, c.id]}
    return(render(:json => @heard_froms))
  end

  def populate_notes
    @notes_type = NoteType.find(:all, :conditions => ["(`creator_id` is NULL OR `creator_id` = ?) and`status` = 'contact_note'", current_user.id]).map{|c| [c.name, c.id]}
    return(render(:json => @notes_type))
  end

  def update_duplicate
    duplicate = 0
    detected = nil
    params[:agent_contact] = params[:agent_contact].merge(:office_id => params[:office_id])

    %w(email mobile_number contact_number work_number home_number).each{|w|
      unless params[:agent_contact][w].blank?
        check = AgentContact.check_exist(params[:agent_contact], w)
        unless check.nil?
          detected = check
          duplicate = 1
        end
      end
    }

    if duplicate == 1
      unless detected.nil?
        @agent_contact = AgentContact.find(detected.id)
        unless params[:categories_id].blank?
          if @agent_contact.update_attributes(params[:agent_contact].delete_if{|x,y| y.blank?})
            update_multiple_value
            flash[:notice] = 'Contact was successfully updated.'
          end
        end
      end
    end
    redirect_to :action => 'index'
  end

  # ------------------------------------------listing------------------------------------------------
  def populate_combobox
    category, last_name, first_name, assigned_to, accessible_by, company_condition, condition = [],[],[],[], []
    condition = [" agent_contacts.office_id = ? ", current_office.id]
    company_condition = [" agent_companies.office_id = ?", current_office.id]
    @contacts = AgentContact.find(:all, :conditions => condition, :select => "DISTINCT(agent_contacts.category_contact_id), agent_contacts.first_name, agent_contacts.last_name, agent_contacts.assigned_to, agent_contacts.accessible_by")
    @companies = AgentCompany.find(:all, :conditions => company_condition, :select => "DISTINCT(agent_companies.category_company_id), agent_companies.company, agent_companies.last_name, agent_companies.assigned_to, agent_companies.accessible_by")
    @contacts.map{|x|
      first_name << {
        :first_name => (x.first_name.to_s.slice(0,1).capitalize unless x.first_name.blank?) ,
        :first_nameText => (x.first_name.to_s.slice(0,1).capitalize unless x.first_name.blank?)
      } unless x.first_name.blank?

      last_name << {
        :last_name => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?) ,
        :last_nameText => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?)
      } unless x.last_name.blank?
    }

    @companies.map{|x|
      first_name << {
        :first_name => (x.company.to_s.slice(0,1).capitalize unless x.company.blank?) ,
        :first_nameText => (x.company.to_s.slice(0,1).capitalize unless x.company.blank?)
      } unless x.company.blank?

      last_name << {
        :last_name => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?) ,
        :last_nameText => (x.last_name.to_s.slice(0,1).capitalize unless x.last_name.blank?)
      } unless x.last_name.blank?
    }

    categories = ContactCategoryRelation.find(:all, :select => "DISTINCT(contact_category_relations.category_contact_id)")
    cid = categories.map{|c| c.category_contact_id}
    cat_cons = CategoryContact.find(:all, :conditions => "id IN (#{cid.join(',')})") if cid.present?

    cat_cons.map{|x|
      category << {
        :category => (x.id) ,
        :categoryText => (x.name.to_s)
      }
    } unless cat_cons.blank?

    @contacts_assigned_to = ContactAssignedRelation.find(:all, :select => "DISTINCT(contact_assigned_relations.assigned_to)")
    a = @contacts_assigned_to.map{|p| p.assigned_to.to_i unless p.assigned_to.blank?}
    usrs = User.find(:all, :conditions =>"id IN (#{a.join(',')}) and office_id = #{@office.id}") if a.present?

    usrs.map{|x|
      assigned_to << {
        :assigned_to => (x.id) ,
        :assigned_toText => (x.full_name)
      }
    } unless usrs.blank?

    @companies_assigned_to = CompanyAssignedRelation.find(:all, :select => "DISTINCT(company_assigned_relations.assigned_to)")
    a = @companies_assigned_to.map{|p| p.assigned_to.to_i unless p.assigned_to.blank?}
    usrs = User.find(:all, :conditions =>"id IN (#{a.join(',')}) and office_id = #{@office.id}") if a.present?

    usrs.map{|x|
      assigned_to << {
        :assigned_to => (x.id) ,
        :assigned_toText => (x.full_name)
      }
    } unless usrs.blank?

    accessible_by_db = ContactAccessibleRelation.find(:all, :select => "DISTINCT(contact_accessible_relations.accessible_by)")
    b = accessible_by_db.map{|p| p.accessible_by.to_i unless p.accessible_by.blank?}
    usrs2 = User.find(:all, :conditions =>"id IN (#{b.join(',')})and office_id = #{@office.id}") if b.present?
    usrs2.map{|x|
      accessible_by << {
        :accessible_by => (x.id) ,
        :accessible_byText => (x.full_name)
      }
    }

    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_assigned_to =[]
    uniq_accessible_by =[]
    uniq_first_name =[]
    uniq_last_name =[]
    uniq_category =[]

    uniq_assigned_to << {:assigned_to=>'all_team',:assigned_toText=>"All Team Members"}
    uniq_accessible_by << {:accessible_by=>'all_team',:accessible_byText=>"All Team Members"}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    accessible_by.each{|x|uniq_accessible_by << x if !uniq_accessible_by.include?(x) and !x.blank?}
    category.each{|x|uniq_category << x if !uniq_category.include?(x) and !x.blank?}
    first_name.each{|x|uniq_first_name << x if !uniq_first_name.include?(x) and !x.blank?}
    last_name.each{|x|uniq_last_name << x if !uniq_last_name.include?(x) and !x.blank?}

    uniq_assigned_to.to_a.sort!{|x,y| x[:assigned_toText].to_s <=> y[:assigned_toText].to_s}
    uniq_accessible_by.to_a.sort!{|x,y| x[:accessible_byText].to_s <=> y[:accessible_byText].to_s}
    uniq_category.to_a.sort!{|x,y| x[:categoryText].to_s <=> y[:categoryText].to_s}
    uniq_first_name.to_a.sort!{|x,y| x[:first_nameText].to_s <=> y[:first_nameText].to_s}
    uniq_last_name.to_a.sort!{|x,y| x[:last_nameText].to_s <=> y[:last_nameText].to_s}

    @results = []
    @results << {[@contacts],[@companies]}

    return(render(:json =>{:results => @results.length, :team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Assigned To"})].flatten,
          :access=>[uniq_accessible_by.unshift({:accessible_by=>'all',:accessible_byText=>"All Accessible By"})].flatten,
          :f_name=>[uniq_first_name.unshift({:first_name=>'all',:first_nameText=>"All First Name/Company Name"})].flatten,
          :l_name=>[uniq_last_name.unshift({:last_name=>'all',:last_nameText=>"All Last Name"})].flatten,
          :category=>[uniq_category.unshift({:category=>'all',:categoryText=>"All Categories"})].flatten
        }))
  end

  def view_listing
    if params[:page_name] == 'listing_view'
      unless session[:ID].blank?
        condition = ["agent_contacts.id = #{session[:ID]} "]
        company_condition = ["agent_companies.id = #{session[:ID]}"]
      else
        get_session_listing
        condition = [" agent_contacts.office_id = ? ", current_office.id]
        company_condition = [" agent_companies.office_id = ?", current_office.id]

        #---------------------------COMPANY---------------------------#

        unless session[:first_name] == "all"
          if(!session[:first_name].blank?)
            company_condition[0] += " AND agent_companies.company LIKE ?"
            company_condition <<  session[:first_name]+"%"
          end
        end

        unless session[:last_name] == "all"
          if(!session[:last_name].blank?)
            company_condition[0] += " AND agent_companies.last_name LIKE ?"
            company_condition <<  session[:last_name]+"%"
          end
        end

        unless session[:company_type] == 'all'
          if(!session[:company_type].blank?)
            company_condition[0] += " AND agent_companies.company_type LIKE ?"
            company_condition << session[:company_type]+"%"
          end
        end

        unless session[:assigned_to] == "all"
          if(!session[:assigned_to].blank?)
            unless session[:assigned_to] == "all_team"
              company_assigned_to = CompanyAssignedRelation.find(:all, :conditions => ["`assigned_to` = ?", session[:assigned_to]])
              company_condition[0] += " AND agent_companies.id IN (?)"
            else
              company_assigned_to = CompanyAssignedRelation.find(:all)
              company_condition[0] += " AND agent_companies.id NOT IN (?)"
            end
            company_condition <<  company_assigned_to.collect(&:agent_company_id)
          end
        end

        order_company = AgentCompany.build_sorting(params[:sort],params[:dir])

        #---------------------------- END ----------------------------#

        unless session[:first_name] == "all"
          if(!session[:first_name].blank?)
            condition[0] += " AND agent_contacts.first_name LIKE ?"
            condition <<  session[:first_name]+"%"
          end
        end

        unless session[:last_name] == "all"
          if(!session[:last_name].blank?)
            condition[0] += " AND agent_contacts.last_name LIKE ?"
            condition <<  session[:last_name]+"%"
          end
        end

        unless session[:contact_type] == 'all'
          if(!session[:contact_type].blank?)
            condition[0] += " AND agent_contacts.contact_type LIKE ?"
            condition << session[:contact_type]+"%"
          end
        end

        unless session[:assigned_to] == "all"
          if(!session[:assigned_to].blank?)
            unless session[:assigned_to] == "all_team"
              contact_assigned_to = ContactAssignedRelation.find(:all, :conditions => ["`assigned_to` = ?", session[:assigned_to]])
              condition[0] += " AND agent_contacts.id IN (?)"
            else
              contact_assigned_to = ContactAssignedRelation.find(:all)
              condition[0] += " AND agent_contacts.id NOT IN (?)"
            end
            condition <<  contact_assigned_to.collect(&:agent_contact_id)
          end
        end
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = AgentContact.build_sorting(params[:sort],params[:dir])
    if current_user.limited_client_access?
      contact_accessible_by2 = ContactAccessibleRelation.find(:all, :conditions => ["`accessible_by` = ?", current_user.id])
      condition[0] += " AND agent_contacts.id IN (?)"
      condition <<  contact_accessible_by2.collect(&:agent_contact_id)
      include = params[:sort] == 'assigned_to'? [:accessible, :assigned] : [:accessible, :category_contact]

      company_accessible_by2 = CompanyAccessibleRelation.find(:all, :conditions => ["`accessible_by` = ?", current_user.id])
      company_condition[0] += " AND agent_companies.id IN (?)"
      company_condition <<  company_accessible_by2.collect(&:agent_company_id)
      company_include = params[:sort] == 'assigned_to'? [:accessible, :assigned] : [:accessible, :category_company]
    else
      include = params[:sort] == 'assigned_to'? [:assigned] : [:accessible, :category_contact]
      company_include = params[:sort] == 'assigned_to'? [:assigned] : [:accessible, :category_company]
    end

    results = 0
    results = []
    @contact_results = AgentContact.find(:all,:select => 'count(agent_contacts.id)',:conditions => condition,:limit => params[:limit].to_i)
    @company_results = AgentCompany.find(:all,:select => 'count(agent_companies.id)',:conditions => company_condition,:limit => params[:limit].to_i)
    if params[:limit].to_i == 20
      results << {[@contact_results],[@company_results]}
    else
      results << {[AgentContact.count(:conditions => condition)],[AgentCompany.count(:conditions => company_condition)]}
    end

    rows = []
    @agent_companies = AgentCompany.paginate(:all, :include => company_include, :order => order_company, :conditions => company_condition,:page =>params[:page], :per_page => params[:limit].to_i)
    @agent_contacts = AgentContact.paginate(:all, :include => include, :order => order, :conditions => condition, :page =>params[:page], :per_page => params[:limit].to_i)
    @agent_contacts.map {|agent_contact|
      rows << {
        'id' => agent_contact.id.to_s + '#' + agent_contact.contact_type.to_s,
        'contact_type' =>(agent_contact.contact_type.blank? ? "" : agent_contact.contact_type.to_s),
        'first_name' =>(agent_contact.first_name.blank? ? agent_contact.contact_data_alternative : agent_contact.first_name.to_s),
        'last_name' => (agent_contact.last_name.blank? ? "" : agent_contact.last_name.to_s),
        'assigned_to'=> get_assigns(agent_contact),
        'actions'=> '<a href="/agents/' + "#{@agent.id}" + '/offices/' + "#{@office.id}" + '/agent_contacts/' + "#{agent_contact.id.to_s}" + '/edit" style="color:#0e7fc7;">Edit</a>' + '|' + '<a href="/agents/' + "#{@agent.id}" + '/offices/' + "#{@office.id}" + '/agent_contacts/' + "#{agent_contact.id.to_s}" + '/alerts" style="color:#0e7fc7;">Requirements</a>' + '|' + '<a href="/agents/' + "#{@agent.id}" + '/offices/' + "#{@office.id}" + '/agent_contacts/' + "#{agent_contact.id.to_s}" + '/notes" style="color:#0e7fc7;">Notes</a>'
      }}

    @agent_companies.map {|agent_company|
      rows << {
        'id' => agent_company.id.to_s + '#' + agent_company.company_type.to_s,
        'contact_type' =>(agent_company.company_type.blank? ? "" : agent_company.company_type.to_s),
        'first_name'=>(agent_company.company.blank? ? "" : agent_company.company.to_s),
        'last_name'=>(agent_company.last_name.blank? ? "" : agent_company.last_name.to_s),
        'assigned_to'=> get_assigns_companies(agent_company),
        'actions'=> '<a href="/agents/' + "#{@agent.id}" + '/offices/' + "#{@office.id}" + '/agent_companies/' + "#{agent_company.id.to_s}" + '/edit" style="color:#0e7fc7;">Edit</a>' + '|' + '<a href="/agents/' + "#{@agent.id}" + '/offices/' + "#{@office.id}" + '/agent_companies/' + "#{agent_company.id.to_s}" + '/alerts" style="color:#0e7fc7;">Requirements</a>' + '|' + '<a href="/agents/' + "#{@agent.id}" + '/offices/' + "#{@office.id}" + '/agent_companies/' + "#{agent_company.id.to_s}" + '/notes" style="color:#0e7fc7;">Notes</a>'
      }}

    return(render(
        :json =>{
          :results => results,
          :rows => rows
        }
      ))

  rescue
    return(render(:json =>{:results => nil, :rows=> nil}))
  end

  def search
    agent_contacts = AgentContact.find(:all, :conditions => "office_id = #{@office.id} and (first_name LIKE '%#{params[:search_value]}%' OR last_name LIKE '%#{params[:search_value]}%' OR email LIKE '%#{params[:search_value]}%')")
    jsons = []
    unless agent_contacts.blank?
      agent_contacts.each{|ac|
        unless ac.blank?
          jsons << {'id' => ac.id, 'first_name' => "#{ac.first_name}", 'last_name' =>"#{ac.last_name}",'email' =>"#{ac.email}", 'href' => agent_office_agent_contacts_path(@agent, @office)+"?ID=#{ac.id}"}
        end
      } unless agent_contacts.nil?
      text = {"results" => jsons, "total" => (agent_contacts ? agent_contacts.length : 0) }
    else
      text = ''
    end
    render :json => text
  end

  def get_categories(agent_contact)
    @list_categories = ''
    agent_contact.contact_category_relations.map{|cat| @list_categories += cat.category_contact.name + ', ' }
    return @list_categories
  end

  def get_property_types(agent_contact)
    @list_property_types = ''
    agent_contact.contact_property_type_relations.map{|cat| @list_property_types += cat.property_type.name + ', ' }
    return @list_property_types
  end

  def get_assigns(agent_contact)
    @list_assigns = ''
    agent_contact.contact_assigned_relations.map{|cat| @list_assigns += assigned_name(cat.assigned_to.to_i) + ', ' }
    return @list_assigns.blank? ? "All Team Members" : @list_assigns
  end

  def get_assigns_companies(agent_company)
    @list_assigns = ''
    agent_company.company_assigned_relations.map{|cat| @list_assigns += assigned_name(cat.assigned_to.to_i) + ', ' }
    return @list_assigns.blank? ? "All Team Members" : @list_assigns
  end

  def get_access(agent_contact)
    @list_access = ''
    agent_contact.contact_accessible_relations.map{|cat| @list_access += assigned_name(cat.accessible_by.to_i) + ', ' }
    return @list_access.blank? ? "All Team Members" : @list_access
  end

  def get_session_listing
    session[:contact_type] = params[:contact_type].nil? ? "all" : params[:contact_type]
    session[:company_type] = params[:company_type].nil? ? "all" : params[:company_type]
    session[:first_name] = params[:first_name].nil? ? "all" : params[:first_name]
    session[:last_name] = params[:last_name].nil? ? "all" : params[:last_name]
    session[:assigned_to] = params[:assigned_to].nil? ? "all" : params[:assigned_to]
    session[:ID] = params[:ID].nil? ? nil : params[:ID]
    session[:sort_by] = []
    session[:sort_by] << {["agent_contacts.updated_at DESC"],["agent_companies.updated_at DESC"]}
  end

  def nil_session_listing
    session[:company_type] = nil
    session[:contact_type] = nil
    session[:first_name] = nil
    session[:last_name] = nil
    session[:assigned_to] = nil
  end

  def tenants
    get_session_listings
    nil_session_listings if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'tenants'}
      format.js
    end
  end

  def populate_combobox_tenant
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    condition = [" tenancy_records.office_id = ? "]
    condition << current_office.ID
    properties = TenancyRecord.find(:all, :include =>[:property, :agent_contact], :conditions => ["tenancy_records.agent_id = ? AND tenancy_records.office_id = ?", @agent.id, @office.id])
    properties.map{|x|
      suburb << {
        :suburb => x.property.suburb.to_s,
        :suburbText =>x.property.suburb.to_s
      } unless x.property.suburb.blank?;
      type << {
        :type => x.property.property_type.to_s,
        :typeText =>x.property.property_type.to_s
      } unless x.property.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?) ,
        :assigned_toText => (x.property.primary_contact.full_name.to_s unless x.property.primary_contact.blank?)
      } unless x.property.primary_contact.blank?
      ((floor << {
            :floor => x.property.detail.number_of_floors.to_s,
            :floorText => x.property.detail.number_of_floors.to_s
          } unless x.property.detail.number_of_floors.blank?) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale")
    }
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_listing_tenant
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listings
        condition = [" tenancy_records.office_id = ? "]
        condition << current_office.id
        condition[0] += " AND tenancy_records.agent_id = ?"
        condition << current_agent.id
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.office_id = ? "]
      condition << current_office.id

      if @property.type.to_s == "Commercial"
        condition[0] += " AND tenancy_records.property_id = ?"
        condition << "#{@property.id}"
      elsif @property.type.to_s == "CommercialBuilding"
        condition[0] += " AND properties.parent_listing_id = ?"
        condition << "#{@property.id}"
      end
    end

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = TenancyRecord.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm'
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
    else
      @properties = TenancyRecord.paginate(:all, :include => [:property,:agent_contact], :conditions => condition, :order => order, :page => params[:page], :per_page => params[:limit].to_i)
    end

    if @properties.present?
      @properties.each do |x|
        check_and_update_icon(x.property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      results = TenancyRecord.find(:all,:select => 'count(tenancy_records.property_id)',:include =>[:property, :agent_contact],:conditions => condition,:limit => params[:limit].to_i)
    else
      results = TenancyRecord.count(:all, :include => [:property,:agent_contact], :conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'property_id' => x.property.id,
              'listing'=>Property.abbrv_properties(x.property.type),
              'address' =>((x.property.street_number.blank? ? "" : x.property.street_number)+" "+(x.property.street.blank? ? "" : x.property.street.to_s)),
              'suite'=>(x.property.unit_number.blank? ? "" : x.property.unit_number),
              'sqm'=>x.property.detail.floor_area.blank? ? "" : x.property.detail.floor_area,
              'level'=>((x.property.detail.number_of_floors.blank? ? "" : x.property.detail.number_of_floors) unless x.property.type == "ProjectSale" || x.property.type == "BusinessSale"),
              'I/E'=> x.property.ownership.blank? ? "Internal" : x.property.ownership,
              'tenancy'=>(x.agent_contact.blank? ? "" : (x.agent_contact.full_name.blank? ? x.agent_contact.first_name : x.agent_contact.full_name)),
              'rent_pa'=>(x.total_rental_pa.blank? ? "" : x.total_rental_pa),
              'start_date'=>(x.start_date.blank? ? "" : x.start_date),
              'end_date'=>(x.end_date.blank? ? "" : x.end_date),
              'suburb' => x.property.suburb.blank? ? "" : x.property.suburb,
              'type' => x.property.property_type.blank? ? "" : x.property.property_type,
              'price' => Property.check_price(x.property),
              'assigned_to'=>x.property.primary_contact.blank? ? '' : x.property.primary_contact.full_name.to_s.strip,
              'edit'=>[x.property_id, x.id],
              'status' => Property.status_name(x.property),
              'fb'=> [x.property.id,@office.url.blank? ? (x.property.detail.respond_to?(:deal_type) ? x.property.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.property.id}", tweet(x.property)]} unless x.property.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end

  def get_session_listings
    session[:type] = params[:type].blank? ? "all" : params[:type]
    session[:suite] = params[:suite].blank? ? "all" : params[:suite]
    session[:save_status] = params[:save_status].blank? ? "all" : params[:save_status]
    session[:status] = params[:status].blank? ? "all" : params[:status]
    session[:suburb] = params[:suburb].blank? ? "all" : params[:suburb]
    session[:floor] = params[:floor].blank? ? "all" : params[:floor]
    session[:sold_price] = params[:sold_price].blank? ? "all" : params[:sold_price]
    session[:sold_date] = params[:sold_date].blank? ? "all" : params[:sold_date]
    session[:purchaser] = params[:purchaser].blank? ? "all" : params[:purchaser]
    session[:primary_contact_fullname] = params[:primary_contact_fullname].blank? ? "all" : params[:primary_contact_fullname]
    session[:property_type] = params[:property_type].blank? ? "all" : params[:property_type]
    # session[:ID] = params[:ID].blank? ? nil : params[:ID]
    session[:sort_by] = "properties.updated_at DESC"
  end

  def nil_session_listings
    session[:type] = nil
    session[:suite] = nil
    session[:save_status] = nil
    session[:status] = nil
    session[:suburb] = nil
    session[:floor] = nil
    session[:sold_price] = nil
    session[:purchaser] = nil
    session[:sold_date] = nil
    session[:primary_contact_fullname] = nil
    session[:property_type] = nil
  end

  def check_and_update_icon(children_listing)
    img_count = children_listing.images.count rescue 0
    floorp_count = children_listing.floorplans.count rescue 0
    if !children_listing.image_exist && img_count > 0
      children_listing.image_exist = true
      children_listing.save(false)
    end
    if children_listing.image_exist && img_count == 0
      children_listing.image_exist = false
      children_listing.save(false)
    end
    if !children_listing.floorplan_exist && floorp_count > 0
      children_listing.floorplan_exist = true
      children_listing.save(false)
    end
    if children_listing.floorplan_exist && floorp_count == 0
      children_listing.floorplan_exist = false
      children_listing.save(false)
    end
  end

  def tweet(children_listing)
    unless children_listing.blank?
      case children_listing.type.to_s.underscore
      when 'business_sale'
        tweet = "Business For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_sale'
        tweet = "For Sale: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'residential_lease'
        tweet = "For Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'commercial'
        tweet = "Commercial Listing: #{children_listing.address.to_s.split(",").join(", ").to_s}, #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'project_sale'
        tweet = "House & Land: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      when 'holiday_lease'
        tweet = "Holiday Lease: #{children_listing.address.to_s.split(",").join(", ").to_s}" + (children_listing.detail.blank? ? "" : ", #{children_listing.detail.bedrooms.blank? ? "0" : children_listing.detail.bedrooms} Bed, #{children_listing.detail.bathrooms.blank? ? "0" : children_listing.detail.bathrooms} Bath, #{children_listing.detail.carport_spaces.to_i + children_listing.detail.garage_spaces.to_i + children_listing.detail.off_street_spaces.to_i} Car") + " #{@office.url.blank? ? translate_url("#{@office.url}") : translate_url("#{@office.url}/#{children_listing.id}")}"
      end
    end
    return tweet
  end

  def translate_url(text)
    return "http://#{(text.gsub('http://', '') unless text.blank?)}"
  end

  def listings
    get_session_listings
    nil_session_listings if params[:sort_by].blank? && params[:filter].blank?
    respond_to do |format|
      format.html {render :action => 'listings'}
      format.js
    end
  end

  def populate_combobox_property_listing
    floor,suburb,type,assigned_to,condition = [],[],[],[],[]
    condition = [" properties.agent_contact_id = ? "]
    condition << params[:id]
    properties = Property.find(:all,:include =>[:primary_contact], :conditions => condition, :select => "properties.primary_contact_id, DISTINCT(properties.suburb),DISTINCT(properties.property_type),users.first_name,users.last_name")
    properties.map{|x|
      suburb << {
        :suburb => x.suburb.to_s,
        :suburbText =>x.suburb.to_s
      } unless x.suburb.blank?;
      type << {
        :type => x.property_type.to_s,
        :typeText =>x.property_type.to_s
      } unless x.property_type.blank?;
      assigned_to << {
        :assigned_to => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?) ,
        :assigned_toText => (x.primary_contact.full_name.to_s unless x.primary_contact.blank?)
      } unless x.primary_contact.blank?
      ((floor << {
            :floor => x.detail.number_of_floors.to_s,
            :floorText => x.detail.number_of_floors.to_s
          } unless x.detail.number_of_floors.blank?) unless x.type == "ProjectSale" || x.type == "BusinessSale")
    }
    #!!server is using ruby 1.8.6 : uniq! for array-hash is deprecated
    uniq_suburb =[]
    uniq_type =[]
    uniq_assigned_to =[]
    uniq_floor =[]
    suburb.each{|x|uniq_suburb << x if !uniq_suburb.include?(x) and !x.blank?}
    type.each{|x|uniq_type << x if !uniq_type.include?(x) and !x.blank?}
    assigned_to.each{|x|uniq_assigned_to << x if !uniq_assigned_to.include?(x) and !x.blank?}
    floor.each{|x|uniq_floor << x if !uniq_floor.include?(x) and !x.blank?}

    uniq_suburb.to_a.sort!{|x,y| x[:suburbText].to_s <=> y[:suburbText].to_s}
    uniq_type.to_a.sort!{|x,y| x[:typeText].to_s <=> y[:typeText].to_s}
    uniq_assigned_to.to_a.sort!{|x,y| x[:assinged_toText].to_s <=> y[:assinged_toText].to_s}
    uniq_floor.to_a.sort!{|x,y| x[:floorText].to_s <=> y[:floorText].to_s}
    return(render(:json =>{:results => properties.length,:suburbs=>[uniq_suburb.unshift({:suburb=>'all',:suburbText=>"All Suburbs"})].flatten,:floors=>[uniq_floor.unshift({:floor=>'all',:floorText=>"All Floors"})].flatten,:types=>[uniq_type.unshift({:type=>'all',:typeText=>"All Property Types"})].flatten,:team=>[uniq_assigned_to.unshift({:assigned_to=>'all',:assigned_toText=>"All Team Member"})].flatten}))
  end

  def view_property_listings
    if params[:page_name] == 'listing_view'
      if !session[:ID].blank?
        condition = ["properties.id = #{session[:ID]} AND properties.office_id = #{current_office.id}"]
      else
        get_session_listings
        condition = [" properties.agent_contact_id = ? "]
        condition << params[:id]

        if current_user.limited_client_access?
          if !@office.control_level3
            condition[0] += " AND properties.primary_contact_id = ? "
            condition << current_user.id
          end
        elsif current_user.allow_property_owner_access?
          condition[0] += " AND properties.agent_user_id = ?"
          condition << current_user.id
        end

        if(!session[:type].blank? && Property::TYPES.map{|s| s[1]}.include?(session[:type]))
          condition[0] += " AND properties.type = ?"
          condition <<  session[:type]
        end


        if ((current_user.allow_property_owner_access? && session[:status] == 1) || (current_user.allow_property_owner_access?  && session[:status].blank?))|| current_user.allow_property_owner_access?
          session[:status] = "all"
        end

        unless session[:status] == "all"
          unless session[:status].blank?
            condition[0] += " AND properties.status = ?"
            condition <<  session[:status]
          end
        end

        unless session[:suburb] == "all" || session[:suburb].blank?
          condition[0] += " AND properties.suburb = ?"
          condition <<  session[:suburb]
        end

        unless session[:floor] == "all"
          unless session[:floor].blank?
            condition[0] += " AND properties.detail.number_of_floors = ?"
            condition <<  session[:floor]
          end
        end

        unless session[:primary_contact_fullname] == "all" || session[:primary_contact_fullname].blank?
          name =  session[:primary_contact_fullname].split(' ')
          condition[0] += " AND users.first_name  = ?"
          condition << name[0]
          unless name[1].blank?
            condition[0] += " AND users.last_name  = ?"
            condition << name[1]
          end
        end

        unless session[:property_type] == "all" || session[:property_type].blank?
          condition[0] += " AND properties.property_type = ?"
          condition << session[:property_type]
        end
      end
    end

    if params[:page_name] == 'office_view'

      condition = [" properties.agent_contact_id = ? "]
      condition << params[:id]

      if current_user.limited_client_access?
        if !@office.control_level3
          condition[0] += " AND properties.primary_contact_id = ? "
          condition << current_user.id
        end
      elsif current_user.allow_property_owner_access?
        condition[0] += " AND properties.agent_user_id = ?"
        condition << current_user.id
      end
    end
    condition[0] += " AND properties.deleted_at is NULL"

    params[:page] = params[:start].to_i / params[:limit].to_i + 1
    order = Property.build_sorting(params[:sort],params[:dir])
    if params[:sort] == 'sqm'
      @properties = Property.sort_by_customize(params[:sort],params[:controller],params[:dir],params[:page],params[:limit],@agent.id,@office.id, @property.id)
    else
      @properties = Property.paginate(:all, :include =>[:primary_contact,:purchaser,:agent_contact,:commercial_details], :conditions => condition, :order => order,:select => "properties.id,properties.office_id,properties.primary_contact_id,properties.agent_user_id,properties.type,properties.save_status,properties.status,properties.suburb,properties.price,properties.property_type,properties.unit_number,properties.street_number,properties.street,properties.updated_at,properties.deleted_at,users.first_name,users.last_name,properties.purchaser.amount,properties.purchaser.date,properties.purchaser.agent_contact.first_name,properties.agent_contact.last_name,properties.detail.number_of_floors,properties.detail.floor_area", :page =>params[:page], :per_page => params[:limit].to_i)
    end

    if @properties.present?
      @properties.each do |property|
        check_and_update_icon(property)
      end
    end
    results = 0
    if params[:limit].to_i == 20
      results = 20 #Property.find(:all,:select => 'count(properties.id), properties.primary_contact_id',:include =>[:primary_contact],:conditions => condition,:limit => params[:limit].to_i)
    else
      results = Property.count(:include =>[:primary_contact],:conditions => condition)
    end
    return(render(:json =>{
          :results => results,
          :rows=> @properties.map{|x|{
              'id' => x.id,
              'listing'=>Property.abbrv_properties(x.type),
              'property_type'=>x.property_type.blank? ? "" : x.property_type,
              'address' =>((x.street_number.blank? ? "" : x.street_number)+" "+(x.street.blank? ? "" : x.street.to_s)),
              'suite'=>(x.unit_number.blank? ? "" : x.unit_number),
              'sqm'=>x.detail.floor_area,
              'level'=>((x.detail.number_of_floors.blank? ? "" : x.detail.number_of_floors) unless x.type == "ProjectSale" || x.type == "BusinessSale"),
              'suburb' => x.suburb,
              'type' => x.property_type,
              'price' => Property.check_price(x),
              'assigned_to'=>x.primary_contact.blank? ? '' : x.primary_contact.full_name.to_s.strip,
              'actions'=>Property.check_color(x),
              'status' => Property.status_name(x),
              'relationship' => Property.check_relationship(x),
              'fb'=> [x.id,@office.url.blank? ? (x.detail.respond_to?(:deal_type) ? x.detail.property_url : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}") : "http://#{(@office.url.gsub('http://', '') unless @office.url.blank?)}" + "/#{x.id}", tweet(x)]} unless x.blank? }}))
  rescue Exception => ex
    Log.create(:message => "View Listings ---> errors : #{ex.inspect} ")
    return(render(:json =>{:results => nil,:rows=> nil}))
  end


end
