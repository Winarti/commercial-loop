class PdfConfigurationsController < ApplicationController
	layout "agent"
	before_filter :login_required
	before_filter :get_agent_and_office
	skip_before_filter :verify_authenticity_token, :only => [:index, :template_view]
	before_filter :check_permission_page
	before_filter :get_pdf_configuration

	def index
		@header_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Header"])
    	@footer_image = Attachment.find(:last, :conditions => ["attachable_id = ? AND attachable_type = ? AND description = ? AND parent_id IS NULL AND thumbnail IS NULL", @office.id, "Office", "PDF Brochure Footer"])
	end

	def template_view
		@color_list = %w(#000000 #F39EEA #E10A3C #D50AE1 #1F0AE1 #0ADDE1 #0DBB37 #FDF944 #FDCE44 #666666 #57585a #999999 #CCCCCC #f0f0f0 #FFFFFF)
		@text_editor = Brochure.theme_editor(@pdf_configuration.theme)
		@editor_limit = Brochure.editor_limit(@pdf_configuration.theme)
		@opt_color = (@pdf_configuration.theme == "theme8") ? "theme4_color_options" : "color_options"
	end

	def create
		respond_to do |format|
			unless params[:pdf_configuration].blank?
				@pdf_configuration = PdfConfiguration.new(params[:pdf_configuration].merge(:office_id => @office.id))
				old_pdf_configuration = @office.pdf_configuration
				if @pdf_configuration.save
				  old_pdf_configuration.try(:destroy)
          	      format.html {redirect_to :action => 'template_view'}
          		else
		          flash[:notice] = 'PDF template failed to save' + @pdf_configuration.errors.to_s
		          format.html {render :action => 'index'}
		        end
			end
		end
	end

	def update
		respond_to do |format|
			if @pdf_configuration.update_attributes(params[:pdf_configuration])
				flash[:notice] = "PDF template was successfuly updated"
				format.html {redirect_to "/agents/#{@agent.id}/offices/#{@office.id}" }
			else
				flash[:notice] = 'PDF template failed to save' + @pdf_configuration.errors.to_s
		        format.html {render :action => 'template_view'}
			end
		end
	end

	private

	def get_pdf_configuration
		@pdf_configuration = current_office.pdf_configuration if current_office.pdf_configuration
	end
end
