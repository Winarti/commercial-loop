class OffersController < ApplicationController
  layout false
  skip_before_filter :verify_authenticity_token, :only => [:auto_complete_for_offer_name]
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office
  before_filter :prepare_offer
  auto_complete_for :agent_contact, :first_name

  def index
    @offer = Offer.new
    @agent_contact = @offer.build_agent_contact
  end

  def show
  end

  def new
  end

  def edit
  end

  def create
    create_contact
    params[:offer] = params[:offer].merge(:property_id => @property.id)
    @offer = Offer.new(params[:offer])

    respond_to do |format|
      if @offer.save
        create_notes
        format.html { redirect_to :action => 'index' }
      else
        session[:close_popup] = false
        format.html { render :action => "index" }
      end
    end
  end

  def update
  end

  def destroy
    @offer = Offer.find_by_id(params[:id])
    @offer.destroy unless @offer.blank?
    redirect_to status_agent_office_property_path(@agent,@office,@property)
  end

  def prepare_offer
    @property = Property.find(params[:property_id])
  end

  def auto_complete_for_offer_name
   contacts = AgentContact.search_for_autocomplete(params[:offer][:name], @agent.id, @office.id)
   render :partial => 'contact_list', :locals => { :contacts => contacts}
  end

  def create_contact
    category_id = (@property.type == 'ResidentialLease')? 11 : 8

    unless params[:offer][:agent_contact_id].blank?
      agent_contact= AgentContact.find_by_id(params[:offer][:agent_contact_id])
      agent_contact.update_category_contact(category_id) unless agent_contact.blank?
    else
      team = []
      team << @property.primary_contact_id unless @property.primary_contact_id.nil?
      team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?

      params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id])

      @agent_contact = AgentContact.new(params[:agent_contact])
      if @agent_contact.save
        @agent_contact.add_categories([category_id])
        @agent_contact.add_assigns(team)
        @agent_contact.add_access(team)
        @agent_contact.use_spawn_for_boom
        @agent_contact.use_spawn_for_md
        @agent_contact.use_spawn_for_irealty
        params[:offer] = params[:offer].merge(:agent_contact_id => @agent_contact.id)
      end
    end
  end

  def update_duplicate
    category_id = (@property.type == 'ResidentialLease')? 11 : 8

    duplicate = 0
    detected = nil
    params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id])

    %w(email mobile_number contact_number work_number home_number).each{|w|
      unless params[:agent_contact][w].blank?
        check = AgentContact.check_exist(params[:agent_contact], w)
        unless check.nil?
          detected = check
          duplicate = 1
        end
      end
    }

    if duplicate == 1
      unless detected.nil?
        @agent_contact = AgentContact.find(detected.id)
          if @agent_contact.update_attributes(params[:agent_contact].delete_if{|x,y| y.blank?})
            @agent_contact.update_category_contact(category_id)
            params[:offer] = params[:offer].merge(:property_id => @property.id, :agent_contact_id => @agent_contact.id)
            @offer = Offer.new(params[:offer])
            create_notes if @offer.save
          end
      end
    end
    redirect_to agent_office_property_offers_path(@agent,@office, @property)
  end

  def create_notes
    @agent_contact= AgentContact.find_by_id(@offer.agent_contact_id)
    unless @agent_contact.blank?
      @agent_contact.create_note(@property.id, @offer.detail, 7, current_user.id)
       if (!@property.lease? || (@property.is_a?(Commercial) && ["Sale","Both"].include?(@property.deal_type)))
        #note type for property = Submit Offer if for sale property
        @agent_contact.create_note_for_property(@property.id, @offer.detail, 7, "", current_user.id)
      end
    end
    @offer.update_attributes({:contact_note_id => @offer.agent_contact_id})
    session[:close_popup] = true
    flash[:notice] = 'Offer Property was successfully created.'
  end
end
