class VaultsController < ApplicationController
  before_filter :login_required, :except => [:generate_watermark_pdf, :contact_vault, :check_vault, :vault_documents, :vault_download, :update_description,:update_brochure,:delete_brochure]
  before_filter :get_agent_and_office, :get_property, :except => [:contact_vault, :check_vault, :vault_documents, :vault_download]
  before_filter :check_permission_page, :only => [:index]
  before_filter :check_permission_vault, :only => [:contact_vault, :vault_documents, :vault_download]
  before_filter :populate_data, :only => [:vault_settings, :vault_files, :vault_contacts]
  skip_before_filter :verify_authenticity_token, :except => [:contact_vault, :check_vault, :vault_documents, :vault_download, :index, :create, :destroy]
  layout "agent", :except => [:add_contact, :create_contact]
  include VaultHelper

  def index
    redirect_to :action => "vault_settings"
  end

  def vault_settings
  end

  def vault_contacts
  end

  def vault_files
    @vault = @property.vault || Vault.new
    browser_uploader  = @office.uploader
    unless params[:isBrowser].blank?
     browser_uploader = (params[:isBrowser] == 'true') ? true :false
     ActiveRecord::Base.connection.execute("UPDATE offices SET uploader= #{(params[:isBrowser] == 'true') ? 1 : 0} WHERE ID=#{@office.id}") unless @office.uploader.to_s == params[:isBrowser]
    end
    render 'vaults/browser_uploader' if browser_uploader == true
  end

  def populate_data
    @vault = @property.vault || Vault.new
    @expiry_hours = [["Please Select", nil], ["2 Hours", 2], ["6 Hours", 6], ["12 Hours", 12], ["24 Hours", 24], ["1 Week", 168], ["1 Month", 720]]
  end

  def contact_vault
    agent_contact_id = session[:contact_login]
    @agent_contact = AgentContact.find(session[:contact_login])
    @vault_contacts = VaultContact.find(:all, :conditions => "agent_contact_id = #{agent_contact_id}")
    respond_to do |format|
      format.html { render :file => "/vaults/contact_vault", :layout => "vault"}
    end
  end

  def check_vault
    @vault = Vault.find_by_id(unrandom_id(params[:vault_id]))
    if @vault.blank?
      flash[:error] = 'Invalid vault Id.'
      redirect_to contact_vault_path
    else
      if @vault.password == params[:password]
        redirect_to vault_documents_path(params[:vault_id])
      else
        flash[:error] = 'Password invalid, Please reenter your correct password.'
        redirect_to contact_vault_path
      end
    end
  end

  def vault_documents
    @vault = Vault.find_by_id(unrandom_id(params[:vault_id]))
    redirect_to contact_vault_path and return if @vault.blank?
    respond_to do |format|
      format.html { render :file => "/vaults/vault_documents", :layout => "vault"}
    end
  end

  def vault_download
    unless params[:id].blank?
     agent_contact_id = session[:contact_login]
     vault = Vault.find_by_id(params[:vault_id])
     pdf  = Attachment.find_by_id(params[:id])
     VaultLog.create({:property_id => vault.property_id, :agent_contact_id => agent_contact_id, :log_title =>  pdf.filename})
     property = Property.find_by_id(vault.property_id)
     unless property.blank?
       ad = [ property.unit_number, property.street_number, property.street ].reject { |a| a.blank? || a == 'blank' }.join(" ")
       address = [ad, property.suburb ].join(", ")
       ContactNote.create({:agent_contact_id => agent_contact_id, :agent_user_id => property.primary_contact_id, :note_type_id => 25, :description => "Download Document #{pdf.filename}", :address => address, :property_id => property.id, :note_date => Time.now})

       note = {:skip_enquiry => true, :property_id => property.id, :agent_contact_id => agent_contact_id, :agent_user_id => property.primary_contact_id, :note_type_id => 25, :address => address, :description => "Download Document #{pdf.filename}", :note_date => Time.now, :duplicate => 0}
       property_note = PropertyNote.new(note)
       property_note.save!
     end

     begin
#       if vault.enable_expiry == true
#         hour = vault.expiry_hour
#         real_file = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
#         ency = Base64.encode64(real_file)
#         redirect_to "http://commercialloopcrm.com.au/php_file/vault.php?status=#{hour}&file=#{ency}"
#         Log.create({:message => "Download vault http://commercialloopcrm.com.au/php_file/vault.php?status=#{hour}&file=#{ency}"})
#       else
#         redirect_to pdf.public_filename
#       end
       redirect_to pdf.public_filename
     rescue
       flash[:notice] = "File not found."
       redirect_to contact_vault_path
     end
    end
  end

  def vault_logs
  end

  def delete_contact
    ActiveRecord::Base.connection.execute("DELETE FROM vault_contacts WHERE vault_id = #{params[:id]} And agent_contact_id = #{params[:contact_id]}") unless params[:contact_id].blank?
    flash[:notice] = 'Contact Already deleted from access'
    redirect_to vault_contacts_agent_office_property_vaults_path(@agent, @office, @property)
  end

  def extend_access
    unless params[:contact_id].blank?
      ActiveRecord::Base.connection.execute("DELETE FROM vault_contacts WHERE vault_id = #{params[:id]} And agent_contact_id = #{params[:contact_id]}")
      VaultContact.create({:vault_id => params[:id], :property_id => @property.id, :agent_contact_id => params[:contact_id]})
      flash[:notice] = 'Contact Access already extended'
    end
    redirect_to vault_contacts_agent_office_property_vaults_path(@agent, @office, @property)
  end

  def search_contact
    jsons = []
    conditions = ""
    conditions = " And (id NOT IN(SELECT vault_contacts.agent_contact_id from vault_contacts WHERE vault_contacts.vault_id = #{params[:vault_id]}))" unless params[:vault_id].blank?
    ag = AgentContact.find(:all, :conditions => "(`first_name` LIKE '%#{params[:selection_contact_name]}%' OR `last_name` LIKE '%#{params[:selection_contact_name]}%') And office_id = #{@office.id} #{conditions}", :order => "first_name, last_name ASC")
    if !ag.blank?
      ag.each do |ac|
        unless ac.blank?
          if ac.inactive != true
            vc = VaultContact.find(:first, :conditions => "property_id = #{@property.id} And agent_contact_id = #{ac.id}")
            granted = status = "-"
             unless vc.blank?
               if vc.vault.access_time != 0
                 last_update = (vc.vault.updated_at.utc.day + (vc.vault.updated_at.utc.month * 30))
                 last_assign = (vc.updated_at.utc.day + (vc.updated_at.utc.month * 30))
                 diff = last_update - last_assign

                 allowed_to_access = "<span style='color:red;'>expired</span>"
                 allowed_to_access = "active" if (diff <= 1) && (vc.vault.access_time == 1)
                 allowed_to_access = "active" if (diff <= 7) && (vc.vault.access_time == 2)
                 allowed_to_access = "active" if (diff <= 31) && (vc.vault.access_time == 3)
                 status = allowed_to_access
               else
                status = "active"
               end
               granted = vc.created_at.strftime('%d/%m/%Y %H:%I:%S')
             end
             jsons << {'id' => ac.id, 'name' => "#{ac.full_name.capitalize}", "date"=> granted, "status" => "<b>#{status}</b>"}
          end
        end
      end
      text = {"results" => jsons, "total" => (ag ? ag.length : 0) }
    else
      text = ''
    end

    render :json => text
  end

  def update_vault
    if @property.vault.blank?
      params[:vault] = params[:vault].merge(:property_id => @property.id, :office_id => @office.id, :contact_access => @contact_name_checkbox_ids)
      @vault = Vault.new(params[:vault])
    else
      @vault = @property.vault
    end

    old_watermark_text = @vault.watermark_text

    if @property.vault.blank?
      sv = @vault.save
    else
      sv = @vault.update_attributes(params[:vault])
    end

    if sv
      params[:contact_name_checkbox].each{|cid|
        vc = VaultContact.find(:first, :conditions => "property_id = #{@property.id} And agent_contact_id = #{cid.gsub(/\s+/, "")}")
        if vc.blank?
          unless cid.blank?
            ContactNote.create({:agent_contact_id => cid.gsub(/\s+/, ""), :agent_user_id => @property.primary_contact_id, :note_type_id => 25, :description => "this user is granted access to property", :address => @property.address.to_s, :property_id => @property.id, :note_date => Time.now})
            VaultContact.create({:vault_id => @vault.id, :property_id => @property.id, :agent_contact_id => cid.gsub(/\s+/, "")})
            spawn(:kill => true) do
              begin
                contact = AgentContact.find(cid)
                if @property.office.vault_template.blank?
                  if request.subdomains[0] == "knightfrank"
                    vt = VaultTemplate.find(:first, :conditions => "office_id = 1027")
                  else
                    vt = VaultTemplate.find(:first, :conditions => "office_id = 1")
                  end
                  VaultTemplate.create({:office_id=> @property.office_id, :headline => vt.headline, :content => vt.content, :enable_autoresponder => true})
                end
                unless @property.office.vault_template.blank?
                  unless @property.primary_contact.blank?
                    if (contact.username.present? and contact.password.present?) and @office.vault_template.enable_autoresponder == true and params[:vault][:welcome_email].present?
                      ContactMailer.deliver_send_vault(@property, @property.primary_contact, @office.vault_template, contact)
                      ContactNote.create({:agent_contact_id => cid.gsub(/\s+/, ""), :agent_user_id => @property.primary_contact_id, :note_type_id => 13, :description => "Welcome email for the Vault sent to this contact.", :address => @property.address.to_s, :property_id => @property.id, :note_date => Time.now})
                      vc2 = VaultContact.find(:first, :conditions => "property_id = #{@property.id} And agent_contact_id = #{cid.gsub(/\s+/, "")}")
                      vc2.update_attributes(:welcome_sent_at => Time.now)
                    end
                  else
                    flash[:notice] = "Property primary contact can't be blank"
                  end
                end
              rescue Exception => ex
                Log.create({:message => "Failed vault send email for #{cid}, property id : #{@property.id}, property.primary_contact: #{@property.primary_contact.inspect}, contact: #{contact.inspect}, Error-->  #{ex.inspect}"})
              end
            end
          end
        end
      } unless params[:contact_name_checkbox].blank?

      if @vault.enable_watermark == true
        watermark_file = @property.vault_files.find(:first, :conditions => "category = 'watermark'")
        require 'fpdf'
        if watermark_file.blank?
          filename = "watermark_#{Time.zone.now.strftime("%d%m%Y-%H%I%S")}" + ".pdf"
          pdf_file = Attachment.create(:size => 1001, :content_type => "application/pdf", :filename => filename, :attachable_id => @property.id, :attachable_type => "Property", :category => "watermark", :title => "watermark", :send_to_api => false)
          if pdf_file
            pdf_file.update_attribute(:type, "VaultFile")
            pdf = Attachment.find(pdf_file.id)
            path = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
            path_folder = path.gsub("/"+filename, "")
            system("mkdir -p #{path_folder}")
            pdf=FPDF.new
            pdf.AddPage
            pdf.SetFont('Arial','B',21)
            pdf.Cell(190,220,@vault.watermark_text,0,0,'C')
            pdf.Output(path)
          end
        else
          if old_watermark_text != @vault.watermark_text
            path = watermark_file.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
            pdf=FPDF.new
            pdf.AddPage
            pdf.SetFont('Arial','B',21)
            pdf.Cell(190,220,@vault.watermark_text,0,0,'C')
            pdf.Output(path)
          end
        end
      end
    end

    flash[:notice] = 'Vault has successfully been updated.'
    if params[:redirect] == "contact"
      redirect_to vault_contacts_agent_office_property_vaults_path(@agent, @office, @property)
    elsif params[:redirect] == "vault_files"
      redirect_to vault_files_agent_office_property_vaults_path(@agent, @office, @property)
    else
      redirect_to vault_settings_agent_office_property_vaults_path(@agent, @office, @property)
    end
  end

  def update_brochure
    if @property.vault.blank?
      property_id = @property.id
      Vault.create({:office_id => @office.id, :property_id => property_id})
      @property = Property.find(property_id)
    end
    return render(:text =>"Limit Error") if (@property.brochure.length + 1) > Property::MAX_VAULT
    vault_file = VaultFile.new(:uploaded_data => params[:uploaded_data])
    vault_file.title = params[:description]
    vault_file.attachable = @property
    vault_file.updated_at = Time.now
    if vault_file.validate_brochures && vault_file.save
      add_watermark_to_pdf_vault(vault_file)
      if @property.vault.update_alert
        spawn(:kill => true) do
          begin
            agent_contacts = VaultContact.find(:all, :conditions => "vault_id = #{@property.vault.id}")
            agent_contacts.each do |contact|
              @cid = contact.agent_contact_id
              contact = AgentContact.find(@cid)
              ContactMailer.deliver_send_update_notice(@property, @property.primary_contact, contact)
              ContactNote.create({:agent_contact_id => @cid, :agent_user_id => @property.primary_contact_id, :note_type_id => 1, :description => "Files update alert email for the Vault sent to this contact.", :address => @property.address.to_s, :property_id => @property.id, :note_date => Time.now})
            end
          rescue Exception => ex
            Log.create({:message => "Failed vault send email for #{@cid} -->  #{ex.inspect}"})
          end
        end
      end
      return render(:text => "#{params[:unit_id]},brochure_images_#{params[:unit_id]},#{vault_file.id},#{vault_file.title},#{vault_file.position},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/vaults/destroy_brochure?image_id=#{vault_file.id}"},#{vault_file.public_filename},brochure,#{vault_file.public_filename},#{"/agents/#{@agent.id}/offices/#{@office.id}/properties/#{@property.id}/vaults/update_description"}")
    end
    return render(:nothing => true)
  end

  def update_simple_brochures
    if @property.vault.blank?
      property_id = @property.id
      Vault.create({:office_id => @office.id, :property_id => property_id})
      @property = Property.find(property_id)
    end
    unless params[:brochures].blank?
      params[:brochures].each do |b|
        unless (@property.vault_files.length + 1) > Property::MAX_VAULT
         original = @property.vault_files.find_by_id(b[:id])
         if b[:uploaded_data]
            if original
              original.uploaded_data = b[:uploaded_data]
              original.description = b[:description]
              original.updated_at = Time.now
              original.save
              vault_file= original
            else
              vault_file = VaultFile.new(:uploaded_data => b[:uploaded_data])
              vault_file.title = b[:description]
              vault_file.attachable = @property
              vault_file.updated_at = Time.now
              vault_file.validate_brochures && vault_file.save
            end
            add_watermark_to_pdf_vault(vault_file)
            if @property.vault.update_alert
                begin
                  agent_contacts = VaultContact.find(:all, :conditions => "vault_id = #{@property.vault.id}")
                  agent_contacts.each do |contact|
                    @cid = contact.agent_contact_id
                    contact = AgentContact.find(@cid)
                    ContactMailer.deliver_send_update_notice(@property, @property.primary_contact, contact)
                    ContactNote.create({:agent_contact_id => @cid, :agent_user_id => @property.primary_contact_id, :note_type_id => 1, :description => "Files update alert email for the Vault sent to this contact.", :address => @property.address.to_s, :property_id => @property.id, :note_date => Time.now})
                  end
                rescue Exception => ex
                  Log.create({:message => "Failed vault send email for #{@cid} -->  #{ex.inspect}"})
                end
            end
          elsif b[:description]
            unless original.nil?
              original.update_attribute(:description, b[:description])
              original.update_attribute(:updated_at ,Time.now)
            end
          end
        end
     end
    end
    redirect_to vault_files_agent_office_property_vaults_path(@agent, @office, @property)
  end

  def sort_brochure
    @property.vault_files.each do |b|
      unless params["brochure"].index(b.id.to_s).blank?
        b.position = params["brochure"].index(b.id.to_s) + 1
        b.save(false)
      end
    end
    render :nothing => true
  end

  def destroy_brochure
    @property.vault_files.find(params[:image_id]).destroy
    respond_to do |format|
      format.html {
        flash[:notice] = "Vault File deleted."
        redirect_to :action => 'index'
      }
      format.js {
        render :update do |page|
          page["tr_brochure_#{params[:image_id]}"].remove
        end
      }
    end
  end

  def update_description
    media = Kernel.const_get(params[:media_type].to_s.capitalize).find_by_id(params[:media_id]) unless params[:media_type].blank?
    begin
      media.update_attribute("description", params["description"]) unless media.blank?
    rescue
    end
    return render(:json =>(media.description unless media.blank?))
  end

  def add_contact
    @agent_contact = AgentContact.new
  end

  def create_contact
    category_id = (@property.type == 'ResidentialLease')? 11 : 8
    team = []
    team << @property.primary_contact_id unless @property.primary_contact_id.nil?
    team << @property.secondary_contact_id unless @property.secondary_contact_id.nil?
    params[:agent_contact] = params[:agent_contact].merge(:category_contact_id => category_id, :agent_id => params[:agent_id], :office_id => params[:office_id])

    respond_to do |format|
      @agent_contact = AgentContact.new(params[:agent_contact])
      require "base64"
      @agent_contact.password=Base64.encode64(params[:agent_contact][:password].gsub(/\s+/, "")) unless params[:agent_contact][:password].blank?
      if @agent_contact.save
        @agent_contact.add_categories([category_id])
        @agent_contact.add_assigns(team)
        @agent_contact.add_access(team)
        @agent_contact.use_spawn_for_boom
        @agent_contact.use_spawn_for_md
        @agent_contact.use_spawn_for_irealty
        flash[:notice] = 'Contact was successfully created.'
        format.html { redirect_to :action => 'add_contact' }
      else
        @agent_contact.password = Base64.decode64(@agent_contact.password) unless @agent_contact.password.blank?
        format.html { render :action => "add_contact" }
      end
    end
  end

  def check_permission_vault
    render_optional_error_file(401) if session[:contact_login].blank?
  end

  def add_watermark_to_pdf_vault(vault_file)
    if @property.vault.enable_watermark == true
      vault_file_tmp = vault_file.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
      vault_file_tmp_filename = vault_file.filename
      watermark_file = @property.vault_files.find(:first, :conditions => "category = 'watermark'")
      unless watermark_file.blank?
        watermark_path = watermark_file.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
        path_folder = vault_file_tmp.gsub("/"+vault_file_tmp_filename, "")
        input_file = path_folder+"/input.pdf"
        system("mv #{vault_file_tmp} #{input_file}")
        system("pdftk #{input_file} stamp #{watermark_path} output #{vault_file_tmp}")
        system("rm -f #{input_file}")
      end
    end
  end

  def send_welcome_mail
    begin
      contact = AgentContact.find(params[:contact_id])
      if @property.office.vault_template.blank?
        if request.subdomains[0] == "knightfrank"
          vt = VaultTemplate.find(:first, :conditions => "office_id = 1027")
        else
          vt = VaultTemplate.find(:first, :conditions => "office_id = 1")
        end
        VaultTemplate.create({:office_id=> @property.office_id, :headline => vt.headline, :content => vt.content, :enable_autoresponder => true})
      end
      unless @property.office.vault_template.blank?
        unless @property.primary_contact.blank?
          if (contact.username.present? and contact.password.present?) and @office.vault_template.enable_autoresponder == true
            ContactMailer.deliver_send_vault(@property, @property.primary_contact, @office.vault_template, contact)
            ContactNote.create({:agent_contact_id => params[:contact_id], :agent_user_id => @property.primary_contact_id, :note_type_id => 13, :description => "Welcome email for the Vault sent to this contact.", :address => @property.address.to_s, :property_id => @property.id, :note_date => Time.now})
            vc2 = VaultContact.find(:first, :conditions => "property_id = #{@property.id} And agent_contact_id = #{params[:contact_id]}")
            vc2.update_attributes(:welcome_sent_at => Time.now)
            flash[:notice] = 'Welcome email sent.'
          elsif contact.username.blank? || contact.password.blank?
            flash[:notice] = "Username and password can't be blank"
          end
        else
          flash[:notice] = "Property primary contact can't be blank"
        end
      end
    rescue Exception => ex
      Log.create({:message => "Failed vault send email for #{params[:contact_id]}, property id : #{@property.id}, property.primary_contact: #{@property.primary_contact.inspect}, contact: #{contact.inspect}, Error-->  #{ex.inspect}"})
    end
    redirect_to vault_contacts_agent_office_property_vaults_path(@agent, @office, @property)
  end

end