class CountryPropertyTypesController < ApplicationController
  prepend_before_filter :login_required

  skip_before_filter :verify_authenticity_token, :only => %w( search )
  include Geokit::Geocoders

  def geosearch_in
    begin
      location = GoogleGeocoder.geocode(params[:id])
      unless location.blank?
        render :json =>[location.lat,location.lng,location.accuracy]
      else
        render :json =>[]
      end
    rescue
      render :json =>[-25.274398,133.775136,4,1]
    end
  end

  def search_ptype
    country = Country.find_by_name(params[:country])
    country_ptypes_id = []
    unless country.blank?
      country_ptypes = CountryPropertyTypeRelationship.find_all_by_country_id(country.id,:select =>"property_type_id")
      country_ptypes.each do |c|
        country_ptypes_id << c.property_type_id
      end
      condition=[' 1=1']
      unless country_ptypes_id.blank?
        condition[0] += " AND id IN(?)"
        condition << country_ptypes_id
      end
      category = "Residential" if params[:ptype].include?('residential') || params[:ptype] == "project_sale"
      category = "Holiday" if params[:ptype].include?('holiday')
      category = "Commercial" if params[:ptype].include?('commercial')
      unless category.blank?
        condition[0] += " AND category like(?)"
        condition << category
      end
      ptypes = PropertyType.find(:all,:conditions =>condition)
      render :json =>ptypes.map{|t| [t.name]}
    else
      render :json =>country_ptypes_id
    end
  end

  def search_eer
    country = Country.find_by_name(params[:country])
    eer = []
    unless country.blank?
      if country.name == "Australia"
        if (params[:ptype] == 'commercial' || params[:ptype] == 'business_sale')
          (0..6).step(0.5){|i| eer << i }
        else
          (0..10).step(0.5){|i| eer << i }
        end
        render :json =>eer.compact
      else
      if country.eer_lower >= 0 && country.eer_upper > country.eer_lower && country.eer_deviation < country.eer_upper
        move = (country.eer_deviation == 0)? 1 : country.eer_deviation
        (country.eer_lower..country.eer_upper).step(move){|i| eer << i }
        render :json =>eer.compact
      else
        render :json =>eer
      end
      end
    else
      render :json =>eer
    end
  end

  def search_label
    country = Country.find_by_name(params[:country])
    unless country.blank?
      address = [country.address1+"<span class='red_star'>*</span>",country.address2+"<span class='red_star'>*</span>",country.address3+"<span class='red_star'>*</span>",country.address4+"<span class='red_star'>*</span>"]

      if ["general_sale", "clearing_sales_event", "livestock_sale", "clearing_sale"].include? (params[:ptype])
        address = [country.address1, country.address2, country.address3, country.address4]
      end

      render :json =>address
    else
      render :json =>[]
    end
  end

  def search_zoning
    country = Country.find_by_name(params[:country])
    unless country.blank?
      render :json =>country.zoning.blank? ? [] : country.zoning
    else
      render :json =>[]
    end
  end
end
