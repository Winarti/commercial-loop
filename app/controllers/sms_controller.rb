class SmsController < ApplicationController
  include SmsHelper


  layout 'agent'

  # before_filter :staging_app, :except => [:update_status]
  before_filter :login_required, :except => [:update_status]
  before_filter :get_agent_and_office, :except => [:update_status]
  before_filter :check_update_status_params, :only => [:update_status]

  skip_before_filter :verify_authenticity_token, :only => [:auto_complete]


  def index
    page = params[:start].to_i / params[:limit].to_i + 1 unless params[:limit].nil?
    order = SmsMessage.build_sorting(params[:sort], params[:dir])

    @sms_messages = SmsMessage.paginate(
      :all,
      :conditions => ["sender_id = ? AND office_id = ?", current_user.id, @office.id],
      :order => order,
      :page => page,
      :per_page => params[:limit]
    )

    sms_messages_count = SmsMessage.count(
      :all,
      :conditions => ["sender_id = ? AND office_id = ?", current_user.id, @office.id]
    )

    respond_to do |format|
      format.html { render :action => 'index' }
      format.js
      format.json { render :json => {
        :results => sms_messages_count,
        :rows => @sms_messages.map{|m| {
          'id' => m.id.to_s,
          'receiver_number' => m.receiver_number,
          'message' => m.message,
          'status' => m.status,
          'created_at' => m.created_at.strftime("%d-%m-%Y %H:%M:%S"),
          'message_id' => m.message_id,
          'received_at' => m.received_at.nil? ? nil : m.received_at.strftime("%d-%m-%Y %H:%M:%S")
        }}
      }}
    end
  end

  def new
    @sms_message = SmsMessage.new
    @purchase_url = purchase_credit_url(SmsMessage::PURCHASE_URL, @office.id, request.url)
  end

  def create
    @sms_message = SmsMessage.new

    receivers = params[:sms_message][:receiver_number].split(',')
    receivers.delete_at(0)

    number_of_messages = total_messages(params[:sms_message][:message])
    credit = total_credit(receivers.length, number_of_messages)

    if receivers.blank?
      @sms_message.errors.add(:receiver_number, " can't be blank")
    end

    if @office.sms_credit < credit
      @sms_message.errors.add(:base, "You have insufficient SMS credit, please top up your balance first.")
    end

    respond_to do |format|
      if @sms_message.errors.blank?
        receivers.each do |r|
          @sms_message = SmsMessage.new(
            :sender_id => current_user.id,
            :office_id => params[:office_id],
            :receiver_number => r,
            :message => params[:sms_message][:message],
            :status => "Pending",
            :created_at => Time.zone.now,
            :updated_at => Time.zone.now
          )

          if @sms_message.send_sms && @office.subtract_sms_credit(number_of_messages)
            flash[:notice] = 'SMS message was successfully saved.'
            format.html { redirect_to :action => 'index' }
          else
            format.html { render :action => "new" }
            format.xml  { render :xml => @sms_message.errors}
          end
        end
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @sms_message.errors }
      end
    end
  end

  def auto_complete
    result = []
    keyword = '%' + params[:q].to_s + '%'

    agent_contacts = AgentContact.find(
      :all,
      :conditions => ["(first_name LIKE ? OR last_name LIKE ? OR mobile_number LIKE ?) AND agent_id = ? AND office_id = ? AND mobile_number IS NOT NULL AND mobile_number <> ''", keyword, keyword, keyword, @agent.id, @office.id]
    )

    agent_contacts.each do |c|
      result << {
        'id' => c.id.to_s,
        'name' => c.full_name,
        'mobile_number' => c.mobile_number
      }
    end

    render :json => {
      'results' => result,
      'total' => (agent_contacts.blank? ? 0 : agent_contacts.length)
    }
  end

  def update_status
    success = false
    message = "Cannot find SMS with current message ID."

    @sms_message = SmsMessage.find(
      :first,
      :conditions => ["message_id = ?", params[:apiMsgId]]
    )

    unless @sms_message.nil?
      @sms_message.update_status(params[:status], params[:timestamp])
      success = true
      message = "Successfully update the message."
    end

    render :json => {
      :success => success,
      :message => message
    }
  end


  private

  def check_update_status_params
    if params[:apiMsgId].nil? || params[:timestamp].nil? || params[:status].nil?
      render :json => {
        :success => false,
        :message => "Invalid parameters."
      }
    end
  end

  def staging_app
    unless params[:office_id].to_i == 243
      redirect_to root_path
    end
  end

end
