class ResidentialBuildingReportsController < ApplicationController
  layout "agent"

  before_filter :login_required, :except => [:generate_building_data_pdf, :print_building_data_pdf, :generate_price_list_pdf, :print_price_list_pdf]
  before_filter :get_agent_and_office

  def index

  end

  ### Start Export XLS ###

  def export_residential_sale_xls
    @residential_sales = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ?", params[:office_id], params[:property_id], "ResidentialSale"], :include => [:extra,:residential_sale_details], :order => "unit_number ASC")
    @building_name = Property.find(params[:property_id])

    @sold_properties = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ? AND status = ?", params[:office_id], params[:property_id], "ResidentialSale", "2"], :include => [:extra,:residential_sale_details])
    @parent_listing = Property.find(params[:property_id])
    respond_to do |format|
      format.html #export_residential_sale_xls.html.erb
      format.xml { render :xml => @residential_sales }
      format.xls
    end
    headers["Content-Disposition"] = "attachment; filename=\"#{@property.detail.development_name} Building Price List #{Time.now.strftime("%d%m%Y%H%M%S")}.xls\""
  end

  def export_residential_sale_xls2
    @residential_sales = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ?", params[:office_id], params[:property_id], "ResidentialSale"], :include => [:extra,:residential_sale_details])
    @building_name = Property.find(params[:property_id])

    @sold_properties = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ? AND status = ?", params[:office_id], params[:property_id], "ResidentialSale", "2"], :include => [:extra,:residential_sale_details])
    @parent_listing = Property.find(params[:property_id])
    respond_to do |format|
      format.html #export_residential_sale_xls2.html.erb
      format.xml { render :xml => @residential_sales }
      format.xls
    end
    headers["Content-Disposition"] = "attachment; filename=\"#{@property.detail.development_name} Buildings #{Time.now.strftime("%d%m%Y%H%M%S")}.xls\""
  end

  ### End Export XLS ###

  ### Start Export CSV ###

  def export_residential_sale_csv
    @residential_sales = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ?", params[:office_id], params[:property_id], "ResidentialSale"], :include => [:extra,:residential_sale_details])
    @parent_listing = Property.find(params[:property_id])
    residential_sales_csv = FasterCSV.generate do |csv|
      # header row
      csv << "#{@parent_listing.address}"
      csv << "\n"
      csv << ["Unit", "Internal Area m2", "Balcony Area m2", "Total Area", "Beds", "Bath", "Car Park", "Floor Coverings", "Aspect", "Purchase Price", "Estimated Stamp Duty Payable", "Estimated Pay Duty Saving", "Price m2", "Owner Corporation Cost (Pa)", "Notes"]
      @residential_sales.each do |res_sale|
        #data rows
        csv << [
          (res_sale.unit_number.blank? ? "" : res_sale.unit_number),
          (res_sale.detail.blank? ? 0 : (res_sale.detail.internal_area.blank? ? 0 : res_sale.detail.internal_area.to_f)),
          (res_sale.detail.blank? ? 0 : (res_sale.detail.balcony_area.blank? ? 0 : res_sale.detail.balcony_area.to_f)),
          (res_sale.detail.blank? ? 0 : (res_sale.detail.floor_area.blank? ? 0 : res_sale.detail.floor_area.to_f)),
          (res_sale.detail.blank? ? 0 : (res_sale.detail.bedrooms.blank? ? 0 : res_sale.detail.bedrooms)),
          (res_sale.detail.blank? ? 0 : (res_sale.detail.bathrooms.blank? ? 0 : res_sale.detail.bathrooms)),
          ((res_sale.detail.blank? ? 0 : (res_sale.detail.carport_spaces.blank? ? 0 : res_sale.detail.carport_spaces.to_i)) + (res_sale.detail.blank? ? 0 : (res_sale.detail.garage_spaces.blank? ? 0 : res_sale.detail.garage_spaces.to_i)) + (res_sale.detail.blank? ? 0 : (res_sale.detail.off_street_spaces.blank? ? 0 : res_sale.detail.off_street_spaces.to_i))),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field1.blank? ? "" : res_sale.extra.field1)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field2.blank? ? "" : res_sale.extra.field2)),
          (res_sale.price.blank? ? ActionController::Base.helpers.number_to_currency(0) : ActionController::Base.helpers.number_to_currency(res_sale.price)),
          (res_sale.extra.blank? ? ActionController::Base.helpers.number_to_currency(0) : (res_sale.extra.field3.blank? ? ActionController::Base.helpers.number_to_currency(0) : ActionController::Base.helpers.number_to_currency(res_sale.extra.field3.to_i))),
          (res_sale.extra.blank? ? ActionController::Base.helpers.number_to_currency(0) : (res_sale.extra.field4.blank? ? ActionController::Base.helpers.number_to_currency(0) : ActionController::Base.helpers.number_to_currency(res_sale.extra.field4.to_i))),
          ((res_sale.price.blank? ? ActionController::Base.helpers.number_to_currency(0) : ((res_sale.price)/(res_sale.detail.blank? ? 1 : (res_sale.detail.floor_area.blank? ? 1 : res_sale.detail.floor_area.to_i))))),
          (res_sale.extra.blank? ? ActionController::Base.helpers.number_to_currency(0) : ActionController::Base.helpers.number_to_currency(res_sale.extra.field5.blank? ? 0 : res_sale.extra.field5)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field6.blank? ? "" : res_sale.extra.field6))
        ]
      end
    end
    title = @property.detail.development_name + " Building Price List " + Time.now.strftime("%d%m%Y%H%M%S")
    send_data residential_sales_csv, :type => 'text/csv; charset=iso-8859-i; header=present', :disposition => "attachment; filename=#{title}.csv"
  end

  def export_residential_sale_csv2
    @residential_sales = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ?", params[:office_id], params[:property_id], "ResidentialSale"], :include => [:extra,:residential_sale_details])
    @parent_listing = Property.find(params[:property_id])
    residential_sales_csv = FasterCSV.generate do |csv|
      #header row
      csv << "#{@parent_listing.address}"
      csv << "\n"
      csv << ["Lot No", "Purchaser", "Purchaser's Solicitors", "Sale Price", "Deposit Paid", "Finance", "FIRB", "Flooring Option", "Date of Sale", "Settle Date", "Trans Rec.", "Dis Let", "Agent", "Sett"]
      @residential_sales.each do |res_sale|
        #data rows
        csv << [
          (res_sale.unit_number.nil? ? "" : res_sale.unit_number),
          (res_sale.purchaser.blank? ? "" : (res_sale.purchaser.agent_contact.blank? ? "" : (res_sale.purchaser.agent_contact.display_agent.blank? ? res_sale.purchaser.agent_contact.contact_data_alternative : res_sale.purchaser.agent_contact.display_agent))),
          (res_sale.purchaser.blank? ? "" : (res_sale.purchaser.agent_contact.blank? ? "" : (res_sale.purchaser.agent_contact.display_solicitor.blank? ? res_sale.purchaser.agent_contact.solicitor_data_alternative : res_sale.purchaser.agent_contact.display_solicitor))),
          ActionController::Base.helpers.number_to_currency(res_sale.price.blank? ? 0 : res_sale.price.to_i),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field7.blank? ? "" : res_sale.extra.field7)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field8.blank? ? "" : res_sale.extra.field8)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field9.blank? ? "" : res_sale.extra.field9)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field10.blank? ? "" : res_sale.extra.field10)),
          (res_sale.sold_on.blank? ? "" : res_sale.sold_on.strftime("%d.%m.%Y")),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field11.blank? ? "" : res_sale.extra.field11)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field12.blank? ? "" : res_sale.extra.field12)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field13.blank? ? "" : res_sale.extra.field13)),
          (res_sale.on_holder.blank? ? "" : (res_sale.on_holder.name.blank? ? "" : res_sale.on_holder.name)),
          (res_sale.extra.blank? ? "" : (res_sale.extra.field14.blank? ? "" : res_sale.extra.field14))
        ]
      end
    end
    title = @property.detail.development_name + " Buildings " + Time.now.strftime("%d%m%Y%H%M%S")
    send_data residential_sales_csv, :type => 'text/csv; charset=iso-8859-i; header=present', :disposition => "attachment; filename=#{title}.csv"
  end

  ### End Export CSV ###

  def generate_building_data_pdf
    @residential_sales = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ?", params[:office_id], params[:property_id], "ResidentialSale"], :include => [:extra,:residential_sale_details])
    @building_name = Property.find(params[:property_id])
    @sold_properties = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ? AND status = ?", params[:office_id], params[:property_id], "ResidentialSale", "2"], :include => [:extra,:residential_sale_details])
    @parent_listing = Property.find(params[:property_id])
    render :layout => false
  end

  def print_building_data_pdf
    @parent_listing = Property.find(params[:property_id])
    html = render_to_string(:layout => false)
    title = @property.detail.development_name.gsub(" ", "_").downcase + "_buildings_" + Time.now.strftime("%d%m%Y%H%M%S")
    filename = title + ".pdf"

    if session[:building_data].blank?
      pdf_file = Attachment.create(:size => 1000, :content_type => "application/pdf", :filename => filename, :attachable_id => @parent_listing.id, :attachable_type => "Property", :title => title, :pdf_layout => html, :send_to_api => false, :position => 15)
      if pdf_file
        pdf_file.update_attribute(:type, "Brochure")
        pdf = Attachment.find(pdf_file.id)
        path = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
        path_folder = path.gsub("/"+filename, "")
        url_path = generate_building_data_pdf_agent_office_property_residential_building_reports_url(@agent,@office,params[:property_id])
        system("mkdir -p #{path_folder}")
        temporary = "#{RAILS_ROOT}/tmp/#{pdf.filename}"
        temporary.gsub!(".pdf","-#{pdf.id}.pdf")
        width_height = ""
        spawn(:kill => true) do
          if RAILS_ENV == "development"
            system("/usr/local/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
          else
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
          end
          begin
            if FileTest.exist?(temporary)
              uploaded = pdf.upload_to_s3(temporary)
            end
            if uploaded
              #              system("rm -rf #{temporary}") if FileTest.exist?(temporary)
            end
          rescue Exception => ex
            Log.create(:message => "ERROR while generate PDF = #{ex.inspect}")
          end
        end
        #      Thread.new {system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{url_path} #{path}")}
      end
      if pdf.moved_to_s3 == false
        session[:building_data] = pdf.id
        flash[:notice] = "the pdf is still creating please try again in a few minutes."
        redirect_to :action => "index"
      else
        session[:building_data] = nil
        redirect_to pdf.public_filename
      end
    else
      file_pdf = Attachment.find(session[:building_data])
      file_temporary = "#{RAILS_ROOT}/tmp/#{file_pdf.filename}"
      file_temporary.gsub!(".pdf","-#{file_pdf.id}.pdf")
      unless file_pdf.blank?
        begin
          if file_pdf.moved_to_s3 == false
            if FileTest.exist?(file_temporary)
              uploaded = file_pdf.upload_to_s3(file_temporary)
            end
            if uploaded
              session[:building_data] = nil
              send_file(file_temporary, :type => "application/pdf", :disposition => "attachment", :filename => "#{file_pdf.filename.gsub!(".pdf", "")}")
              #              system("rm -rf #{file_temporary}") if FileTest.exist?(file_temporary)
            else
              session[:building_data] = file_pdf.id
              flash[:notice] = "the pdf is still creating please try again in a few minutes."
              redirect_to :action => "index"
            end
          else
            session[:building_data] = nil
            send_file(file_temporary, :type => "application/pdf", :disposition => "attachment", :filename => "#{file_pdf.filename.gsub!(".pdf", "")}")
            #            system("rm -rf #{file_temporary}") if FileTest.exist?(file_temporary)
          end
        rescue Exception => ex
          Log.create(:message => "ERROR while generate PDF = #{ex.inspect}")
        end
      end
    end
  end

  def generate_price_list_pdf
    @residential_sales = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ?", params[:office_id], params[:property_id], "ResidentialSale"], :include => [:extra,:residential_sale_details])
    @building_name = Property.find(params[:property_id])
    @sold_properties = Property.find(:all, :conditions => ["office_id = ? AND parent_listing_id = ? AND type = ? AND status = ?", params[:office_id], params[:property_id], "ResidentialSale", "2"], :include => [:extra,:residential_sale_details])
    @parent_listing = Property.find(params[:property_id])
    render :layout => false
  end

  def print_price_list_pdf
    @parent_listing = Property.find(params[:property_id])
    html = render_to_string(:layout => false)
    title = @property.detail.development_name.gsub(" ", "_").downcase + "_buildings_price_list_" + Time.now.strftime("%d%m%Y%H%M%S")
    filename = title + ".pdf"

    if session[:price_list].blank?
      pdf_file = Attachment.create(:size => 1000, :content_type => "application/pdf", :filename => filename, :attachable_id => @parent_listing.id, :attachable_type => "Property", :title => title, :pdf_layout => html, :send_to_api => false, :position => 15)
      if pdf_file
        pdf_file.update_attribute(:type, "Brochure")
        pdf = Attachment.find(pdf_file.id)
        path = pdf.real_filename.gsub(BASE_URL, RAILS_ROOT+"/public")
        path_folder = path.gsub("/"+filename, "")
        url_path = generate_price_list_pdf_agent_office_property_residential_building_reports_url(@agent,@office,params[:property_id])
        system("mkdir -p #{path_folder}")
        temporary = "#{RAILS_ROOT}/tmp/#{pdf.filename}"
        temporary.gsub!(".pdf","-#{pdf.id}.pdf")
        width_height = ""
        spawn(:kill => true) do
          if RAILS_ENV == "development"
            system("/usr/local/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
          else
            system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{width_height} #{url_path} #{temporary}")
          end
          begin
            if FileTest.exist?(temporary)
              uploaded = pdf.upload_to_s3(temporary)
            end
            if uploaded
              #              system("rm -rf #{temporary}") if FileTest.exist?(temporary)
            end
          rescue Exception => ex
            Log.create(:message => "ERROR while generate PDF = #{ex.inspect}")
          end
        end
        #      Thread.new {system("/usr/bin/wkhtmltopdf -T 0 -B 0 -R 0 -L 0 #{url_path} #{path}")}
      end
      if pdf.moved_to_s3 == false
        session[:price_list] = pdf.id
        flash[:notice] = "the pdf is still creating please try again in a few minutes."
        redirect_to :action => "index"
      else
        session[:price_list] = nil
        redirect_to pdf.public_filename
      end
    else
      file_pdf = Attachment.find(session[:price_list])
      file_temporary = "#{RAILS_ROOT}/tmp/#{file_pdf.filename}"
      file_temporary.gsub!(".pdf","-#{file_pdf.id}.pdf")
      unless file_pdf.blank?
        begin
          if file_pdf.moved_to_s3 == false
            if FileTest.exist?(file_temporary)
              uploaded = file_pdf.upload_to_s3(file_temporary)
            end
            if uploaded
              session[:building_data] = nil
              send_file(file_temporary, :type => "application/pdf", :disposition => "attachment", :filename => "#{file_pdf.filename.gsub!(".pdf", "")}")
            else
              session[:price_list] = file_pdf.id
              flash[:notice] = "the pdf is still creating please try again in a few minutes."
              redirect_to :action => "index"
            end
          else
            session[:building_data] = nil
            send_file(file_temporary, :type => "application/pdf", :disposition => "attachment", :filename => "#{file_pdf.filename.gsub!(".pdf", "")}")
          end
        rescue Exception => ex
          Log.create(:message => "ERROR while generate PDF = #{ex.inspect}")
        end
      end
    end
  end

end
