class SuburbsController < ApplicationController
  prepend_before_filter :login_required
  skip_before_filter :verify_authenticity_token, :only => %w( search )

  def search
    condition = ""
    if params[:type].blank?
      condition = " (tm_code IS NOT NULL) AND " if params[:country].downcase == "new zealand"
    else
      condition = (params[:type] == "realestate") ? " re_code IS NOT NULL AND " : " rca_code IS NOT NULL AND "
    end
    suburbs = Suburb.find_by_sql("SELECT * FROM suburbs WHERE #{condition} LOWER(name) LIKE '%#{params[:q].downcase.gsub("o'","")}%' AND country_id = (SELECT id FROM countries WHERE LOWER(name) = '#{params[:c].downcase.gsub("o'","")}')") unless params[:q].blank?
    jsons = []
    suburbs.each{|s| jsons << {'id' => "#{s.state_name}",'town' => "#{s.town_country_name}", 'name' => "#{s.name}", 'post' => "#{s.country.name == "United States" ? Suburb.set_post_code(s.postcode) : s.postcode}"} } unless suburbs.nil?
    render :json => {"results" => jsons, "total" => (suburbs ? suburbs.length : 0) }
  end
end
