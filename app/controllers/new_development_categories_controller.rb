class NewDevelopmentCategoriesController < ApplicationController
  layout false
  before_filter :login_required, :except => [:new]
  before_filter :get_agent_and_office, :get_property
  append_before_filter :prepare_creation

  def index
  end

  def show
  end

  def new
    @new_development_category = NewDevelopmentCategory.new
  end

  def edit
    @new_development_category = NewDevelopmentCategory.find(params[:id])
  end

  def create
   @new_development_category = NewDevelopmentCategory.new(params[:new_development_category].merge(:property_id => params[:property_id]))

    respond_to do |format|
      if @new_development_category.save
        flash[:notice] = 'NewDevelopmentCategory was successfully created.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "index" }
      end
    end
  end

  def update
    @new_development_category = NewDevelopmentCategory.find(params[:id])
    respond_to do |format|
      if @new_development_category.update_attributes(params[:new_development_category])
        flash[:notice] = 'NewDevelopmentCategory was successfully updated.'
        format.html { redirect_to :action => 'index' }
      else
        format.html { render :action => "edit" }
      end
    end
  end

  def destroy
    @new_development_category = NewDevelopmentCategory.find_by_id(params[:id])
    @new_development_category.destroy unless @new_development_category.blank?

    respond_to do |format|
      format.html { redirect_to :action => 'index'}
    end
  end

  def populate
    @new_development_categories = NewDevelopmentCategory.find(:all, :conditions => ["`property_id` = ?", params[:property_id]]).map{|c| [c.name, c.id]}
    return(render(:json => @new_development_categories))
  end

  def prepare_creation
    @new_development_categories = NewDevelopmentCategory.paginate(:all, :conditions => ["`property_id` = ?", params[:property_id]], :page =>params[:page], :per_page => 10)
  end
end
