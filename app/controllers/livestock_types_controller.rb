class LivestockTypesController < ApplicationController
  layout false

  require_role ['AgentUser','DeveloperUser']

  before_filter :get_agent_and_office

  #winarti@kiranatama.com
  #Oct 7, 2013
  #GET parents template
  def index
    @types = LivestockType.child_types
    respond_to do |format|
      format.html
    end
  end

  def new
    @livestock_type = LivestockType.new
    @livestock_parent_types = LivestockType.parent_types.map {|s| [ s.name, s.id ] }

    respond_to do |format|
      format.html
      format.xml  { render :xml => @livestock_type }
    end
  end

  def create
    @livestock_type = LivestockType.new(params[:livestock_type])

    respond_to do |format|
      if @livestock_type.save
        flash[:notice] = 'New livestock type has been successfully created'

        format.html { redirect_to new_agent_office_livestock_type_path(@agent,@office)  }
        format.xml  { render :xml => @livestock_type, :status => :created, :location => @livestock_type }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @livestock_type.errors, :status => :unprocessable_entity }
      end
    end
  end

  def refresh_types
    @property = Property.find(params[:property_id])
    @livestock_types = LivestockType.build_select_options

    respond_to do |format|
      format.js
    end
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #PUT update parent process
  def update
    @livestock_type = LivestockType.find_by_id params[:id]
    respond_to do |format|
      if @livestock_type.update_attributes(params[:livestock_type])
        flash.now[:notice] = 'The type has been successfully updated'
        format.html {redirect_to agent_office_livestock_types_path(@agent, @office)}
      else
        format.html {render :action => "edit"}
      end
    end
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #GET edit parent template
  def edit
    @livestock_type = LivestockType.find_by_id params[:id]
    respond_to {|format| format.html }
  end

  #winarti@kiranatama.com
  #Oct 7, 2013
  #DELETE parent process
  def destroy
    @livestock_type = LivestockType.find_by_id params[:id]
    respond_to do |format|
      if @livestock_type.destroy
        flash.now[:notice] = 'The type has been successfully deleted'
      else
        flash.now[:notice] = 'The type cant be deleted'
      end
      format.html {redirect_to agent_office_livestock_types_path(@agent, @office)}
    end
  end
end
