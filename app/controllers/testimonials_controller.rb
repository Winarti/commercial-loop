class TestimonialsController < ApplicationController
  before_filter :login_required
  before_filter :get_agent_and_office
  before_filter :get_agent_user, :only => :new


  # # GET /testimonials
  # # GET /testimonials.xml
  # def index
  #   @testimonials = Testimonial.all
  #
  #   respond_to do |format|
  #     format.html # index.html.erb
  #     format.xml  { render :xml => @testimonials }
  #   end
  # end

  # GET /testimonials/1
  # GET /testimonials/1.xml
  # def show
  #   @testimonial = Testimonial.find(params[:id])
  #
  #   respond_to do |format|
  #     format.html # show.html.erb
  #     format.xml  { render :xml => @testimonial }
  #   end
  # end

  # GET /testimonials/new
  # GET /testimonials/new.xml
  def new
    @testimonial = Testimonial.new
    @testimonial.agent_user = @agent_user
    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @testimonial }
      format.js { render :layout => false }
    end
  end

  # GET /testimonials/1/edit
  def open
    @testimonial = Testimonial.find(params[:id])
    @testi_img = @testimonial.testi_image.first
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  # POST /testimonials
  # POST /testimonials.xml
  def create
    @testimonial = Testimonial.new(params[:testimonial])

    respond_to do |format|
      if @testimonial.save
        unless params[:testi_img][:uploaded_data].blank?
          old_img = @testimonial.testi_image.first
          testi_image = TestiImage.new(params[:testi_img])
          if testi_image.content_type.to_s == "image/jpeg" || testi_image.content_type.to_s == "image/pjpeg" ||
             testi_image.content_type.to_s == "image/gif"
            testi_image.attachable = @testimonial
            if testi_image.save
              old_img.destroy unless old_img.blank?
            end
          else
            flash[:notice] = 'Please upload image for jpeg / gif format.'
            format.html { render :action => "new" }
          end
        end
        flash[:notice] = 'Testimonial was successfully created.'
        format.html { redirect_to( params[:come_from] ) }
        format.xml  { render :xml => @testimonial, :status => :created, :location => @testimonial }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @testimonial.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /testimonials/1
  # PUT /testimonials/1.xml
  def update
    @testimonial = Testimonial.find(params[:id])
    respond_to do |format|
      if @testimonial.update_attributes(params[:testimonial])
        unless params[:testi_img][:uploaded_data].blank?
          old_img = @testimonial.testi_image.first
          testi_image = TestiImage.new(params[:testi_img])
          if testi_image.content_type.to_s == "image/jpeg" || testi_image.content_type.to_s == "image/pjpeg"
            testi_image.attachable = @testimonial
            if testi_image.save
              old_img.destroy unless old_img.blank?
            end
          else
            flash[:notice] = 'Please upload image for jpeg format.'
            format.html { render :action => "edit" }
          end
        end
        flash[:notice] = 'Testimonial was successfully updated.'
        format.html { redirect_to( params[:come_from] ) }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @testimonial.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /testimonials/1
  # DELETE /testimonials/1.xml
  def destroy
    @testimonial = Testimonial.find(params[:id])
    @testimonial.destroy

    respond_to do |format|
      format.xml  { head :ok }
      format.js do
        render :update do |page|
          page << "jQuery('#testimonial_#{params[:id]}').remove();"
        end
      end
    end
  end

  private

  def get_agent_user
    @agent_user = AgentUser.find(params[:agent_user_id])
  end


end
