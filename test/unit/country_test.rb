# == Schema Info
# Schema version: 20090614051807
#
# Table name: countries
#
#  id                       :integer(4)      not null, primary key
#  address1                 :string(255)
#  address2                 :string(255)
#  address3                 :string(255)
#  code                     :string(2)
#  code_iso3                :string(3)
#  currency_code            :string(5)
#  currency_name            :string(255)
#  eer_deviation            :integer(4)
#  eer_lower                :integer(4)
#  eer_upper                :integer(4)
#  energy_efficiency_rating :integer(4)
#  energy_star_rating       :integer(4)
#  flag_content_type        :string(255)
#  flag_file_name           :string(255)
#  flag_file_size           :integer(4)
#  has_states               :boolean(1)      default(TRUE)
#  metrics_building         :string(255)
#  metrics_land             :string(255)
#  metrics_length           :string(255)
#  name                     :string(50)
#  numcode                  :string(3)
#  zoning                   :text
#  flag_updated_at          :datetime

require 'test_helper'

class CountryTest < ActiveSupport::TestCase

  def setup
    @us = countries(:us)
    @cn = countries(:cn)
    @utopia = countries(:utopia)
  end

  test "country is valid" do
    assert @us.valid?
    assert @cn.valid?
    assert @utopia.valid?
  end

  test "country must have a name" do
    @us.name = ''
    assert !@us.valid?
  end

  test "country must have a code" do
    @us.code = ''
    assert !@us.valid?
  end

  test "country name must be unique" do
    assert !Country.new(:name => @cn.name, :code => 'TW').valid?
  end

  test "country code must be unique" do
    assert !Country.new(:name => 'Taiwan', :code => @cn.code).valid?
  end

  test "country iso3 code must be unique" do
    assert !Country.new(:name => 'Somewhere', :code => 'ZZ', :code_iso3 => @cn.code_iso3).valid?
  end

  test "country number code must be unique" do
    assert !Country.new(:name => 'Somewhere', :code => 'ZZ', :numcode => @cn.numcode).valid?
  end

#  Not need now
#  test "country must have currency" do
#    @us.currency_name = ''
#    assert !@us.valid?
#  end
#
#  test "country must have currency code" do
#    @us.currency_code = ''
#    assert !@us.valid?
#  end

  test "country may have many states" do
    assert(@cn.states.count > 1)
  end

  test "country may have suburbs" do
    assert(@utopia.suburbs.count > 1)
  end

  test "country may have many energy efficiency ratings" do
    assert(@cn.energy_efficiency_ratings.count > 1)
  end

  test "country can have a flag image" do
    assert_nil @us.flag
    flag = @us.build_flag(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'))
    flag.dimension = Image::FLAG
    assert flag.save
    assert_not_nil @us.flag
  end

end
