# == Schema Info
# Schema version: 20090614051807
#
# Table name: features
#
#  id            :integer(4)      not null, primary key
#  office_id     :integer(4)
#  name          :string(255)
#  property_type :string(255)
#  type          :string(255)

require 'test_helper'

class GeneralFeatureTest < ActiveSupport::TestCase

  test "get an general feature" do
    assert_equal GeneralFeature, Feature.find(features(:nonsmoking).id).class
  end

end
