# == Schema Info
# Schema version: 20090614051807
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

require 'test_helper'

class UserFileTest < ActiveSupport::TestCase

  test "file must have category" do
    assert_equal 0, users(:alice).files.count
    file = UserFile.new :uploaded_data => fixture_file_upload('shareonbox.png', 'image/png')
    file.attachable = users(:alice)
    assert !file.valid?
    file.category = 'Documents'
    assert file.valid?
  end

end
