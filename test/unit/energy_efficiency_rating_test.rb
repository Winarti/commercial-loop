# == Schema Info
# Schema version: 20090614051807
#
# Table name: energy_efficiency_ratings
#
#  id         :integer(4)      not null, primary key
#  country_id :integer(4)
#  name       :string(255)
#  order      :integer(4)

require 'test_helper'

class EnergyEfficiencyRatingTest < ActiveSupport::TestCase

  def setup
    @good = energy_efficiency_ratings(:good)
    @normal = energy_efficiency_ratings(:normal)
    @below = energy_efficiency_ratings(:below)
  end

  test "energy efficiency rating is valid" do
    assert @good.valid?
    assert @normal.valid?
    assert @below.valid?
  end

  test "energy efficiency rating must have a name" do
    @good.name = ''
    assert !@good.valid?
  end

  test "energy efficiency rating must belong to a country" do
    assert_equal countries(:cn), @good.country
    @good.country_id = nil
    assert !@good.valid?
  end

end
