# == Schema Info
# Schema version: 20090614051807
#
# Table name: holiday_lease_details
#
#  id                       :integer(4)      not null, primary key
#  holiday_lease_id         :integer(4)
#  additional_notes         :text
#  bathrooms                :integer(4)      default(0)
#  bedrooms                 :integer(4)      default(0)
#  bond                     :decimal(16, 3)
#  carport_spaces           :integer(4)      default(0)
#  cleaning_fee             :decimal(16, 3)
#  energy_efficiency_rating :string(255)
#  energy_star_rating       :string(255)
#  floor_area               :decimal(16, 3)
#  garage_spaces            :integer(4)      default(0)
#  high_season_price        :decimal(16, 3)
#  land_area                :decimal(16, 3)
#  latitude                 :decimal(12, 7)
#  longitude                :decimal(12, 7)
#  max_persons              :integer(4)
#  mid_season_price         :decimal(16, 3)
#  number_of_floors         :integer(4)
#  off_street_spaces        :integer(4)      default(0)
#  peak_season_price        :decimal(16, 3)
#  virtual_tour             :string(255)
#  year_built               :integer(4)

require 'test_helper'

class HolidayLeaseDetailTest < ActiveSupport::TestCase

  def setup
    @hl = properties(:holiday_lease)
    @hld = holiday_lease_details(:one)
  end

  test "holiday lease detail is valid" do
    assert @hld.valid?
  end

  test "holiday lease details belongs to a holiday lease property" do
    assert_equal @hl, @hld.holiday_lease
  end

  test "user must provide number of floors" do
    valid_positive_integer_assertions @hld, :number_of_floors=, true
  end

  test "user must provide valid floor area" do
    valid_area_assertions @hld, :floor_area=, true
  end

  test "user must provide valid land area" do
    valid_area_assertions @hld, :land_area=, true
  end

  test "user must provide valid build year" do
    valid_year_assertions @hld, :year_built=, true
  end

  test "bond must be valid number" do
    valid_price_assertions @hld, :bond=, true
  end

  test "cleaning fee must be valid number" do
    valid_price_assertions @hld, :cleaning_fee=, true
  end

  test "mid season price must be valid number" do
    valid_price_assertions @hld, :mid_season_price=, true
  end

  test "high season price must be valid number" do
    valid_price_assertions @hld, :high_season_price=, true
  end

  test "peak season price must be valid number" do
    valid_price_assertions @hld, :peak_season_price=, true
  end

  test "max person number must be present" do
    @hld.max_persons = nil
    assert !@hld.valid?
  end

  test "bedroom number must be present" do
    @hld.bedrooms = nil
    assert !@hld.valid?
  end

  test "bathroom number must be present" do
    @hld.bathrooms = nil
    assert !@hld.valid?
  end

  test "should formalize prices rate to nightly based" do
    @hld.mid_season_price = 100
    @hld.save
    assert_equal 100, @hld.mid_season_price.to_i
    @hld.mid_season_period = 'Nightly'
    @hld.save
    assert_equal 100, @hld.mid_season_price.to_i
    @hld.mid_season_period = 'Weekly'
    @hld.save
    assert_in_delta @hld.mid_season_price.to_f, 100/7.0, 0.001
    @hld.peak_season_price = 100
    @hld.peak_season_period = 'Monthly'
    @hld.save
    assert_in_delta @hld.peak_season_price, 100/30.0, 0.001
  end

  test "should validates area metrics only when area is provided" do
    @new = HolidayLeaseDetail.new @hld.attributes
    @new.floor_area = nil
    @new.floor_area_metric = nil
    @new.land_area = nil
    @new.land_area_metric = nil
    assert @new.valid?

    @new.floor_area = 123
    assert !@new.valid?

    @new.floor_area_metric = 'Acres'
    assert @new.valid?
  end

  test "should save standard area in db" do
    @hld.land_area = 100
    @hld.land_area_metric = 'Square Meters'
    @hld.save
    assert_equal 100, @hld.land_area

    @hld.land_area = 100
    @hld.land_area_metric = 'Acres'
    @hld.save
    assert_in_delta 100*4046.85642, @hld.land_area.to_f, 0.0001

    @hld.land_area = 100
    @hld.land_area_metric = 'Hectares'
    @hld.save
    assert_equal 100*10000, @hld.land_area.to_i

    @hld.land_area = 100
    @hld.land_area_metric = 'Square Feets'
    @hld.save
    assert_in_delta 100*0.3048*0.3048, @hld.land_area.to_f, 0.0001

    @hld.land_area = 100
    @hld.land_area_metric = 'Square Yards'
    @hld.save
    assert_equal 100*0.9144*0.9144, @hld.land_area.to_f, 0.0001
  end

  test "saved detail has default metric" do
    assert_equal 'Square Meters', @hld.land_area_metric
    assert_equal 'Square Meters', @hld.floor_area_metric
  end

end
