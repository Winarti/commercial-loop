# == Schema Info
# Schema version: 20090614051807
#
# Table name: commercial_details
#
#  id                            :integer(4)      not null, primary key
#  commercial_id                 :integer(4)
#  additional_notes              :text
#  all_the_building              :boolean(1)
#  all_the_floor                 :boolean(1)
#  annual_outgoings              :decimal(16, 3)
#  annual_outgoings_include_tax  :boolean(1)
#  auction_date                  :date
#  auction_place                 :string(255)
#  auction_time                  :time
#  bond                          :decimal(16, 3)
#  building_name                 :string(255)
#  current_leased                :boolean(1)
#  current_outgoings             :decimal(16, 3)
#  current_outgoings_include_tax :boolean(1)
#  current_rent                  :decimal(16, 3)
#  current_rent_include_tax      :boolean(1)
#  date_available                :date
#  energy_efficiency_rating      :string(255)
#  energy_star_rating            :string(255)
#  floor_area                    :decimal(16, 3)
#  forthcoming_auction           :boolean(1)
#  land_area                     :decimal(16, 3)
#  latitude                      :decimal(12, 7)
#  lease_commencement            :string(255)
#  lease_end                     :string(255)
#  lease_option                  :string(255)
#  lease_plus_another            :string(255)
#  longitude                     :decimal(12, 7)
#  number_of_floors              :integer(4)
#  outgoings_paid_by_tenant      :string(255)
#  patron_capacity               :string(255)
#  price_include_tax             :boolean(1)
#  rent_review                   :string(255)
#  return_percent                :decimal(4, 2)
#  virtual_tour                  :string(255)
#  year_built                    :integer(4)
#  zoning                        :string(255)

require 'test_helper'

class CommercialDetailTest < ActiveSupport::TestCase

  def setup
    @cs = properties(:commercial_sale)
    @cl = properties(:commercial_lease)
    @cb = properties(:commercial_both)
    @csd = commercial_details(:sale)
    @cld = commercial_details(:lease)
    @cbd = commercial_details(:both)
  end

  test "commercial detail is valid" do
    assert @csd.valid?
    assert @cld.valid?
    assert @cbd.valid?
  end

  test "commercial detail belongs to a commercial propertie" do
    assert_equal @cs, @csd.commercial
    assert_equal @cl, @cld.commercial
    assert_equal @cb, @cbd.commercial
  end

  # test "user must provide valid annual outgoings for commercial lease" do
  #   valid_price_assertions @cld, :annual_outgoings=, false, true
  # end

  # test "user must provide valid bond for commercial lease" do
  #   valid_price_assertions @cld, :bond=
  # end

  test "user must provide date available for commercial lease" do
    @cld.date_available = nil
    assert !@cld.valid?
  end

  test "user must provide valid floor area" do
    valid_area_assertions @csd, :floor_area=, true
    valid_area_assertions @cld, :floor_area=, true
    valid_area_assertions @cbd, :floor_area=, true
  end

  test "user must provide valid land area" do
    valid_area_assertions @csd, :land_area=, true
    valid_area_assertions @cld, :land_area=, true
    valid_area_assertions @cbd, :land_area=, true
  end

  test "user must provide number of floors" do
    valid_positive_integer_assertions @csd, :number_of_floors=, true
    valid_positive_integer_assertions @cld, :number_of_floors=, true
    valid_positive_integer_assertions @cbd, :number_of_floors=, true
  end

  test "user must provide valid build year" do
    valid_year_assertions @csd, :year_built=, true
    valid_year_assertions @cld, :year_built=, true
    valid_year_assertions @cbd, :year_built=, true
  end

  test "return percent number must be a valid percentage number" do
    valid_price_assertions @csd, :return_percent=, true
    valid_price_assertions @cld, :return_percent=, true
    valid_price_assertions @cbd, :return_percent=, true
  end

  test "user should provide auction place, date and time if there's forthcoming auction for commercial sale property" do
    @csd.auction_place = nil
    assert !@csd.valid?
    @csd.forthcoming_auction = false
    assert @csd.valid?
    @csd.auction_place = "somewhere"
    @csd.auction_datetime = nil
    @csd.forthcoming_auction = true
    assert !@csd.valid?
    @csd.forthcoming_auction = false
    assert @csd.valid?
  end

  test "should validates area metrics only when area is provided" do
    @new = CommercialDetail.new @csd.attributes
    @new.floor_area = nil
    @new.floor_area_metric = nil
    @new.land_area = nil
    @new.land_area_metric = nil
    assert @new.valid?

    @new.floor_area = 123
    assert !@new.valid?

    @new.floor_area_metric = 'Acres'
    assert @new.valid?
  end

  test "should save standard area in db" do
    @csd.land_area = 100
    @csd.land_area_metric = 'Square Meters'
    @csd.save
    assert_equal 100, @csd.land_area

    @csd.land_area = 100
    @csd.land_area_metric = 'Acres'
    @csd.save
    assert_in_delta 100*4046.85642, @csd.land_area.to_f, 0.0001

    @csd.land_area = 100
    @csd.land_area_metric = 'Hectares'
    @csd.save
    assert_equal 100*10000, @csd.land_area.to_i

    @csd.land_area = 100
    @csd.land_area_metric = 'Square Feets'
    @csd.save
    assert_in_delta 100*0.3048*0.3048, @csd.land_area.to_f, 0.0001

    @csd.land_area = 100
    @csd.land_area_metric = 'Square Yards'
    @csd.save
    assert_equal 100*0.9144*0.9144, @csd.land_area.to_f, 0.0001
  end

  test "saved detail has default metric" do
    assert_equal 'Square Meters', @csd.land_area_metric
    assert_equal 'Square Meters', @csd.floor_area_metric
  end

end
