# == Schema Info
# Schema version: 20090614051807
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

require 'test_helper'

class AttachmentTest < ActiveSupport::TestCase

  def setup
    @new_attachment = Attachment.new :attachable => users(:jan), :filename => 'foo.txt', :size => 1024, :content_type => 'text/plain'
  end

  test "attachment is valid" do
    assert @new_attachment.valid?
  end

  test "attachment is attached to an attachable object" do
    assert @new_attachment.save
    assert_equal users(:jan), @new_attachment.attachable
  end

  test "attachment must belong to an attachable object" do
    @new_attachment.attachable_id = nil
    assert !@new_attachment.valid?
  end

  test "attachment must have a filename" do
    @new_attachment.filename = nil
    assert !@new_attachment.valid?
  end

  test "attachment must have size" do
    @new_attachment.size = nil
    assert !@new_attachment.valid?
  end

  test "attachment must have content type" do
    @new_attachment.content_type = nil
    assert !@new_attachment.valid?
  end

end
