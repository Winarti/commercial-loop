# == Schema Info
# Schema version: 20090614051807
#
# Table name: attachments
#
#  id              :integer(4)      not null, primary key
#  attachable_id   :integer(4)
#  parent_id       :integer(4)
#  attachable_type :string(255)
#  category        :string(255)
#  content_type    :string(255)
#  description     :string(255)
#  filename        :string(255)
#  height          :integer(4)
#  order           :integer(4)
#  position        :integer(4)      default(0)
#  private         :boolean(1)
#  size            :integer(4)
#  thumbnail       :string(255)
#  type            :string(255)
#  width           :integer(4)
#  created_at      :datetime
#  updated_at      :datetime

require 'test_helper'

class ImageTest < ActiveSupport::TestCase

  def setup
    @filename = 'shareonbox.png'
    @image = Image.new :uploaded_data => fixture_file_upload(@filename, 'image/png')
    @image.dimension = [100,200]
    @image.attachable = users(:jan)
    @image.save
  end

  test "image is attached to an attachable object " do
    assert_equal users(:jan), @image.attachable
  end

  test "image metadata is saved in database" do
    assert !@image.new_record?
    assert_equal 'image/png', @image.content_type
  end

  test "image is stored in correct directory" do
    assert File.exists?(@image.public_filename)
  end

end
