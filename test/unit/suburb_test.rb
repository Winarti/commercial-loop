# == Schema Info
# Schema version: 20090614051807
#
# Table name: suburbs
#
#  id         :integer(4)      not null, primary key
#  country_id :integer(4)
#  state_id   :integer(4)
#  name       :string(255)
#  postcode   :string(10)

require 'test_helper'

class SuburbTest < ActiveSupport::TestCase

  def setup
    @one = suburbs(:one)
    @two = suburbs(:two)
    @hz = suburbs(:hz)
    @nb = suburbs(:nb)
  end

  test "suburb is valid" do
    assert @one.valid?
    assert @two.valid?
    assert @hz.valid?
    assert @nb.valid?
  end

  test "suburb must have a name" do
    @one.name = ''
    assert !@one.valid?
  end

  test "suburb must belong to some area" do
    @one.area_id = nil
    assert !@one.valid?
    @two.area_type = ''
    assert !@one.valid?
  end

  test "suburb can belong to a country" do
    assert_equal countries(:utopia), @one.area
    assert_equal Country, @two.area.class
  end

  test "suburb can belong to a state" do
    assert_equal states(:zj), @hz.area
    assert_equal states(:zj), @nb.area
  end

end
