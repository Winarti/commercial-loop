require 'test_helper'

class UserDefinedFeaturesControllerTest < ActionController::TestCase

  test "should require agent user login" do
    get :index
    assert_response :redirect
    assert_redirected_to new_session_path
  end

  test "should get index" do
    login_as :alice
    get :index, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_response :success
    assert_not_nil assigns(:user_defined_features)
  end

  test "should get new" do
    login_as :alice
    get :new, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_response :success
  end

  test "should create user_defined_feature" do
    login_as :alice
    assert_difference('UserDefinedFeature.count') do
      post :create, :user_defined_feature => {:name => 'foobar', :property_type => 'barfoo' }, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    end

    assert_redirected_to agent_office_user_defined_feature_path(agents(:domain), offices(:domain_office_2), assigns(:user_defined_feature))
  end

  test "should show user_defined_feature" do
    login_as :alice
    get :show, :id => features(:electric).id, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_response :success
  end

  test "should get edit" do
    login_as :alice
    get :edit, :id => features(:electric).id, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_response :success
  end

  test "should update user_defined_feature" do
    login_as :alice
    put :update, :id => features(:electric).id, :user_defined_feature => { }, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    assert_redirected_to agent_office_user_defined_feature_path(agents(:domain), offices(:domain_office_2), assigns(:user_defined_feature))
  end

  test "should destroy user_defined_feature" do
    login_as :alice
    assert_difference('UserDefinedFeature.count', -1) do
      delete :destroy, :id => features(:electric).id, :agent_id => agents(:domain).id, :office_id => offices(:domain_office_2).id
    end

    assert_redirected_to agent_office_user_defined_features_path(agents(:domain), offices(:domain_office_2))
  end
end
