require 'test_helper'

class AgentsControllerTest < ActionController::TestCase

  test "need login to view pages" do
    get :index, :developer_id => developers(:agentsquare)
    assert_response :redirect
    get :show, :id => agents(:domain), :developer_id => developers(:agentsquare)
    assert_response :redirect
  end

  test "normal agent user can only view agent info" do
    login_as :alice
    assert_raise(::NoMethodError) {
      get :edit, :id => agents(:domain), :developer_id => developers(:agentsquare)
    }
    assert_raise(ActionView::TemplateError) {
      get :index, :developer_id => developers(:agentsquare)
    }
  end

  test "agent user can't view other agents info" do
    login_as :bob
    get :show, :id => agents(:google), :developer_id => developers(:agentsquare)
    assert_response 401
  end

  test "developer can manage their agents" do
    login_as :louies
    get :show, :id => agents(:domain), :developer_id => developers(:agentsquare)
    assert_response :success
  end

  test "developer can't manage other developer's agent" do
    login_as :louies
    assert_raise(ActiveRecord::RecordNotFound) {
      get :show, :id => agents(:yahoo).id, :developer_id => developers(:agentline)
    }
    assert_raise(ActiveRecord::RecordNotFound) {
      get :edit, :id => agents(:yahoo).id, :developer_id => developers(:agentline)
    }
  end

  test "should get index" do
    login_as :louies
    get :index, :developer_id => developers(:agentsquare)
    assert_response :success
    assert_not_nil assigns(:agents)
  end

  test "should get new" do
    login_as :louies
    get :new, :developer_id => developers(:agentsquare)
    assert_response :success
  end

  test "should create agent" do
    login_as :louies
    assert_difference('Agent.count') do
      post :create, :agent => {:name => 'hello', :country => 'CN'}, :developer_id => developers(:agentsquare)
      assert_equal 'CN', assigns['agent'].country
    end

    assert_redirected_to developer_agents_path(assigns(:developer))
  end

  test "should show agent" do
    login_as :louies
    get :show, :id => agents(:domain).id, :developer_id => developers(:agentsquare)
    assert_response :success
  end

  test "should get edit" do
    login_as :louies
    get :edit, :id => agents(:domain).id, :developer_id => developers(:agentsquare)
    assert_response :success
  end

  test "should update agent as developer" do
    login_as :louies
    put :update, :id => agents(:domain).id, :agent => {:name => 'hello' }, :developer_id => developers(:agentsquare)
    assert_redirected_to developer_agents_path(assigns(:developer))
  end

  test "should destroy agent" do
    login_as :louies
    assert_difference('Agent.count', -1) do
      delete :destroy, :id => agents(:domain).id, :developer_id => developers(:agentsquare)
    end

    assert_redirected_to agents_path
  end
end
