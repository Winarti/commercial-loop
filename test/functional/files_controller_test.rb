require 'test_helper'

class FilesControllerTest < ActionController::TestCase

  def setup
    users(:alice).files.create(:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'), :category => 'Pictures', :private => true)
    users(:alice).files.create(:uploaded_data => fixture_file_upload('users.yml', 'text/plain'), :category => 'Documents')
  end

  test "alice should have two files" do
    assert_equal 2, users(:alice).files.count
  end

  test "should get index" do
    login_as :alice
    get :index, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    assert_response :success
    assert_equal 2, assigns(:files).size
  end

  test "should get sorted fileds" do
    login_as :alice
    get :index, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :order => 'atoz'
    assert_equal "shareonbox.png", assigns(:files).first.filename
    get :index, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :order => 'privateshared'
    assert_equal "users.yml", assigns(:files).first.filename
  end

  test "should get files in category" do
    login_as :alice
    get :index, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :category => 'Pictures'
    assert_response :success
    assert_equal 1, assigns(:files).size
  end

  test "should get new file page" do
    login_as :alice
    get :new, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    assert_response :success
  end

  test "should create new file" do
    login_as :alice
    assert_difference('users(:alice).files.count') do
      post :create, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :file => {:category => 'Documents', :uploaded_data => fixture_file_upload('properties.yml', 'text/plain')}
    end
    assert_redirected_to agent_office_files_path(agents(:domain),offices(:domain_office_2))
  end

  test "should get edit file page" do
    login_as :alice
    get :edit, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => users(:alice).files.first.id
    assert_response :success
  end

  test "should update file" do
    login_as :alice
    f = users(:alice).files.first
    assert_equal true, f.private?
    assert_not_equal 'Foobar', f.category
    put :update, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => f.id, :file => {:category => 'Foobar', :private => false}
    f.reload
    assert_equal false, f.private?
    assert_equal 'Foobar', f.category
    assert_redirected_to agent_office_files_path(agents(:domain),offices(:domain_office_2))
  end

  test "should delete file" do
    login_as :alice
    assert_difference('users(:alice).files.count', -1) do
      post :destroy, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => users(:alice).files.first.id
    end
    assert_redirected_to agent_office_files_path(agents(:domain),offices(:domain_office_2))
  end
end
