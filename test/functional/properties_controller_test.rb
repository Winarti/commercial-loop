require 'test_helper'
require 'rubygems'
require 'test/unit'
require 'mocha'

class PropertiesControllerTest < ActionController::TestCase

  setup do
    @property = Property.new
  end

  test "post 'search' should search address" do
    login_as :alice
    Property.expects(:search).with('queens town', offices(:domain_office_2).id).returns(@property)
    post :search, :office_id => offices(:domain_office_2), :agent_id => agents(:domain), :q => 'queens town'
  end

  test "should get index" do
    login_as :alice
    get :index, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert(assigns(:properties).size > 0)
  end

  test "should get listing" do
    login_as :alice
    get :listing, :office_id => offices(:domain_office_2), :agent_id => agents(:domain), :type => 'residential_lease'
    assert_response :success
    assert(assigns(:properties).size > 0)
  end

  test "should get total area" do
    login_as :alice
    get :update_area, :office_id => offices(:domain_office_2), :agent_id => agents(:domain), :floor_area => 100, :floor_area_metric => 'Square Meters'
    assert_response :success
  end

  test "should get new residential lease property" do
    login_as :alice
    get :residential_lease, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert_prepared
  end

  test "should get new residential sale property" do
    login_as :alice
    get :residential_sale, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert_prepared
    assert(assigns(:periods).size > 1)
  end

  test "should get new commercial property" do
    login_as :alice
    get :commercial, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert_prepared
    assert(assigns(:deal_types).size > 1)
    assert(assigns(:lease_options).size > 1)
    assert(assigns(:lease_years).size > 1)
    assert(assigns(:tax_options).size > 1)
    assert(assigns(:price_periods).size > 1)
  end

  test "should get new project sale property" do
    login_as :alice
    get :project_sale, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert_prepared
    assert(assigns(:categories).size > 1)
    assert(assigns(:periods).size > 1)
    assert(assigns(:length_metrics).size > 1)
  end

  test "should get new business sale property" do
    login_as :alice
    get :business_sale, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert_prepared
    assert(assigns(:presimes).size > 1)
    assert(assigns(:lease_years).size > 1)
    assert(assigns(:tax_options).size > 1)
    assert(assigns(:tax_options_bool).size > 1)
    assert(assigns(:franchise_options).size > 1)
  end

  test "should get new holiday lease property" do
    login_as :alice
    get :holiday_lease, :office_id => offices(:domain_office_2), :agent_id => agents(:domain)
    assert_response :success
    assert_prepared
    assert(assigns(:price_periods).size > 1)
  end

  test "should create a residential lease property" do
    login_as :alice
    assert_create_property ResidentialLease, :date_available => 1.month.from_now
  end

  test "should create a residential sale property" do
    login_as :alice
    assert_create_property ResidentialSale, :number_of_floors => 3, :auction_place => 'abcd', :forthcoming_auction => true, :auction_datetime => 1.month.from_now
  end

  test "should create a holiday lease property" do
    login_as :alice
    u = users(:alice)
    attrs = valid_property
    type = HolidayLease
    detail = {:max_persons => 5}
    [ :price2, :price2_include_tax, :deal_type ].each {|k| attrs[k] = detail.delete k }
    assert_difference("#{type.name}.count", 1) do
      post type.name.underscore, :office_id => offices(:domain_office_2), :agent_id => agents(:domain), :property => attrs, "#{type.name.underscore}_detail".to_sym => detail, :new_rental_season => [{:season => 'normal', :start_date => '02/20/2009', :end_date => '03/20/2009'}]
      p assigns(:property).errors unless assigns(:property).errors.empty?
      p assigns(:property).detail.errors unless assigns(:property).detail.errors.empty?
    end
    assert_redirected_to agent_office_property_path(agents(:domain),offices(:domain_office_2),assigns(:property))
    assert_equal type, assigns(:property).class
  end

  test "should create a commercial sale property" do
    login_as :alice
    assert_create_property Commercial, :deal_type => 'Sale', :price2 => 12345, :price2_include_tax => true, :patron_capacity => 123, :annual_outgoings => 1234, :bond => 123.12, :date_available => 1.month.from_now
  end

  test "should create a commercial lease property" do
    login_as :alice
    assert_create_property Commercial, :deal_type => 'Lease', :patron_capacity => 123, :annual_outgoings => 1234, :bond => 123.12, :date_available => 1.month.from_now
  end

  test "should create a commercial sale/lease property" do
    login_as :alice
    assert_create_property Commercial, :deal_type => 'Both', :price2 => 12345, :price2_include_tax => true, :patron_capacity => 123, :annual_outgoings => 1234, :bond => 123.12, :date_available => 1.month.from_now
  end

  test "should create a project sale property" do
    login_as :alice
    assert_create_property ProjectSale, :category => 'foo'
  end

  test "should create a business sale property" do
    login_as :alice
    assert_create_property BusinessSale, :category => 'foo', :patron_capacity => 12
  end

  test "should show property" do
    login_as :alice
    get :show, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => properties(:residential_lease).id
    assert_response :success
  end

  test "should get edit" do
    login_as :alice
    get :edit, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => properties(:residential_lease).id
    assert_response :success
  end

  test "should update property" do
    login_as :alice
    assert_not_equal 1213, properties(:residential_lease).price.to_i
    assert_not_equal 3, properties(:residential_lease).detail.reload.bathrooms
    assert_equal 0, properties(:residential_lease).feature_ids.size
    put :update, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => properties(:residential_lease).id, :property => {:price => 1213, :feature_ids => [1,2,3]}, :residential_lease_detail => {:bathrooms => 3}
    assert_equal 1213, properties(:residential_lease).reload.price.to_i
    assert_equal 3, properties(:residential_lease).detail.reload.bathrooms
    assert_equal 3, properties(:residential_lease).feature_ids.size
    assert_redirected_to agent_office_properties_path(agents(:domain),offices(:domain_office_2))
  end

  test "failed to update property" do
    login_as :alice
    put :update, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => properties(:residential_lease).id, :property => {:price => 'abcd'}
    assert_response :success
  end

  test "should destroy property" do
    login_as :alice
    assert_difference('Property.count', -1) do
      delete :destroy, :agent_id => agents(:domain), :office_id => offices(:domain_office_2), :id => properties(:residential_lease).id
    end
    assert_redirected_to agent_office_properties_path(agents(:domain),offices(:domain_office_2))
  end

  def assert_prepared
    assert(assigns(:countries).size > 1)
    assert(assigns(:vendors).size > 1)
    assert(assigns(:contacts).size > 1)
    assert(assigns(:periods).size > 1)
    assert(assigns(:range).to_a.size > 1)
    assert(assigns(:ratings).size > 1)
    assert(assigns(:area_metrics).size > 1)
    assert(assigns(:floors).size > 1)
    assert(assigns(:pre_defined_features).size == 6)
    assert(assigns(:ptypes).size > 1)
  end

  def assert_create_property(type, detail, rental_season = {})
    u = users(:alice)
    attrs = valid_property
    [ :price2, :price2_include_tax, :deal_type ].each {|k| attrs[k] = detail.delete k }
    assert_difference("#{type.name}.count", 1) do
      post type.name.underscore, :office_id => offices(:domain_office_2), :agent_id => agents(:domain), :property => attrs, "#{type.name.underscore}_detail".to_sym => detail, :new_rental_season => rental_season
      p assigns(:property).errors unless assigns(:property).errors.empty?
      p assigns(:property).detail.errors unless assigns(:property).detail.errors.empty?
    end
    assert_redirected_to agent_office_property_path(agents(:domain),offices(:domain_office_2),assigns(:property))
    assert_equal type, assigns(:property).class
  end

end
