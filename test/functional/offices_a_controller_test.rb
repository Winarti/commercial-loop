require 'test_helper'

class OfficesAControllerTest < ActionController::TestCase

  test "should get index" do
    login_as :alice
    get :index, :agent_id => agents(:domain).id
    assert_response :success
    assert_not_nil assigns(:offices)
  end

  test "should get new" do
    login_as :alice
    get :new, :agent_id => agents(:domain).id
    assert_response :success
  end

  test "should create office" do
    login_as :alice
    assert_difference('Office.count') do
      post :create, :office => valid_office, :agent_id => agents(:domain).id
    end
    assert_redirected_to agent_offices_path(assigns(:agent))
  end

  test "failed to create office" do
    login_as :alice
    assert_no_difference('Office.count') do
      post :create, :office => valid_office(:code => 'abcde'), :agent_id => agents(:domain).id
    end
    assert_response :success
    assert !assigns(:office).errors.empty?
  end

  test "should show office" do
    login_as :alice
    get :show, :id => offices(:domain_office_1).id, :agent_id => agents(:domain).id
    assert_response :success
  end

  test "should get edit" do
    login_as :alice
    get :edit, :id => offices(:domain_office_1).id, :agent_id => agents(:domain).id
    assert_response :success
  end

  test "should update office" do
    login_as :alice
    put :update, :id => offices(:domain_office_1).id, :office => { :country => 'china' }, :agent_id => agents(:domain).id
    assert_equal 'china', assigns(:office).country
    assert_redirected_to agent_offices_path(assigns(:agent))
  end

  test "failed to update" do
    login_as :alice
    put :update, :id => offices(:domain_office_1).id, :office => { :code => 'china' }, :agent_id => agents(:domain).id
    assert_response :success
    assert !assigns(:office).errors.empty?
  end

  test "should destroy office" do
    login_as :alice
    assert_difference('Office.count', -1) do
      delete :destroy, :id => offices(:domain_office_1).id, :agent_id => agents(:domain).id
    end

    assert_redirected_to agent_offices_path(assigns(:agent))
  end

  test "failed to get logo" do
    login_as :alice
    get :logo, :agent_id => agents(:domain).id, :id => offices(:domain_office_1), :view_type => 1
    assert_redirected_to agent_offices_path(agents(:domain))
  end

  test "invalid logo type" do
    login_as :alice
    post :logo, :agent_id => agents(:domain).id, :id => offices(:domain_office_1), :logo => {:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'), :order => 0}
    assert_redirected_to edit_agent_office_path(agents(:domain),offices(:domain_office_1))
  end

  test "upload office logo" do
    login_as :alice
    assert_nil offices(:domain_office_1).logos.find_by_order(1)
    post :logo, :agent_id => agents(:domain).id, :id => offices(:domain_office_1), :logo => {:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'), :order => 1}
    assert_redirected_to edit_agent_office_path(agents(:domain),offices(:domain_office_1))
    assert_not_nil offices(:domain_office_1).logos.find_by_order(1)

    #upload again
    post :logo, :agent_id => agents(:domain).id, :id => offices(:domain_office_1), :logo => {:uploaded_data => fixture_file_upload('shareonbox.png', 'image/png'), :order => 1}
    assert_redirected_to edit_agent_office_path(agents(:domain),offices(:domain_office_1))
    assert_equal 1, offices(:domain_office_1).logos.find_all_by_order(1).size
  end

end
