require 'test_helper'

class UserFileCategoriesControllerTest < ActionController::TestCase
  def setup
    login_as :alice
  end

  test "should get index" do
    get :index, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    assert_response :success
    assert_not_nil assigns(:user_file_categories)
  end

  test "should get new" do
    get :new, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    assert_response :success
  end

  test "should create user_file_category" do
    assert_difference('UserFileCategory.count') do
      post :create, :user_file_category => { }, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    end
    assert_redirected_to agent_office_user_file_categories_path(agents(:domain),offices(:domain_office_2))
  end

  test "should get edit" do
    get :edit, :id => user_file_categories(:one).id, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    assert_response :success
  end

  test "should update user_file_category" do
    put :update, :id => user_file_categories(:one).id, :user_file_category => { }, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    assert_redirected_to agent_office_user_file_categories_path(agents(:domain),offices(:domain_office_2))
  end

  test "should destroy user_file_category" do
    assert_difference('UserFileCategory.count', -1) do
      delete :destroy, :id => user_file_categories(:one).id, :agent_id => agents(:domain), :office_id => offices(:domain_office_2)
    end
    assert_redirected_to agent_office_user_file_categories_path(agents(:domain),offices(:domain_office_2))
  end

end
